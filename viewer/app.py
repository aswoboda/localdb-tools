#!/usr/bin/env python3
# coding:UTF-8
#################################
# Contacts: Arisa Kubota (akubota@hep.phys.titech.ac.jp)
# Project: localDB-tools
# Description: Viewer Application
# Usage: python app.py --config conf.yml
# Date: Sep 2019
################################
# Magical word
from functions.imports import *  ### import function modules
from pages.route import *  ### import page modules
from functions import startup

# temporarily disabled
# startup.startup()

# ==============================
# Setup logging
# ==============================
# setupLogging("logs/development.log")

# ==============================
# Setup SSL Certificate
# ==============================
if args.fcert and args.fkey:
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain(args.fcert, args.fkey)
    localdb_url = "https" + localdb_url
else:
    context = None
    localdb_url = "http" + localdb_url

# app
app = Flask(__name__)
logger.info("Viewer Application URL: {}/".format(localdb_url))

with app.app_context():
    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix="/localdb")
    app.config["SECRET_KEY"] = os.urandom(24)
    # app.config['SECRET_KEY'] = 'key'

    ## for spescific users
    # app.config['MAIL_SERVER'] = ''
    # app.config['MAIL_PORT'] = 465
    # app.config['MAIL_USERNAME'] = ''
    # app.config['MAIL_PASSWORD'] = ''
    # app.config['MAIL_USE_TLS'] = False
    # app.config['MAIL_USE_SSL'] = True

    mail = Mail(app)

    # Regist Blue Prints
    app.register_blueprint(sync_statistics_api)
    app.register_blueprint(plot_data_api)
    app.register_blueprint(picture_data_api)
    app.register_blueprint(retrieve_api)
    app.register_blueprint(static_api)

    ### Pages
    ## Top Page
    app.register_blueprint(toppage_api)
    ## Component/Result Page
    app.register_blueprint(component_api)
    ## FE configs
    app.register_blueprint(config_api)
    ## QC
    app.register_blueprint(qc_api)
    ## Checkout
    app.register_blueprint(checkout_api)
    ## Register
    app.register_blueprint(register_api)
    ## Tag
    app.register_blueprint(tag_api)
    ## User
    app.register_blueprint(user_api)
    ## DCS
    app.register_blueprint(plot_dcs_api)
    ## Error handlers
    app.register_blueprint(error_api)

    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    
    userdb.message.delete_many( {} )

@app.errorhandler(500)
def internal_server_error(error):
#    repo = git.Repo('.', search_parent_directories=True)
#    status = repo.git.status().split('\n')
#    branch = status[0].split()[2]
#    log = repo.git.log(max_count=1).split('\n')

#    git_result = { "branch": branch, "log": log }

    message = traceback.format_exc()
    logging.error(message)
    return render_template('500.html', message = message), 500
#    return render_template('500.html', message = message, git_result = git_result), 500

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=args.fport, threaded=True, debug=False, ssl_context=context)
#    while True:
#        try:
#            app.run(host="0.0.0.0", port=args.fport, threaded=True)
#        except Exception as E:
#            logger.warning(E)
