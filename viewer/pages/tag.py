from functions.imports import *

tag_api = Blueprint("tag_api", __name__)

@tag_api.route("/create_tag", methods=["GET", "POST"])
def create_tag():
    stage = request.form.get("stage", "input")

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    taginfo = request.form.getlist("taginfo")
    if taginfo == []:
        session.pop("tag", None)

    if session.get("tag", None):
        query = {"name": taginfo[0]}
        thistime = datetime.utcnow()
        tags = userdb.viewer.tag.categories.find(query)

        if not tags.count() == 0:
            text = "This tag is already in use, please input an alternative."
            stage = "input"
            return render_template(
                "create_tag.html",
                tagInfo=taginfo,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if stage == "input":
            return render_template(
                "create_tag.html",
                tagInfo=taginfo,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        elif stage == "confirm":
            return render_template(
                "create_tag.html",
                tagInfo=taginfo,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        else:
            userdb.viewer.tag.categories.insert_one(
                {
                    "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                    "name": taginfo[0]
                }
            )

            session.pop("tag")
            return render_template(
                "create_tag.html",
                tagInfo=taginfo,
                stage=stage,
                timezones=pytz.all_timezones,
            )

    taginfo = [""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["tag"] = True

    return render_template(
        "create_tag.html", tagInfo=taginfo, stage=stage, timezones=pytz.all_timezones
    )

@tag_api.route("/put_tag", methods=["GET", "POST"])
def put_tag():
    thistime = datetime.utcnow()
    tag_name = request.form.get("tag_name")
    tagdocs = {
        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
        "name": tag_name
    }
    componentid = request.form.get("componentid", -1)
    runId = request.form.get("runId", -1)
    
    logger.info( 'tag_name = {}, componentid = {}, runId = {}'.format( tag_name, componentid, runId ) )

    if not componentid == -1:
        query = {"name": tag_name, "componentid": componentid}
        tags = userdb.viewer.tag.docs.find(query)
        if tags.count() == 0:
            componentid = {
                "componentid": componentid,
            }
            tagdocs.update(componentid)
            userdb.viewer.tag.docs.insert(tagdocs)
        else:
            userdb.viewer.tag.docs.remove(query)

    if not runId == -1:
        query = {"name": tag_name, "runId": runId}
        tags = userdb.viewer.tag.docs.find(query)
        if tags.count() == 0:
            query_doc = userdb.viewer.query.find_one({"runId": runId})
            new_list = query_doc["data"]
            new_list.append(str(tag_name))
            userdb.viewer.query.update({"runId": runId}, {"$set": {"data": new_list}})
            runId = {
                "runId": runId,
            }
            tagdocs.update(runId)
            userdb.viewer.tag.docs.insert(tagdocs)
        else:
            query_doc = userdb.viewer.query.find_one({"runId": runId})
            if query_doc:
                new_list = query_doc["data"]
                new_list.remove(str(tag_name))
                userdb.viewer.query.update({"runId": runId}, {"$set": {"data": new_list}})
                userdb.viewer.tag.docs.remove(query)

    return redirect(request.headers.get("Referer"))



@tag_api.route("/archive", methods=["GET", "POST"])
def archive():
    return
