from logging import log
from functions.imports import *
from flask_httpauth import HTTPBasicAuth, HTTPDigestAuth
from flask_mail import Mail, Message
from json2html import *

import numpy as np

import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.core import Module
from module_qc_database_tools.utils import (
    chip_uid_to_serial_number,
    get_layer_from_serial_number,
)


auth = HTTPBasicAuth()

config_api = Blueprint("config_api", __name__)

from itertools import islice

def chunks(data, SIZE=10000):
    it = iter(data)
    for i in range(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}

@config_api.route("/show_config", methods=["GET", "POST"])
def show_config():
    configId = request.args.get("configId", type=str, default = None )
    revision = request.args.get("revision", type=str, default = None )

    config_api    = ChipConfigAPI( client )
    config_info   = config_api.get_info( configId )
    
    doc = { 'configId' : configId,
            'revision' : revision,
            'config_info' : config_info
           }

    try:
        config_data   = config_api.get_config( configId, revision, True )
        config_global = config_data.get('RD53B').get('GlobalConfig')
        config_param  = config_data.get('RD53B').get('Parameter')
        config_pixel  = config_data.get('RD53B').get('PixelConfig')

        revision_data = localdb.fe_config_revision.find_one( { '_id' : ObjectId(revision) } )
        revision_prev = revision_data.get('parent_revision_id')

        try:
            diff = json.loads( revision_data.get('diff') )
        except:
            diff = revision_data.get('diff')
            
        config_diff_html = json2html.convert( diff )
        if config_diff_html == '' :
            config_diff_html = '<p>No diff is present.</p>'

        doc.update( { 'diff_html' : config_diff_html,
                      'prev_revision' : revision_prev } )

        len_config_global = len( config_global.items() )

        config_global_chunks = chunks( config_global, 25 )
        config_param_chunks  = chunks( config_param, 11 )

        config_global_html = [ json2html.convert( chunk ) for chunk in config_global_chunks ]
        config_param_html =  [ json2html.convert( chunk ) for chunk in config_param_chunks ]

        doc.update( {
            'config_global_html' : config_global_html,
            'config_param_html' : config_param_html
        } )

        pixelDataMap = {}
        pixelDataImages = {}

        if config_pixel != None:

            for elem in config_pixel:
                for k,v in elem.items():
                    if k == 'Col':
                        continue
                    if not k in pixelDataMap:
                        pixelDataMap[k] = []
                    pixelDataMap[k].append( v )


            for key, data in pixelDataMap.items():
                arr = np.array( data )
                plt.imshow( arr.T, origin='lower', cmap='turbo')
                plt.title( key, fontsize=28 )
                plt.xticks( fontsize=24 )
                plt.yticks( fontsize=24 )
                plt.xlabel( "Column", fontsize=28 )
                plt.ylabel( "Row", fontsize=28 )
                plt.tight_layout()
                cb = plt.colorbar( label = "Value" )
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)

                tmpfile = f"{THUMBNAIL_DIR}/{revision}_{key}.png"
                thumbfile = f"{THUMBNAIL_DIR}/{revision}_{key}_thumb.png"
                plt.savefig( tmpfile, dpi=200 )
                plt.savefig( thumbfile, dpi=50 )
                plt.clf()

                pixelDataImages[key] = tmpfile.replace(VIEWER_DIR+'/', '')
                pixelDataImages[key+'_thumb'] = thumbfile.replace(VIEWER_DIR+'/', '')

        doc['pixelData'] = pixelDataImages

    except Exception as e:
        logging.warning(e)
        config_data = {}
        
    return render_template( 'displayConfig.html', m_doc = doc )



@config_api.route("/download_config", methods=["GET", "POST"])
def download_config():
    serialNumber = request.args.get("serialNumber", type=str, default = None )
    configId = request.args.get("configId", type=str, default = None )
    revision = request.args.get("revision", type=str, default = None )

    config_api    = ChipConfigAPI( client )
    config_info   = config_api.get_info( configId )
    
    doc = { 'configId' : configId,
            'revision' : revision,
            'config_info' : config_info
           }

    name = f'{serialNumber}_{revision}.json'

    try:
        config_data   = config_api.get_config( configId, revision, True )

        with open( f'{TMP_DIR}/{revision}', 'w' ) as f:
            json.dump( config_data, f )

        return send_file(f'{TMP_DIR}/{revision}', as_attachment=True, mimetype="text/html", download_name=name )

    except Exception as e:
        response = make_response( f'something went wrong: config_id = {configId}, revision = {revision}, cache_path = {TMP_DIR}/{code}: {str(e)}' )
        return response
    
