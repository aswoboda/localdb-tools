#!/usr/bin/env python3
# -*- coding: utf-8 -*
##################################
## Author1: Eunchong Kim (eunchong.kim at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Aug. 2019
## Project: Local Database Tools
## Description: Plot data
##################################

PAGE_NAME = "plot_data"

from functions.imports import *

import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

plot_data_api = Blueprint("plot_data_api", __name__)


@plot_data_api.route("/plotData", methods=["GET", "POST"])
def plotData():
    run_oid = request.args.get("testRunId")
    map_type = request.args.get("mapType")
    data_type = "Histo1d"

    ### get testtime
    query = {"_id": ObjectId(run_oid)}
    this_run = localdb.testRun.find_one(query)
    from_zone = timezone.utc
    to_zone = pytz.timezone(session.get("timezone", str(get_localzone())))
    try:
        testtime_utc = this_run["startTime"].replace(tzinfo=from_zone)
        testtime_local = testtime_utc.astimezone(to_zone)
    except:
        testtime_local = None
        
    ### componentTestRun entries
    entries = localdb.componentTestRun.find({"testRun": run_oid})
    oids = []
    for entry in entries:
        oids.append(str(entry["_id"]))
    if not oids:
        return render_template("plotly.html", timezones=pytz.all_timezones)

    graph = {"data": []}
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_ctr = localdb.componentTestRun.find_one(query)
        if not this_ctr["attachments"] == []:
            # Get data
            for attachment in this_ctr["attachments"]:
                if map_type in attachment["title"]:
                    data = fs.get(ObjectId(attachment["code"])).read()
                    if attachment["contentType"] == "dat":
                        data = jsonFromDat(data.decode("utf-8"))
                    elif attachment["contentType"] == "json":
                        data = json.loads(data.decode("ascii"))

                    data_type = data["Type"]
                    ### axis title
                    x_axis_title = data["x"].get("AxisTitle", "")
                    y_axis_title = data["y"].get("AxisTitle", "")
                    z_axis_title = data["z"].get("AxisTitle", "")
                    ### x axis
                    x_low  = data["x"].get('Low',  0)
                    x_high = data["x"].get('High', 1)
                    x_bins = data["x"].get('Bins', 1)
                    x_bin_size = (x_high - x_low) / x_bins
                    ### y axis
                    y_low  = data['y'].get('Low',  0)
                    y_high = data['y'].get('High', 1)
                    y_bins = data['y'].get('Bins', 1)
                    y_bin_size = (y_high - y_low) / y_bins
                    ### values
                    values = data["Data"]
                    x_val = []
                    y_val = []
                    z_val = []
                    if data_type == "Histo1d":
                        for i in range(x_bins):
                            x_val.append(x_low + x_bin_size * i)
                            y_val.append(values[i])
                        graph["data"].append(
                            toGraph(1, this_ctr["name"], x_val, y_val, [])
                        )
                    if data_type == "Histo2d":
                        val = [[0 for i in range(x_bins)] for j in range(y_bins)]
                        for i in range(x_bins):
                            for j in range(y_bins):
                                x_val.append(x_low + x_bin_size * i)
                                y_val.append(y_low + y_bin_size * j)
                                z_val.append(values[i][j])
                                (val[j])[i] = values[i][j]
                        graph["data"].append(
                            toGraph(
                                2, this_ctr["name"], x_val, y_val, z_val, z_axis_title
                            )
                        )
                        graph["data"].append(
                            toGraph(3, this_ctr["name"], [], [], val, z_axis_title)
                        )
                    break

    layout = toLayout(
        data_type, map_type, testtime_local, x_axis_title, y_axis_title, z_axis_title
    )
    layout.update({"updatemenus": []})
    if data_type == "Histo1d":
        ### barmode
        button = []
        barmodes = ["overlay", "stack", "group"]
        for barmode in barmodes:
            button.append(
                {"method": "relayout", "args": ["barmode", barmode], "label": barmode}
            )
        layout["updatemenus"].append(toButton(1.1, 1.15, button))
    if data_type == "Histo2d":
        graph["data"][0]["visible"] = True
        graph["data"][1]["visible"] = True
        ### maps
        button = []
        for i, map_data in enumerate(graph["data"]):
            if not i % 2 == 0:
                continue
            visible = [False] * len(graph["data"])
            visible[i] = True
            visible[i + 1] = True
            button.append(
                {
                    "method": "restyle",
                    "args": ["visible", visible],
                    "label": map_data["chip_name"],
                }
            )
        layout["updatemenus"].append(toButton(1.1, 1.15, button))
        ### colorscale
        button = []
        colorscales = [
            "Portland",
            "Rainbow",
            "Blackbody",
            "Bluered",
            "Blues",
            "Earth",
            "Electric",
            "Greens",
            "Greys",
            "Hot",
            "Jet",
            "Picnic",
            "Rdbu",
            "Reds",
            "Viridis",
            "YlGnBu",
            "YlOrRd",
        ]
        for colorscale in colorscales:
            button.append(
                {
                    "method": "restyle",
                    "args": ["colorscale", colorscale],
                    "label": colorscale,
                }
            )
        layout["updatemenus"].append(toButton(1.1, 1.05, button))

    graph_json = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
    layout_json = json.dumps(layout, cls=plotly.utils.PlotlyJSONEncoder)

    info = {"type": data_type}

    # Only work online mode
    # figure = Figure(data=graph, layout=layout)
    # plotly.plotly.image.save_as(figure, filename='tmp/'+city, format='jpeg')

    return render_template(
        "plotly.html",
        info=info,
        graph_json=graph_json,
        layout_json=layout_json,
        timezones=pytz.all_timezones,
    )


def jsonFromDat(i_data):
    lines = i_data.split("\n")
    data = {
        "Type": lines[0].split(" ")[0],
        "x": {"AxisTitle": lines[2]},
        "y": {"AxisTitle": lines[3]},
        "z": {"AxisTitle": lines[4]},
        "Data": [],
    }
    l = 5
    ## x axis
    data["x"]["Low"] = float(lines[l].split(" ")[1])
    data["x"]["High"] = float(lines[l].split(" ")[2])
    data["x"]["Bins"] = int(lines[l].split(" ")[0])
    l += 1
    ### y axis
    if data["Type"] == "Histo2d":
        data["y"]["Low"] = float(lines[l].split(" ")[1])
        data["y"]["High"] = float(lines[l].split(" ")[2])
        data["y"]["Bins"] = int(lines[l].split(" ")[0])
        l += 1
    else:
        data["y"]["Low"] = 1
        data["y"]["High"] = 0
        data["y"]["Bins"] = 1
    l += 1
    ### data
    for row in range(data["y"]["Bins"]):
        for col in range(data["x"]["Bins"]):
            if data["Type"] == "Histo2d":
                if (len(data["Data"]) - 1) < col:
                    data["Data"].append([])
                # if not col in data['Data']: data['Data'][col] = {}
                # data['Data'][col][row] = float(lines[l].split(' ')[col])
                data["Data"][col].append(float(lines[l].split(" ")[col]))
            else:
                data["Data"].append(float(lines[l].split(" ")[col]))
        l += 1
    return data


def toLayout(i_data_type, i_map_type, i_time, i_x_title, i_y_title, i_z_title):
    try:
        time_format = i_time.strftime("%y/%m/%d, %h:%m:%s")
    except:
        time_format = ""
        
    layout = {
        "title": i_map_type + ", " + time_format,
        "titlefont": {"size": 18},
        "xaxis": axisDict(i_x_title, 18, 18, [0.05, 0.95]),
        "yaxis": axisDict(i_y_title, 18, 18, [0.05, 0.95]),
        "zaxis": axisDict(i_z_title, 18, 18),
        "legend": {"font": {"size": 18}},
        "autosize": False,
        "width": 1400,
        "height": 750,
        "margin": {"t": 150, "b": 100, "l": 100, "r": 100},
        "template": "plotly_white",
    }
    if i_data_type == "Histo1d":
        layout.update({"barmode": "overlay"})
    if i_data_type == "Histo2d":
        layout["xaxis"]["domain"] = [0.05, 0.45]
        ### 3d map
        scene = {
            "domain": {"x": [0.55, 0.95], "y": [0.05, 0.95]},
            "xaxis": axisDict(i_x_title, 12, 12),
            "yaxis": axisDict(i_y_title, 12, 12),
            "zazis": axisDict(i_z_title, 12, 12),
        }
        layout.update({"scene": scene})
    return layout


def axisDict(i_title, i_title_font, i_tick_font, i_domain=[]):
    d = {
        "title": i_title,
        "titlefont": {"size": i_title_font},
        "tickfont": {"size": i_tick_font},
    }
    if not i_domain == []:
        d.update({"domain": i_domain})
    return d


def toButton(i_xanchor_left, i_yanchor_bottom, i_buttons):
    button = {
        "pad": {"r": 10, "t": 10},
        "x": i_xanchor_left,
        "xanchor": "left",
        "y": i_yanchor_bottom,
        "yanchor": "bottom",
        "buttons": i_buttons,
    }
    return button


def toGraph(i_type, i_name, i_x, i_y, i_z, i_title="z"):
    graph = {}
    if i_type == 1:
        graph.update(
            {
                "x": i_x,
                "y": i_y,
                "name": i_name,
                "type": "bar",
                "marker": {"opacity": 0.5},
            }
        )
    elif i_type == 2:
        graph.update(
            {
                "x": i_x,
                "y": i_y,
                "z": i_z,
                "type": "heatmap",
                "name": "2D map",
                "chip_name": i_name,
                "colorscale": "Portland",
                "colorbar": {"title": i_title},
                "xaxis": "x",
                "yaxis": "y",
                "zaxis": "z",
                "visible": False,
            }
        )
    elif i_type == 3:
        graph.update(
            {
                "z": i_z,
                "type": "surface",
                "name": "3D surface",
                "chip_name": i_name,
                "colorscale": "Portland",
                "colorbar": {"title": i_title},
                "scene": "scene",
                "visible": False,
            }
        )
    return graph
