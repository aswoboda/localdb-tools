from functions.imports import *
from json2html import *
from pathlib import Path
import jsonschema
import copy
import shutil
import functools
import traceback
from typing import Any
import gridfs
import pymongo
import statistics
import pandas as pd

from datetime import datetime, timezone

import typer
from pathlib import Path

import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.core import Module
from module_qc_database_tools.core import Chip
from module_qc_database_tools.utils import (
    chip_uid_to_serial_number,
    get_layer_from_serial_number,
)

import zipfile


qc_api = Blueprint("qc_api", __name__)

name_map = { "module":"MODULE", "module_pcb":"PCB", "bare_module":"BARE_MODULE", "sensor_tile":"SENSOR_TILE", "front-end_chip":"FE_CHIP" }
    
rawSchema = { "type":"array",
              "minItems":0,
              "items": { "type":"array",
                         "minItems":1,
                         "items": {
                             "type": "object",
                             "required": ["serialNumber", "testType", "results"],
                             "additionalProperties": True,
                             "properties" : {
                                 "serialNumber" : { "type": "string" },
                                 "testType"     : { "type": "string" }
                             }
                         }
                        }
             }
    

@qc_api.route("/qc_tests", methods=["GET", "POST"])
def qc_tests():

    search_phrase = request.form.get("keyword", type=str, default = None )
    page_index    = request.form.get("page", type=int, default = 0 )

    logger.info( f'search phrase = {search_phrase}' )
    logger.info( f'page index = {page_index}' )

    table_docs = { 'messages': format_messages( { 'component' : None } ), 'search_phrase' : search_phrase if search_phrase else '' }

    modules = localdb.component.find( {'componentType':'module'}, sort = [ ('sys.cts', DESCENDING) ] )

    #tests = list( localdb.QC.result.find( {} , sort = [ ('component', DESCENDING), ('sys.cts', DESCENDING) ] ) )

    output_tests = []

    for index,module in enumerate(modules) :

        if search_phrase != None:
            if not search_phrase in module.get('serialNumber'):
                continue
        elif index < page_index*10 or index >= (page_index+1)*10 :
            continue

        module['delim_sn'] = delim_SN( module.get('serialNumber') )
        module['typeInfo'] = SN_typeinfo( module.get('serialNumber') )
        module['stage'] = localdb.QC.module.status.find_one( { 'component' : str( module.get('_id') ) } ).get('currentStage')
        
        sub_cpts = gather_subcomponents( str(module.get('_id')), [ module ] )

        tests = {}

        tests['moduleInfo'] = module
        tests['subComponents'] = sub_cpts

        tests['testTypes'] = {}

        testCount = 0

        for subcpt in sub_cpts:
            subcpt_tests = list( localdb.QC.result.find( { 'component':str( subcpt.get('_id')), 'stage' : module.get('stage') },
                                                  sort = [ ('component', DESCENDING), ('sys.cts', DESCENDING) ] ) )
            for test in subcpt_tests:
                if not test.get('testType') in tests['testTypes']:
                    tests['testTypes'][test.get('testType')] = 0
                    
                test.update( { 'table_index' : tests['testTypes'][test.get('testType')] } )
                test.update( { 'timestamp' : get_localtime_str( test.get('sys').get('mts') ) } )
                    
                tests['testTypes'][test.get('testType')] += 1

                testCount += 1

            subcpt.update( { 'tests' : subcpt_tests } )

        module['testCount'] = testCount
        
        output_tests+= [ tests ]
        
        
    return render_template( 'qcTests.html', tests = output_tests, table_docs = table_docs )


def gather_subcomponents( top_id, _list ):

    cprs = localdb.childParentRelation.find( {'parent' : top_id } )
    
    for cpr in cprs:
        
        subcpt = localdb.component.find_one( { '_id':ObjectId( cpr.get('child') )  } )
        subcpt['delim_sn'] = delim_SN( subcpt.get('serialNumber') )
        subcpt['typeInfo'] = SN_typeinfo( subcpt.get('serialNumber') )

        _list += [ subcpt ]

        #gather_subcomponents( cpr.get('child'), _list )

    return _list



@qc_api.route("/create_configs", methods=["GET", "POST"])
def create_configs():
    
    logger.info( f'create_configs(): called' )

    componentId = request.args.get("componentId", type=str, default = None )

    try:
        cpt_doc = localdb.component.find_one( {"_id":ObjectId(componentId) } )
        serialNumber = cpt_doc['serialNumber']
    except Exception as e:
        logger.warning( str(e) )
        logger.warning( f'Failed in getting the serialNumber for component {componentId}' )
        return redirect( url_for( 'component_api.show_component', id=componentId, collection='component') )

    # check itkdb authentication
    pd_client = get_pd_client()

    if pd_client == None:
        session['url'] = url_for( 'qc_api.create_configs', componentId = componentId )
        logger.info( f'create_configs(): redirecting to itkdb_authenticate:' + url_for( 'user_api.itkdb_authenticate' ) )
        return redirect( url_for( 'user_api.itkdb_authenticate' ) )
    
    # Call config generation alg
    try:
        module = Module(pd_client, serialNumber)
        logger.info("create_config(): Getting layer-dependent config from module SN...")
    except Exception as e:
        logger.warning( str(e) )
        pass
        # return redirect( url_for( 'toppage_api.show_toppage' ) )

    try:
        layer_config = get_layer_from_serial_number(serialNumber)
    except Exception as e:
        logger.error( str(e) )
        return redirect( url_for( 'component_api.show_component', id=componentId, collection='component') )
        

    chip_template_path = (module_qc_database_tools.data / "YARR" / "chip_template.json").resolve()
    os.makedirs( str(chip_template_path.parent), exist_ok=True )
    chip_template = json.loads(chip_template_path.read_text())

    for suffix in ["warm", "cold", "LP"]:

        try:
            module
        except:
            continue
            
        generated_configs = module.generate_config(
            chip_template, layer_config, suffix=suffix, version = "latest"
        )

        revise_pd_record = False
        
        if len( generated_configs.get('chips') ) == 0:
            logger.warning( f'Fetching latest config failed for "{suffix}". Try the fallback option with TESTONWAFER...' )
            generated_configs = module.generate_config(
                chip_template, layer_config, suffix=suffix, version = "TESTONWAFER", speed = None, reverse = False
            )
            revise_pd_record = True

        chip_config_client = ChipConfigAPI(client)

        for chip_spec in generated_configs["chips"]:
            
            chip_serial_number = chip_uid_to_serial_number(
                chip_spec["RD53B"]["Parameter"]["Name"]
            )
            config_id = chip_config_client.create_config(
                chip_serial_number, "MODULE/INITIAL_WARM", branch=suffix
            )
            revision_id = chip_config_client.commit(
                config_id,
                chip_spec,
                "initial generation from module-qc-database-tools",
            )
            logger.info(
                f"new chip config {config_id} with a new revision {revision_id} saved to mongodb."
            )

            if revise_pd_record:
                upload_chip_config( chip_serial_number,
                                    config_id,
                                    revision_id,
                                    layer_config )
            
    return redirect( url_for( 'component_api.show_component', id=componentId, collection='component', config_create_processed = True) )


    
@qc_api.route("/qc_uploader", methods=["GET", "POST"])
def upload_qcTests():
    
    if not session.get("logged_in", False):
        return render_template( "401.html" )
        
    page_docs = {"page": None, "title" : "QC Test Results Uploading Page"}
    
    if not 'fileUpload' in request.files:
        return render_template( "qc/upload_qcTests.html", page_docs=page_docs, timezones=pytz.all_timezones )
    
    
    try:
        inputJson = json.load( request.files['fileUpload'] )
    except Exception as e:
        page_docs['message'] = 'Input file is not a valid JSON: ' + str(e)
        
        return render_template( "qc/upload_qcTests.html", page_docs=page_docs, timezones=pytz.all_timezones )
        
    
    page_docs['testData'] = json2html.convert( inputJson )

    
    try:
        
        upload_qcTests_core( inputJson, page_docs )
        
    except Exception as e:
        page_docs['message'] = str(e)
        
        return render_template( "qc/upload_qcTests.html", page_docs=page_docs, timezones=pytz.all_timezones )
    
    page_docs['message_done'] = 'QC TestRun submission was processed successfully!'
    
    return render_template( "qc/upload_qcTests.html", page_docs=page_docs, timezones=pytz.all_timezones )

@qc_api.route("/qc_uploader_post", methods=["POST"])
def upload_qcTests_post():
    
    page_docs = {"page": None, "title" : "QC Test Results Uploading Page"}
    inputJson = request.get_json(force=True) 
    
    try:
        
        anaResults = upload_qcTests_core( inputJson, page_docs )
        
    except Exception as e:
        
        return json.dumps(  { "ERROR" : str(e), "traceback" : traceback.format_exc() } )

    for a in anaResults:
        if a == None:
            a = {}
            continue

        # Keep ID
        a['id'] = str( a['_id'] )

        try:
            a['address'] = a['address']['code']
        except:
            pass
        
        for key in ['_id', 'raw_id', 'sys', 'gridfs_attachments']:
            a.pop( key )

    logger.info( pprint.pformat( anaResults ) )

    log = json.dumps( anaResults )

    return log
    

    
def upload_qcTests_core( inputJson, page_docs ):
    
    jsonschema.validate( inputJson, rawSchema )
    
    logger.info( f'inputJson is valid: contains {len(inputJson)} results' )
    
    anaResults = []
    skipped = []

    for index, test in enumerate( inputJson ):

        # here, each "test" is an array and it may contain several measurement objects
        logger.info( f'inputJson Result-#{index}' )

        # check the validity of the serialNumber
        serialNumber = None
        for t in test:
            serialNumber = t['serialNumber']

        logger.info( f'serialNumber = {serialNumber}' )

        # reject if serialNumber is not found
        component = localdb.component.find_one( { 'serialNumber': serialNumber } )
        if component == None:
            skipped += [ serialNumber ]
            logger.warning( f'Serial number {serialNumber} was not found in LocalDB ==> You need to pull the component first.' )
            continue

        logger.info( f'component = {component["_id"]}' )

        # acquire the stage
        qcStatus = localdb.QC.module.status.find_one( { 'component':str( component['_id'] ) } )

        if qcStatus == None:
            raise Exception( 'Input test type {} is not listed in the current stage {}'.format( test['testType'], qcStatus['currentStage'] ) )

        for t in test:
            t['stage'] = qcStatus['currentStage']

            if not t['testType'] in qcStatus['QC_results'][qcStatus['currentStage']]:
                
                # it could be the case that the stage definition has changed.
                # as a fallback,Check the latest stage structure

                stage_tests = None

                logger.info( f'componentType = {component["componentType"]}' )
                
                if component['componentType'] == 'module':
                    stage_tests = userdb.QC.stages.find_one( {'code':'MODULE'} ).get('stage_test').get( qcStatus['currentStage'] )
                elif component['componentType'] == 'front-end_chip':
                    stage_tests = userdb.QC.stages.find_one( {'code':'FE_CHIP'} ).get('stage_test').get( qcStatus['currentStage'] )
                elif component['componentType'] == 'pcb':
                    stage_tests = userdb.QC.stages.find_one( {'code':'PCB'} ).get('stage_test').get( qcStatus['currentStage'] )
                elif component['componentType'] == 'bare_module':
                    stage_tests = userdb.QC.stages.find_one( {'code':'BARE_MODULE'} ).get('stage_test').get( qcStatus['currentStage'] )
                elif component['componentType'] == 'sensor_tile':
                    stage_tests = userdb.QC.stages.find_one( {'code':'SENSOR_TILE'} ).get('stage_test').get( qcStatus['currentStage'] )
                    
                if stage_tests is None or not t['testType'] in stage_tests:
                    raise Exception( 'Input test type {} is not a valid test of the current stage {}'.format( t['testType'], qcStatus['currentStage'] ) )


        # at this point, the schema is valid in termf os general format.
        # if this passes the testType-specific JSON schema, then record it to LocalDB as RAW.

        # schema validation is done.
        # register this test as RAW, then analyze it

        anaResult = save_raw_and_analyze( test, component )

        if anaResult.get('was_already_registerd', None) == True:
            anaResult['_id'] = 'Skipped to record'
            anaResult.pop('was_already_registerd')
            
        anaResults += [ anaResult ]

        for t in test:
            t['Component info in LocalDB'] = component


        if len(skipped)>0:

            if not 'message' in page_docs:
                page_docs['message'] = ''
            
            page_docs['message'] += 'Input serial number' + ('s ' if len(skipped)==1 else ' ') + ', '.join( skipped ) + ('is ' if len(skipped)==1 else 'are ') + 'not present in LocalDB and skipped!'
            logger.warning( page_docs['message'] )

    page_docs['testData'] = json2html.convert( inputJson )
    page_docs['testDataAnaResult'] = json2html.convert( anaResults )

    return anaResults


@qc_api.route("/recycle_analysis", methods=["GET", "POST"])
def recycle_analysis():

    test_run_id = request.form.get('test_run_id', type=str, default=None)

    component_id, new_test_run_id = recycle_analysis_core( test_run_id )
    
    return redirect( url_for('component_api.show_component', id=component_id, collection='component', test='qctest', runId=new_test_run_id ) )
    



def recycle_analysis_core( test_run_id ):

    prev_test_run = localdb.QC.result.find_one( {'_id':ObjectId( test_run_id ) } )

    serial_number = prev_test_run.get('serialNumber')

    if serial_number:
        logger.info( f'recycle_analysis_core(): serial_number = {serial_number}' )
        component = localdb.component.find_one( { 'serialNumber': serial_number } )
    else:
        component_id = prev_test_run.get('component')
        if component_id:
            component = localdb.component.find_one( { '_id' : ObjectId( component_id ) } )
        
    if component is None:
        logger.warning( f'recycle_analysis_core(): component doc for serial number {serial_number} was not found. Recycling aborts for this TestRun {test_run_id}')
        create_message( f'ERROR: component doc for serial number {serial_number} was not found. Recycling aborts for this TestRun {test_run_id}', 'recycle_analysis_core', serial_number, 'ERROR_RECYCLE_ANALYSIS_CORE' )
        return str(component.get('_id')), str(prev_test_run.get('_id'))
    
    raw_id = prev_test_run.get('raw_id')
    raw_obj = localdb.QC.testRAW.find_one( {'_id':ObjectId( raw_id ) } )

    if raw_obj is None:
        logger.warning( f'recycle_analysis_core(): no RAW data associated to TestRun {test_run_id}' )
        create_message( f'WARNING: failure in reycle_analysis(): no RAW data associated to TestRun', 'recycle_analysis', serial_number, 'WARNING_RECYCLE_ANALYSIS' )
        return redirect( url_for('component_api.show_component', id=str(component.get('_id')), collection='component', test='qctest', runId=str(prev_test_run.get('_id')) ) )

    raw = localdb.QC.testRAW.find_one( {'_id':ObjectId( raw_id ) } ).get('raw')
    
    component_type = component.get('componentType')

    module_serial_number = None

    for raw_elem in raw:
        if component_type == 'front-end_chip':
            if not 'Metadata' in raw_elem['results']:
                raw_elem['Metadata'] = {}

            if not 'ModuleSN' in raw_elem['results']['Metadata']:
                if not module_serial_number:
                    logger.info( '"ModuleSN" was not found in raw metadata. seeking the parent module from LocalDB...' )
                    for cpr in localdb.childParentRelation.find( {'child':str(component.get('_id')) } ):
                        parent_id = cpr.get('parent')
                        parent = localdb.component.find_one( {'_id':ObjectId( parent_id ) } )
                        if parent is None:
                            continue
                        if parent.get('componentType') == 'module':
                            module_serial_number = parent.get('serialNumber')
                            logger.info( f'module serial number was identified to be {module_serial_number}' )
                            break
            
                raw_elem['results']['Metadata']['ModuleSN'] = module_serial_number


            if not 'Institution' in raw_elem['results']:
                try:
                    inst_id = prev_test_run.get('address')
                    inst = localdb.pd.institution.find_one( { '_id':ObjectId( inst_id ) } ).get('code')
                    raw_elem['results']['Metadata']['Institution'] = inst
                except:
                    try:
                        inst = prev_test_run.get('prodDB_record').get('institution').get('code')
                        raw_elem['results']['Metadata']['Institution'] = inst
                    except:
                        pass
                    
                
        for var in [ 'address', 'component']:
            if var in raw_elem:
                raw_elem[var] = str( raw_elem[var] )

    test = copy.deepcopy( raw )
    for t in test:
        t.pop( 'sys' )

    try:
        new_test_run = analyze_measurement( test, raw, component, raw_id, update_config = False, recycle = True )
    except Exception as e:
        logger.warning( str(e) )
        create_message( f'ERROR: failure in reycle_analysis() <pre>'+ traceback.format_exc()+"</pre>", 'recycle_analysis', serial_number, 'ERROR_RECYCLE_ANALYSIS' )
        return str(component.get('_id')), str(prev_test_run.get('_id'))
    
    create_message( f'INFO: Analysis for {new_test_run.get("testType")} was recycled successfully', 'recycle_analysis', serial_number, 'INFO_RECYCLE_ANALYSIS_DONE' )

    return str(component.get('_id')), str(new_test_run.get('_id'))
    


@qc_api.route("/recycle_complex_analysis", methods=["GET", "POST"])
def recycle_complex_analysis():

    '''
    Recycling mqat analysis for MHT, TUN, PFA
    '''

    test_run_id = request.form.get('test_run_id', type=str, default=None)

    component_id, new_test_run_id = recycle_complex_analysis_core( test_run_id )
    
    return redirect( url_for('component_api.show_component', id=component_id, collection='component', test='qctest', runId=new_test_run_id ) )
    

def recycle_complex_analysis_core( test_run_id ):

    prev_test_run = localdb.QC.result.find_one( {'_id':ObjectId( test_run_id ) } )

    test_type = prev_test_run.get('testType')
    
    serial_number = prev_test_run.get('serialNumber')

    logger.info( f'recycle_complex_analysis_core(): serial_number = {serial_number}' )
    
    component = localdb.component.find_one( { 'serialNumber': serial_number } )

    stage_alt = prev_test_run.get('stage').replace('/','__')
    test_type_hyphened = test_type.replace('_', '-')

    work_dir = Path( f"{AT_DIR}/{serial_number}_{stage_alt}_{test_type_hyphened}" )
    os.makedirs( str(work_dir), exist_ok = True )

    fs = gridfs.GridFS(localdb)
    
    # Dump the attachment zip file and expand
    for filename, oid in prev_test_run.get("gridfs_attachments").items():

        if not filename.lower().find('zip') >= 0:
            continue
        
        os.makedirs( THUMBNAIL_DIR, exist_ok=True )

        ext = filename.split('.')[-1]

        cache_file = '{}/{}.{}'.format( THUMBNAIL_DIR, str(oid), ext )

        if os.path.exists( cache_file ):
            pass
        else:
            try:
                data = fs.get( oid ).read()

                with open( cache_file, "wb" ) as f:
                    f.write( data )
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'Failed in dumping attachment to {cache_file}' )

        logger.info( f'attachment dumped to cache {cache_file}' )

        with zipfile.ZipFile( cache_file, 'r' ) as zfile:
            zfile.extractall( str(work_dir) )

        info_file = Path( work_dir / 'info.json' )

        with info_file.open( 'r' ) as f:
            info_json = json.load( f )

        info_json['datadir'] = str(work_dir)
        with info_file.open( 'w' ) as f:
            json.dump( info_json, f )

        out_dir = str(Path(work_dir) / "output")
        shutil.rmtree( out_dir )
        
        command = [ f"analysis-{test_type}".replace('_', '-'),
                    "-i", str( info_file ),
                    '-o', out_dir ]

        logger.info( ' '.join( command ) )
        subprocess.call( command )

        output=None
        output_others = []
        for curdir, dirs, files in os.walk(f'{str(work_dir)}/output'):
            for filename in files:
                if filename.endswith('.json'):
                    output = os.path.join(curdir, filename)
                else:
                    output_others += [ os.path.join(curdir, filename) ]

        if output != None:
            with open( output ) as f:
                anaResult = json.load( f )[0]

        # gather attachment files
        output_others = []
        try:
            # seek the analysis result file inside the output path
            for curdir, dirs, files in os.walk( f'{str(work_dir)}/output' ):
                for filename in files:
                    if 'json' in filename:
                        continue
                    else:
                        output_others += [ os.path.join(curdir, filename) ]

            logger.info( f'[{threading.current_thread().name}] subsidiary files: {output_others}' )

        except Exception as e:
            logger.warning( str(e) )
            logger.warning( traceback.format_exc() )
            raise Exception( f'[{threading.current_thread().name}] Null output file is detected: the analysis comand\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout}' )

        
        # attach other files first
        attachments = {}
        for attachment in output_others:
            if any( attachment.endswith(ext) for ext in ['.jpg', 'jpeg', 'png', '.JPG', '.JPEG', '.PNG', '.pdf', '.log' ] ):
                with open(attachment, "rb") as f:
                    binary = f.read()
                ret = fs.put(binary)
                item = { str(Path(attachment).name) : ret }
                
                attachments.update( item )
                logger.info( f'[{threading.current_thread().name}] submitted {attachment} to localdb gridfs: {item}' )

        logger.info( 'attachments = ' + pprint.pformat( attachments ) )
        
        # zipping the raw input
        logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): zipping the input files...' )
        make_archive_threadsafe(f'{str(work_dir)}.zip', str(work_dir) )

        zfile = Path( str(work_dir)+'.zip' )
        with zfile.open('rb') as f:
            binary = f.read()
            md5 = hashlib.md5(binary).hexdigest()

            if localdb.fs.files.find_one( { "md5" : md5 } ) is None:
                archive_id = fs.put( binary )
                logging.info( f'createElecTestDocs(): the zip file {zfile.name} is new, uploaded to gridfs. md5 = {md5}' )
            else:
                logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): identical binary was detected in gridfs, not pushing. md5 = {md5}' )
                archive_id = localdb.fs.files.find_one( { "md5" : md5 } ).get("_id")

            if archive_id != None:
                attachments[zfile.name] = archive_id
                logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): registered zip archive as gridfs_attachment of TestRun. md5 = {md5}' )
            else:
                logging.warning( f'[{threading.current_thread().name}] createElecTestDocs(): archive_id is None! not possible to register the binary as a gridfs_attachment. md5 = {md5}' )

    new_test_run = copy.deepcopy( prev_test_run )
    new_test_run.pop( '_id' )
    for k,v in anaResult.items():
        new_test_run[k] = v
        
    new_test_run['gridfs_attachments'] = attachments

    new_test_run['sys'] = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }

    new_test_run_id = localdb.QC.result.insert_one( new_test_run ).inserted_id
    
    return str(component.get('_id')), str(new_test_run_id)




@qc_api.route("/recycle_summary", methods=["GET", "POST"])
def recycle_summary():

    '''
    Recycling analyses for E_SUMMARY as a bulk
    '''
    
    global qcAnalysisVersion
    
    test_run_id = request.form.get('test_run_id', type=str, default=None)

    prev_test_run = localdb.QC.result.find_one( {'_id':ObjectId( test_run_id ) } )

    serial_number = prev_test_run.get('serialNumber')

    if not serial_number:
        serial_number = request.form.get('serial_number', type=str, default=None)

    logger.info( f'recycle_analysis(): serial_number = {serial_number}, test_run_id = {test_run_id}' )
    
    component = localdb.component.find_one( { 'serialNumber': serial_number } )

    new_test_run = copy.deepcopy( prev_test_run )
    new_test_run.pop( '_id' )
    if 'prodDB_record' in new_test_run:
        new_test_run.pop( 'prodDB_record' )

    summaryEvals = {}
        
    for parameter,value in prev_test_run.get('results').items():
        if parameter.find('FE_LINK') >= 0:

            fe_test_run_id = value
            fe_test_run = localdb.QC.result.find_one( { '_id' : ObjectId( fe_test_run_id ) } )

            summaryEvals.update( { parameter : fe_test_run.get('passed', None) if fe_test_run is not None else None } )

            if fe_test_run is None:
                continue

            fe_component_id = fe_test_run.get('component')
            fe_component = localdb.component.find_one( { '_id' : ObjectId( fe_component_id ) } )
            
            if any( [ parameter.find( complex_test ) >=0 for complex_test in ['MIN_HEALTH_TEST', 'TUNING', 'PIXEL_FAILURE_ANALYSIS' ] ] ):
                print( f'{parameter} is complex' )

                try:
                    fe_component_id, new_fe_test_run_id = recycle_complex_analysis_core( fe_test_run_id )
                except Exception as e:
                    logger.warning( str(e) )
                
            else:

                try:
                    fe_component_id, new_fe_test_run_id = recycle_analysis_core( fe_test_run_id )
                except Exception as e:
                    logger.warning( str(e) )

            try:
                new_test_run['results'][parameter] = new_fe_test_run_id
            except Exception as e:
                logger.warning( str(e) )


    # Parameter Loop-2: collect bad pixels
    for parameter,value in prev_test_run.get('results').items():

        # print( f'######## loop2: {parameter}' )

        if 'ELECTRICALLY_BAD_PIXEL_NUMBER_FE' in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_ELECTRICALLY_FAILED')
                    if value >= 0:
                        new_test_run['results'][parameter] = value
                        logger.info( f'inserted {value} to {parameter}' )
                    else:
                        new_test_run['results'][parameter] = None

                else:
                    logger.info( f'reference result for {parameter} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {parameter}' )

        elif 'DISCONNECTED_PIXEL_NUMBER_FE' in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_DISCONNECTED_PIXELS')
                    if value >= 0:
                        new_test_run['results'][parameter] = value
                        logger.info( f'inserted {value} to {parameter}' )
                    else:
                        new_test_run['results'][parameter] = None

                    
                else:
                    logger.info( f'reference result for {parameter} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {parameter}' )

        elif 'BAD_PIXEL_NUMBER_FE' in parameter:
            feIndex = parameter[-1]

            try:
                referenceResultId = new_test_run['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_FAILING_PIXELS')
                    if value >= 0:
                        new_test_run['results'][parameter] = value
                        logger.info( f'inserted {value} to {parameter}' )
                    else:
                        new_test_run['results'][parameter] = None

                else:
                    logger.info( f'reference result for {parameter} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {parameter}' )

    # Fill the rest parameters
    summaryVarList = ['MODULE_ELECTRICALLY_BAD_PIXEL_NUMBER', 'MODULE_DISCONNECTED_PIXEL_NUMBER', 'MODULE_BAD_PIXEL_NUMBER' ]

    # Sum up all FEs
    for var in summaryVarList:
        elems = [ new_test_run['results'][ f'{var}_FE_{feIndex}' ] for feIndex in range(1, 5) if new_test_run['results'][ f'{var}_FE_{feIndex}' ] is not None ]
        if len( elems ) == 0:
            new_test_run['results'][var] = None
        else:
            new_test_run['results'][var] = sum( elems )


    # Eval flag for each test
    feTests = ['ADC_CALIBRATION', 'ANALOG_READBACK', 'SLDO',
               'VCAL_CALIBRATION', 'INJECTION_CAPACITANCE', 'DATA_TRANSMISSION',
               'LP_MODE', 'OVERVOLTAGE_PROTECTION', 'UNDERSHUNT_PROTECTION',
               'MIN_HEALTH_TEST', 'TUNING', 'PIXEL_FAILURE_ANALYSIS'  ]

    qcEvals = []

    cprs = localdb.childParentRelation.find( { 'parent': str( component['_id'] ) } )

    fes = []
    for cpr in cprs:
        subcpt = localdb.component.find_one( { '_id':ObjectId( cpr['child'] ) } )
        if subcpt['componentType'] == 'front-end_chip':
            fes += [ str( subcpt['_id'] ) ]

    logger.info( f'#FEs = {len(fes)}: {fes}' )
    
    
    for feTest in feTests:
        
        keyname = '_'.join( [ 'MODULE', feTest ] )

        passFails = []

        # The case for "not-evaluated" in the original analysis is dismissed.
        if new_test_run.get( keyname ) == -1:
            continue

        for parameter, value in new_test_run['results'].items():

            # the code should contain the feTest string and LINK
            if not ( parameter.find( feTest ) >= 0 and parameter.find('LINK') >= 0 ):
                continue
    
            result = None
            if value:
                result = localdb.QC.result.find_one( { '_id':ObjectId( value ) } )

            if result != None:
                try:
                    passFails += [ result['passed'] ]
                except:
                    # skipped case --> True
                    passFails += [ None ]
            else:
                passFails += [ None ]

            #print( feTest, code, result )
                
        # Take AND of the FE QC flags up to the last non-blank FE, but for blank input, always False
        isAllNone = all( [ f == None for f in passFails[:len(fes)] ] )
        qcFlag = all( passFails[:len(fes)] ) if isAllNone == False else None
        
        new_test_run['results'][keyname] = int( qcFlag ) if qcFlag is not None else -1
        
        qcEvals += [ qcFlag ]

    new_test_run['passed'] = all( qcEvals )
        
    new_test_run['sys'] = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }
    new_test_run['results']['properties']['ANALYSIS_VERSION'] = qcAnalysisVersion

    new_test_run_id = str( localdb.QC.result.insert_one( new_test_run ).inserted_id )

    return redirect( url_for('component_api.show_component', id=str(component.get('_id')), collection='component', test='qctest', runId=new_test_run_id ) )
    

@qc_api.route("/qc_input_manual", methods=["GET", "POST"])
def input_qcTests():
    
    if not session.get("logged_in", False):
        return render_template( "401.html" )
        
    page_docs = {"page": None, "title" : "QC Test Results Manual Input Page"}
    
    componentID   = request.args.get('componentID', None)
    if not componentID:
        componentID   = request.form.get('componentID', None)
        
    serialNumber  = localdb.component.find_one( { '_id':ObjectId(componentID) } )['name']
    
    cpt_type_raw = request.args.get('componentType', None)
    if not cpt_type_raw:
        cpt_type_raw = request.form.get('componentType', None)
    
    try:
        componentType = name_map[ cpt_type_raw ]
    except:
        componentType = cpt_type_raw
        
        
    stage         = request.args.get('stage', None)
    if not stage:
        stage         = request.form.get('stage', None)
        
    testType      = request.args.get('testType', None)
    if not testType:
        testType      = request.form.get('testType', None)
    
    
    mode = request.form.get('mode', 'input')
    
    testFormat = userdb.QC.tests.find_one( { 'componentType.code':componentType, 'code':testType } )

    page_docs.update( { 'componentID':componentID, 'serialNumber':serialNumber, 'componentType':componentType, 'stage':stage, 'testType':testType } )
    page_docs['testFormat'] = testFormat
    page_docs['testFormatHTML'] = json2html.convert( testFormat )
    page_docs['mode'] = mode
    
    page_docs['properties'] = {}
    page_docs['parameters'] = {}
    
    for prop in testFormat['properties']:
        page_docs['properties'][ prop['code'] ] = request.form.get( prop['code'], None )
        
    for param in testFormat['parameters']:
        page_docs['parameters'][ param['code'] ] = request.form.get( param['code'], None )
        
    logger.info( pprint.pformat( page_docs ) )
        
    if mode == 'submit':
        
        testRun = {
            "serialNumber": serialNumber,
            "testType": testType,
            "results": {
                "property": {
                    prop['code'] : page_docs['properties'][prop['code']]
                    for prop in testFormat["properties"]
                },
                "comment": "",
                "Measurements": {
                    param['code'] : page_docs['parameters'][param['code']]
                    for param in testFormat["parameters"]
                },
                "Metadata": { "LocalDB version": qcAnalysisVersion },
            },
            "stage" : stage
        }
        
        for prop in testFormat['properties']:
            if prop['dataType'] == 'integer':
                try:
                    testRun['results']['property'][ prop['code'] ] = int( testRun['results']['property'][ prop['code'] ] )
                except:
                    testRun['results']['property'][ prop['code'] ] = None
                    
            if prop['dataType'] == 'float':
                try:
                    testRun['results']['property'][ prop['code'] ] = float( testRun['results']['property'][ prop['code'] ] )
                except:
                    testRun['results']['property'][ prop['code'] ] = None
        
            if prop['dataType'] == 'boolean':
                try:
                    testRun['results']['property'][ prop['code'] ] = int( testRun['results']['property'][ prop['code'] ] ) == 1
                except:
                    testRun['results']['property'][ prop['code'] ] = None
                    
        for param in testFormat['parameters']:
            if param['dataType'] == 'integer':
                try:
                    testRun['results']['Measurements'][ param['code'] ] = int( testRun['results']['Measurements'][ param['code'] ] )
                except:
                    testRun['results']['Measurements'][ param['code'] ] = None
                    
            if param['dataType'] == 'float':
                try:
                    testRun['results']['Measurements'][ param['code'] ] = float( testRun['results']['Measurements'][ param['code'] ] )
                except:
                    testRun['results']['Measurements'][ param['code'] ] = None
        
            if param['dataType'] == 'boolean':
                try:
                    testRun['results']['Measurements'][ param['code'] ] = int( testRun['results']['Measurements'][ param['code'] ] ) == 1
                except:
                    testRun['results']['Measurements'][ param['code'] ] = None
                    
                    
        logger.info( 'submit testRun: ' + pprint.pformat( testRun ) )
        
        component = localdb.component.find_one( { 'serialNumber': serialNumber } )
        
        anaResult = save_raw_and_analyze( [ testRun ], component )
        
        logger.info( pprint.pformat( anaResult ) )
        
        
    return render_template( "qc/input_qcTests.html", **page_docs )


def save_raw_and_analyze( test, component ):

    logger.info( 'save_raw_and_analyze(): begin' )

    raw = copy.deepcopy( test )
    rawHash = hashlib.md5( json.dumps(test).encode() ).hexdigest()

    for r in raw:
        r['component'] = component['_id']
        r['dbVersion'] = dbv
        r['address']   = component['address']
        try:
            r['user']      = session['username']
        except:
            pass
        r['sys']       = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }

    stage = test[0]['stage']

    rawRecord = localdb.QC.testRAW.find_one( { 'rawHash':rawHash, 'stage':stage } )
    
    if rawRecord == None:
        raw_id = localdb.QC.testRAW.insert_one( { 'raw':raw, 'rawHash':rawHash, 'stage':stage } ).inserted_id
    else:
        logger.info('save_raw_and_analyze(): identical rawHash was detected on LocalDB: skipping to register RAW' )
        
        rawRecord['serialNumber'] = raw[0]['serialNumber']
        rawRecord['component'] = component['_id']
        rawRecord['dbVersion'] = dbv
        rawRecord['address']   = component['address']
        try:
            rawRecord['user']      = session['username']
        except:
            pass
        rawRecord['sys']       = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }

        raw = [ rawRecord ]
        raw_id = raw[0]['_id']
        
    logger.info('save_raw_and_analyze(): analyzing' )

    try:
        ana = analyze_measurement( test, raw, component, raw_id )
    except Exception as e:
        localdb.QC.testRAW.remove( { 'rawHash':rawHash, 'stage':stage } )
        raise e

    if ana.get('was_already_registerd', None) == True:
        logger.warning( 'save_raw_and_analyze(): identical TestRun was detected to be already present in LocalDB; skipped duplicated registration' )

    logger.info( 'save_raw_and_analyze(): end' )

    return ana

    return None
    



def analyze_measurement( test, raw, component, raw_id, update_config = True, recycle = False ):

    logger.info( 'analyze_measurement() begin' )
    
    testTypeHyphened = None
    for t in test:
        testTypeHyphened = t['testType'].replace('_', '-')

    rawHash = hashlib.md5( json.dumps(test).encode() ).hexdigest()
    tmpInputPath  = f'{TMP_DIR}/{rawHash}/input'
    tmpOutputPath = f'{TMP_DIR}/{rawHash}/output'

    try:
        shutil.rmtree( tmpInputPath )
        shutil.rmtree( tmpOutputPath )
    except:
        pass
    
    os.makedirs( tmpInputPath,  exist_ok=True )
    os.makedirs( tmpOutputPath, exist_ok=True )

    # write the RAW result to a temporary place
    try:
        with open( f'{tmpInputPath}/measurement.json', 'w' ) as f:
            logger.info( f'opened {tmpInputPath}/measurement.json' )
            f.write( json.dumps( [ test ] ) )
    except Exception as e:
        logger.warning( str(e) )


    logger.info( f'testTypeHyphened = {testTypeHyphened}' )

    target_stage = raw[0].get('stage')

    """
    For Sensor Tile IV, the test name is identical with Module's but the anlaysis is made differently.
    """
    if (testTypeHyphened == 'IV-MEASURE') and (component.get("componentType") == 'sensor_tile'):
        testTypeHyphened = 'SENSOR-TILE-IV-MEASURE'
        logger.info( f'testTypeHyphened altered to: {testTypeHyphened} for sensor_tile IV-MEASURE' )


    site_option = ""

    elecTests = ['ADC-CALIBRATION', 'ANALOG-READBACK', 'SLDO',
                 'VCAL-CALIBRATION', 'INJECTION-CAPACITANCE', 'DATA-TRANSMISSION',
                 'LP-MODE', 'OVERVOLTAGE-PROTECTION', 'UNDERSHUNT-PROTECTION',
                 'MIN-HEALTH-TEST', 'TUNING', 'PIXEL-FAILURE-ANALYSIS', 'IV-MEASURE'  ]
    
    try:
        raw_metadata = raw[0].get("results").get("Metadata")
        if raw_metadata:
            if raw_metadata.get("Institution") is None:
                if testTypeHyphened in elecTests:
                    try:
                        site_option = " --site " + component.get("address").get("code")
                    except:
                        site_doc = localdb.pd.institution.find_one( {'_id' : ObjectId(component.get("address")) } )
                        if site_doc:
                            site_code = site_doc.get("code")
                            site_option = " --site " + site_code
        else:
            if testTypeHyphened in elecTests:
                try:
                    site_option = " --site " + component.get("address").get("code")
                except:
                    site_doc = localdb.pd.institution.find_one( {'_id' : ObjectId(component.get("address")) } )
                    if site_doc:
                        site_code = site_doc.get("code")
                        site_option = " --site " + site_code
    except Exception as e:
        logger.warning( str(e) )
        pass


    depl_volt_option = ""
    if testTypeHyphened == "IV-MEASURE":
        # fetch the sensor property
        sensors = get_module_sensors( component )
        # logger.info( pprint.pformat( [ sensor.get('serialNumber') for sensor in sensors ] ) )
        logger.info( pprint.pformat( get_component_property_codes( sensors[0] ) ) )

        sensor_depl_info = list( filter( lambda x: x is not None, [ get_component_property_value( sensor, 'V_FULLDEPL' ) for sensor in sensors ] ) )

        if len( sensor_depl_info ) > 0:
            vdepl_average = statistics.mean( sensor_depl_info )
            depl_volt_option = f"--vdepl {vdepl_average}"
        
    
    command = f'analysis-{testTypeHyphened} -i {tmpInputPath}/measurement.json -o {tmpOutputPath} {site_option} {depl_volt_option}'
    
    """
    Special treatment for IV where previous measurement result is referred.
    """
    
    previous_iv_data = fetch_reference_ivs( component ) if testTypeHyphened in ['IV-MEASURE', 'BARE-MODULE-SENSOR-IV', 'SENSOR-TILE-IV-MEASURE'] else None

    # logger.info( pprint.pformat( previous_iv_data ) )

    if previous_iv_data is not None:
        command += f' -q {tmpInputPath}/aux.json'
        
        try:
            with open(f'{tmpInputPath}/aux.json', 'w') as f:
                logger.info( f'opened {tmpInputPath}/aux.json' )
                json.dump(previous_iv_data,f, indent=4)
                
        except Exception as e:
            logger.warning( str(e) )
            pass

    logger.info( command )


    
    try:
        subout = subprocess.run( command, shell=True, capture_output = True )
        
        logger.info( subout.stdout.decode() )
        logger.info( subout.stderr.decode() )

        # seek the analysis result file inside the output path
        output=None
        output_others = []
        for curdir, dirs, files in os.walk(f'{TMP_DIR}/{rawHash}/output'):
            for filename in files:
                if filename.endswith('.json'):
                    output = os.path.join(curdir, filename)
                    outdir = curdir
                else:
                    output_others += [ os.path.join(curdir, filename) ]

        if not output:
            raise Exception( f'Null output from {testTypeHyphened} analysis!' )
        
        logger.info( 'output path: ' + output )
        logger.info( f'subsidiary files: {output_others}' )

        with open( output ) as f:
            anaResult = json.load( f )[0]

        # revise the FE config with the analysis output
        serialNumber = raw[0]['serialNumber']
        cpt_doc = localdb.component.find_one( { 'serialNumber' : serialNumber } )

        
    except Exception as e:
        logger.warning( str(e) )
        logger.warning( traceback.format_exc() )
        logger.warning( subout.stderr.decode() )
        raise Exception( f'Null output file is detected: the analysis comand\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout.decode()}\n\nstderr:\n\n{subout.stderr.decode()}' )


    chip_api = ChipConfigAPI( client )
    
    try:
        if update_config and cpt_doc.get('componentType') == 'front-end_chip':
            
            qcStatus = localdb.QC.module.status.find_one( { 'component' : str( cpt_doc.get('_id') ) } )
            stage = qcStatus.get('currentStage')

            warm_list = ['WARM', 'PARYLENE_UNMASKING', 'WIREBOND_PROTECTION', 'THERMAL_CYCLES', 'LONG_TERM_STABILITY_TEST' ]
            cold_list = ['COLD']

            if testTypeHyphened == 'LP-MODE':
                branch = 'LP'
            elif any( [ phrase in stage for phrase in warm_list ] ):
                branch = 'warm'
            elif any( [ phrase in stage for phrase in cold_list ] ):
                branch = 'cold'

            logger.info( f'config: {serialNumber}, {stage}, {branch}' )

            config_id = chip_api.checkout( serialNumber, stage, branch )

            if config_id:

                config_data = chip_api.get_config( config_id, None, True )

                config_dir = f'{TMP_DIR}/{rawHash}/config'
                os.makedirs( config_dir, exist_ok = True )
                with open( f'{TMP_DIR}/{rawHash}/config/{serialNumber}_{branch}.json', 'w') as f:
                    json.dump( config_data, f, indent = 0 )

                update_command = f'analysis-update-chip-config -i {outdir} -c {config_dir} | tee'

                logger.info( update_command )
                subout = subprocess.run( update_command, shell=True )

                try:
                    with open( f'{outdir}/{serialNumber}_{branch}.json.after' ) as f:
                        updated_config_data = json.load( f )
                except Exception as e:
                    logger.warning( str(e) )
                    pass
                
    except Exception as e:
        logger.warning( str(e) )
        logger.warning( traceback.format_exc() )
        raise Exception( f'Null output file is detected: the analysis comand\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout}\n\n`stderr:\n\n{subout.stderr}' )


    # identity check with the existing records
    # -- if the raw input hash and the analysis version are both identical,
    #    skip to record a new instance. Otherwise, insert one.

    prev_results = localdb.QC.result.find_one( { 'raw_id' : ObjectId(raw_id), 'results.property.ANALYSIS_VERSION' : anaResult.get('results').get('property').get('ANALYSIS_VERSION') } )
    logger.info( f'analyze_measurement(): queried existing TestRuns with {{ "raw_id" : {ObjectId(raw_id)}, "results.property.ANALYSIS_VERSION" : {anaResult.get("results").get("property").get("ANALYSIS_VERSION")} }}' )

    if not prev_results or recycle:

        logger.info( f'analyze_measurement(): no duplicated records: inserting a new analysis' )
        
        # attach other files first
        attachments = {}
        for attachment in output_others:
            if any( attachment.endswith(ext) for ext in ['.jpg', 'jpeg', 'png', '.JPG', '.JPEG', '.PNG', '.pdf', '.log' ] ):
                item = { str(Path(attachment).name) : submit_binary( attachment ) }
                attachments.update( item )
                logger.info( f'submitted {attachment} to localdb gridfs: {item}' )

        # acquire the output format from the test bank
        try:
            outFormat = userdb.QC.tests.find_one( { 'code':test[0]['testType'] } )
            if outFormat == None:
                raise Exception('ERROR: testType {} is not registered in LocalDB.'.format( test[0]['testType'] ) )

            for key in [ 'user', 'address', 'stage', 'testType', 'sys' ]:
                try:
                    anaResult[key] = raw[0][key]
                except:
                    pass

            # Format skeleton
            anaResult['component']    = str( raw[0]['component'] )
            anaResult['serialNumber'] = raw[0]['serialNumber']
            anaResult['raw_id']       = ObjectId(raw_id)
            anaResult['gridfs_attachments'] = attachments

        except Exception as e:
            logger.warning( str(e) )
            logger.warning( traceback.format_exc() )

        localdb.QC.result.insert_one( anaResult )
        
        # attach the record in QC status
        qcStatus = localdb.QC.module.status.find_one({ 'component':str(component['_id']) } )
        if qcStatus == None:
            raise Exception('ERROR: internal glitch in LocalDB: QC status of {} is not found!'.format( component['_id'] ) )
    
        localdb.QC.module.status.update_one(
            { 'component':str(component['_id']) },
            { '$set': { 'QC_results.{}.{}'.format( test[0]['stage'], test[0]['testType'] ) : str(anaResult['_id']) } }
        )

        try:
            if update_config and cpt_doc.get('componentType') == 'front-end_chip' and all( [ chip_api, config_id, updated_config_data, raw_id, anaResult['_id'] ] ):
                revision_prev    = chip_api.get_info( config_id ).get( 'current_revision_id' )
                revision_current = chip_api.commit( config_id, updated_config_data,
                                                    f'config revision by {testTypeHyphened} with RAW data {raw_id} and analysis result {str( anaResult["_id"] )}' )
                
                localdb.QC.result.update_one( {'_id':anaResult['_id']}, { '$set' : { 'config_id' : config_id, 'config_revision_prev' : str(revision_prev), 'config_revision_current' : str(revision_current) } }  )

        except Exception as e:
            logger.warning( str(e) )
            
            
            
    else:
        logger.info( f'analyze_measurement(): duplicated record identified (ObjectId {prev_results}): : not inserting' )
        anaResult = localdb.QC.result.find_one( { 'raw_id' : ObjectId(raw_id), 'results.property.ANALYSIS_VERSION' : anaResult.get('results').get('property').get('ANALYSIS_VERSION') } )
        anaResult['was_already_registerd'] = True
    
    # clean workplace
    shutil.rmtree( f'{TMP_DIR}/{rawHash}' )

    logger.info( 'analyze_measurement() end' )

    return anaResult


def enumerate_subcomponents( component_id, match_component_type ):

    this_component = localdb.component.find_one( { '_id' : ObjectId( component_id ) } )
    if this_component.get('componentType') == match_component_type:
        return [ this_component ]

    cprs = list( localdb.childParentRelation.find( { 'parent' : str( component_id ) } ) )

    out = []

    for cpr in cprs:
        child = localdb.component.find_one( ObjectId( cpr.get('child') ) )
        out += enumerate_subcomponents( child.get('_id'), match_component_type )

    return out


def fetch_reference_ivs( component ):

    component_type = component.get('componentType')
    target_stage   = localdb.QC.module.status.find_one( {'component' : str( component.get('_id') ) } ).get('currentStage')

    # logger.info( pprint.pformat( component ) )
    
    logger.info( f'fetch_reference_ivs: componentType = { component.get("componentType") }' )
    
    reference_map = { 'module'      : [ { 'componentType' : 'sensor_tile',
                                          'stage'         : 'BAREMODULERECEPTION',
                                          'iv_testType'   : 'IV_MEASURE',
                                          'cv_testType'   : 'CV_MEASURE'
                                         } ],
                      'bare_module' : [ { 'componentType' : 'sensor_tile',
                                          'stage'         : 'sensor_manufacturer',
                                          'iv_testType'   : 'IV_MEASURE',
                                          'cv_testType'   : 'CV_MEASURE'
                                         },
                                         { 'componentType' : 'sensor_tile',
                                           'stage'         : 'BAREMODULERECEPTION',
                                           'iv_testType'   : 'IV_MEASURE',
                                           'cv_testType'   : 'CV_MEASURE'
                                          } ],
                      'sensor_tile'  : [ { 'componentType' : 'sensor_tile',
                                           'stage'         : 'sensor_manufacturer',
                                           'iv_testType'   : 'IV_MEASURE',
                                           'cv_testType'   : 'CV_MEASURE'
                                         } ],
                     }

    

    reference_IVs = []

    for reference_info in reference_map.get( component_type ):

        if reference_info is None:
            logger.error( f'fetch_reference_ivs(): Reference map for component {componentType} is undefined!' )
            return None

        reference_components = enumerate_subcomponents( component.get('_id'), reference_info.get('componentType') )

        logger.info( 'reference_components = ' + pprint.pformat( [ r.get('serialNumber') for r in reference_components ] ) )
        logger.info( f'reference_info = { reference_info }' )

        reference_iv_testRuns = [ localdb.QC.result.find_one( { "$or" : [ { 'component'    : str( component.get('_id') ),
                                                                            'stage'        : reference_info.get('stage'),
                                                                            'testType'     : reference_info.get('iv_testType') },
                                                                          { 'component'    : str( component.get('_id') ),
                                                                            'currentStage' : reference_info.get('stage'),
                                                                            'testType'     : reference_info.get('iv_testType') } ] } )
                                  for component in reference_components ]


        reference_cv_testRuns = [ localdb.QC.result.find_one( { "or" : [ { 'component'    : str( component.get('_id') ),
                                                                           'stage'        : reference_info.get('stage'),
                                                                           'testType'     : reference_info.get('cv_testType') },
                                                                         { 'component'    : str( component.get('_id') ),
                                                                           'currentStage' : reference_info.get('stage'),
                                                                           'testType'     : reference_info.get('cv_testType') } ] } )
                                  for component in reference_components ]

        # Loop over references
        for ref_cpt, ref_iv_tr, ref_cv_tr in zip( reference_components, reference_iv_testRuns, reference_cv_testRuns ):

            logger.info( f'fetch_reference_ivs(): Reference component: {ref_cpt.get("serialNumber")}' )

            prop_Vfd = -9999.0
            for prop in ref_cpt.get('properties'):
                if prop.get('code') == 'V_FULLDEPL' and prop.get('value'):
                    prop_Vfd = prop.get('value')

            prop_Vbd = -9999.0
            for prop in ref_cpt.get('properties'):
                if prop.get('code') == 'BREAKDOWN_VOLTAGE' and prop.get('value'):
                    prop_Vbd = prop.get('value')

            default_Ilc = -9999.0

            if ref_iv_tr is None:
                logger.warning( f'fetch_reference_ivs(): Reference IV TestRun is not found!' )
                return None

            else:
                
                if 'properties' in ref_iv_tr["results"] and not 'property' in ref_iv_tr["results"]:
                    ref_iv_tr['results']['property'] = copy.deepcopy( ref_iv_tr['results']['properties'] )

                reference_IVs.append( 
                    { 'component_sn': ref_cpt.get('serialNumber'),
                      'stage'       : reference_info.get('stage'),
                      'Vbd'         : ref_iv_tr["results"]["BREAKDOWN_VOLTAGE"] if ref_iv_tr["results"]["BREAKDOWN_VOLTAGE"] >= 0.0 else prop_Vbd,
                      'Vfd'         : ref_cv_tr["results"]["V_FULLDEPL"]        if ref_cv_tr else prop_Vfd,
                      'Ilc'         : ref_iv_tr["results"]["LEAK_CURRENT"]      if ref_iv_tr["results"]["LEAK_CURRENT"] >= 0.0 else default_Ilc,
                      'temperature' : ref_iv_tr["results"]["property"]["TEMP"],
                      'IV_ARRAY'    : {
                          "voltage"     : ref_iv_tr["results"]["IV_ARRAY"].get("voltage") if "voltage" in ref_iv_tr["results"]["IV_ARRAY"] else ref_iv_tr["results"]["IV_ARRAY"].get('VOLTAGE'),
                          "current"     : ref_iv_tr["results"]["IV_ARRAY"].get("current") if "current" in ref_iv_tr["results"]["IV_ARRAY"] else ref_iv_tr["results"]["IV_ARRAY"].get('CURRENT'),
                          "temperature" : ref_iv_tr["results"]["IV_ARRAY"].get("temperature") if "temperature" in ref_iv_tr["results"]["IV_ARRAY"] else ref_iv_tr["results"]["IV_ARRAY"].get('TEMPERATURE')
                      },
                      'qc_passed'   : ref_iv_tr["passed"]
                     }
                )
            
    previous_data = {
        "target_component" : component.get("serialNumber"),
        "target_stage"     : target_stage,
        "reference_IVs"    : reference_IVs
    }

    # logger.info( f'previous_data = {pprint.pformat( previous_data )}' )

    return previous_data




def submit_binary(path):
    fs = gridfs.GridFS(localdb)
    with open(path, "rb") as f:
        binary = f.read()
    return fs.put(binary)
            
            



@qc_api.route("/create_summary", methods=["GET", "POST"])
def create_summary():
    
    if not session.get("logged_in", False):
        return render_template( "401.html" )
    
    component = request.args.get('component', None )
    stage     = request.args.get('stage', None )
    testType  = request.args.get('test', None )
    
    mode      = request.form.get('mode', 'default')
    
    formData  = request.form

    if not component:
        component = request.form.get('component', None )
    if not stage:
        stage     = request.form.get('stage', None )
    if not testType:
        testType  = request.form.get('test', None )
        
    logger.debug( 'create_summary(): component = {}, stage = {}, testType = {}, mode = {}'.format( component, stage, testType, mode ) )
    logger.debug( pprint.pformat( formData ) )
        
    if not component:
        return render_template( "500.html" )
        
    cpt = localdb.component.find_one( {'_id':ObjectId( component ) } )
    
    testFormat = userdb.QC.tests.find_one( { 'code':testType, 'componentType.code':cpt['componentType'].upper() } )

    title = 'Create Summary Test'

    
    pageDocs = { 'mode' : mode,
                 'title': title,
                 'component': cpt,
                 'stage':stage,
                 'testType':testType,
                 'testFormat':testFormat,
                 'formatRaw': json2html.convert( testFormat ),
                 'componentRaw' : json2html.convert( cpt ),
                 'input' : formData
    }
    
    logger.debug( 'pageDocs = {}'.format( pprint.pformat( pageDocs ) ) )
    
    callbacks = { 'default' : processCreateSummaryDefault,
                  'input'   : processCreateSummaryInput,
                  'done'    : processCreateSummaryDone }
    
    try:
        callback = callbacks[mode]
        
        if callback:
            return callback( pageDocs )
    
    except Exception as e:
        raise( e )



def processCreateSummaryDefault( pageDocs ):
    
    component  = pageDocs['component']
    stage      = pageDocs['stage']
    testType   = pageDocs['testType']
    testFormat = pageDocs['testFormat']
    
    resultCandidates = {}
    
    # List of supported test types
    processFunctions = { 'E_SUMMARY' : process_E_SUMMARY }
    
    # Loop over parameters
    for parameter in testFormat['parameters']:
            
        code = parameter['code']
        
        processFunctions[ testType ]( component, stage, code, resultCandidates, pageDocs['mode'] )
        
    
    pageDocs.update( { 'resultCandidates': resultCandidates } )
    pageDocs.update( { 'resultCandidatesHTML': json2html.convert( resultCandidates ) } )
        
    return render_template( "qc/create_summaryTest.html", **pageDocs, timezones=pytz.all_timezones )
    
    
    
def processCreateSummaryInput( pageDocs ):
    
    component  = pageDocs['component']
    stage      = pageDocs['stage']
    testType   = pageDocs['testType']
    testFormat = pageDocs['testFormat']
    
    pageDocs['mode'] = 'confirm'
        
    resultCandidates = {}
    
    # List of supported test types
    processFunctions = { 'E_SUMMARY' : process_E_SUMMARY }
    
    # Loop-1: gather all inputs
    for parameter in testFormat['parameters']:
            
        code = parameter['code']
        
        processFunctions[ testType ]( component, stage, code, resultCandidates, pageDocs['mode']  )
        
    pageDocs.update( { 'resultCandidates': resultCandidates } )
    pageDocs.update( { 'resultCandidatesHTML': json2html.convert( resultCandidates ) } )

    
    if pageDocs['mode'] == 'default':
        return render_template( "qc/create_summaryTest.html", **pageDocs, timezones=pytz.all_timezones )
        
        

    ###########################################################
    # Input: create other field values from user's input

    cprs = localdb.childParentRelation.find( { 'parent': str( component['_id'] ) } )

    fes = []
    for cpr in cprs:
        subcpt = localdb.component.find_one( { '_id':ObjectId( cpr['child'] ) } )
        if subcpt['componentType'] == 'front-end_chip':
            fes += [ str( subcpt['_id'] ) ]

    logger.info( f'#FEs = {len(fes)}: {fes}' )
    
    FE_testInfo = userdb.QC.stages.find_one( {'code':'FE_CHIP'} )['stage_test']
    
    try:
        feTests = FE_testInfo[stage]
        logger.info( f'processCreateSummaryInput(): feTests = {feTests}' )
    except:
        logger.error( f'stage {stage} was not found in the FE_CHIP info of userdb.QC.stages' )

    if len( feTests ) == 0:
        feTests = ['ADC_CALIBRATION', 'SLDO', 'VCAL_CALIBRATION', 'ANALOG_READBACK', 'LP_MODE', 'OVERVOLTAGE_PROTECTION', 'INJECTION_CAPACITANCE', 'MIN_HEALTH_TEST', 'TUNING', 'PIXEL_FAILURE_ANALYSIS', 'BAD_PIXEL_NUMBER', 'UNDERSHUNT_PROTECTION', 'DATA_TRANSMISSION' ]

    # logger.info( pprint.pformat(pageDocs['input'] ) )

    allowSkip = True

    summaryEvals = {}
    qcEvals = []

    for feTest in feTests:
        
        passFails = []
        
        for parameter in testFormat['parameters']:
            code = parameter['code']

            # the code should contain the feTest string and LINK
            if not ( code.find( feTest ) >= 0 and code.find('LINK') >= 0 ):
                continue
    
            # here, pageDocs['input'] is ImmutableMultiDict, use get(key)
            candidateId = pageDocs['input'].get( code )
                
            if candidateId == 'on':
                candidateId = 'skipped'

            result = None
            if candidateId != None and candidateId != 'skipped':
                result = localdb.QC.result.find_one( { '_id':ObjectId( candidateId ) } )
            elif candidateId == 'skipped':
                result = 'skipped'

            if result != None:
                try:
                    passFails += [ result['passed'] ]
                except:
                    # skipped case --> True
                    passFails += [ None ]
            else:
                passFails += [ None ]

            #print( feTest, code, result )
                
        # Take AND of the FE QC flags up to the last non-blank FE, but for blank input, always False
        isAllNone = all( [ f == None for f in passFails[:len(fes)] ] )
        qcFlag = all( passFails[:len(fes)] ) if isAllNone == False else None
        
        #print( f'Test = {feTest}, isAllNone = {isAllNone}, qcFlag = {qcFlag}, passFails = {passFails[:len(fes)]}' )

        summaryEvals[ '_'.join( ['MODULE',feTest] )] = qcFlag
        qcEvals += [ qcFlag ]

    pageDocs.update( { 'summaryEvals' : summaryEvals } )
    pageDocs.update( { 'qcPassed' : all( qcEvals ) } )
    
    return render_template( "qc/create_summaryTest.html", **pageDocs, timezones=pytz.all_timezones )



def getCandidateResults( componentId, stage, testType ):
    results = []
    
    #logger.info( pprint.pformat( { 'component':componentId, 'stage':stage, 'testType':testType } ) )
    
    rs1 = localdb.QC.result.find( { 'component':componentId, 'stage':stage,        'testType':testType } )
    rs2 = localdb.QC.result.find( { 'component':componentId, 'currentStage':stage, 'testType':testType } )

    dropKeys = ['_id', 'user', 'address', 'currentStage', 'testType', 'raw_id', 'component', 'sys', 'prodDB_record', 'dbVersion' ]
    
    for r in rs1:
        r_copy = copy.deepcopy( r )
        r_copy['Time Stamp'] = r['sys']['mts']
        for key in dropKeys:
            if key in r_copy:
                r_copy.pop( key )
        r_copy['Result Data'] = r_copy.pop( 'results' )
        r['resultsHTML'] = json2html.convert( r_copy )
        r['Time Stamp'] = get_localtime_str( r['sys']['mts'] )
        r['componentId'] = componentId
        r['analysis_version'] = r['results']['properties']['ANALYSIS_VERSION'] if 'properties' in r['results'] else r['results']['property']['ANALYSIS_VERSION']
        results.append( r )
    
    for r in rs2:
        r_copy = copy.deepcopy( r )
        r_copy['Time Stamp'] = r['sys']['mts']
        for key in dropKeys:
            if key in r_copy:
                r_copy.pop( key )
        r_copy['Result Data'] = r_copy.pop( 'results' )
        r['resultsHTML'] = json2html.convert( r_copy )
        r['Time Stamp'] = get_localtime_str( r['sys']['mts'] )
        r['componentId'] = componentId
        r['analysis_version'] = r['results']['properties']['ANALYSIS_VERSION'] if 'properties' in r['results'] else r['results']['property']['ANALYSIS_VERSION']
        results.append( r )
    
    return results
                



def process_E_SUMMARY( component, stage, code, resultCandidates, mode ):
    
    cprs = localdb.childParentRelation.find( { 'parent':str( component['_id'] ) } )
    
    chips = []
    for cpr in cprs:
        childId = cpr['child']
        child = localdb.component.find_one( { '_id': ObjectId(childId) } )
        if child['componentType'].upper() == 'FRONT-END_CHIP':
            chips += [ child ]
            
            
    tokens = code.split('_')
    componentCode = tokens[0]
    
    tmpList1 = []
    tmpList2 = []
    
    tmpFlag = False
    for tok in tokens[1:]:
        if tok == "FE":
            tmpFlag = True
        
        if tmpFlag == False:
            tmpList1 += [ tok ]
        else:
            tmpList2 += [ tok ]
                
    paramTestType = "_".join( tmpList1 )
    fieldName     = "_".join( tmpList2 )

    '''
    if fieldName != '':
        logger.info( f'componentCode = {componentCode}, paramTestType = {paramTestType}, fieldName = {fieldName}' )
    else:
        logger.info( f'componentCode = {componentCode}, paramTestType = {paramTestType}' )
    '''
    
            
    for componentType in [ "MODULE", "PCB", "BARE_MODULE", "SENSOR_TILE", "FE" ]:
    
        if fieldName.find( componentType ) != 0:
            continue
            
        for chipOrder in range(4):
            
            if len( chips ) < chipOrder+1:
                continue
            
            if fieldName == "{}_LINK_{}".format( componentType, chipOrder+1 ):
                
                    resultCandidates[code] = getCandidateResults( str( chips[chipOrder]['_id'] ), stage, paramTestType )
                    
    
    return
    


def processCreateSummaryDone( pageDocs ):
    
    component  = pageDocs['component']
    stage      = pageDocs['stage']
    testType   = pageDocs['testType']
    testFormat = pageDocs['testFormat']

    pageDocs['mode'] = 'done'
        
    # acquire the output format from the test bank
    
    outFormat = userdb.QC.tests.find_one( { 'code':testType } )
    
    if outFormat == None:
        raise Exception( f'ERROR: testType {testType} is not registered in LocalDB.' )
        
    # logger.debug( pprint.pformat( outFormat ) )
    
    out = {}
    out['component'] = str( component['_id'] )
    out['address']   = component['address']
    
    try:
        out['user']      = session['username']
    except:
        out['user']      = None
    
    out['sys']       = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }
    out['stage']     = stage
    out['testType']  = testType
    out['raw_id']    = None
    
    out['results'] = { 'properties':{} }

    summaryEvals = []
    
    # Parameter Loop-1: embedding user's inputs
    for parameter in outFormat['parameters']:
        code = parameter['code']
        dataType = parameter['dataType']
        
        if code in pageDocs['input']:
            if dataType == 'boolean':
                out['results'][ code ] = pageDocs['input'][ code ].lower() in ['true', '1']
            elif dataType == 'float':
                try:
                    out['results'][ code ] = float( pageDocs['input'][ code ] )
                except:
                    logger.warning( f'processCreateSummaryDone(): parameter { code }  failed in formatting to float' )
                    out['results'][ code ] = None
            elif dataType == 'int':
                try:
                    out['results'][ code ] = int( pageDocs['input'][ code ] )
                except:
                    logger.warning( f'processCreateSummaryDone(): parameter { code }  failed in formatting to int' )
                    out['results'][ code ] = None
            elif dataType == 'codeTable':
                try:
                    if pageDocs['input'][ code ] == 'True':
                        out['results'][ code ] = 1
                        summaryEvals += [ True ]
                    elif pageDocs['input'][ code ] == 'False':
                        out['results'][ code ] = 0
                        summaryEvals += [ False ]
                    else:
                        out['results'][ code ] = -1
                except:
                    logger.warning( f'processCreateSummaryDone(): parameter { code }  failed in formatting to codeTable' )
                    out['results'][ code ] = -1
            else:
                out['results'][ code ] = pageDocs['input'][ code ] if pageDocs['input'][ code ] != 'on' else None
                if out['results'][ code ] == None:
                    logger.warning( f'processCreateSummaryDone(): parameter { code } was not filled (None)' )
        else:
            logger.info( f'processCreateSummaryDone(): dismissed parameter { code } (dataType: { dataType }' )
            out['results'][ code ] = None

    # Parameter Loop-2: collect bad pixels
    for parameter in outFormat['parameters']:
        code = parameter['code']
        dataType = parameter['dataType']

        if 'ELECTRICALLY_BAD_PIXEL_NUMBER_FE' in code:
            feIndex = code[-1]

            try:
                referenceResultId = out['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_ELECTRICALLY_FAILED')
                    out['results'][code] = value

                else:
                    logger.info( f'reference result for {code} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {code}' )

        elif 'DISCONNECTED_PIXEL_NUMBER_FE' in code:
            feIndex = code[-1]

            try:
                referenceResultId = out['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_DISCONNECTED_PIXELS')
                    out['results'][code] = value

                else:
                    logger.info( f'reference result for {code} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {code}' )

        elif 'BAD_PIXEL_NUMBER_FE' in code:
            feIndex = code[-1]

            try:
                referenceResultId = out['results'][ f'MODULE_PIXEL_FAILURE_ANALYSIS_FE_LINK_{feIndex}' ]

                if referenceResultId:
                    referenceResult = localdb.QC.result.find_one( {'_id':ObjectId( referenceResultId ) } )

                    value = referenceResult.get('results').get('PIXEL_FAILURE_FAILING_PIXELS')
                    if value >= 0:
                        out['results'][code] = value
                    else:
                        out['results'][code] = None

                else:
                    logger.info( f'reference result for {code} is missing, skipped to evaluate' )
                    
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'processCreateSummaryDone(): failure in evaluating {code}' )

    # Fill the rest parameters
    summaryVarList = ['MODULE_ELECTRICALLY_BAD_PIXEL_NUMBER', 'MODULE_DISCONNECTED_PIXEL_NUMBER', 'MODULE_BAD_PIXEL_NUMBER' ]
    
    for var in summaryVarList:
        elems = [ out['results'][ f'{var}_FE_{feIndex}' ] for feIndex in range(1, 5) if out['results'][ f'{var}_FE_{feIndex}' ] is not None ]
        if len( elems ) == 0:
            out['results'][var] = None
        else:
            out['results'][var] = sum( elems )
        

    for prop in outFormat['properties']:

        if prop['code'] == 'ANALYSIS_VERSION':
            out['results']['properties'][ prop['code'] ] = qcAnalysisVersion
            
        if prop['code'] == 'MODULE_TEMPERATURE':
            if stage.lower().find('warm')>=0:
                out['results']['properties'][ prop['code'] ] = 20
            elif stage.lower().find('cold')>=0:
                out['results']['properties'][ prop['code'] ] = -15
                
        

    qcPassed = all( summaryEvals )
    out['passed'] = qcPassed
    
    # logger.info( pprint.pformat( out ) )

    # Insert TestRun
    localdb.QC.result.insert_one( out )
    
    # attach the record in QC status
    logger.info( 'attach the record in QC status' )
    qcStatus = localdb.QC.module.status.find_one({ 'component':str(component['_id']) } )
    if qcStatus == None:
        raise Exception('ERROR: internal glitch in LocalDB: QC status of {} is not found!'.format( component['_id'] ) )
        
    localdb.QC.module.status.update_one(
        { 'component':str(component['_id']) },
        { '$set': { 'QC_results.{}.{}'.format( out['stage'], out['testType'] ) : str(out['_id']) } }
    )

    logger.info( f'finding cprs: parent_id = {component["_id"]}' )
    _cprs = localdb.childParentRelation.find( {'parent':str( component['_id'] ) } )
    FEs = []
    for cpr in _cprs:
        child = localdb.component.find_one( { '_id':ObjectId( cpr['child'] ) } )
        if child['componentType'] != 'front-end_chip':
            continue
        FEs += [ str( child['_id'] ) ]

    # loop over test parameters and update the QC.module.status of the FE chips accordingly
    logger.info( 'loop over test parameters and update the QC.module.status of the FE chips accordingly' )
    
    for parameter in outFormat['parameters']:
        
        code = parameter['code']
        
        if not code.find('FE_LINK')>=0:
            continue
        
        feTestCode = '_'.join( code.split('_')[1:-3] )
        feIndex    = int( code.split('_')[-1] ) - 1

        #logger.info( f'{code} --> {feTestCode}, {feIndex}' )
        
        if feIndex >= len(FEs):
            continue
        
        fe_cpt_id = FEs[feIndex]

        logger.info( f'updating FE chip {fe_cpt_id} QC_results.{out["stage"]}.{feTestCode} --> {out["results"][ code ]}' )
        if out['results'][ code ] != None:
            localdb.QC.module.status.update_one( { 'component':fe_cpt_id }, { '$set': { f'QC_results.{out["stage"]}.{feTestCode}' : out['results'][ code ] } } )
        else:
            localdb.QC.module.status.update_one( { 'component':fe_cpt_id }, { '$set': { f'QC_results.{out["stage"]}.{feTestCode}' : '-1' } } )
        
        
    return redirect( url_for('component_api.show_component', id=str( component['_id'] ), collection='component') )





@qc_api.route("/select_test", methods=["GET", "POST"])
def select_test():
    initPage()

    page_docs = { "run":[], 'text':'' }
    page_docs["mode"] = request.form.get("mode", "input")
    
    page_docs["page"]  = "select_test"
    page_docs["title"] = "Sign off QC test in this stage"
    
    if not session.get("logged_in", False):
        return render_template( "401.html" )
    
    try:
        session.get("logged_in", False)
    except:
        page_docs["mode"] = "input"
        page_docs["text"] = "Please log in before the selection."
        return render_template( "localSignoff.html", **page_docs, timezones=pytz.all_timezones )
        
    componentId = request.args.get("componentId", None)
    page_docs["componentId"]   = componentId
    
    thisComponent = localdb.component.find_one( {"_id":ObjectId(page_docs["componentId"]) } )
    page_docs["componentName"] = str(thisComponent["name"])
    
    #logging.debug( 'select_test(): thisComponent = '+pprint.pformat( thisComponent ) )
    
    componentType = name_map[ thisComponent['componentType'] ]
    page_docs['componentType'] = componentType
    #logging.debug( componentType )
    
    stageInfo = userdb.QC.stages.find_one( {'code':componentType } )
    #logging.debug( 'select_test(): stageInfo = '+pprint.pformat( stageInfo ) )
    
    thisComponentQCStatus = localdb.QC.module.status.find_one( { "component":str(thisComponent["_id"]) } )
    #logging.debug( pprint.pformat( thisComponentQCStatus ) )
    
    stage = thisComponentQCStatus["currentStage"]
    nextStage = stageInfo['stage_flow'][ stageInfo['stage_flow'].index( stage )+1 ]
    
    page_docs["stage"]     = stage
    try:
        page_docs["testTypes"] = [ test for test in thisComponentQCStatus["QC_results"][page_docs["stage"]] ]
    except:
        page_docs["testTypes"] = []
    page_docs['tests']     = [ t for t in localdb.QC.result.find( { "$or" : [ { 'component':componentId, 'stage':stage }, { 'component':componentId, 'currentStage':stage } ] }  ) ]

    # Augment test results
    for test in page_docs['tests']:
        test['resultHTML'] = json2html.convert( test['results'] )

        test['timestamp'] = get_localtime_str( test['sys']['mts'] )
        test['analysis_version'] = test['results']['properties']['ANALYSIS_VERSION'] if 'properties' in test['results'] else test['results']['property']['ANALYSIS_VERSION']
        
        selected = request.form.get( '{}_id'.format( test['testType'] ), None )
        
        if selected == str( test['_id'] ):
            test['selected'] = True
            
    # unselected flags (indicating blank submission of the test)
    page_docs['unselected'] = []
    entitledTests = {} # { testName : testID }
    for testType in page_docs['testTypes']:
        selected = request.form.get( f'{testType}_id', None )
        if selected == "":
            page_docs['unselected'] += [ testType ]
            entitledTests[testType] = "-1"
        else:
            entitledTests[testType] = selected
            
    
    if page_docs['mode'] == 'complete' or page_docs['mode'] == 'complete_sync':
        
        localdb.QC.module.status.update( { "component" : str(thisComponent["_id"]) },
                                         { "$set": { ( 'QC_results.'+page_docs['stage'] )    : entitledTests,
                                                     ( 'upload_status.'+page_docs['stage'] ) : "-1"         } } )

        for testname,doc_id in entitledTests.items():
            localdb.QC.module.status.update( { "component" : str(thisComponent["_id"]) },
                                             { "$set": { ( f'QC_results_pdb.{page_docs["stage"]}.{testname}' )    : "-1" } } )
        
        for testType in page_docs['testTypes']:
            
            entitledTestId = entitledTests[ testType ]
            
            test = None
            for t in page_docs['tests']:
                if str(t['_id']) == entitledTestId:
                    test = t
                    break

            if test == None:
                continue
                   
            for fieldCode in test['results']:
                value = test['results'][fieldCode]
                
                #logging.debug( f'{fieldCode}, {value}' )
                
                # Rule: tests with a test link should have LINK in the name
                if fieldCode.find('LINK') < 0:
                    continue
                    
                resultId = value
                
                if resultId == None:
                    continue
                    
                result = localdb.QC.result.find_one( { '_id':ObjectId( resultId ) } )
                
                if result == None:
                    logging.warning( f'select_test(): ResultId {resultId} is requested but not found in DB!' )
                    continue

                #logger.info( pprint.pformat( result ) )
                subComponentId       = result['component']
                subComponentStage    = result['currentStage'] if 'currentStage' in result else result['stage']
                subComponentTestType = result['testType']
                            
                localdb.QC.module.status.update_one( { "component" : subComponentId },
                                                     { '$set':{ f'QC_results.{subComponentStage}.{subComponentTestType}' : resultId,
                                                                ( 'upload_status.'+page_docs['stage'] )                  : "-1"         } } )
                
                subComponentQCStatus = localdb.QC.module.status.find_one( { "component":subComponentId } )
                logger.debug( pprint.pformat( subComponentQCStatus ) )
                
            # endfor fieldCode
        # endfor testType
                
                
        # Recursively increment stages of the component and all sub-components
        
        incrementStagesRecursively( str( thisComponent['_id'] ) )
        
        if page_docs['mode'] == 'complete':
            return redirect( url_for('component_api.show_component', id=str( thisComponent['_id'] ), collection='component') )
            
        if page_docs['mode'] == 'complete_sync':
            return redirect( url_for('qc_api.result_transceiver', module=thisComponent['name'], mode='upload_input') )
        
    return render_template( "localSignoff.html", **page_docs, timezones=pytz.all_timezones )



def incrementStagesRecursively( componentId, revert = False, destStage = None ):
    
    '''
    Description:
      Recursively increment the stage of components in the assembly breakdown structure.
      This function just assumes that each component's current stage is all coherently aligned.
    
      - If the component has children, call this function to each of them
      - If no child component is present, return
    
    '''
    
    
    # increment the stage of this component
    
    thisComponent = localdb.component.find_one( {"_id":ObjectId(componentId) } )
    componentType = name_map[ thisComponent['componentType'] ]
    
    stageInfo = userdb.QC.stages.find_one( {'code':componentType } )
    #logging.debug( f'incrementStagesRecursively(): componentId = {componentId}' )
    #logging.debug( f'incrementStagesRecursively(): componentType = {componentType}' )
    #logging.debug( pprint.pformat( stageInfo ) )
    
    thisComponentQCStatus = localdb.QC.module.status.find_one( { "component":str(thisComponent["_id"]) } )
    # logging.debug( f'incrementStagesRecursively(): qcStatus = ')
    # logging.debug( pprint.pformat( thisComponentQCStatus ) )
    
    stage = thisComponentQCStatus["currentStage"]
    
    if destStage != None:
        localdb.QC.module.status.update( { "component" : componentId },
                                         { "$set": { 'currentStage' : destStage } } )
        localdb.component.update( { "_id" : ObjectId(componentId) },
                                  { "$set": { 'currentStage' : destStage } } )
        logging.debug( f'incrementStagesRecursively(): successfully incremented the stage of {componentType} ID {componentId} from {stage} to {destStage}.' )

        """
        # copy configs from previous stage's last revisions
        if componentType == 'FE_CHIP':

            initialWarmIndex = stageInfo['stage_flow'].index('MODULE/INITIAL_WARM')
            stageFlowAfterInitialWarm = stageInfo['stage_flow'][ initialWarmIndex+1: ]
        
            if destStage in stageFlowAfterInitialWarm :
            
                chip_api = ChipConfigAPI( client )

                prev_configs = list( localdb.fe_configs.find( { 'serialNumber' : thisComponent.get('serialNumber'), 'stage' : stage } ) )

                for prev in prev_configs:
                    prev_id = str( prev.get('_id') )
                    prev_info = chip_api.get_info( prev_id )
                    chip_api.copy_config( prev_id, thisComponent.get('serialNumber'), destStage, prev_info.get('branch') )
        """
            
        
    else:
        try:
            stageIndex = stageInfo['stage_flow'].index( stage )
        except:
            stageIndex = 0
        
        if revert == False and stageIndex < len( stageInfo['stage_flow'] ) -1:
            
            destStage = stageInfo['stage_flow'][ stageIndex+1 ]
            
        elif revert == True and stageIndex > 0:
            
            destStage = stageInfo['stage_flow'][ stageIndex-1 ]
            
        localdb.QC.module.status.update( { "component" : componentId },
                                         { "$set": { 'currentStage' : destStage } } )

        logging.debug( f'incrementStagesRecursively(): successfully incremented the stage of {componentType} ID {componentId} from {stage} to {destStage}.' )
            
    # recursively increment the stage of this component, but exclude BARE_MODULE, to avoid duplication (FE_CHIP already incremented under MODULE)
    if componentType != 'BARE_MODULE':
        for cpr in localdb.childParentRelation.find( { 'parent':componentId } ):
            incrementStagesRecursively( cpr['child'], destStage = destStage )
    else:
        for cpr in localdb.childParentRelation.find( { 'parent':componentId } ):
            child = localdb.component.find_one( { '_id' : ObjectId(cpr['child']) } )
            if child.get('componentType') == 'sensor_tile':
                incrementStagesRecursively( cpr['child'], destStage = destStage )
        
    return




@qc_api.route("/switch_stage", methods=["GET", "POST"])
def switch_stage():

    if not session.get("logged_in", False):
        return render_template( "401.html" )
        
    component = request.args.get("module", "")
    destStage = request.form.get("destStage", None)
    mode = request.form.get("mode", "local_input")
    
    
    text = ""
    
    component_doc = localdb.component.find_one( { "name": component } )
    componentId = str(component_doc["_id"])
    #logging.info( 'switch_stage: component_doc = ' + pprint.pformat(component_doc) )

    componentType = component_doc.get('componentType')
    logging.info( f'switch_stage: componentType = {componentType}' )
    
    if componentType == 'module':
        qcStagesDoc = userdb.QC.stages.find_one( {'code':'MODULE'} )
    elif componentType == 'module_pcb':
        qcStagesDoc = userdb.QC.stages.find_one( {'code':'PCB'} )
    elif componentType == 'bare_module':
        qcStagesDoc = userdb.QC.stages.find_one({'code':'BARE_MODULE'})
    elif componentType == 'sensor_tile':
        qcStagesDoc = userdb.QC.stages.find_one({'code':'SENSOR_TILE'})
        
    #logging.info( pprint.pformat( qcStagesDoc ) )
    stageListAll = copy.deepcopy( qcStagesDoc['stage_flow'] )
    stageList = qcStagesDoc['stage_flow']
    alternativeList = qcStagesDoc['alternatives']
    for alt in alternativeList:
        stageList.pop( stageList.index( alt ) )

    # for component other than module, remove stages other than 'MODULE/'
    if componentType != 'module':
        moduleStages = [ stage for stage in stageList if stage.find('MODULE/')==0 ]
        for stage in moduleStages:
            stageList.pop( stageList.index( stage ) )
    
    qc_component_doc = localdb.QC.module.status.find_one( { "component":componentId } )
    try:
        currentStage = qc_component_doc["currentStage"]
        currentStageIndex = stageListAll.index( currentStage )
    except Exception as e:
        logger.warning( str(e) )
        logger.warning( f'failed in identifying the current stage in localdb.QC.module.status of componentId {componentId}. Using the first stage instead.' )
        currentStage = stageList[0]
        currentStageIndex = 0
        localdb.QC.module.status.update_one( { "component":componentId }, { "$set" : { 'currentStage' : currentStage } } )
    
    if mode == "local_complete":
        if destStage == None:
            
            text = "Please select stage from the pull-down menu..."
            mode = "local_input"
            doc = { "_id":componentId, "module": component, "mode": mode, "currentStage": currentStage, "nstage":destStage, "text": text, "stages":stageList, "alternatives":alternativeList }
            return render_template("switchStage.html", doc = doc)
            
        
        destStageIndex    = stageListAll.index( destStage )
        
        logging.debug( f'switch_stage(): attempting to switch stage from {currentStage} (index {currentStageIndex}) to {destStage} (index {destStageIndex})' )
        
        while True:
            
            qc_component_doc = localdb.QC.module.status.find_one( { "component":componentId } )
            currentStage = qc_component_doc["currentStage"]
            try:
                currentStageIndex = stageListAll.index( currentStage )
            except:
                currentStageIndex = 0
            
            incrementStagesRecursively( componentId, revert = (destStageIndex < currentStageIndex) )
            
            qc_component_doc = localdb.QC.module.status.find_one( { "component":componentId } )
            currentStage = qc_component_doc["currentStage"]
            
            if currentStage == destStage:
                break
            
        mode = "local_input"
        text = "The current stage was switched to " + destStage + '.'
        
        return redirect( url_for('component_api.show_component', id=componentId, collection='component') )

    doc = { "_id":componentId, "module": component, "mode": mode, "currentStage": currentStage, "nstage":destStage, "text": text, "stages":stageList, "alternatives":alternativeList }
    return render_template("switchStage.html", doc = doc)





@qc_api.route("/result_transceiver", methods=["GET", "POST"])
def result_transceiver():
    
    if not session.get("logged_in", False):
        return render_template( "401.html" )

    doc = {}
    module = request.args.get("module", "")
    doc["_id"] = request.args.get("id", "")
    mode = request.args.get("mode", None)
    if mode == None:
        mode = request.form.get("mode", "select")
    text = ""
    
    query = { "name": module }
    component_doc = localdb.component.find_one( query )
    
    doc = { "_id":str(component_doc["_id"]),"mname": module, "mode": mode, "text": text }
    
    logger.debug( 'result_transceiver(): mode = {}'.format( doc['mode'] ) )
    
    ####################
    ## Upload results ##
    if doc["mode"] == "select":
        doc["upload_summary"] = {}
        status_doc = localdb.QC.module.status.find_one({"component": doc["_id"]})
        doc["upload_summary"] = status_doc.get("upload_status", {})
    
    if doc["mode"] == "upload_input":
        summary = {}
        uploaded_stage = []
        status_doc = localdb.QC.module.status.find_one( {"component": doc["_id"]} )
        
        doc['stageInfo'] = userdb.QC.stages.find_one( { 'code' : status_doc['componentType'] } )
        stage_flow = doc['stageInfo']['stage_flow']
        
        #logger.debug( pprint.pformat( status_doc ) )
        #logger.debug( pprint.pformat( status_doc["currentStage"] ) )
        
        currentIndex = stage_flow.index( status_doc['currentStage'] )
        
        for index, stage in enumerate(stage_flow):
            if stage == status_doc["currentStage"]:
                break
            
            # skip alternative stage if it is not the previous stage
            if stage in doc['stageInfo']['alternatives'] and index != currentIndex-1:
                continue
                
            if status_doc["upload_status"][stage] == "-1":
                summary[stage] = status_doc["QC_results"][stage]
            else:
                uploaded_stage.append(stage)
            
        doc["upload_summary"] = summary
        doc["uploaded_stage"] = uploaded_stage
    
    if doc["mode"] == "upload_complete":

        code1 = request.form.get('code1', None)
        code2 = request.form.get('code2', None)

        thread = threading.Thread( target = upload_worker, args = ( module, code1, code2 ) )
        thread.start()
        return redirect( url_for('component_api.show_component', id=doc['_id'], collection='component') )
            
    return render_template( "result_tranceiver.html", doc = doc )




@qc_api.route("/site_qualification", methods=["GET", "POST"])
def site_qualification():

    #if not session.get("logged_in", False):
    #    return render_template( "401.html" )
    
    mode = request.form.get('mode', None )

    print( f'mode = {mode}' )

    doc = {}

    if mode == None:
        doc['mode'] = 'init'
        return render_template( "site_qualification.html", doc = doc )
    

    elif mode == 'select':
        code1 = request.form.get('code1', None )
        code2 = request.form.get('code2', None )

        flag, site_qualification.client = process_request(code1, code2)

        if flag == 0:
            doc['mode'] = 'init'
            doc['message'] = 'Failed in ITkPD authentication: Please retry.'
            return render_template( "site_qualification.html", doc = doc )

        site_qualification.sites = [ site for site in site_qualification.client.get('listComponents', json={"project" : 'P', "componentType": 'MODULESQ' } ) ]

        doc['sites'] = site_qualification.sites

        doc['mode'] = mode
        
        return render_template( "site_qualification.html", doc = doc )
    
    elif mode == 'submit':

        site = request.form.get('selectedSite', None)

        site_qualification.institution_code = None

        for s in site_qualification.sites:
            if s['serialNumber'] == site:
                site_qualification.institution_code = s['institution']['code']
        
        print( f'Site code == { site }' )

        doc['site'] = site
        doc['localdb_version'] = qcAnalysisVersion

        fetch_sysdata( doc )
        fetch_dbdata( doc )
            
        doc['dataHTML'] = json2html.convert( doc )
        doc['mode'] = mode
        
        return render_template( "site_qualification.html", doc = doc )

    elif mode == 'complete':
        
        site = request.form.get('selectedSite', None)
        
        print( f'Site code == { site }' )

        doc['site'] = site
        doc['localdb_version'] = qcAnalysisVersion

        fetch_sysdata( doc )
        fetch_dbdata( doc )
        
        doc['mode'] = mode

        site_qualification.client.post( 'setComponentStage', json = { 'component':site, 'stage':'DB' } )

        for test in [ 'DB_SETUP', 'DB_ACC', 'DB_NON', 'DB_HELP' ]:
            
            tr = site_qualification.client.get( "generateTestTypeDtoSample", json={ 'project':'P', 'componentType':'MODULESQ', 'code':test } )

            hashid = hashlib.md5( json.dumps(doc, cls=MongoJSONEncoder).encode() ).hexdigest()[:30]

            tr.update( {
                "component": site,
                "institution": site_qualification.institution_code,
                "date": datetime.now(timezone.utc).strftime('%Y-%m-%dT%H:%MZ'),
                "runNumber": hashid,
                'properties': {'SQ_STATUS': "0"}
            } )

            pprint.pprint( tr )

            res = site_qualification.client.post( "uploadTestRunResults", json= tr )

            pprint.pprint( res )

            os.makedirs( IF_DIR, exist_ok = True )
            tmpFilePath = f'{IF_DIR}/{hashid}.json'

            with open( tmpFilePath, 'w') as f:
                json.dump( json.loads(json.dumps(doc, cls=MongoJSONEncoder)), f, indent=4)

            attachment_file = open(tmpFilePath, 'rb')

            page_attachment = { "data": (tmpFilePath, attachment_file, "json") }

            res = site_qualification.client.post( 'createTestRunAttachment',
                                                  data = { 'testRun':res['testRun']['id'], 'type':'file', 'title':'server_info.json', 'url':tmpFilePath, 'description':'server_info.json' },
                                                  files = page_attachment )

            pprint.pprint( res )
        
        os.remove( tmpFilePath )
        
        return render_template( "site_qualification.html", doc = doc )


class MongoJSONEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)

def fetch_sysdata( doc ):
    
    subout = subprocess.run( 'uname -a', shell=True, capture_output=True, text=True )
    doc['uname'] = subout.stdout


    if doc['uname'].find( 'Linux' ) == 0:

        subout = subprocess.run( 'lscpu', shell=True, capture_output=True, text=True )
        doc['cpu'] = subout.stdout.split("\n")

        subout = subprocess.run( 'lsmem', shell=True, capture_output=True, text=True )
        doc['memory'] = subout.stdout.split("\n")

    elif doc['uname'].find( 'Darwin' ) == 0:

        subout = subprocess.run( 'system_profiler SPSoftwareDataType SPHardwareDataType', shell=True, capture_output=True, text=True )
        doc['sys_profile'] = subout.stdout.split("\n")

    subout = subprocess.run( 'df -h', shell=True, capture_output=True, text=True )
    doc['disk_space'] = subout.stdout.split("\n")

    subout = subprocess.run( 'mongod --version', shell=True, capture_output=True, text=True )
    doc['mongod'] = subout.stdout.split("\n")

    subout = subprocess.run( 'influxd version', shell=True, capture_output=True, text=True )
    doc['influxd'] = subout.stdout.split("\n")

    return


def fetch_dbdata( doc ):
    doc['localdb'] = {}
    doc['localdbtools'] = {}

    for collection in localdb.list_collection_names():
        
        if collection in [ 'fs.chunks', 'fs.files' ]:
            continue
        
        doc['localdb'][collection] = localdb[collection].find_one( {}, sort=[( '_id', DESCENDING )] )

    for collection in userdb.list_collection_names():
        doc['localdbtools'][collection] = userdb[collection].find_one( {}, sort=[( '_id', DESCENDING )] )


    return



@qc_api.route("/compare_tests", methods=["GET", "POST"])
def compare_tests():
    
    module_sn = request.form.get("module_sn", type=str, default = None )
    subcpt    = request.form.get("subcpt",    type=str, default = None )
    test_type = request.form.get("test_type", type=str, default = None )
    stage1    = request.form.get("stage1",    type=str, default = None )
    stage2    = request.form.get("stage2",    type=str, default = None )

    
    state_info = { 'module_sn' : module_sn,
                   'subcpt'    : subcpt,
                   'test_type' : test_type,
                   'stage1'    : stage1,
                   'stage2'    : stage2      }


    
    if not module_sn:

        modules = sorted( [ doc.get('serialNumber') for doc in localdb.component.find( { 'componentType' : 'module' }, { 'serialNumber' : 1 } ) ] )
        state_info.update( { "module_list" : modules } )
        
        state_info.update( { "step" : "init" } )
        
        return render_template( 'compare_tests.html', state = state_info )

    elif not subcpt:
        
        state_info.update( { "step" : "subcpt" } )

        component_types = list( userdb.componentType.find( {}, {'name':1, 'code':1} ) )
        state_info.update( { "cpt_list" : component_types } )
        
        return render_template( 'compare_tests.html', state = state_info )
    
    elif not test_type:
        
        state_info.update( { "step" : "test_type" } )

        test_types = list( userdb.QC.tests.find( { "componentType.code" : subcpt, "state" : "active" } ).sort( [ ( 'code', pymongo.ASCENDING ) ] ) )

        # In order for a comparison to work, the test is defined in at least 2 stages
        stage_tests = userdb.QC.stages.find_one( {'code' : subcpt} ).get("stage_test")

        test_types_valid = []
        
        for test in test_types:
            n_tests = sum( [ (test.get("code") in test_list) for _stage, test_list in stage_tests.items() ] )
            if n_tests >= 2:
                test_types_valid.append( test )
            

        state_info.update( { "test_list" : test_types_valid } )
        
        return render_template( 'compare_tests.html', state = state_info )
    
    elif not stage1 or not stage2:

        # Limit the stages only to those which have the specified test type
        stage_info = userdb.QC.stages.find_one( {'code' : subcpt} )
        stage_tests = stage_info.get("stage_test")
        stage_flow = stage_info.get("stage_flow")

        module_doc_id = str( localdb.component.find_one( { 'serialNumber' : module_sn } ).get("_id") )
        current_stage = localdb.QC.module.status.find_one( { 'component' : module_doc_id } ).get('currentStage')

        print( "currentStage: ", current_stage )

        # Up to the current stage
        stage_tests_valid = []
        
        for st in stage_flow:
            tests = stage_tests.get(st)
            if test_type in tests:
                print( st )
                stage_tests_valid.append( st )
                
            if st == current_stage:
                break
            
        
        state_info.update( { "step" : "stage" } )
        state_info.update( { "stage_list" : stage_tests_valid } )

        return render_template( 'compare_tests.html', state = state_info )

    #---------------------------------
    # Display comparison
    
    state_info.update( { "step" : "display" } )

    compare_data = []

    for stage in [ stage1, stage2 ]:

        # list matching components recursively
        module_doc= localdb.component.find_one( { 'serialNumber' : module_sn } )
        name_map = { "MODULE":"module", "PCB":"module_pcb", "BARE_MODULE":"bare_module", "SENSOR_TILE":"sensor_tile", "FE_CHIP":"front-end_chip" }

        valid_child_docs = [ child_doc for child_doc in get_module_children( module_doc )
                             if child_doc.get('componentType') == name_map.get( subcpt ) ]

        data = []
        
        for child_doc in valid_child_docs:
            data_obj = {}
        
            child_sn = child_doc.get("serialNumber")
            qc_status = localdb.QC.module.status.find_one( { 'component' : str( child_doc.get("_id") ) } ).get("QC_results")

            test_id = qc_status.get( stage ).get( test_type )

            try:
                test_data = localdb.QC.result.find_one( { '_id' : ObjectId( test_id ) } )
                [ test_data.pop(key) for key in ['prodDB_record', 'component', 'user', 'address', 'sys', 'testType', 'pdb_runNumber', 'currentStage', '_id', 'dbVersion'] ]

                if 'raw_id' in test_data:
                    raw_data = localdb.QC.testRAW.find_one( { "_id" : test_data.get("raw_id") } )
                    test_data['raw_id'] = json2html.convert( raw_data )

                if 'gridfs_attachments' in test_data:
                    attachments = dump_cache( test_data['gridfs_attachments'] )
                    test_data['gridfs_attachments'] = attachments
                    
                data_obj = { 'stage' : stage, 'serialNumber' : child_sn, 'result': test_data }

            except:
                data_obj = { 'stage' : stage, 'serialNumber' : child_sn, 'result':None }

            data.append( data_obj )

        compare_data.append( data )

    state_info["compare_data"] = compare_data
    return render_template( 'compare_tests.html', state = state_info )





def dump_cache( attachments ):

    att_data = {}
    
    for filename, oid in attachments.items():
        os.makedirs( THUMBNAIL_DIR, exist_ok=True )

        ext = filename.split('.')[-1]

        cache_file = '{}/{}.{}'.format( THUMBNAIL_DIR, str(oid), ext )

        if os.path.exists( cache_file ):
            pass
        else:
            try:
                data = fs.get( oid ).read()

                with open( cache_file, "wb" ) as f:
                    f.write( data )
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'Failed in dumping attachment to {cache_file}' )

        att_data.update( { cache_file[ cache_file.find('static'): ] : [filename, ext] } )

        logger.info( f'attachment dumped to cache {cache_file}' )

    return att_data

