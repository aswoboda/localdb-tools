#!/usr/bin/env python3
# -*- coding: utf-8 -*
##################################
## Author4: Satoshi Kinoshita (kinoshi at hep.phys.titech.ac.jp)
## Copyright: Copyright 2019, ldbtools
## Date: July. 2020
## Project: Local Database Tools
## Description: Compare pictures
##################################

PAGE_NAME = "picture_data"

from functions.imports import *
from PIL import Image
import io
import subprocess

picture_data_api = Blueprint("picture_data_api", __name__)

#######################
### route functions ###
#######################

############################################################
# display pictures of golden module or previous stage module
@picture_data_api.route("/picData", methods=["GET", "POST"])
def picData():
    initPage()

    session["this"] = request.args.get("id", None)
    session["collection"] = request.args.get("collection", "chip")
    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId", None)
    session["unit"] = "front-end_chip"  ### TODO just temporary coding
    session["logged_in"] = session.get("logged_in",False)

    components = VIResults(session["this"], session["collection"], session["runId"])

    return render_template("picturely.html", components = components)



@picture_data_api.route("/download", methods=["GET", "POST"])
def download():
    path = request.args.get("cache", "")
    with open( path, 'rb' ) as f:
        return base64.b64encode(f.read())


    
##############################################
# Set VI Result Information & Data
# i_old: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_old: ObjectId of this testRun document
def VIResults(i_oid, i_col, i_tr_oid):

    if not i_tr_oid:
        return {}

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.QC.result.find_one(query)
    if this_tr == None:
        return {}
    query = {"_id": ObjectId(this_tr["component"])}
    this_cp = localdb.component.find_one(query)

    results = {}

    if this_tr["testType"] == "OPTICAL":
        ### output data
        pictures = this_tr["results"]["img_entire"]
        pics = []
        thumbs = []
        for i in pictures:
            pic = pictures[i]
            data = fs.get( ObjectId( str(pic)) ).read()
            buf  = base64.b64encode(data)
            code_base64 = buf.decode()
            pic = bin2image("png", code_base64)
            pics.append(pic)
            
            tmp = io.BytesIO( base64.b64decode(buf) )
            tmp2 = Image.open( tmp )
            buf_thumb = io.BytesIO()
            tmp3 = tmp2.resize(( 600,600), Image.ANTIALIAS )
            tmp3.save( '/var/tmp/tmp.png', format="PNG" )
            
            with open("/var/tmp/tmp.png", "rb") as f:
                binary = base64.b64encode(f.read()).decode()
                thumb = "data:image/png;base64,{}".format(binary)
                thumbs.append(thumb)
            
        results.update({
            "_id": i_oid,
            "anomaly": this_tr["results"]["anomaly"],
            "comment": this_tr["results"]["comment"],
            "img": pics,
            "thumb" : thumbs
        })


    return results
