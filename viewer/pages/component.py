from functions.imports import *
from functions.checkouter import *

# import module for metrology
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import MultipleLocator
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import axes3d
from scipy import linalg
import sys
import re
import imghdr
import time
from io import open
from json2html import *

import pickle

import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI

component_api = Blueprint("component_api", __name__)

#######################
### route functions ###
#######################
from functions.imports import *

########################
# display component page
@component_api.route("/component", methods=["GET", "POST"])
def show_component():
    initPage()

    session["this"]            = request.args.get("id", None)
    session["collection"]      = request.args.get("collection", "chip")
    session["code"]            = request.args.get("code", "")
    session["test"]            = request.args.get("test", "")
    session["runId"]           = request.args.get("runId", None)
    session["unit"]            = "front-end_chip"  ### TODO just temporary coding
    session["logged_in"]       = session.get("logged_in",False)
    session["regeneratePlots"] = request.form.get("regeneratePlots", type=bool, default = False )
    session["expandPlots"]     = request.form.get("expandPlots", type=bool, default = False )

    config_create_processed = request.args.get("config_create_processed", False)

    # component info
    this_component_doc = setComponentInfo(session.get("this", None), session["collection"])
    this_component_doc["name_delim"] = delim_SN( this_component_doc['serialNumber'] )
    this_component_doc["typeinfo"]   = SN_typeinfo( this_component_doc['serialNumber'], verbose=1 )

    serialNumber = this_component_doc.get('serialNumber', this_component_doc['name'] )

    # comment
    comments = setComments(session.get("this", None), session.get("runId",None), this_component_doc)

    # check test items for the current stage
    latest_tests = setLatestQCSummary(session.get("this", None))
    latest_props = setLatestPropSummary(session.get("this", None))

    # make status of current stage
    try:
        selection_result = [ item["runId"] for item in latest_tests["results"]]
        selection_prop = [ item["runId"] for item in latest_props["results"]]
    except:
        selection_result = []
        selection_prop = []

    # set summary
    summary = setQCSummary(session.get("this", None))
    summary_pdb = setQCSummaryAtProdDB(session.get("this", None))

    latest_qc_status = localdb.QC.module.status.find_one( {"component":session.get("this", None) } )

    upload_status = None
    flow_version = None
    if latest_qc_status != None:
        upload_status = latest_qc_status.get("upload_status", None)
        flow_version = latest_qc_status.get("stageVersion", "RD53A")

    qcmenu = None
    test_items = None
    try:
        # left: LDB, right: PDB code
        name_map = { "module":"MODULE", "module_pcb":"PCB", "bare_module":"BARE_MODULE", "sensor_tile":"SENSOR_TILE", "front-end_chip":"FE_CHIP" }
        code = name_map[ this_component_doc['type'] ]
        stageInfo  = userdb.QC.stages.find_one( { 'code':code } )
        qcmenu = stageInfo['stage_test']
        test_items = stageInfo['test_items']
        #logger.info( 'qcmenu = {}'.format( qcmenu ) )
    except:
        logger.warning( 'Not able to find {} QC menu'.format( code ) )

    qctest_item_list = None

    try:
        if this_component_doc['type'] == 'module_pcb':
            # set results for QC tests
            with open("{}/json-lists/supported_test_pcb.json".format(VIEWER_DIR)) as f:
                qctest_item_list = json.load(f)
        elif this_component_doc['type'] == 'bare_module':
            # set results for QC tests
            with open("{}/json-lists/supported_test_bare.json".format(VIEWER_DIR)) as f:
                qctest_item_list = json.load(f)
        elif this_component_doc['type'] == 'sensor_tile':
            # set results for QC tests
            with open("{}/json-lists/supported_test_sensor.json".format(VIEWER_DIR)) as f:
                qctest_item_list = json.load(f)
        elif this_component_doc['type'] == 'module':
            # set results for QC tests
            with open("{}/json-lists/supported_test.json".format(VIEWER_DIR)) as f:
                qctest_item_list = json.load(f)
        else:
            qctest_item_list = {}
    except:
        logger.error( 'component.py(): Exception in loading supported tests! Check JSON format.' )
        raise

    qcresult_index = setQCResultIndex(session["this"], session["collection"])
    qcresults, tr_ser = setQCResults(session["this"], session["collection"], session["runId"], session['regeneratePlots'], session['expandPlots'])
    qctest = {
        "test_item":qctest_item_list,
        "resultIndex": qcresult_index,
        "results": qcresults,
        "menu": qcmenu,
        "test_items":test_items
    }

    if 'results' in qctest and 'results' in qctest.get('results'):
        for key,value in qctest['results']['results'].items():

            if (key.find('_LINK')>0 and value != None) or ('LINK_TO_SENSOR_IV' in key and value != None):
                try:
                    result = localdb.QC.result.find_one( {'_id':ObjectId(value)} )
                    component = localdb.component.find_one( {'_id':ObjectId( result['component'] ) } )

                    # logger.info( pprint.pformat(result) )
                    passed = 'Passed' if result['passed'] == True else 'Failed'
                    ana_version = result['results']['property']['ANALYSIS_VERSION'] if 'property' in result['results'] else result['results']['properties']['ANALYSIS_VERSION']
                    url = url_for('component_api.show_component', id=result['component'], collection='component', test='qctest', runId=value)

                    if passed == 'Passed':
                        value = f"<a href=\"{url}\" class=\"badge badge-success\">{component['name']} : {passed} (mqat v{ana_version})</a>"
                    else:
                        value = f"<a href=\"{url}\" class=\"badge badge-dark\">{component['name']} : {passed} (mqat v{ana_version})</a>"

                    qctest['results']['results'][key] = value
                except Exception as e:
                    logger.warning( str(e) )
                    logger.warning( f'failure in result rendering for linking subcomponent TestRuns: item = {key},{value}' )

            elif value == None:
                qctest['results']['results'][key] = "<span class=\"badge badge-warning\">N/A</span>"
            else:
                pass

    '''
    # set results for Module properties
    moduleprop = {}
    try:
        if this_component_doc['type'] == 'module_pcb':
            pass
        elif this_component_doc['type'] == 'bare_module':
            pass
        elif this_component_doc['type'] == 'sensor_tile':
            pass
        elif this_component_doc['type'] == 'module':
            with open("{}/json-lists/module_prop.json".format(VIEWER_DIR)) as f:
                module_prop_list = json.load(f)
                moduleprop_index = setModulePropIndex(session["this"], session["collection"])
                modulepropresults = setModuleProp(session["this"], session["collection"], session["runId"])
                moduleprop = {
                    "prop_item": module_prop_list,
                    "propIndex": moduleprop_index,
                    "properties": modulepropresults
                }
        else:
            pass
    except:
        logger.error( 'component.py(): Exception in loading properties! Check JSON format.' )
        raise
    '''

    mode = request.form.get('mode', None)
    if mode == 'updateProperties':
        properties_orig = this_component_doc['properties']

        propChanges = {}

        for index, prop in enumerate( properties_orig ):
            code = prop['code']
            value_orig = prop['value']

            value_input = request.form.get( code, None, str )

            if value_input == "" or value_input == "N/A" or value_input == None:
                if value_orig != None:
                    logger.debug( f'- Property change for code {code} : value_orig = "{value_orig}" value_input = "{value_input}"' )
                    prop['value'] = None

                    localdb.component.update( { '_id':ObjectId( session['this'] ) }, { '$set': { f'properties.{index}' : prop } } )
                continue

            if prop['dataType'] == 'boolean':
                if value_input == 'true':
                    value_input = True
                elif value_input == 'false':
                    value_input = False

            elif prop['dataType'] == 'float':
                value_input = float( value_input )

            elif prop['dataType'] == 'int':
                value_input = int( value_input )

            if value_input != value_orig:

                logger.debug( f'- Property change for code {code} : value_orig = "{value_orig}" value_input = "{value_input}"' )
                prop['value'] = value_input

                localdb.component.update( { '_id':ObjectId( session['this'] ) }, { '$set': { f'properties.{index}' : prop } } )

    # In case of module, check if chip config created or not
    if this_component_doc['type'] == 'module':
        chips = []

        subcpts = localdb.childParentRelation.find( { 'parent': session['this'] } )
        for subcpt in subcpts:
            try:
                subcpt_doc = localdb.component.find_one( { '_id':ObjectId( subcpt.get('child',None) ), 'componentType':'front-end_chip' } )
            except:
                pass

            if subcpt_doc != None:
                subcpt_SN = subcpt_doc.get("serialNumber", None)
                if subcpt_SN != None:
                    chips += [ subcpt_SN ]

        branches = ['warm', 'cold', 'LP']

        is_config_created = True

        for chip in chips:
            for branch in branches:
                cfg = localdb.fe_configs.find_one( { 'serialNumber' : chip, 'stage' : this_component_doc.get('stage'), 'branch' : branch } )
                if not cfg:
                    logger.info( f'  ==> cfg not found for ({chip}, {this_component_doc.get("stage")}, {branch})')
                    is_config_created = False
                    continue

                elif not cfg.get('current_revision_id'):
                    logger.info( '  ==> current_revision_id not found')
                    is_config_created = False


        localdb.component.update( {'_id':ObjectId( session['this'] )}, { "$set" : { 'isConfigGenerated' : is_config_created } } )
        this_component_doc['is_config_created'] = is_config_created


        if this_component_doc.get('stage') == 'MODULE/INITIAL_WARM' and not is_config_created and not config_create_processed:
            return redirect( url_for('qc_api.create_configs', componentId = session.get("this") ) )

        if userdb.message.find_one( { 'serialNumber':serialNumber, 'code':'DOWNLOAD_CONFIG_INFO' } ):
            return redirect( url_for( 'toppage_api.show_toppage' ) )


        if not is_config_created:
            if not userdb.message.find_one( { 'component':serialNumber, 'code':'WARNING_FE_CONFIG_NOT_READY' } ):
                if this_component_doc.get('stage') in [ 'MODULE/WIREBONDING', 'MODULE/ASSEMBLY' ]:
                    create_message( f"WARNING: For the current stage ({this_component_doc.get('stage')}), FE configs are not present, please do not upload YARR scans to LocalDB", 'show_components', serialNumber, 'WARNING_FE_CONFIG_NOT_READY', False )
                else:
                    create_message( f"WARNING: FE Chip configs for the current stage ({this_component_doc.get('stage')}) is not ready", 'show_components', serialNumber, 'WARNING_FE_CONFIG_NOT_READY', False )

        else:
            userdb.message.remove( { 'component':serialNumber, 'code':'WARNING_FE_CONFIG_NOT_READY' } )
            logger.info('all needed configs are stored!')


    # In case of fe-chips, create config revision index
    config_api = ChipConfigAPI( client )

    config_revisions = {}

    if this_component_doc['type'] == 'front-end_chip':
        serialNumber = this_component_doc.get('serialNumber', this_component_doc['name'] )

        chip_configs = [ cfg for cfg in localdb.fe_configs.find( { 'serialNumber' : serialNumber } ) ]

        stages = []
        for cfg in chip_configs:
            if not cfg.get('stage', None) in stages:
                stages += [ cfg.get('stage') ]

        for stage in reversed(stages):

            config_revisions[stage] = {}

            stageIndex = stages.index( stage )

            branches = []
            for cfg in chip_configs:
                if stage != cfg.get('stage', None):
                    continue

                if not cfg.get('branch', None) in branches:
                    branches += [ cfg.get('branch') ]

            for branch in branches:

                config_revisions[stage][branch] = { 'prev_stage' : stages[stageIndex-1] if stageIndex>0 else None }

                config_id = config_api.checkout( serialNumber, stage, branch )

                if not config_id:
                    continue

                config_api.info( config_id )

                try:

                    revision_history = config_api.get_revision_history( config_id )

                except Exception as e:
                    logger.warning( f'branch {branch}: {str(e)}' )
                    logger.warning( traceback.format_exc() )
                    continue

                for revision_id in revision_history:
                    try:
                        revision = localdb.fe_config_revision.find_one( {'_id':ObjectId(revision_id)} )
                    except:
                        continue

                    revision.pop('config_data')
                    revision.pop('diff')
                    revision.pop('pix_config')
                    if 'timestamp' in revision:
                        revision['timestamp'] = get_localtime_str( revision['timestamp'] )
                    else:
                        revision['timestamp'] = ''
                    revision['configId'] = config_id
                    revision['serialNumber'] = serialNumber
                    revision['stage'] = stage
                    revision['branch'] = branch
                    config_revisions[stage][branch][revision_id] = revision



    # set results for electrical scans
    scan_result_index = setResultIndex(session["this"], session["collection"])
    scan_results = setResults(session["this"], session["collection"], session["runId"])
    # plots = setPlots(session["this"], session["collection"], session["runId"], selection = True, dumpPlots = True, regeneratePlots = session['regeneratePlots'] , expandPlots = session['expandPlots'] )

    try:
        plots_light = set_light_plots( session['this'], this_component_doc['type'], session['runId'], session['regeneratePlots'] )
    except Exception as e:
        plotting_mutex_lock = False
        raise e



    dcs = setDCS(session["runId"])
    electrical = {
      "resultIndex": scan_result_index,
      "results": scan_results,
      "dcs": dcs,
      "plots_light": plots_light
    }

    component = {
      "_id": session.get("this", None),
      "collection": session["collection"],
      "test": session["test"],
      "runId": session["runId"],
      "info": this_component_doc,
      "comments": comments,
      "property": {},
      "electrical": electrical,
      "qctest":qctest,
      "tr_ser" : tr_ser,
      "summary": summary,
      "summary_pdb": summary_pdb,
      "upload_status": upload_status,
      "flow_version": flow_version,
      "latest_tests": latest_tests,
      "latest_props": latest_props,
      "selection_result": selection_result,
      "selection_prop": selection_prop,
      "stageInfo": stageInfo,
      "config_revision" : config_revisions
    }

    if this_component_doc:
        component["upload_result"] = {}
        progressMonFile = IF_DIR+"/doing_upload_" + this_component_doc["name"] + ".txt"

        try:
            with open( progressMonFile ) as f:
                component["upload_result"]["uploading"] = '\n'.join( [item.rstrip('\n') for item in f.readlines()] )
        except:
            component["upload_result"] = None

    else:
        component["upload_result"] = None

    # messages
    messages = format_messages( { 'component' : serialNumber } )
    table_docs = { 'messages' : messages }

    # mqat version
    global mqat_version
    table_docs.update( { 'mqat_version' : mqat_version } )
    table_docs.update( { 'qc_analysis_version' : qcAnalysisVersion } )

    return render_template( "component.html", component=component, table_docs = table_docs, timezones=pytz.all_timezones )


@component_api.route("/nomodule_result", methods=["GET", "POST"])
def nomodule_result():
    initPage()

    session["collection"] = request.args.get("collection", "chip")
    session["code"] = request.args.get("code", "")
    session["test"] = request.args.get("test", "")
    session["runId"] = request.args.get("runId", None)
    session["unit"] = "front-end_chip"  ### TODO just temporary coding

    scan_results = setResults("", session["collection"], session["runId"])
    plots = setPlots("", session["collection"], session["runId"], selection = True, dumpPlots = True)
    plots_light = set_light_plots( session['this'], 'front-end_chip', session['runId'] )
    dcs = setDCS(session["runId"])
    electrical = {
      "results": scan_results,
      "dcs": dcs,
      "plots": plots
    }

    component = {
      "electrical": electrical
    }


    return render_template("nomodule_result.html", component=component)


###################
# show summary plot
@component_api.route("/show_plot", methods=["GET"])
def show_plot():
    name = request.args.get("name")
    tr_oid = request.args.get("runId")

    filepath = "{0}/{1}/{2}_root.png".format(CACHE_DIR, tr_oid, name)

    image = Image.open(filepath)
    filename = "{0}_{1}_{2}_root.png".format(session["uuid"], tr_oid, name)
    image.save("{0}/{1}".format(THUMBNAIL_DIR, filename))
    url = url_for("thumbnail.static", filename=filename)

    return redirect(url)




###################
# show summary plot
@component_api.route("/show_summary", methods=["GET"])
def show_summary():
    initPage()

    # get from args
    code = request.args.get("code")
    scan = request.args.get("scan")
    stage = request.args.get("stage")

    query = {"_id": ObjectId(code)}
    data = localdb.fs.files.find_one(query)
    if not "png" in data["filename"]:
        filePath = "{0}/{1}/{2}_{3}_{4}.png".format(
            THUMBNAIL_DIR, session["uuid"], stage, scan, data["filename"]
        )
    else:
        filePath = "{0}/{1}/{2}_{3}_{4}".format(
            THUMBNAIL_DIR, session["uuid"], stage, scan, data["filename"]
        )

    thum_dir = "{0}/{1}".format(THUMBNAIL_DIR, session["uuid"])
    cleanDir(thum_dir)

    binary = fs.get(ObjectId(code)).read()
    image_bin = io.BytesIO(binary)
    image = Image.open(image_bin)
    image.save(filePath)
    if not "png" in data["filename"]:
        url = url_for(
            "thumbnail.static",
            filename="{0}/{1}_{2}_{3}.png".format(
                session["uuid"], stage, scan, data["filename"]
            ),
        )
    else:
        url = url_for(
            "thumbnail.static",
            filename="{0}/{1}_{2}_{3}".format(
                session["uuid"], stage, scan, data["filename"]
            ),
        )

    return redirect(url)


#######################
# Display JSON/DAT Data
@component_api.route("/display_data", methods=["GET"])
def display_data():
    initPage()

    code = request.args.get("code")
    data_format = request.args.get("format")

    logger.info( f'code = {code}, data_format = {data_format}' )

    filesystem = gridfs.GridFS( localdb )

    if data_format == "pickle":

        try:

            fs_data = filesystem.get( ObjectId(code) )

            if not fs_data:
                response = make_response( f'data is not found on gridfs code = {code}' )
                return response

            pickle_data = fs_data.read()
            data = pickle.load(io.BytesIO(pickle_data))

            if data.get("Type") == 'Histo1d':

                bins = np.linspace( data.get("x").get("Low"), data.get("x").get("High"), data.get("x").get("Bins") )
                binRange = ( data.get("x").get("Low"), data.get("x").get("High") )

                plt.hist( bins, len(bins), weights = data.get("Data") )

            elif data.get("Type") == 'Histo2d':
                arr = np.array( data.get("Data") )
                plt.imshow( arr.T, origin='lower', cmap='turbo')
                cb = plt.colorbar( label = data.get("z").get("AxisTitle") )
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)


            plt.title( data.get("Name"), fontsize=22 )
            plt.xlabel( data.get("x").get("AxisTitle"), fontsize=18 )
            plt.ylabel( data.get("y").get("AxisTitle"), fontsize=18 )
            plt.tight_layout()

            tmpfile = f"{TMP_DIR}/{code}.png"
            plt.savefig( tmpfile, dpi=100 )

            with open(tmpfile, 'rb') as f:
                image = f.read()

                response = make_response( image )

                response.headers.set('Content-Type', 'image/png')
                response.headers.set('Content-Disposition', 'inline')

            plt.clf()

        except Exception as e:
            if "Data" in data:
                data.pop("Data")
                response = make_response( f'Plot rendering error: {str(e)}; metadata = {data}' )
            else:
                response = make_response( f'Plot rendering error: {str(e)}' )

    else:
        pickle_data = filesystem.get( ObjectId(code) ).read()
        data = json2html.convert( pickle.load(io.BytesIO(pickle_data)) )

        print( data )

        response = make_response(data)
        response.headers.set('Content-Type', 'text/html')

    return response


@component_api.route("/download_each_data", methods=["GET"])
def download_each_data():
    initPage()

    code = request.args.get("code")
    data_format = request.args.get("format")
    name = request.args.get("name")

    logger.info( f'code = {code}, data_format = {data_format}' )

    filesystem = gridfs.GridFS( localdb )

    fs_data = filesystem.get( ObjectId(code) )
    if not fs_data:
        response = make_response( f'data is not found on gridfs code = {code}' )
        return response


    if data_format == "pickle":

        try:

            pickle_data = fs_data.read()
            data = pickle.load(io.BytesIO(pickle_data))

            with open( f'{TMP_DIR}/{code}', 'w' ) as f:
                json.dump( data, f )

            return send_file(f'{TMP_DIR}/{code}', as_attachment=True, mimetype="text/html", download_name=name )

        except Exception as e:
            response = make_response( f'something went wrong: gridfs code = {code}, cache_path = {TMP_DIR}/{code}: {str(e)}' )
            return response

    else:

        logger.info( f'data_format = {data_format}' )

        try:
            pickle_data = filesystem.get( ObjectId(code) ).read()
            data = pickle.load(io.BytesIO(pickle_data))

            if '_id' in data:
                data.pop('_id')

            with open( f'{TMP_DIR}/{code}', 'w' ) as f:
                json.dump( data, f )

            return send_file(f'{TMP_DIR}/{code}', as_attachment=True, mimetype="text/html", download_name=name )
        except Exception as e:
            response = make_response( f'something went wrong: gridfs code = {code}, cache_path = {TMP_DIR}/{code}: {str(e)}' )
            return response



###################
# show summary plot
@component_api.route("/display_module", methods=["GET"])
def display_module():
    module = ['apple', 'pine']
    return render_template(
        module = module
    )



########################
# Download JSON/DAT Data
@component_api.route("/download_data", methods=["GET", "POST"])
def download_data():
    initPage()
    cmp_name = request.form.get("cmp", None)
    run_number = request.form.get("run", "data")
    if cmp_name:
        zip_name = "{0}_{1}".format(cmp_name, run_number)
    else:
        zip_name = run_number

    zip_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    cleanDir(zip_dir)
    zip_path = "{0}/{1}/{2}.zip".format(TMP_DIR, session["uuid"], zip_name)
    zip_file = zipfile.ZipFile(zip_path, "a")

    entries = request.form.getlist("data")
    for entry in entries:
        this = ast.literal_eval(entry)
        filename = "{0}.{1}".format(this["type"], this["format"])
        if not this["serialNumber"] == "":
            filename = "{0}_{1}".format(this["serialNumber"], filename)
        filepath = "{0}/{1}".format(zip_dir, filename)
        with open(filepath, "wb") as f:
            f.write(fs.get(ObjectId(this["code"])).read())
        zip_file.write(filepath, filename)
    zip_file.close()

    return send_file(zip_path, as_attachment=True, mimetype=ZIP_MIMETYPE)


##################
# Add/Edit Comment
@component_api.route("/edit_comment", methods=["GET", "POST"])
def edit_comment():
    oid = request.args.get("id", -1)
    tr_oid = request.args.get("runId", -1)

    thistime = datetime.utcnow()
    info = {
        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
        "component": oid,
        "runId": tr_oid,
        "comment": request.form.get("text").replace("\r\n", "<br>"),
        "componentType": session["unit"],
        "collection": session["collection"],
        "user_id": session["user_id"],
        "name": request.form.get("text2"),
        "institution": request.form.get("text3"),
        "datetime": thistime,
    }

    localdb.comments.insert(info)

    return redirect(request.headers.get("Referer"))


@component_api.route("/upload_flag", methods=["GET", "POST"])
def upload_flag():

    initPage()
    table_docs = { "run":[] }
    table_docs["_id"] = request.args.get("id", "")
    table_docs["mname"] = str(localdb.component.find_one( {"_id":ObjectId(table_docs["_id"]) } )["name"])
    table_docs["stage"] = request.args.get("stage", "")
    table_docs["test"] = request.args.get("test", "")
    table_docs["text"] = ""
    table_docs["pstage"] = request.form.get("pstage", "input")
    table_docs["flag"] = request.form.get("flag", "")

    if not session["logged_in"]:
        table_docs["pstage"] = "input"
        table_docs["text"] = "Please log in before the selection."
    else:
        if table_docs["pstage"] == "input":
            pass

        elif table_docs["pstage"] == "complete":
            institute = userdb.viewer.user.find_one({"username": session['username']})["institution"]
            result_para = {}
            if "ADC_CALIBRATION" in table_docs["test"]:
                result_para["flag"] = table_docs["flag"]
            elif "TUNING" in table_docs["test"]:
                result_para["flag"] = table_docs["flag"]
                temp = table_docs["test"].split("_")[1].replace("MINUS", "-").replace("DEG","")
                result_para["setting_temp"] = int(temp)

            new_flag_doc = {
                    "component"   : table_docs["_id"],
                    "user"        : session['username'],
                    "address"     : institute,
                    "currentStage": table_docs["stage"],
                    "sys"         : { "mts":datetime.now(),"cts":datetime.now(),"rev":0 },
                    "dbVersion"   : dbv,
                    "testType"    : table_docs["test"],
                    "results"     : result_para
                }
            localdb.QC.result.insert(new_flag_doc)

            ### script for updating a result
            #old_result_flag = localdb.QC.result.find_one({ "component": table_docs["_id"], "currentStage": table_docs["stage"], "testType": table_docs["test"] })
            #if old_result_flag:
            #    localdb.QC.result.update_one({"_id": old_result_flag["_id"]}, { "$set": new_flag_doc})
            #else:
            #    localdb.QC.result.insert(new_flag_doc)


    return render_template( "upload_flag.html", table_docs=table_docs, timezones=pytz.all_timezones )


@component_api.route("/set_stage", methods=["GET", "POST"])
def set_stage():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    module = request.args.get("module", "")
    nstage = request.form.get("nstage", "")
    pstage = request.form.get("pstage", "input")
    cstage = ""
    text = ""

    query = { "name": module }
    module_doc = localdb.component.find_one( query )

    query = { "component":str(module_doc["_id"]),"proddbVersion":proddbv }
    qc_module_doc = localdb.QC.module.status.find_one( query )
    stage_list = [s for s in qc_module_doc["QC_results"]]
    cstage = qc_module_doc["currentStage"]
    if pstage == "complete":
        if not nstage:
            text = "Please select stage from pull-down menue..."
            pstage = "input"
            doc = { "_id":str(module_doc["_id"]), "module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
            return render_template("set_stage.html", doc = doc)
        localdb.QC.module.status.update_one( query, { "$set":{"currentStage":nstage} })
        pstage = "input"
        text = "The current stage was switched to " + nstage + '.'

    doc = { "_id":str(module_doc["_id"]),"module": module, "pstage": pstage, "cstage": cstage, "nstege":nstage, "text": text, "stages":stage_list }
    return render_template("set_stage.html", doc = doc)



@component_api.route("/download_rootfile", methods=["GET", "POST"])
def download_rootfile():
    root_path = request.form.get("path","")
    return send_file(root_path, as_attachment=True, mimetype=ROOT_MIMETYPE)

@component_api.route("/download_json", methods=["GET", "POST"])
def download_json():
    path = request.form.get("path","")
    logger.info( f'download_json(): path = {path}' )
    return send_file(path, as_attachment=True, mimetype="application/json")

@component_api.route("/download_file", methods=["GET", "POST"])
def download_file():
    path = request.form.get("path","")
    filename = request.form.get("download_name","")
    mimetype = request.form.get("mimetype","")
    logger.info( f'download_json(): path = {path}' )
    return send_file(path, as_attachment=True, mimetype=mimetype, download_name=filename )

#######################
### other functions ###
#######################

###########################
# Set Component Information
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setComponentInfo(i_oid, i_col):
    if not i_oid:
        return {}
    ### parent
    parents = setCpr(i_oid, "child", "parent")
    ### child
    children = setCpr(i_oid, "parent", "child")
    ### component
    query = {"_id": ObjectId(i_oid)}
    this = localdb[i_col].find_one(query)
    qc_doc = localdb.QC.module.status.find_one( { "component": i_oid } )

    #logging.info( i_oid )
    #logging.info( i_col )
    #logging.info( pprint.pformat(this) )

    if qc_doc == None: qc_doc = {}

    if this and 'componentType' in this:
        session["unit"] = this["componentType"].lower()  ### TODO wakarinikui
    else:
        session["unit"] = "front-end_chip"

    docs = {
        "serialNumber" : this['serialNumber'],
        "name": this["name"],
        "component":i_oid,
        "stage": qc_doc.get("currentStage","..."),
        "parents": parents,
        "children": children,
        "type": this["componentType"].lower(),
        "chipType": this["chipType"],
        "qc": this.get("proDB",False),
        "proID": this.get("proID",False),
        "properties": this.get('properties', None)
    }
    return docs

###########################
# Set Child Parent Relation
# i_oid: ObjectId of this component/chip document
# i_cp_in: set this component to child or parent
# i_cp_out: get child or parent of this component
def setCpr(i_oid, i_cp_in, i_cp_out):
    docs = []
    query = {i_cp_in: i_oid}
    entries = localdb.childParentRelation.find(query)
    oids = []
    for entry in entries:
        oids.append(entry[i_cp_out])
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this = localdb.component.find_one(query)
        if this != None and 'name' in this:
            docs.append({"_id": str(oid), "collection": "component", "name": this["name"]})
        else:
            docs.append({"_id": str(oid), "collection": "component", "name": None})
    return docs

def setQCSummary(i_oid):
    summary = {}
    doc = localdb.QC.module.status.find_one( {"component":i_oid} )


    if doc == None:
        return summary

    # Adaptation to old version
    try:
        qcDoc = userdb.QC.stages.find_one( {"code":doc['componentType'] } )
    except:
        qcDoc = userdb.QC.stages.find_one( {"code":"MODULE" } )
    stage_flow = qcDoc['stage_flow']

    cstage = doc["currentStage"]
    for stage in stage_flow:
        if stage == cstage:
            break
        if stage in doc["QC_results"]:
            summary[stage] = doc["QC_results"][stage]
        else:
            summary[stage] = {}
    return summary

def setQCSummaryAtProdDB(i_oid):
    summary = {}
    doc = localdb.QC.module.status.find_one( {"component":i_oid} )


    if doc == None:
        return summary

    try:
        qcDoc = userdb.QC.stages.find_one( {"code":doc['componentType'] } )
    except:
        qcDoc = userdb.QC.stages.find_one( {"code":"MODULE" } )

    stage_flow = qcDoc['stage_flow']

    cstage = doc["currentStage"]
    for stage in stage_flow:
        if stage == cstage:
            break
        try:
            summary[stage] = doc["QC_results_pdb"][stage]
        except:
            summary[stage] = "-1"

    return summary

def setLatestQCSummary(i_oid):

    tests = {}
    doc = localdb.QC.module.status.find_one( {"component":i_oid} )
    if doc == None:
        return tests

    mname = localdb.component.find_one( {"_id":ObjectId(i_oid)} )["name"]
    cstage = doc["currentStage"]
    tests["stage"] = cstage
    tests["results"] = []
    etests = ScanCheckoutManager( mname, cstage ).test_scan_map

    try:
        logger.debug('setLatestQCSummary()')
        logger.debug( pprint.pformat(doc["QC_results"][cstage]) )

        for test in doc["QC_results"][cstage]:

            test_docs = None

            if localdb.QC.result.count({"$and":[{"component":i_oid},{"currentStage":cstage},{"testType":test}]}) > 0:
                test_docs = localdb.QC.result.find({"$and":[{"component":i_oid},{"currentStage":cstage},{"testType":test}]}).sort([("$natural", -1)]).limit(1)
            elif localdb.QC.result.count({"$and":[{"component":i_oid},{"stage":cstage},{"testType":test}]}) > 0:
                test_docs = localdb.QC.result.find({"$and":[{"component":i_oid},{"stage":cstage},{"testType":test}]}).sort([("$natural", -1)]).limit(1)
            else:
                # instance is not present, but at least a dummy slot is needed.
                test_docs = [ {
                    "component" : i_oid,
                    "currentStage" : cstage,
                    "dbVersion" : dbv,
                    "testType" : test,
                    "results" : {}
                } ]

            logger.debug( 'test_docs = {}'.format( test_docs ) )

            test_doc = {}
            for doc in test_docs:
                test_doc = doc

            tests["results"].append({
              "name"           :test,
              "runId"          :str(test_doc["_id"])  if test_doc != {} and '_id' in test_doc  else "",
              "scan_selection" :True                  if test in etests else False
            })
    except:
        pass
    return tests

def setLatestPropSummary(i_oid):

    tests = {}
    doc = ["RD53A_PULL-UP_RESISTOR", "IREFTRIM_FE", "ORIENTATION"]
    if doc == None:
        return tests

    mname = localdb.component.find_one( {"_id":ObjectId(i_oid)} )["name"]
    tests["results"] = []
    for test in doc:
        test_docs = localdb.QC.module.prop.find({"$and":[{"component":i_oid},{"testType":test}]}).sort([("$natural", -1)]).limit(1)
        test_doc = {}
        for item in test_docs:
            test_doc = item
        tests["results"].append({
          "name"           :test,
          "runId"          :str(test_doc["_id"])  if test_doc != {} else "",
        })
    return tests

#################
# Set Result List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setResultIndex(i_oid, i_col):
    docs = []
    query = {i_col: i_oid, "dbVersion":dbv}
    entries = localdb.componentTestRun.find(query)
    oids = []
    for entry in entries:
        oids.append(str(entry["_id"]))
    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_ctr = localdb.componentTestRun.find_one(query)
        query = {"_id": ObjectId(this_ctr["testRun"])}
        this_tr = localdb.testRun.find_one(query)

        ### result data
        result = (not this_tr["plots"] == [] and this_ctr["chip"] == "module") or (
            not this_ctr["attachments"] == [] and not this_ctr["chip"] == "module"
        )
        ### user
        query = {"_id": ObjectId(this_tr["user_id"])}
        this_user = localdb.user.find_one(query)
        ### site
        query = {"_id": ObjectId(this_tr["address"])}
        this_site = localdb.institution.find_one(query)
        ### score
        count = setCount()

        ### tags
        tag_docs = userdb.viewer.tag.docs.find( { "runId": str( this_tr["_id"] ) } )
        tags = [ tag['name'] for tag in tag_docs ]

        docs.append(
            {
                "_id": str(this_tr["_id"]),
                "runNumber": this_tr["runNumber"],
                "datetime": get_localtime_str( this_tr['startTime'] ),
                "testType": this_tr["testType"],
                "result": result,
                "stage": this_tr.get("stage", "N/A"),
                "user": this_user["userName"],
                "site": this_site["institution"],
                "rate": count.get("module", {}).get("rate", "-"),
                "score": count.get("module", {}).get("score", None),
                "values": count.get("module", {}).get("parameters", {}),
                "summary": this_tr.get("summary"),
                'tags': tags
            }
        )

    docs = sorted(docs, key=lambda x: (x["datetime"]), reverse=True)

    return docs


##############################
# Set QC Result List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setQCResultIndex(i_oid, i_col):
    docs = []
    query = { "component": i_oid }
    entries = localdb.QC.result.find(query)
    oids = []

    for entry in entries:
        oids.append(str(entry["_id"]))

    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_tr = localdb.QC.result.find_one(query)
        this_status = localdb.QC.module.status.find_one({"component":this_tr["component"]})

        try:
            QC_passed = this_tr['passed']
        except:
            QC_passed = None

        try:
            QC_result = (oid == this_status["QC_results"][this_tr["currentStage"]][this_tr["testType"]])
        except:
            QC_result = None


        stage = this_tr.get("currentStage", None)
        if stage == None:
            stage = this_tr.get("stage", "N/A")

        try:
            analysis_version = this_tr.get('results').get('property').get('ANALYSIS_VERSION')
        except:
            try:
                analysis_version = this_tr.get('results').get('properties').get('ANALYSIS_VERSION')
            except:
                analysis_version = ''

        ### result data of non-electrical test
        docs.append(
            {
                "component_id": str(this_tr["component"]),
                "_id": str(this_tr["_id"]),
                "testType": this_tr["testType"],
                "user": this_tr["user"] if 'user' in this_tr else None,
                "institute": this_tr["address"],
                "datetime": get_localtime_str( this_tr['sys']['mts'] ),
                "currentStage": stage,
                "QC_passed": QC_passed,
                "QC_result": QC_result,
                'analysis_version' : analysis_version
            }
        )

    docs = sorted(docs, key = lambda x: (x["datetime"]), reverse=True)

    return docs


###############################
# Set Result Information & Data
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_oid: ObjectId of this testRun document
def setResults(i_oid, i_col, i_tr_oid):
    if not i_tr_oid:
        return {}

    try:
        query = {"_id": ObjectId(i_tr_oid)}
    except Exception as e:
        logger.warning( str(e) )
        return {}

    this_tr = localdb.testRun.find_one(query)
    if this_tr == None:
        return {}
    ### output data
    outputs = {}
    outputs = setOutput(this_tr, outputs)
    query = {"testRun": i_tr_oid}
    ctr_entries = localdb.componentTestRun.find(query)
    cmps = []
    for this_ctr in ctr_entries:
        outputs = setOutput(this_ctr, outputs)
        cmps.append(
            {
                "name": this_ctr["name"],
                "_id": this_ctr[i_col]
                if not this_ctr[i_col] == "module"
                else this_ctr["component"],
                "collection": i_col if not this_ctr[i_col] == "module" else "component",
            }
        )
    ### basic info
    info = setTestRunInfo(this_tr, cmps)

    results = {"runId": i_tr_oid, "info": info, "output": outputs}

    return results


##############################################
# Set QC Result Information & Data
# i_old: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_old: ObjectId of this testRun document
def setQCResults(i_oid, i_col, i_tr_oid, regeneratePlots, expandPlots):
    if not i_tr_oid:
        return {}, {}

    try:
        query = {"_id": ObjectId(i_tr_oid)}
    except Exception as e:
        logger.warning( str(e) )
        return {}, {}

    this_tr = localdb.QC.result.find_one(query)
    if this_tr == None:
        return {}, {}
    query = {"_id": ObjectId( this_tr["component"] ) }

    #logger.info( 'this_tr = {}'.format( this_tr ) )
    this_cp = localdb.component.find_one(query)

    raw = None
    if 'raw_id' in this_tr:
        raw = localdb.QC.testRAW.find_one( {'_id':ObjectId( this_tr['raw_id'] ) } )
    else:
        this_tr['raw_id'] = None

    passed = None
    try:
        passed = this_tr['passed']
    except:
        pass


    # cache testRun
    os.makedirs( CACHE_DIR, exist_ok=True )
    ana_out = '{}/{}.json'.format( CACHE_DIR, i_tr_oid )

    tr_ser = copy.deepcopy( this_tr )
    for key in [ '_id', 'address', 'component', 'raw_id', 'sys', 'user', 'gridfs_attachments' ]:
        if key in tr_ser:
            tr_ser.pop(key)

    #logger.info( 'tr_ser = ' + pprint.pformat( tr_ser ) )

    with open( ana_out, 'w' ) as f:
        f.write( json.dumps( [ tr_ser ], indent=4 ) )

    logger.info( f'dumped TestRun: {ana_out}' )


    # cache attachment files

    attachments = {}

    #logger.info( 'gridfs_attachments = ' + pprint.pformat( this_tr.get("gridfs_attachments") ) )

    if this_tr.get("gridfs_attachments") != None:

        for filename, oid in this_tr.get("gridfs_attachments").items():
            os.makedirs( THUMBNAIL_DIR, exist_ok=True )

            ext = filename.split('.')[-1]

            cache_file = '{}/{}.{}'.format( THUMBNAIL_DIR, str(oid), ext )

            if os.path.exists( cache_file ):
                pass
            else:
                try:
                    data = fs.get( oid ).read()

                    with open( cache_file, "wb" ) as f:
                        f.write( data )
                except Exception as e:
                    logger.warning( str(e) )
                    logger.warning( f'Failed in dumping attachment to {cache_file}' )

            attachments.update( { cache_file[ cache_file.find('static'): ] : [filename, ext] } )

            logger.info( f'attachment dumped to cache {cache_file}' )

    # cache visual inspection images
    vi_doc = {}

    if this_tr.get("testType") == "VISUAL_INSPECTION":

        raw_id = this_tr.get('raw_id')

        try:
            raw = localdb.QC.testRAW.find_one( { '_id': raw_id } ).get('raw')[0]
            metadata = raw.get('results').get('metadata')

            front_image = metadata.get('front_image')
            back_image  = metadata.get('back_image')

            front_defect_images = metadata.get('front_defect_images')
            back_defect_images  = metadata.get('back_defect_images')

            for name, image_id in { 'front' : front_image, 'back' : back_image }.items():

                cache_file, ext = dump_image( image_id )

                vi_doc.update( { cache_file[ cache_file.find('static'): ] : [name+"_image", ext] } )


            if front_defect_images:
                for tile, image_id in front_defect_images.items():

                    cache_file, ext = dump_image( image_id )

                    defects = metadata.get('front_defects').get(tile)
                    comment = metadata.get('front_comments').get(tile)

                    vi_doc.update( { cache_file[ cache_file.find('static'): ] : ["front_tile"+tile, ext, defects, comment] } )


            if back_defect_images:
                for tile, image_id in back_defect_images.items():

                    cache_file, ext = dump_image( image_id )

                    defects = metadata.get('back_defects').get(tile)
                    comment = metadata.get('back_comments').get(tile)

                    vi_doc.update( { cache_file[ cache_file.find('static'): ] : ["back_tile"+tile, ext, defects, comment] } )
                    
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( 'Failed in acquiring RAW data for visula inspection' )


    results = {
        "testType"      : this_tr["testType"],
        "currentStage"  : this_tr["currentStage"] if 'currentStage' in this_tr else this_tr["stage"],
        "component"     : this_tr["component"],
        "datetime"      : get_localtime_str( this_tr['sys']['mts'] ),
        "user"          : this_tr["user"] if 'user' in this_tr else None,
        "institute"     : this_tr["address"],
        "runId"         : i_tr_oid,
        "rawId"         : this_tr['raw_id'],
        "id"            : str( this_tr['_id'] ),
        "attachments"   : attachments,
        "vi_doc"        : vi_doc,
        "passed"        : passed,
        "prodDB_record" : json2html.convert( this_tr["prodDB_record"] ) if 'prodDB_record' in this_tr else {},
        "raw_record"    : json2html.convert( raw ) if raw != None else {},
        "result_json"   : ana_out,
    }

    #logger.info( 'this_cp = {}'.format( this_cp ) )
    #logger.info( 'results = {}'.format( results ) )

    if this_tr["results"] == {}:
        return results, tr_ser

    ## For electrical tests
    # checkouter = ScanCheckoutManager( this_cp["name"], this_tr["currentStage"] if 'currentStage' in this_tr else this_tr["stage"] )

    results.update({
        "results": this_tr["results"]
    })

    return results, tr_ser



def dump_image( image_id ):

    os.makedirs( THUMBNAIL_DIR, exist_ok=True )
    cache_file = '{}/{}'.format( THUMBNAIL_DIR, image_id )

    if os.path.exists( cache_file ):
        pass
    else:

        try:
            data = fs.get( ObjectId(image_id) ).read()
            with open( cache_file, "wb" ) as f:
                f.write( data )
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( f'Failed in dumping attachment to {cache_file}' )

    try:
        ext = imghdr.what( cache_file )
    except:
        ext = None

    return cache_file, ext



##############################################
# Set Module Property List
# i_oid: ObjectId of this component/chip document
# i_col: collection name (component or chip)
def setModulePropIndex(i_oid, i_col):
    docs = []
    query = { "component": i_oid, "dbVersion":dbv }
    entries = localdb.QC.module.prop.find(query)
    oids = []

    for entry in entries:
        oids.append(str(entry["_id"]))

    for oid in oids:
        query = {"_id": ObjectId(oid)}
        this_tr = localdb.QC.module.prop.find_one(query)
        this_status = localdb.QC.module.status.find_one({"component":this_tr["component"]})
        try:
            QC_result = (oid == this_status["QC_results"][this_tr["stage"]][this_tr["testType"]])
        except:
            QC_result = False

        ### result data of non-electrical test
#            docs[this_tr["testType"]] = {}
#            docs[this_tr["testType"]]["component_id"] = str(this_tr["component"])
#            docs[this_tr["testType"]]["_id"] = str(this_tr["_id"])
#            docs[this_tr["testType"]]["testType"] = this_tr["testType"]
#            docs[this_tr["testType"]]["user"] = this_tr["user"]
#            docs[this_tr["testType"]]["institute"] = this_tr["address"]
#            docs[this_tr["testType"]]["datetime"] = setTime(this_tr["sys"]["mts"], session.get("timezone", str(get_localzone())))
#            docs[this_tr["testType"]]["currentStage"] = this_tr.get("currentStage", "...")
#            docs[this_tr["testType"]]["QC_result"] = QC_result

        docs.append(
            {
                "component_id": str(this_tr["component"]),
                "_id": str(this_tr["_id"]),
                "testType": this_tr["testType"],
                "user": this_tr["user"],
                "institute": this_tr["address"],
                "datetime": setTime(
                            this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
                "currentStage": this_tr.get("currentStage", "N/A"),
                "QC_result": QC_result
            }
        )
        docs = sorted(docs, key = lambda x: (x["datetime"]), reverse=True)

    return docs


##############################################
# Set Module Property
# i_old: ObjectId of this component/chip document
# i_col: collection name (component or chip)
# i_tr_old: ObjectId of this testRun document
def setModuleProp(i_oid, i_col, i_tr_oid):
    if not i_tr_oid:
        return {}

    query = {"_id": ObjectId(i_tr_oid)}
    this_tr = localdb.QC.module.prop.find_one(query)
    if this_tr == None:
        return {}
    query = {"_id": ObjectId(this_tr["component"])}
    this_cp = localdb.component.find_one(query)

    localdbtools_db = client.localdbtools
    module_config = localdbtools_db["QC.module.types"].find_one()
    triplet_list = {}
    triplet_list["STAVE"] = []
    triplet_list["RING"] = []
    for i in range(len(module_config["types"])):
        if "TRIPLET" in module_config["types"][i]["code"]:
            if "STAVE" in module_config["types"][i]["code"]:
                triplet_list["STAVE"].append(module_config["types"][i]["subprojects"][0]["code"] + module_config["types"][i]["snComponentIdentifier"])
            if "RING" in module_config["types"][i]["code"]:
                triplet_list["RING"].append(module_config["types"][i]["subprojects"][0]["code"] + module_config["types"][i]["snComponentIdentifier"])

    results = {}
    results =  {
      "testType"     : this_tr["testType"],
      "currentStage" : this_tr["currentStage"],
      "component"    : this_tr["component"],
      "datetime"     : setTime( this_tr["sys"]["mts"], session.get("timezone", str(get_localzone()))),
      "user"         : this_tr["user"],
      "institute"    : this_tr["address"],
      "runId"        : i_tr_oid,
      "triplet"      : triplet_list
    }

    results.update({
      "results": this_tr["results"]
    })

    return results

########################
# Set Result Information
# i_tr: this testRun document
# i_cmps: component list of this testRun
def setTestRunInfo(i_tr, i_cmps):
    info = {
        "runNumber": None,
        "testType": None,
        "stage": None,
        "component": i_cmps,
        "startTime": None,
        "finishTime": None,
        "user": None,
        "site": None,
        "targetCharge": None,
        "targetTot": None
    }
    for key in i_tr:
        if (
            "Cfg" in key
            or key == "_id"
            or key == "sys"
            or key == "chipType"
            or key == "dbVersion"
            or key == "timestamp"
        ):
            continue
        elif key == "startTime" or key == "finishTime":
            info.update(
                {key: get_localtime_str( i_tr[key] ) }
            )
        elif key == "user_id" and not i_tr[key] == "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_user = localdb.user.find_one(query)
            info.update({"user": this_user["userName"]})
        elif key == "address" and not i_tr[key] == "...":
            query = {"_id": ObjectId(i_tr[key])}
            this_site = localdb.institution.find_one(query)
            info.update({"site": this_site["institution"]})
        elif type(i_tr[key]) == type([]):
            value = ""
            for i in i_tr[key]:
                value += "{}<br>".format(i)
            info.update({key: value})
        elif type(i_tr[key]) == type({}):
            value = ""
            for i in i_tr[key]:
                value += "{0}: {1}<br>".format(i, i_tr[key][i])
            info.update({key: value})
        else:
            info.update({key: i_tr[key]})

    return info


#################
# Set Result Data
# i_docs: this testRun/componentTestRun document
# i_files: result data file list
def setOutput(i_docs, i_files):
    for key in i_docs:
        if "Cfg" in key and not i_docs[key] == "...":
            if not key in i_files:
                i_files.update({key: []})
            query = {"_id": ObjectId(i_docs[key])}
            this = localdb.config.find_one(query)
            i_files[key].append(
                {
                    "type": key,
                    "code": this["data_id"],
                    "serialNumber": i_docs.get("serialNumber", ""),
                    "format": "json",
                }
            )

    # attachments
    for this in i_docs.get("attachments", []):
        if not this["title"] in i_files:
            i_files.update({this["title"]: []})
        i_files[this["title"]].append(
            {
                "type": this["title"],
                "code": this["code"],
                "serialNumber": i_docs.get("serialNumber", ""),
                "format": this["contentType"],
            }
        )

    # chip config
    for cfg in ['feCfg.before', 'feCfg.after']:
        if not cfg in i_files:
            i_files[cfg] = []

        doc = { 'serialNumber' : i_docs.get('serialNumber') }

        if 'before' in cfg:
            for var in ['config_id', 'config_revision_prev' ]:
                if var in i_docs:
                    doc[var.replace('_prev','')] = i_docs.get(var)

        if 'after' in cfg:
            for var in ['config_id', 'config_revision_current' ]:
                if var in i_docs:
                    doc[var.replace('_current','')] = i_docs.get(var)

        if 'config_id' in doc:
            i_files[cfg].append( doc )

    return i_files


##############
# Set DCS Plot
def setDCS(i_tr_oid):
    docs = {}
    if not i_tr_oid:
        return {}

    try:
        query = {"_id": ObjectId(i_tr_oid)}
    except Exception as e:
        logger.warning( str(e) )
        return {}

    this_tr = localdb.testRun.find_one(query)
    if this_tr == None:
        return docs

    docs.update({"dcs_data_exist": False, "environment": "..."})
    if not i_tr_oid:
        return docs
    if this_tr.get("environment", False):
        docs.update({"dcs_data_exist": True})
    return docs


###########
# Set Score
def setCount():  # TODO ./dev/component.py
    docs = {}
    return docs


##############
# Set Comments
# i_oid: ObjectId of this component/chip document
# i_tr_oid: ObjectId of this testRun document
# i_info: component information
def setComments(i_oid, i_tr_oid, i_info):
    docs = []
    query = []
    if i_tr_oid:
        query.append({ 'runId': i_tr_oid })
    if i_oid:
        query.append({ "component": i_oid })
        for child in i_info.get("children", []):
            query.append({"component": child["_id"]})
        for parent in i_info.get("parents", []):
            query.append({"component": parent["_id"]})

    if not query:
        return docs

    entries = localdb.comments.find({"$or": query})
    for entry in entries:
        entry['datetime'] = datetime.fromisoformat( str(entry['datetime']) ).strftime('%Y-%m-%d %H:%M:%S')
        docs.append(entry)
        componentId = entry['component']
        entry['componentInfo'] = localdb.component.find_one( { '_id':ObjectId( componentId ) } )

    return docs

### Others ###

def getResultIds(mname, stage):

    run_ids = []
    this_cp = localdb.component.find_one( { "name":mname } )
    if this_cp != None:
        results = localdb.QC.result.find({"$and": [{ "component": str(this_cp["_id"]) }, {"currentStage":stage} ]},{ "_id":1 } )
        run_ids = [ str(result["_id"]) for result in results ]
    return run_ids

def getPropIds(mname, stage):

    run_ids = []
    this_cp = localdb.component.find_one( {"name":mname} )
    if this_cp != None:
        results = localdb.QC.module.prop.find({"$and": [{ "component": str(this_cp["_id"]) }, {"currentStage": "MODULEWIREBONDING"} ]},{ "_id":1 } )
        run_ids = [ str(result["_id"]) for result in results ]
    return run_ids

def checkQCTestItems(c_oid, stage, test_list):
    with open("{}/json-lists/supported_test.json".format(VIEWER_DIR)) as f:
        supported_test = json.load(f)
    for testType in test_list:
        test_doc = localdb.QC.result.find_one({"$and":[{"component": c_oid},{"currentStage":stage},{"testType":testType}]})
        if testType in supported_test["test"]:
            if not test_doc: return False

    return True

def createQCtestSummary( run_ids ):
    list_ = []

    nrun_entries = len(run_ids)
    if not nrun_entries == 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] == None or run_ids[i] == "":
                list_.append({ "run_data": {} })
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.result.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one( {"_id": ObjectId(this_run["component"]) } )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"], session.get("timezone", str(get_localzone()))
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["currentStage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append( { "run_data": run_data } )

    #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_

def createPropSummary( run_ids ):
    list_ = []
    nrun_entries = len(run_ids)
    if not nrun_entries == 0:
        # for i in run_num_list:
        for i in range(nrun_entries):
            if run_ids[i] == None or run_ids[i] == "":
                list_.append({ "run_data": {} })
                continue

            query = {"_id": ObjectId(run_ids[i])}
            this_run = localdb.QC.module.prop.find_one(query)

            ### user
            user_name = this_run["user"]

            ### site
            site_name = this_run["address"]

            ### component
            this_cp = localdb.component.find_one( {"_id": ObjectId(this_run["component"]) } )
            component = this_cp["name"]

            ### tags
            query = {}
            testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
            tag_candidate = []
            for testRun_tag in testRun_tag_candidates:
                tag_candidate.append(testRun_tag)
            ### put tags
            query = {"runId": run_ids[i]}
            testRun_tags = userdb.viewer.tag.docs.find(query)
            tag = []
            for testRun_tag in testRun_tags:
                tag.append(testRun_tag)

            run_data = {
                "_id": run_ids[i],
                "datetime": setTime(
                    this_run["sys"]["cts"], session.get("timezone", str(get_localzone()))
                ),
                "testType": this_run["testType"],
                "runNumber": 0,
                "stage": this_run["currentStage"],
                "component": component,
                "user": user_name,
                "site": site_name,
                "testRun_tag_candidate": tag_candidate,
                "testRun_tag": tag,
            }

            list_.append( { "run_data": run_data } )

    #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
    return list_

def getNextStage(module_id, stage):
    qc_status = localdb.QC.module.status.find_one({"component" : str(module_id)})
    stage_list = [item for item in qc_status["QC_results"]]
    nextStage = stage_list[stage_list.index(stage)+1]
    return nextStage

def skipNextStage(module_id, stage):
    qc_status = localdb.QC.module.status.find_one({"component" : str(module_id)})
    stage_list = [item for item in qc_status["QC_results"]]

    try:
        nextStages = [stage_list[stage_list.index(stage)+1], stage_list[stage_list.index(stage)+2]]
    except:
        nextStages = []
    return nextStages

####################################
# results for Sensor_IV & SLDO_VI
def SensorIV_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "Sensor_IV")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')

    return pngImageB64String

def SensorIV_units(sensorIVresult):
    units = {}
    try:
        units["V_step"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Step_duration"] = sensorIVresult["results"]["unit"]["Time"]
        units["N_measurements_per_step"] = ""
        units["I_Compliance"] = sensorIVresult["results"]["unit"]["Current"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get("Temperature", "no unit")
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as E:
        logger.warning(E)
        return units

def SLDOVI_graph(sensorIVresult):
    graph = layout_ModuleQC(sensorIVresult, "SLDO_VI")

    pngImage = io.BytesIO()
    FigureCanvas(graph).print_png(pngImage)

    # Encode PNG image to base64 string
    pngImageB64String = "data:image/png;base64,"
    pngImageB64String += base64.b64encode(pngImage.getvalue()).decode('utf8')

    return pngImageB64String

def SLDOVI_units(sensorIVresult):
    units = {}
    try:
        units["I_step"] = sensorIVresult["results"]["unit"]["Current"]
        units["N_iteration"] = ""
        units["N_measurements_per_step"] = ""
        units["Voltage_limit"] = sensorIVresult["results"]["unit"]["Voltage"]
        units["Temperature"] = sensorIVresult["results"]["unit"].get("Temperature", "no unit")
        units["Humidity"] = sensorIVresult["results"]["unit"].get("Humidity", "no unit")
        units["comment"] = ""
        return units
    except Exception as E:
        logger.warning(E)
        return units






def set_light_plots( component_id, component_type, test_run_id, refresh_cache = False ):

    global plotting_mutex_lock
    global plotting_mutex_lock_timestamp


    start_time = datetime.now()

    filesystem = gridfs.GridFS( localdb )

    plots = {}

    cpt = localdb.component.find_one( {'_id': ObjectId( component_id ) } )

    serial_number = cpt.get('serialNumber')

    test_run = localdb.testRun.find_one( {'_id' : ObjectId( test_run_id ) } )

    if not test_run:
        return plots

    logger.info( f'set_light_plots(): component_id = { component_id }, component_type = { component_type }, test_run_id = { test_run_id }' )

    ctrs = { ctr.get('geomId') : ctr for ctr in localdb.componentTestRun.find( {'testRun': test_run_id, 'componentType':'front-end_chip' } ) }

    plot_data = []

    serialNumbers = []

    for geomId in range(0, 4):
        ctr = ctrs.get(geomId)

        if not ctr:
            continue

        serialNumbers += [ ctr.get('name') ]

        for index, att in enumerate( ctr.get('attachments') ):

            if att.get('title') in plot_data:
                continue

            if not att.get('contentType') == 'pickle':
                continue

            if len( [ d for d in plot_data if  d.get('title') == att.get('title') ] ) == 0:
                plot_data.append( { 'index' : index, 'title' : att.get('title') } )

    for p in plot_data:
        logger.info( f'plot-{p.get("index")} : {p.get("title")}' )


    for plot in plot_data:

        plot['data'] = {}

        tmpfile = f"{THUMBNAIL_DIR}/{test_run_id}_{plot.get('title')}.png"

        if os.path.isfile( tmpfile ) and refresh_cache is False:
            plots[ plot.get('title') ] = tmpfile.replace( f'{VIEWER_DIR}/', '' )
            continue
        elif os.path.isfile( tmpfile ):
            os.remove( tmpfile )

        if ( datetime.now() - plotting_mutex_lock_timestamp ).seconds < 60:
            while plotting_mutex_lock:
                logger.info('waiting for other thread to unlock...')
                time.sleep(0.1)

        plotting_mutex_lock_timestamp = datetime.now()
        plotting_mutex_lock = True


        for geomId in range(0, 4):

            if not ctrs.get(geomId):
                continue

            attachments = ctrs.get(geomId).get('attachments')

            try:
                if plot.get('index') < len( ctrs.get(geomId).get('attachments') ):
                    plot_doc = None
                    for att in ctrs.get(geomId).get('attachments'):
                        if att.get('title') == plot.get('title'):
                            plot_doc = att
                            break
                else:
                    plot['data'].update( { geomId : np.zeros( (400, 384) ) } )
                    continue
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'geomId {geomId}: attachment data not found' )
                plot['data'].update( { geomId : np.zeros( (400, 384) ) } )
                continue

            if plot_doc is None:
                logger.warning( f'no good plot_doc was found for plot title {plot.get("title")} for geomId {geomId}. Skipping this FE' )
                continue


            if plot_doc.get('title') != plot.get('title'):
                logger.warning( f'plot_doc title {plot_doc.get("title")} did not match with plot title {plot.get("title")} for geomId {geomId}. Skipping this FE' )
                continue

            pickle_data = filesystem.get( ObjectId( plot_doc.get('code') ) ).read()
            data = pickle.load(io.BytesIO(pickle_data))
            #logger.info( f'{plot.get("title")}, {data.get("Type")}, {geomId}' )

            plot['type'] = data.get("Type")

            if len( plot['data'] ) == 0:
                for var in ['x', 'y', 'z']:
                    if var in data:
                        plot[var] = data.get( var )

            if plot.get('type') == 'Histo2d':
                plot['data'].update( { geomId : np.array( data.get("Data") ) } )
            else:
                plot['data'].update( { geomId :  data } )


        if plot.get('type') == 'Histo2d' and plot.get('title') != "InjVcalDiff":

            """
            # debug rotation
            for ig in range(0, geomId+1):
                for iy in range(0,4):
                    for ix in range(0,100):
                        for k in range(0, ig+1):
                            plot.get("data")[ig][ix, k*10+iy] = -100
            """


            if any( XXYY in serial_number for XXYY in ["PIMS", "PIM0", "PIM5", "PIR6", "PIR7", "PIR8", "PIX3", "PIX4", "PIX5"] ):

                plt.figure( figsize=(18, 6) )
                ax = plt.gca();
                ax.tick_params( axis='both', which = 'major', labelsize = 14 )


                module = np.concatenate( (plot.get("data")[0].T, plot.get("data")[1].T, plot.get("data")[2].T), axis = 1 )

                ma = np.ma.MaskedArray( module, module <= 0 )
                min_nonzero_value = np.ma.min( ma )
                max_nonzero_value = np.ma.max( ma )
                min_max_ratio = max_nonzero_value / (min_nonzero_value+1.e-19)

                set_log_scale = ( min_nonzero_value > 0 and min_max_ratio > 100 )


                if set_log_scale:
                    module += min_nonzero_value*1.e-2
                    plt.imshow( module, origin='lower', cmap='turbo', norm = LogNorm(min_nonzero_value*0.5, max_nonzero_value*2 ) )
                else:
                    plt.imshow( module, origin='lower', cmap='turbo' )

                plt.title( 'Triplet Module: ' + serial_number + ' ' + plot.get('title'), fontsize = 18 )
                ax.text(  200, -20, 'FE1: '+delim_SN(serialNumbers[0]), horizontalalignment='center', verticalalignment='top', size=10, color='blue' )
                ax.text(  600, -20, 'FE2: '+delim_SN(serialNumbers[1]), horizontalalignment='center', verticalalignment='top', size=10, color='blue' )
                ax.text( 1000, -20, 'FE3: '+delim_SN(serialNumbers[2]), horizontalalignment='center', verticalalignment='top', size=10, color='blue' )

                if module.shape == (384, 1200):
                    ax.set_xticks( np.arange( 0, 1200+1, 400) )
                    ax.set_yticks( np.arange( 0, 384+1, 384) )
                    ax.set_xticks( np.arange( 0, 1200+1, 16), minor=True )
                    ax.set_yticks( np.arange( 0, 384+1, 16), minor=True )

                    ax.grid(which='major', color='black', linestyle='solid', linewidth=0.5)

                plt.xlabel( "Column", fontsize=18 )
                plt.ylabel( "Row",    fontsize=18 )
                cb = plt.colorbar( label = data.get("z").get("AxisTitle") )
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)

            else:
                up   = np.zeros( (400, 768) )
                try:
                    up   = np.concatenate( ( np.flip( np.flip( plot.get("data")[0], axis=0), axis=1), plot.get("data")[3] ), axis=1 )
                except:
                    try:
                        up   = np.concatenate( ( np.flip( np.flip( plot.get("data")[0], axis=0), axis=1), np.zeros( (400, 384) ) ), axis=1 )
                    except:
                        try:
                            up   = np.concatenate( np.zeros( (400, 384) ), plot.get("data")[3] , axis=1 )
                        except:
                            pass

                down   = np.zeros( (400, 768) )
                try:
                    down   = np.concatenate( ( np.flip( np.flip( plot.get("data")[1], axis=0), axis=1), plot.get("data")[2] ), axis=1 )
                except:
                    try:
                        down   = np.concatenate( ( np.flip( np.flip( plot.get("data")[1], axis=0), axis=1), np.zeros( (400, 384) ) ), axis=1 )
                    except:
                        try:
                            down   = np.concatenate( np.zeros( (400, 384) ), plot.get("data")[1] , axis=1 )
                        except:
                            pass


                try:
                    module = np.concatenate( (down, up), axis = 0 )
                except Exception as e:
                    plotting_mutex_lock = False
                    logger.warning( f"Plotting for {plot.get('title')} failed: str{e}" )
                    continue

                ma = np.ma.MaskedArray( module, module <= 0 )
                min_nonzero_value = np.ma.min( ma )
                max_nonzero_value = np.ma.max( ma )
                min_max_ratio = max_nonzero_value / (min_nonzero_value)

                set_log_scale = ( min_nonzero_value > 0 and min_max_ratio > 100 )

                median = np.ma.median( ma )

                plt.figure( figsize=(12, 9) )

                if set_log_scale:
                    module += min_nonzero_value*1.e-2
                    plt.imshow( module, origin='lower', cmap='turbo', extent = [-384, 384, -400, 400], norm = LogNorm(min_nonzero_value*0.5, max_nonzero_value*2 ) )
                else:
                    plt.imshow( module, origin='lower', cmap='turbo', extent = [-384, 384, -400, 400] )

                ax = plt.gca();
                ax.tick_params( axis='both', which = 'major', labelsize = 10 )

                #assert( module.shape == (800, 768) )
                ax.set_yticks( np.arange(-400, 400+1, 400) )
                ax.set_xticks( np.arange(-384, 384+1, 384) )
                ax.set_yticks( np.arange(-400, 400+1, 16), minor=True )
                ax.set_xticks( np.arange(-384, 384+1, 16), minor=True )

                ax.grid(which='major', color='black', linestyle='solid', linewidth=0.5)

                ax.text( -415,  200, 'FE1: '+delim_SN(serialNumbers[0]), horizontalalignment='center', verticalalignment='center', size=12, color='blue', rotation = -90 )
                ax.text( -415, -200, 'FE2: '+delim_SN(serialNumbers[1]), horizontalalignment='center', verticalalignment='center', size=12, color='blue', rotation = -90 )
                ax.text(  415, -200, 'FE3: '+delim_SN(serialNumbers[2]), horizontalalignment='center', verticalalignment='center', size=12, color='blue', rotation =  90 )
                ax.text(  415,  200, 'FE4: '+delim_SN(serialNumbers[3]), horizontalalignment='center', verticalalignment='center', size=12, color='blue', rotation =  90 )

                plt.title( 'Quad Module: ' + serial_number + ' ' + plot.get('title'), fontsize = 18 )
                plt.xlabel( "Row", fontsize=18 )
                plt.ylabel( "Column", fontsize=18 )
                cb = plt.colorbar( label = data.get("z").get("AxisTitle") )
                for t in cb.ax.get_yticklabels():
                    t.set_fontsize(12)


        elif plot.get('type') == 'Histo2d' and plot.get('title') == "InjVcalDiff":

            try:
                module = np.sum( [ d for k,d in plot.get('data').items() ], axis=0 )
            except Exception as e:
                plotting_mutex_lock = False
                logger.warning( f"Plotting for {plot.get('title')} failed: str{e}" )
                continue

            ma = np.ma.MaskedArray( module, module <= 0 )
            min_nonzero_value = np.ma.min( ma )
            max_nonzero_value = np.ma.max( ma )
            min_max_ratio = max_nonzero_value / (min_nonzero_value+1.e-19)

            set_log_scale = ( min_nonzero_value > 0 and min_max_ratio > 100 )

            plt.figure( figsize=(12, 9) )
            if set_log_scale:
                module += min_nonzero_value*1.e-2
                plt.imshow( module.T, origin='lower', cmap='turbo', norm=LogNorm(min_nonzero_value*0.5, max_nonzero_value*2 ) )
            else:
                plt.imshow( module.T, origin='lower', cmap='turbo' )

            ax = plt.gca();
            ax.tick_params( axis='both', which = 'major', labelsize = 10 )

            plt.title( 'Quad Module: ' + serial_number + ' ' + plot.get('title'), fontsize = 18 )
            plt.xlabel( xlabel = data.get("x").get("AxisTitle"), fontsize=18 )
            plt.ylabel( ylabel = data.get("y").get("AxisTitle"), fontsize=18 )
            cb = plt.colorbar( label = data.get("z").get("AxisTitle") )
            for t in cb.ax.get_yticklabels():
                t.set_fontsize(12)

        elif plot.get('type') == 'Histo1d':
            plt.figure( figsize=(12, 9) )
            plt.title( 'Module: ' + serial_number + ' ' + plot.get('title'), fontsize = 18 )

            for geomId, d in plot.get("data").items():
                try:
                    bins = np.linspace( d.get("x").get("Low"), d.get("x").get("High"), d.get("x").get("Bins") )
                    binRange = ( d.get("x").get("Low"), d.get("x").get("High") )

                    plt.hist( bins, len(bins), weights = d.get("Data"), histtype='step', label = f'FE{geomId+1}: '+delim_SN(serialNumbers[geomId]) )
                    plt.legend(loc='upper right', fontsize=10)
                except Exception as e:
                    logger.info( f'{plot.get("title")}, geomId-{geomId}: skipped to render (likely the chip is disabled)' )
                    plotting_mutex_lock = False
                    continue

            try:
                plt.xlabel( d.get("x").get("AxisTitle"), fontsize=18 )
                plt.ylabel( d.get("y").get("AxisTitle"), fontsize=18 )
            except:
                pass

        plt.tight_layout()

        if not os.path.isfile( tmpfile ):
            plt.savefig( tmpfile, dpi=100 )
            plt.clf()
            plt.close()

        plots[ plot.get('title') ] = tmpfile.replace( f'{VIEWER_DIR}/', '' )

        plotting_mutex_lock = False

    end_time = datetime.now()
    process_time = ( end_time - start_time ).total_seconds()
    logger.info( 'set_light_plots: prcess time = ' + pprint.pformat( process_time ) )

    return plots

