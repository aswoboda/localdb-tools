from logging import log
from functions.imports import *
from flask_httpauth import HTTPBasicAuth, HTTPDigestAuth
from flask_mail import Mail, Message
import subprocess

auth = HTTPBasicAuth()

user_api = Blueprint("user_api", __name__)
mail = Mail()


@user_api.route("/login", methods=["POST"])
def login():
    pre_url = request.headers.get("Referer")
    pre_url_lastpass = pre_url.split("/")

    query = {"username": request.form["username"]}
    user = userdb.viewer.user.find_one(query)
    query = {}
    if user == None:
        txt = "This user does not exist"
        if not pre_url_lastpass[-1] == "login":
            session["pre_url"] = pre_url
        return render_template("error.html", txt=txt, timezones=pytz.all_timezones)

    else:
        if not user["password"] == "":
            if (
                hashlib.md5(request.form["password"].encode("utf-8")).hexdigest()
                == user["password"]
            ):

                session["logged_in"] = True
                session["username"] = user["username"]
                session["fullname"] = user["name"]
                session["institution"] = user["institution"]
                session["user_id"] = str(user["_id"])
                url_list = ["register_password","register_password_from_admin","login","signup"]
                if pre_url_lastpass[-1] in url_list:
                    return redirect(session["pre_url"])
                else:
                    return redirect(pre_url)
            else:
                txt = "This password is not correct"
                if not pre_url_lastpass[-1] == "login":
                    session["pre_url"] = pre_url
                return render_template(
                    "error.html", txt=txt, timezones=pytz.all_timezones
                )
        else:
            txt = "Password is not registered yet."
            if not pre_url_lastpass[-1] == "login":
                session["pre_url"] = pre_url
            return render_template("error.html", txt=txt, timezones=pytz.all_timezones)


@user_api.route("/logout", methods=["GET", "POST"])
def logout():
    pre_url = request.headers.get("Referer")
    session["logged_in"] = False
    session["username"] = ""
    session["institution"] = ""
    session["user_id"] = ""

    return redirect(pre_url)


@user_api.route("/register_password", methods=["GET", "POST"])
def register_password():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("registerpass", None)
    if session.get("registerpass", None):
        if userdb.viewer.user.find({"username": userinfo[0]}).count() == 0:
            text = "This username does not exist"
            stage = "input"
            return render_template(
                "register_password.html",
                userInfo=userinfo,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        else:
            query = {"username": userinfo[0]}
            userdata = userdb.viewer.user.find_one(query)
            if not userdata["Email"] == userinfo[1]:
                text = "This Email is not correct"
                stage = "input"
                return render_template(
                    "register_password.html",
                    userInfo=userinfo,
                    nametext2=text,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            else:
                if stage == "input":
                    return render_template(
                        "register_password.html",
                        userInfo=userinfo,
                        stage=stage,
                        timezones=pytz.all_timezones,
                    )
                if stage == "confirm":
                    if args.fmail:
                        return_num = updatePasswordAndMail(userinfo)
                        if return_num == 0: return "Mail sending failed. You cannot use the function."
                    return render_template(
                        "register_password.html",
                        userInfo=userinfo,
                        stage=stage,
                        timezones=pytz.all_timezones,
                    )
                else:
                    if (
                        not userdata["password"]
                        == hashlib.md5(userinfo[2].encode("utf-8")).hexdigest()
                    ):
                        text = "This current password is not correct"
                        stage = "confirm"
                        return render_template(
                            "register_password.html",
                            userInfo=userinfo,
                            nametext3=text,
                            stage=stage,
                            timezones=pytz.all_timezones,
                        )
                    if not userinfo[3] == userinfo[4]:
                        text = "Please make sure your password match"
                        stage = "confirm"
                        return render_template(
                            "register_password.html",
                            userInfo=userinfo,
                            nametext4=text,
                            stage=stage,
                            timezones=pytz.all_timezones,
                        )
                    else:
                        changeUserPassword(userinfo[0],userinfo[3])
                        session.pop("registerpass")
                        return render_template(
                            "register_password.html",
                            userInfo=userinfo,
                            stage=stage,
                            timezones=pytz.all_timezones,
                        )

    userinfo = ["", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["registerpass"] = True
    return render_template(
        "register_password.html",
        userInfo=userinfo,
        stage=stage,
        timezones=pytz.all_timezones,
    )



@user_api.route("/itkdb_authenticate", methods=["GET", "POST"])
def itkdb_authenticate():
    
    global pd_client
    
    prev_url = session['url']
        

    logger.info('itkdb_authenticate()' )

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    stage = request.form.get("stage", False)
    
    if not stage:
        return render_template( "itkdb_authenticate.html" )

    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")
    messages = []

    try:

        flag, client = process_request(code1, code2)

        if client == None:
            messages += [ "Itkdb not authorized. Please input correct codes." ]
            return render_template( "itkdb_authenticate.html", messages = messages )

        else:
            logger.info(f'itkdb_authenticate(): successfully authenticated' )

        pd_client = client

    except:
        return render_template( "itkdb_authenticate.html", messages = messages )
    
    return redirect( prev_url )




@user_api.route("/download_component", methods=["GET", "POST"])
def download_component():
    
    logger.info( 'download_component(): start' )

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    stage = request.form.get("stage", "input").split("|")[0]
    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")
    debug_mode = True if request.form.get("debug_mode", False) == "on" else False

    logger.info( f'download_component(): debug_mode = {debug_mode}' )

    component_id = request.form.get("component_id", None)
    
    skip_attachments = True if 'skipAtt' in request.form.get("stage", "input").split("|") else False
    skip_synched = False if 'noSkipSynched' in request.form.get("stage", "input").split("|") else True
    
    logger.info( 'stage = {}, component_id = {}, skip_attachments = {}, skip_synched = {}'.format( stage, component_id, skip_attachments, skip_synched ) )

    if stage == 'submit':
        
        flag, client = process_request(code1, code2)
        print( flag )
        print( client )

        if flag == 0:
            stage = "input"
            create_message( "ITkPD not authorized. Please input correct codes.", 'download_component', 'ERROR_ITKPD_AUTH' )
            
        elif stage == "submit":
            try:

                thread = threading.Thread( target = download_worker, args = ( component_id, code1, code2, None, skip_attachments, skip_synched, debug_mode ) )
                thread.start()
                time.sleep(3)

            except Exception as e:
                logger.error( '{}'.format(e) )
                stage = "input"
                create_message( "An error occurred. Please contact LocalDB admin.", 'download_component', 'ERROR_GENERAL' )

    table_docs = { 'messages' : format_messages( { 'function':'RecursiveComponentsSynchronizer' } ) }

    #logger.info( "messages to the user: " )
    #logger.info( pprint.pformat( messages ) )
    
    return render_template( "download_component.html", stage=stage, table_docs = table_docs )


@user_api.route("/register_chipid", methods=["GET", "POST"])
def register_chipid():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    component = {}
    module = request.args.get("module", "")
    stage = request.form.get("stage", "input")
    chipid = request.form.getlist("chipid")
    text = ""

    if stage == "input":
        this_cp = localdb.component.find_one({"name":module})
        if not this_cp:
            text = "The downloading process have not been over. Try again."
        else:
            component["module"] = this_cp
            this_cprs = localdb.childParentRelation.find({"parent":str(this_cp["_id"])})
            chips = []
            for this_cpr in this_cprs:
                this_chip = localdb.component.find_one( {"_id":ObjectId(this_cpr["child"]) } )
                chips.append(this_chip)
            component["chips"] = chips
            if this_cp["children"] == 4 and this_cp["chipType"] == "RD53A":
                component["picture"] = "/localdb/static/images/RD53A_quad.png"
            else:
                component["picture"] = ""

    elif stage == "complete":
        this_cp = localdb.component.find_one({"name":module})
        component["module"] = this_cp
        this_cprs = localdb.childParentRelation.find({"parent":str(this_cp["_id"])})
        for i, this_cpr in enumerate(this_cprs):
            localdb.component.update( {"_id":ObjectId(this_cpr["child"]) },{"$set":{"chipId":int(chipid[i])}} )

    return render_template( "register_chipid.html", stage=stage, text=text, module=module, component=component )

@user_api.route("/check_mail", methods=["GET", "POST"])
def check_mail():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("mailsending", None)
    if session.get("mailsending", None):
        if stage == "input":
            return render_template(
                "check_mail.html",
                userInfo=userinfo,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if stage == "request":
            return_num = sendTestMail(userinfo)
            if return_num == 0:
                return "Mail sending failed. Cannot use the mail-sending function on your network environment."

    userinfo = [""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["mailsending"] = True
    return render_template(
        "check_mail.html",
        userInfo=userinfo,
        stage=stage,
        timezones=pytz.all_timezones,
    )

@user_api.route("/set_time", methods=["GET", "POST"])
def set_time():
    session["timezone"] = request.form.get("timezone")

    return redirect(request.headers.get("Referer"))


############
## For Admin

if args.KeyFile:
    keys = readKey(args.KeyFile)
    admin_username = keys["username"]
    admin_password = keys["password"]
else:
    if args.username:
        admin_username = hashlib.md5(args.username.encode("utf-8")).hexdigest()
        admin_password = hashlib.md5(args.password.encode("utf-8")).hexdigest()
    else:
        admin_username = hashlib.md5("username".encode("utf-8")).hexdigest()
        admin_password = hashlib.md5("password".encode("utf-8")).hexdigest()

users = {admin_username: admin_password}


@auth.get_password
def get_pw(username):
    if hashlib.md5(username.encode("utf-8")).hexdigest() in users:
        return users.get(hashlib.md5(username.encode("utf-8")).hexdigest())
    return None

@auth.hash_password
def hash_pw(password):
    return hashlib.md5(password.encode("utf-8")).hexdigest()

@user_api.route("/admin_toppage", methods=["GET"])
#@auth.login_required
def show_admin_toppage():
    initPage()

    user_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    if os.path.isdir(user_dir):
        shutil.rmtree(user_dir)

    session.pop("signup", None)
    table_docs = {"page": None}
    mail = args.fmail

    return render_template( "admin_toppage.html", mail=mail, table_docs=table_docs, timezones=pytz.all_timezones )

@user_api.route("/signup", methods=["GET", "POST"])
#@auth.login_required
def signup():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    Address = localdb.pd.institution.find({})
    if userinfo == []:
        session.pop("signup", None)

    if session.get("signup", None):
        username = userinfo[0].split()
        if not userinfo[5] == userinfo[6]:
            text = "Please make sure your Email match"
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                passtext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if userdb.viewer.user.find({"username": userinfo[0]}).count() == 1:
            text = "This username is already in use, please select an alternative."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )

        username_hash = hashlib.md5(userinfo[0].encode("utf-8")).hexdigest()
        if username_hash == admin_username:
            text = "This is the admin's username. Please use different one."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )

        if userinfo[3] == "other" and userinfo[4] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "signup.html",
                userInfo=userinfo,
                Address=Address,
                nametext3=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        else:
            if stage == "input":
                return render_template(
                    "signup.html",
                    userInfo=userinfo,
                    Address=Address,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            if stage == "confirm":
                return render_template(
                    "signup.html",
                    userInfo=userinfo,
                    Address=Address,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            else:
                if args.fmail:
                    return_num = sendSignupMail(userinfo)
                    if return_num == 0: return "Mail sending Failed. You cannot use the function."

                insertDBUser(userinfo)

                session.pop("signup")
                return render_template(
                    "signup.html",
                    userInfo=userinfo,
                    Address=Address,
                    stage=stage,
                    mail=args.fmail,
                    timezones=pytz.all_timezones,
                )

    userinfo = ["", "", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url
    session["signup"] = True

    return render_template(
        "signup.html",
        userInfo=userinfo,
        Address=Address,
        stage=stage,
        timezones=pytz.all_timezones,
    )

@user_api.route("/register_password_from_admin", methods=["GET", "POST"])
#@auth.login_required
def register_password_from_admin():
    stage = request.form.get("stage", "input")
    userinfo = request.form.getlist("userinfo")
    if userinfo == []:
        session.pop("registerpass", None)
    if session.get("registerpass", None):
        if stage == "input":
            return render_template(
                "register_password_from_admin.html",
                userInfo=userinfo,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if stage == "request":
            query = {"username": userinfo[0]}
            if userdb.viewer.user.find(query).count() == 0:
                text = "This username does not exist"
                stage = "input"
                return render_template(
                    "register_password_from_admin.html",
                    userInfo=userinfo,
                    nametext1=text,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            else:
                num = 8
                pool = string.ascii_letters
                pool2 = string.digits
                pin_number = "".join([secrets.choice(pool+pool2) for i in range(num)])
                changeUserPassword(userinfo[0],pin_number)
                session.pop("registerpass")
                return render_template(
                    "register_password_from_admin.html",
                    userInfo=userinfo,
                    stage=stage,
                    pin_number=pin_number,
                    timezones=pytz.all_timezones,
                )

    userinfo = ["", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["registerpass"] = True
    return render_template(
        "register_password_from_admin.html",
        userInfo=userinfo,
        stage=stage,
        timezones=pytz.all_timezones,
    )

@user_api.route("/download_pdinfo", methods=["GET", "POST"])
#@auth.login_required
def download_pdinfo():
    stage = request.form.get("stage", "input")
    code1 = request.form.get("code1", " ")
    code2 = request.form.get("code2", " ")
    text = ""

    if stage == "complete":
        flag, client = process_request(code1, code2)
        if flag == 0:
            stage = "input"
            text = "Not authorized. Something is wrong."
        else:
            try:
                download_institution(code1,code2)
            except Exception as e:
                stage = "input"
                text = "The error was occurred in the downloading institutions process.: {}".format( e )
            try:
                download_ModuleType(code1,code2)
            except Exception as e:
                stage = "input"
                text = "The error was occurred in the downloading component types process: {}".format( e )

    return render_template("download_pdinfo.html", stage=stage, text=text)

@user_api.route("/rename_sn", methods=["GET", "POST"])
#@auth.login_required
def rename_sn():
    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    modules = []
    chips = {
        'old': [],
        'oldId': [],
        'new': [],
        'newId': []
        }
    change_test_run = []
    text = ""
    if stage == "input":
        components = localdb.component.find()
        modules = [ item['name'] for item in components if item['componentType'] == 'module']

    if stage == "confirm":
        old_moduleId = str(localdb.component.find_one({'name': moduleinfo[0]})['_id'])
        old_chips = localdb.childParentRelation.find({'parent': old_moduleId})
        old_chips = sorted(old_chips, key=lambda x:x['chipId'])
        for chip in old_chips:
            chip_name = localdb.component.find_one({'_id': ObjectId(chip['child'])})['name']
            chips['old'].append(chip_name)

        new_moduleId = str(localdb.component.find_one({'name': moduleinfo[1]})['_id'])
        new_chips = localdb.childParentRelation.find({'parent': new_moduleId})
        new_chips = sorted(new_chips, key=lambda x:x['chipId'])
        for chip in new_chips:
            chip_name = localdb.component.find_one({'_id': ObjectId(chip['child'])})['name']
            chips['new'].append(chip_name)

        if len(new_chips) != len(old_chips):
            stage = "input"
            text = "The number of chips in the new module and the old one are different..."

        query = {"component": old_moduleId}
        entries = localdb.componentTestRun.find(query)
        for run_entry in entries:
            change_test_run.append(getScanSummary(run_entry["testRun"]))

    if stage == "complete":
        old_module_info = localdb.component.find_one({'name': moduleinfo[0]})
        old_moduleId = str(old_module_info['_id'])
        old_chips = localdb.childParentRelation.find({'parent': old_moduleId})
        old_chips = sorted(old_chips, key=lambda x:x['chipId'])
        for chip in old_chips:
            chip_info = localdb.component.find_one({'_id': ObjectId(chip['child'])})
            chips['old'].append(chip_info['name'])
            chips['oldId'].append(chip['child'])

        new_module_info = localdb.component.find_one({'name': moduleinfo[1]})
        new_moduleId = str(new_module_info['_id'])
        new_chips = localdb.childParentRelation.find({'parent': new_moduleId})
        new_chips = sorted(new_chips, key=lambda x:x['chipId'])
        for chip in new_chips:
            chip_info = localdb.component.find_one({'_id': ObjectId(chip['child'])})
            chips['new'].append(chip_info['name'])
            chips['newId'].append(chip['child'])

        module_testRun = localdb.componentTestRun.find({"name" : old_module_info["name"]})
        for run in module_testRun:
            pre_sn = run.get('history', [])
            pre_sn.append(old_module_info["name"])
            scan_sn = run.get('scanSN', old_module_info['name'])

            localdb.componentTestRun.update({
                "name": old_module_info["name"],
                "testRun": run["testRun"]
                },
                {"$set": {
                    "name": new_module_info["name"],
                    "component": str(new_module_info["_id"]),
                    "scanSN": scan_sn,
                    "history": pre_sn
                    }
                })
            for i in range(len(old_chips)):
                try:
                    newchipID = str(localdb.chip.find_one({"name": chips['new'][i]})['_id'])
                except:
                    new_chip_info = localdb.chip.find_one({"name": chips['old'][i]})
                    new_chip_info["name"] = chips['new'][i]
                    new_chip_info.pop("_id")
                    localdb.chip.insert_one(new_chip_info)
                    newchipID = str(localdb.chip.find_one({"name": chips['new'][i]})['_id'])

                pre_info = localdb.componentTestRun.find_one({"name": chips['old'][i], "testRun": run["testRun"]})
                pre_sn = pre_info.get('history', [])
                pre_sn.append(chips['old'][i])
                scan_sn = pre_info.get('scanSN', chips['old'][i])

                localdb.componentTestRun.update({
                    "name": chips['old'][i],
                    "testRun": run["testRun"]
                    },
                    {"$set": {
                        "name": chips["new"][i],
                        "scanSN": scan_sn,
                        "history": pre_sn,
                        "component": chips["newId"][i],
                        "chip": newchipID
                    }
                    })

    return render_template("rename_moduleSN.html", stage=stage, moduleinfo=moduleinfo, modules=modules, chips=chips, change_test_run=change_test_run, text = text)

@user_api.route("/scan_stage", methods=["GET", "POST"])
#@auth.login_required
def scan_stage():
    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    run_ids = request.form.getlist("run_ids")
    modules = []
    stages = []
    table_docs = {"run": []}
    scans = localdb.componentTestRun.find()
    for item in scans:
        if item["chip"] == "module":
            modules.append(item["name"])
    modules = sorted(list(set(modules)))

    if stage == "scans":
        stages = ["Testing"]
        try:
            module_id = localdb.component.find_one({"name": moduleinfo[0]})["_id"]
            query = { "component":str(module_id) }
            qc_module_doc = localdb.QC.module.status.find_one( query )
            stage_list = [s for s in qc_module_doc["QC_results"]]
            stages = stages + stage_list
        except:
            stage = "input"

        scans = localdb.componentTestRun.find({"name": moduleinfo[0]})
        for scan in scans:
            run_data = getScanSummary(scan["testRun"])
            run_data["stage"] = localdb.testRun.find_one({"_id": ObjectId(scan["testRun"])})["stage"]
            table_docs["run"].append({"run_data": run_data})

    if stage == "confirm":
        for run_id in run_ids:
            run_data = getScanSummary(run_id)
            run_data["stage"] = localdb.testRun.find_one({"_id": ObjectId(run_id)})["stage"]
            table_docs["run"].append({"run_data": run_data})

    if stage == "complete":
        for run_id in run_ids:
            localdb.testRun.update({
                    "_id": ObjectId(run_id)
                    },
                    {"$set": {
                        "stage": moduleinfo[1]
                    }
                    })
        for run_id in run_ids:
            run_data = getScanSummary(run_id)
            run_data["stage"] = localdb.testRun.find_one({"_id": ObjectId(run_id)})["stage"]
            table_docs["run"].append({"run_data": run_data})


    return render_template("scan_stage.html", stage=stage, modules=modules, stages = stages, moduleinfo=moduleinfo, table_docs=table_docs, run_ids=run_ids)

@user_api.route("/upload_result", methods=["GET", "POST"])
#@auth.login_required
def upload_result():
    stage = request.form.get("stage", "input")
    code1 = request.form.get("code1", " ")
    code2 = request.form.get("code2", " ")
    text = ""
    tests = localdb.QC.results.find({})

    if os.path.exists(IF_DIR + "/doing_upload.txt"):
        stage = "waiting"

    if stage == "complete":
        flag, client = process_request(code1, code2)
        if flag == 0:
            stage = "input"
            text = "Not authorized. Something is wrong."
        else:
            dic1 = {"code1": code1, "code2": code2}
            thread_2 = threading.Thread(target=upload_results, kwargs=dic1)
            thread_2.start()

    return render_template("upload_result.html", stage=stage, text=text, tests=tests)

def changeUserPassword(username,password):
    query = {"username":username}
    userdb.viewer.user.update_one( query,
        {
            "$set": {
                "password": hashlib.md5(
                    password.encode("utf-8")
                ).hexdigest()
            }
        }
    )
    try:
        localdb.remove_user( username )
        localdb.remove_user( hashlib.md5(username.encode("utf-8")).hexdigest() )
        logger.info("The password is changed.")
    except:
        logger.info("A new password is registered.")

    localdb.add_user( username, password, roles=[{'role':'readWrite','db':'localdb'},{'role':'readWrite','db':'localdbtools'}] )
    localdb.add_user( hashlib.md5(username.encode("utf-8")).hexdigest(), hashlib.md5(password.encode("utf-8")).hexdigest(), roles=[{'role':'readWrite','db':'localdb'},{'role':'readWrite','db':'localdbtools'}] )

    return 0

def sendTestMail(userinfo):
    msg = Message(
        "Test mail from LocalDB",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[0]],
    )
    msg.html = ("This is a test mail to check the mail-sending function in LocalDB.")
    try:
        mail.send(msg)
    except:
        return 0

    return 1

def sendSignupMail(userinfo):
    msg = Message(
        "Welcome to Local DB! Register your Password",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[5]],
    )
    mail_text = open("static/text/signup_mail_text.txt", "r")
    contents = mail_text.read()
    msg.html = (
        contents.replace("USERNAME", userinfo[0])
        .replace("ADDRESS", userinfo[5])
        .replace("LOCALDBURL", localdb_url)
        .replace("FIRSTNAME", userinfo[1])
        .replace("LASTNAME", userinfo[2])
    )
    mail_text.close()
    try:
        mail.send(msg)
        return 1
    except:
        return 0

def updatePasswordAndMail(userinfo):

    query = {"username": userinfo[0]}
    num = 8
    pool = string.ascii_letters
    pool2 = string.digits
    pin_number = "".join([secrets.choice(pool+pool2) for i in range(num)])
    msg = Message(
        "Local DB password notice",
        sender="admin-no-reply@localdb.com",
        recipients=[userinfo[1]],
    )
    mail_text = open("static/text/pin_mail_text.txt", "r")
    contents = mail_text.read()
    msg.html = contents.replace("PINNUMBER", pin_number)
    mail_text.close()
    try:
        mail.send(msg)
    except:
        return 0

    userdb.viewer.user.update_one(
        query,
        {
            "$set": {
                "password": hashlib.md5(
                    pin_number.encode("utf-8")
                ).hexdigest()
            }
        },
    )

    return 1


def insertDBUser(userinfo):

    if not userinfo[3] == "other": institution = userinfo[3]
    else: institution = userinfo[4]
    thistime = datetime.now()
    userdb.viewer.user.insert(
        {
            "sys": {"rev": 0, "cts": thistime, "mts": thistime},
            "username": userinfo[0],
            "name": userinfo[1] + " " + userinfo[2],
            "auth": "readWrite",
            "institution": institution,
            "Email": userinfo[5],
            "password": "",
        }
    )

    return 0



@user_api.route("/delete_message", methods=["POST", "GET"])
def delete_message():

    msg_id = request.args.get('id')
    query = { '_id' : ObjectId( msg_id ) }
    if userdb.message.find_one( query ):
        userdb.message.delete_one( query )
    
    pre_url = request.headers.get("Referer")
    return redirect( pre_url )
    
