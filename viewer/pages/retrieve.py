from functions.imports import *

retrieve_api = Blueprint("retrieve_api", __name__)


@retrieve_api.route("/retrieve/config", methods=["GET"])
def retrieve_config():
    oid = request.args["oid"]
    data_type = request.args["type"]
    if data_type == "json":
        data = json.loads(fs.get(ObjectId(oid)).read().decode("ascii"))
    else:
        data = fs.get(ObjectId(oid)).read().decode("ascii")
    return_json = {"data": data}

    return jsonify(return_json)


@retrieve_api.route("/retrieve/log", methods=["GET"])
def retrieve_log():
    return_json = {}

    run_query = {}
    log_query = {"$and": []}

    chip_name = request.args.get("chip", None)
    if chip_name:
        query = {"name": chip_name}
        chip_entries = localdb.chip.find(query)
        chip_query = []
        for this_chip in chip_entries:
            chip_query.append({"chip": str(this_chip["_id"])})
        if chip_query == []:
            return_json = {
                "message": "Not found chip data: {}".format(chip_name),
                "error": True,
            }
            return jsonify(return_json)
        else:
            run_query.update({"$or": chip_query})

    if not run_query == {}:
        run_entries = localdb.componentTestRun.find(run_query)
        run_oids = []
        for run_entry in run_entries:
            run_oids.append({"_id": ObjectId(run_entry["testRun"])})
        log_query["$and"].append({"$or": run_oids})

    if request.args.get("user", None):
        query = {"userName": {"$regex": request.args["user"].lower().replace(" ", "_")}}
        entries = localdb.user.find(query)
        if entries.count() == 0:
            return_json = {
                "message": "Not found user data: {}".format(request.args["user"]),
                "error": True,
            }
            return jsonify(return_json)
        user_oids = []
        for entry in entries:
            user_oids.append({"user_id": str(entry["_id"])})
        log_query["$and"].append({"$or": user_oids})
    if request.args.get("site", None):
        query = {
            "institution": {"$regex": request.args["site"].lower().replace(" ", "_")}
        }
        entries = localdb.pd.institution.find(query)
        if entries.count() == 0:
            return_json = {
                "message": "Not found site data: {}".format(request.args["site"]),
                "error": True,
            }
            return jsonify(return_json)
        site_oids = []
        for entry in entries:
            site_oids.append({"address": str(entry["_id"])})
        log_query["$and"].append({"$or": site_oids})

    if log_query["$and"] == []:
        log_query = {}

    run_entries = localdb.testRun.find(log_query).sort([("startTime", DESCENDING)])

    return_json = {"log": []}
    for this_run in run_entries:
        query = {"_id": ObjectId(this_run["user_id"])}
        this_user = localdb.user.find_one(query)
        query = {"_id": ObjectId(this_run["address"])}
        this_site = localdb.pd.institution.find_one(query)
        query = {"testRun": str(this_run["_id"])}
        ctr_entries = localdb.componentTestRun.find(query)
        chips = []
        this_dcs = {}
        for this_ctr in ctr_entries:
            if chip_name and this_ctr["name"] == chip_name:
                chips.append("\033[1;31m{}\033[0m".format(this_ctr["name"]))
            else:
                chips.append(this_ctr["name"])
            if not this_ctr.get("environment", "...") == "...":
                this_dcs.update({this_ctr["name"]: []})
                query = {"_id": ObjectId(this_ctr["environment"])}
                this_env = localdb.environment.find_one(query)
                for key in this_env:
                    if not key == "_id" and not key == "dbVersion" and not key == "sys":
                        this_dcs[this_ctr["name"]].append(key)
        test_data = {
            "user": this_user["userName"],
            "site": this_site["institution"],
            "datetime": setTime(
                this_run["startTime"], session.get("timezone", str(get_localzone()))
            ),
            "runNumber": this_run["runNumber"],
            "testType": this_run["testType"],
            "runId": str(this_run["_id"]),
            "chips": chips,
            "dbVersion": this_run["dbVersion"],
            "environment": this_dcs,
        }
        return_json["log"].append(test_data)

    return jsonify(return_json)


@retrieve_api.route("/retrieve/data", methods=["GET"])
def retrieve_data():
    ###################################
    ### Pull data files from testRun ID
    def __pull_test_run(i_oid, i_chip):
        data_entries = []
        query = {"_id": ObjectId(tr_oid), "dbVersion": dbv}
        this_tr = localdb.testRun.find_one(query)
        chip_type = (
            this_tr.get("chipType", "NULL")
            if this_tr.get("chipType", "NULL") != "FE-I4B"
            else "FEI4B"
        )

        ### scanLog
        log_json = {}
        for key in this_tr:
            if "Cfg" in key and not this_tr[key] == "...":
                ### config file
                query = {"_id": ObjectId(this_tr[key])}
                this_cfg = localdb.config.find_one(query)
                docs = getData(
                    "json", dir_path, this_cfg["filename"], this_cfg["data_id"]
                )
                data_entries.append(docs)
            elif key == "_id":
                log_json.update({key: str(this_tr[key])})
            elif key == "startTime" or key == "finishTime":
                log_json.update({key: this_tr[key].timestamp()})
            elif not key == "address" and not key == "user_id" and not key == "sys":
                log_json.update({key: this_tr[key]})

        ### connectivity
        query = {"testRun": tr_oid, "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        conn_json = {
            "stage": this_tr.get("stage", "..."),
            "chipType": chip_type,
            "chips": [],
        }
        chips = []
        for this_ctr in entries:
            if this_ctr["chip"] == "module":
                query = {"_id": ObjectId(this_ctr["component"])}
                this_cmp = localdb.component.find_one(query)
                conn_json.update(
                    {
                        "module": {
                            "serialNumber": this_cmp["name"],
                            "componentType": this_cmp["componentType"],
                        }
                    }
                )
                continue
            chip_conn = {}
            for key in this_ctr:
                if key == "config":
                    chip_conn.update({key: "{0}/{1}".format(dir_path, this_ctr[key])})
                elif "Cfg" in key and not this_ctr[key] == "...":
                    ### chip config
                    query = {"_id": ObjectId(this_ctr[key])}
                    this_cfg = localdb.config.find_one(query)
                    if key == "beforeCfg":
                        docs = getData(
                            "json",
                            dir_path,
                            this_ctr.get("config", "{}.json".format(this_ctr["name"])),
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                        docs = getData(
                            "json",
                            dir_path,
                            "{}.before".format(
                                this_ctr.get(
                                    "config", "{}.json".format(this_ctr["name"])
                                )
                            ),
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                    elif key == "afterCfg":
                        docs = getData(
                            "json",
                            dir_path,
                            "{}.after".format(
                                this_ctr.get(
                                    "config", "{}.json".format(this_ctr["name"])
                                )
                            ),
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                    else:
                        docs = getData(
                            "json",
                            dir_path,
                            "{0}_{1}.json".format(this_ctr["name"], key),
                            this_cfg["data_id"],
                        )
                        data_entries.append(docs)
                elif key == "attachments":
                    ### attachement
                    for attachment in this_ctr[key]:
                        docs = getData(
                            attachment["contentType"],
                            dir_path,
                            "{0}_{1}".format(this_ctr["name"], attachment["filename"]),
                            attachment["code"],
                        )
                        data_entries.append(docs)
                elif not key in [
                    "_id",
                    "component",
                    "sys",
                    "chip",
                    "testRun",
                    "environment",
                    "dbVersion",
                ]:
                    chip_conn.update({key: this_ctr[key]})
            conn_json["chips"].append(chip_conn)
            ### output texts
            if i_chip and this_ctr["name"] == i_chip:
                chips.append("\033[1;31m{}\033[0m".format(this_ctr["name"]))
            else:
                chips.append(this_ctr["name"])
        docs = getData("json", dir_path, "connectivity.json", conn_json, True)
        data_entries.append(docs)

        log_json["connectivity"] = []
        log_json["connectivity"].append(conn_json)
        docs = getData("json", dir_path, "scanLog.json", log_json, True)
        data_entries.append(docs)

        ### user data
        query = {"_id": ObjectId(this_tr["user_id"])}
        this_user = localdb.user.find_one(query)

        ### site data
        query = {"_id": ObjectId(this_tr["address"])}
        this_site = localdb.pd.institution.find_one(query)

        ### output texts
        console_data = {
            "_id": str(this_tr["_id"]),
            "col": "testRun",
            "log": {
                "User": "{0} at {1}".format(
                    this_user["userName"], this_site["institution"]
                ),
                "Date": setTime(
                    this_tr["startTime"], session.get("timezone", str(get_localzone()))
                ),
                "Chips": ", ".join(chips),
                "Run Number": this_tr["runNumber"],
                "Test Type": this_tr["testType"],
            },
            "data": data_entries,
        }

        return console_data

    ##############################################
    ### Pull data files from component information
    def __pull_component(i_chip):
        data_entries = []
        query = {"name": i_chip, "dbVersion": dbv}
        this_cmp = localdb.component.find_one(query)
        this_ch = localdb.chip.find_one(query)
        if not this_cmp and not this_ch:
            return_json = {
                "message": "Not found chip data: {}".format(i_chip),
                "error": True,
            }
            return return_json
        if this_cmp:
            this_chip = this_cmp
        else:
            this_chip = this_ch
        ### connectivity
        conn_json = {"stage": "Testing", "chips": []}
        module = False
        children = []
        query = {"parent": str(this_chip["_id"]), "dbVersion": dbv}
        entries = localdb.childParentRelation.find(query)
        if entries.count() == 0:
            children.append(str(this_chip["_id"]))
        else:
            conn_json.update(
                {
                    "module": {
                        "serialNumber": this_chip["serialNumber"],
                        "componentType": this_chip["componentType"],
                    }
                }
            )
            for entry in entries:
                children.append(entry["child"])
            module = True

        chips = []
        for i, chip in enumerate(children):
            query = {"_id": ObjectId(chip)}
            this = localdb.component.find_one(query)
            if not this:
                continue

            chip_type = (
                this.get("chipType", "NULL")
                if this.get("chipType", "NULL") != "FE-I4B"
                else "FEI4B"
            )
            cfg_json = requests.get(
                "https://gitlab.cern.ch/YARR/YARR/raw/master/configs/defaults/default_{}.json".format(
                    chip_type.lower()
                )
            )
            if cfg_json.status_code == 404:
                return_json = {
                    "message": "Not found default chip config: {}".format(cfg_path),
                    "error": True,
                }
                return return_json
            cfg_json = cfg_json.json()
            if chip_type == "FEI4B":
                cfg_json["FE-I4B"]["name"] = this["name"]
                cfg_json["FE-I4B"]["Parameter"]["chipId"] = this.get("chipId", 0)
            else:
                cfg_json[chip_type]["Parameter"]["Name"] = this["name"]
                cfg_json[chip_type]["Parameter"]["ChipId"] = this.get("chipId", 0)
            ### chip config
            docs = getData(
                "json", dir_path, "{}.json".format(this["name"]), cfg_json, True
            )
            data_entries.append(docs)

            conn_json["chips"].append(
                {
                    "config": "{0}/{1}.json".format(dir_path, this["name"]),
                    "tx": i,
                    "rx": i,
                }
            )
            conn_json.update({"chipType": chip_type})
            ### output texts
            if chip == str(this_chip["_id"]):
                chips.append("\033[1;31m{}\033[0m".format(this["name"]))
            else:
                chips.append(this["name"])
        docs = getData("json", dir_path, "connectivity.json", conn_json, True)
        data_entries.append(docs)

        ### output texts
        console_data = {
            "_id": str(this_chip["_id"]),
            "col": "component",
            "log": {
                "Parent": "\033[1;31m{0} ({1})\033[0m".format(
                    this_chip["serialNumber"], this_chip["componentType"]
                )
                if module
                else None,
                "Chip Type": chip_type,
                "Chips": ", ".join(chips),
            },
            "data": data_entries,
        }
        return console_data

    #################
    ### main function
    return_json = {}
    dir_path = request.args.get("dir")

    tr_oid = None
    if request.args.get("test", None):
        tr_oid = request.args["test"]
    elif request.args.get("chip", None):
        query = {"name": request.args["chip"], "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        if not entries.count() == 0:
            query = {"$or": [], "dbVersion": dbv}
            for entry in entries:
                query["$or"].append({"_id": ObjectId(entry["testRun"])})
            entry = (
                localdb.testRun.find(query).sort([("startTime", DESCENDING)]).limit(1)
            )
            if not entry.count() == 0:
                tr_oid = str(entry[0]["_id"])
    else:
        query = {"dbVersion": dbv}
        entry = localdb.testRun.find(query).sort([("startTime", DESCENDING)]).limit(1)
        if not entry.count() == 0:
            tr_oid = str(entry[0]["_id"])

    if tr_oid:
        query = {"testRun": tr_oid, "dbVersion": dbv}
        entries = localdb.componentTestRun.find(query)
        if entries.count() == 0:
            if request.args.get("chip", None):
                return_json.update(
                    {"warning": "Not found test data ( ID: {} )".format(tr_oid)}
                )
                console_data = __pull_component(request.args["chip"])
            else:
                return_json = {
                    "message": "Not found test data ( ID: {} )".format(tr_oid),
                    "error": True,
                }
                return jsonify(return_json)
        else:
            console_data = __pull_test_run(tr_oid, request.args.get("chip", None))
    elif request.args.get("chip", None):
        return_json.update(
            {
                "warning": "Not found test data of the component: {}".format(
                    request.args["chip"]
                )
            }
        )
        console_data = __pull_component(request.args["chip"])
    else:
        return_json = {"message": "Not found test data", "error": True}
        return jsonify(return_json)

    if console_data.get("error", False):
        return_json = console_data
    else:
        return_json.update({"console_data": console_data})

    return jsonify(return_json)


@retrieve_api.route("/retrieve/list", methods=["GET"])
def retrieve_list():
    opt = request.args["opt"]
    return_json = {}

    if opt == "component":
        return_json.update({"parent": [], "child": {}})
        entries = localdb.childParentRelation.find()
        oids = []
        for entry in entries:
            oids.append(entry["parent"])
        oids = list(set(oids))
        for oid in oids:
            query = {"_id": ObjectId(oid)}
            this = localdb.component.find_one(query)
            query = {"_id": ObjectId(this["user_id"])}
            this_user = localdb.user.find_one(query)
            query = {"_id": ObjectId(this["address"])}
            this_site = localdb.pd.institution.find_one(query)
            docs = {
                "oid": oid,
                "name": this["name"],
                "type": this["componentType"],
                "asic": this["chipType"],
                "chips": [],
                "user": this_user["userName"],
                "site": this_site["institution"],
            }
            query = {"parent": oid}
            entries = localdb.childParentRelation.find(query)
            for entry in entries:
                docs["chips"].append(entry["child"])
            return_json["parent"].append(docs)
        query = {"componentType": "front-end_chip"}
        entries = localdb.component.find(query)
        oids = []
        for entry in entries:
            oids.append(str(entry["_id"]))
        for oid in oids:
            query = {"_id": ObjectId(oid)}
            this = localdb.component.find_one(query)
            query = {"_id": ObjectId(this["user_id"])}
            this_user = localdb.user.find_one(query)
            query = {"_id": ObjectId(this["address"])}
            this_site = localdb.pd.institution.find_one(query)
            return_json["child"].update(
                {
                    oid: {
                        "name": this["name"],
                        "type": this["componentType"],
                        "asic": this["chipType"],
                        "chipId": this["chipId"],
                        "user": this_user["userName"],
                        "site": this_site["institution"],
                    }
                }
            )
        return_json["parent"] = sorted(return_json["parent"], key=lambda x: (x["type"]))
    elif opt == "user":
        entries = localdb.user.find()
        users = []
        for entry in entries:
            users.append(entry["userName"])
        for user in users:
            query = {"userName": user}
            entries = localdb.user.find(query)
            docs = []
            for entry in entries:
                docs.append(entry["institution"])
            docs = list(set(docs))
            return_json.update({user: docs})
    elif opt == "site":
        entries = localdb.pd.institution.find()
        docs_list = []
        for entry in entries:
            docs_list.append(entry["institution"])
        docs_list = list(set(docs_list))
        return_json.update({"site": docs_list})

    return jsonify(return_json)


def getData(i_type, i_dir, i_filename, i_data, i_bool=False):
    docs = {
        "type": i_type,
        "path": "{0}/{1}".format(i_dir, i_filename),
        "data": i_data,
        "bool": i_bool,
    }
    return docs
