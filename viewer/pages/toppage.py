from functions.imports import *
from json2html import *
import jsonschema
from RecursiveComponentsSynchronizer import *

toppage_api = Blueprint("toppage_api", __name__)

###############
# Show top page
@toppage_api.route("/", methods=["GET"])
def show_toppage():
    initPage()

    user_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    if os.path.isdir(user_dir):
        shutil.rmtree(user_dir)

    session.pop("signup", None)
    #    session.pop('keywords', None)
    #    session.pop('match', None)
    table_docs = {"page": None}

    ############
    ## for admin
#    try:
#        if os.path.exists('../config.yml'):
#            with open('../config.yml') as f:
#                l_strip = [s.strip() for s in f.readlines() if ' KeyFile' in s]
#                path_to_adminfile = l_strip[0].split(':')[1].split()[0]
#        else:
#            with open('admin_conf.yml') as f:
#                l_strip = [s.strip() for s in f.readlines() if ' KeyFile' in s]
#                path_to_adminfile = l_strip[0].split(':')[1].split()[0]
#        admin_file = open(path_to_adminfile, "r")
#        admin_info = admin_file.readline().replace( '\n' , '' )
#    except:
#        admin_info = ''

    try:
        query = {"auth" : "adminViewer"}
        admin_info = client.localdbtools.viewer.user.find_one(query, sort = [('_id', -1)])
        admin_username = admin_info["name"]
    except:
        admin_username = ""


    try:
        user_name = session['username']
    except:
        user_name = ""

    if admin_username == user_name:
        table_docs["admin"] = "admin"
    else:
        table_docs["admin"] = "shomin"

    mail = args.fmail

    
    # messages
    messages = format_messages( {'component': None} )

    table_docs['messages'] = messages

    return render_template(
        "toppage.html", table_docs=table_docs, mail=mail, timezones=pytz.all_timezones
    )


#####################
# Show component list
@toppage_api.route("/components", methods=["GET", "POST"])
def show_comps():
    initPage()

    table_docs = {"components": [] }

    # messages
    messages = format_messages( {'component': None} )

    table_docs['messages'] = messages

    table_docs['view'] = request.args.get("view", None)
    
    if table_docs['view'] == None:
        table_docs['view'] = request.form.get("view", "module")
    
    view = table_docs['view']
    
    logger.debug('show_comps(): view = {}'.format( view ) )
    
    
    # Action modes
    archive_list = []
    doArchive = False
    if request.form.get("archive"):
        logger.debug('show_comps(): archive = {}'.format( request.form.get("archive") ) )
        doArchive = True
        
        for key in request.form.keys():
            flag = request.form.get( key )
            if flag:
                archive_list.append( key[key.find('_')+1:] )
        
    doShowAll = False
    if request.form.get("showall"):
        logger.debug('show_comps(): showall = {}'.format( request.form.get("showall") ) )
        doShowAll = True if request.form.get("showall") == "true" else False

        
    # get keywords
    if request.form.get("keywords"):
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")
    
    if table_docs["keywords"] != "":
        doShowAll = True
        
    table_docs['doArchive'] = doArchive
    table_docs['doShowAll'] = doShowAll
        
    compsPerPage = 10
    
    thistime = datetime.utcnow()
    
    if not userdb.viewer.tag.categories.find_one( { "name":"archived" } ):
        userdb.viewer.tag.categories.insert_one(
            {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "user_id": None
            }
        )
    
    page = 1
    if not request.args.get("p", 1) == "":
        page = int(request.args.get("p", 1))

    components = localdb.component.find( { "componentType":view } )
    cmp_ids = []
    for component in components:
        
        # logger.info( 'component: {}'.format( component ) )
        cmp_id = str(component["_id"])
        
        ### tags
        tag_candidates = [ c for c in userdb.viewer.tag.categories.find( {} ) ]
        
        ### put tags
        tags = [ t for t in userdb.viewer.tag.docs.find( {"componentid": cmp_id} ) ]
        
        isArchived = False
        for tag in tags:
            if tag['name'] == "archived":
                isArchived = True
                
        if (not isArchived) and component['name'] in archive_list:
            tagsdocs = {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "componentid" : cmp_id
            }
            userdb.viewer.tag.docs.insert_one( tagsdocs )
            isArchived = True
        
        if not isArchived:
            cmp_ids.append( cmp_id )
        
        
        cps = []
        
        cprs1 = localdb.childParentRelation.find( { "parent":cmp_id } )
        cprs2 = localdb.childParentRelation.find( { "child":cmp_id } )
        
        for cpr in cprs1:
            
            if cpr == None: continue
        
            child = localdb.component.find_one( { "_id":ObjectId( cpr['child'] ) } )
            
            if child == None: continue
            
            # logger.info( 'child = {}'.format( child ) )
            
            child_name = child['name']
            
            typeinfo = SN_typeinfo( child_name )

            try:
                if child_name.find('20UPGFC')==0:
                    hexName = hexName  = "{0:#0{1}x}".format(int( child_name[-7:] ),7)
                else:
                    hexName = ''
            except:
                hexName = ''

            cps.append(
                {"_id": str( child["_id"] ),
                 "collection": "component",
                 "name": delim_SN( child['name'] ),
                 "name_orig": child['name'],
                 "grade": {},
                 "hex" : hexName,
                 "typeinfo" : typeinfo
             } )
            
        # endfor
            
        for cpr in cprs2:
        
            parent = localdb.component.find_one( { "_id":ObjectId( cpr['parent'] ) } )
            
            # logger.info( 'parent = {}'.format( parent ) )

            if parent:
                parent_name = parent['name']

                typeinfo = SN_typeinfo( parent_name )

                cps.append(
                    {"_id": str( parent["_id"] ),
                     "collection": "component",
                     "name": delim_SN( parent['name'] ),
                     "name_orig": parent['name'],
                     "grade": {},
                     "hex" : hexName if parent_name.find('20UPGFC')==0 else '',
                     "typeinfo" : typeinfo
                 } )
            
        # endfor
            
        name = component['name']
        
        typeinfo = SN_typeinfo( name )
        
        qcStatus = localdb.QC.module.status.find_one( {"component": cmp_id} )
        
        result = None
        results = localdb.QC.result.find( {"component": cmp_id} )
        for r in results:
            result = r

        component_data = {
            "_id": cmp_id,
            "collection": "component",
            "name": delim_SN( name ),
            "name_orig": name,
            "typeinfo": typeinfo,
            "cps": cps,
            "grade": {},
            "proDB": component.get("proDB", False),
            "component_tag_candidate": tag_candidates,
            "component_tag": tags,
        }
        
        if qcStatus:
            component_data.update( { "stage":qcStatus["currentStage"] } )
        
        if result:
            logger.debug( pprint.pformat( result ) )
            component_data.update( {
                "runId"    : str( result["_id"] ),
                "datetime" : get_localtime_str( result['sys']['mts'] ),
                "testType" : result["testType"] if 'testType' in result else None ,
                "user"     : result["user"]     if 'user'     in result else None ,
                "site"     : result["address"]  if 'address'  in result else None   } )
            
        #logger.debug( 'data: {}'.format( component_data ) )
        
        # component search field
        search_targets = []
        search_targets.append(component_data["name_orig"])
        if 'stage'    in component_data: search_targets.append(component_data["stage"])
        if 'user'     in component_data: search_targets.append(component_data["user"])
        if 'site'     in component_data: search_targets.append(component_data["site"])
        if 'datetime' in component_data: search_targets.append(component_data["datetime"])
        
        for tag in component_data["component_tag"]:
            search_targets.append(tag["name"])
            
        for subcomp in component_data["cps"]:
            search_targets.append(subcomp["name_orig"])
            
        isKeywordMatched = query_keywords(
            table_docs["keywords"], search_targets, table_docs["match"]
        )
        
        # logger.info( '{} {}'.format( component_data['name'], component_data["component_tag"] ) )
        if isKeywordMatched:
            if doShowAll:
                table_docs["components"].append( component_data )
            else:
                if not isArchived:
                    table_docs["components"].append( component_data )
        
        
    #end for component in components:
    
    pageMinIndex = compsPerPage * (page-1)
    pageMaxIndex = compsPerPage * page
    
    nModulesTotal = len( table_docs['components'] )
    maxPage = len( table_docs['components'] ) // compsPerPage + 1
    minRange = max(1, page - 2)
    maxRange = min(maxPage, minRange+3)
    pageRange = list( range( minRange, maxRange+1 ) )

    pagingInfo = {
        "thisPage": page,
        "range" : pageRange,
        "minPage": 1,
        "maxPage": maxPage,
        "entries" : nModulesTotal
    }
    
    table_docs.update( { 'pagingInfo':pagingInfo } )
    table_docs["page"] = "components"
    table_docs["title"] = "{} List".format( view.upper() )
    table_docs["download_module"] = os.path.exists(IF_DIR+"/doing_download.txt")
    
    return render_template(
        "toppage.html", table_docs=table_docs, timezones=pytz.all_timezones
    )


####################
# Show test run list
@toppage_api.route("/yarr_scans", methods=["GET", "POST"])
def show_scans():

    initPage()
    check_database_update()

    max_num = 15
    sort_cnt = 0
    if not request.args.get("p", 0) == "":
        sort_cnt = int(request.args.get("p", 0))

    table_docs = {"run": []}
    
    # Action modes
    archive_list = []
    doArchive = False
    if request.form.get("archive"):
        logger.debug('show_comps(): archive = {}'.format( request.form.get("archive") ) )
        doArchive = True
        
        for key in request.form.keys():
            flag = request.form.get( key )
            if flag:
                archive_list.append( key[key.find('_')+1:] )
        
    doShowAll = False
    if request.form.get("showall"):
        logger.debug('show_comps(): showall = {}'.format( request.form.get("showall") ) )
        doShowAll = True if request.form.get("showall") == "true" else False
        
    # get keywords
    if not request.form.get("keywords") == None:
        table_docs["keywords"] = request.form.get("keywords", "")
        table_docs["match"] = request.form.get("match", "None")
    else:
        table_docs["keywords"] = request.args.get("keywords", "")
        table_docs["match"] = request.args.get("match", "None")
        
    if table_docs["keywords"] != "":
        doShowAll = True

    table_docs['doArchive'] = doArchive
    table_docs['doShowAll'] = doShowAll
        
    print(table_docs["keywords"])
    run_ids = query_docs(Keywords=table_docs["keywords"], Match=table_docs["match"])
    print("[Component.py] show_scans", len(run_ids))

    if "config" in run_ids:
        run_ids.remove("config")

    nrun_entries = len(run_ids)
    run_num_list = []
    if nrun_entries < (sort_cnt + 1) * max_num:
        for i in range(nrun_entries % max_num):
            run_num_list.append(sort_cnt * (max_num) + i)
    else:
        for i in range(max_num):
            run_num_list.append(sort_cnt * (max_num) + i)


    thistime = datetime.utcnow()
    
    if not userdb.viewer.tag.categories.find_one( { "name":"archived" } ):
        userdb.viewer.tag.categories.insert_one(
            {
                "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                "name": "archived",
                "user_id": None
            }
        )
    
    run_counts = 0
    if not nrun_entries == 0:
        for i in run_num_list:
            try:
                run_data = getScanSummary(run_ids[i])
                
                for component in run_data['components']:
                    component['name_delim'] = delim_SN( component['name'] )
                
                isArchived = False
                tags = run_data['testRun_tag']
                for tag in tags:
                    if tag['name'] == 'archived':
                        isArchived = True
                        break
                
                if (not isArchived) and str( run_data['_id'] ) in archive_list:
                    tagsdocs = {
                        "sys": {"rev": 0, "cts": thistime, "mts": thistime},
                        "name": "archived",
                        "runId" : str( run_data['_id'] )
                    }
                    userdb.viewer.tag.docs.insert_one( tagsdocs )
                    userdb.viewer.query.update( {"runId": str( run_data['_id'] ) }, {"$push": {"data": tagsdocs['name'] } } )
                    isArchived = True
        
                if doShowAll:
                    table_docs["run"].append( {"run_data": run_data, "nrun": run_counts} )
                    run_counts += 1
                
                else:
                    if not isArchived:
                        table_docs["run"].append( {"run_data": run_data, "nrun": run_counts} )
                        run_counts += 1
            except Exception as e:
                logger.warning( '{}'.format(e) )
                pass

    table_docs["run"] = sorted(
        table_docs["run"], key=lambda x: x["run_data"]["datetime"], reverse=True
    )
    cnt = []
    for i in range(((nrun_entries - 1) // max_num) + 1):
        if sort_cnt - (max_num / 2) < i:
            cnt.append(i)
        if len(cnt) == max_num:
            break

    scans_num = {
        "total": run_counts,
        "cnt": cnt,
        "now_cnt": sort_cnt,
        "max_cnt": ((nrun_entries - 1) // max_num),
        "max_num": max_num,
    }
    table_docs.update(scans_num)

    # table_docs['run'] = sorted(table_docs['run'], key=lambda x:x['run_data']['datetime'], reverse=True)

    table_docs["page"] = "yarr_scans"
    table_docs["title"] = "YARR Scan List"

    return render_template(
        "toppage.html", table_docs=table_docs, timezones=pytz.all_timezones
    )


####################
# Show test run list


@toppage_api.route("/assemble_module", methods=["GET", "POST"])
def assemble_module():
    initPage()

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    table_docs = {"components": {}}
    table_docs["page"] = "module"
    table_docs["title"] = "Assemble a new Module on ITkPD"

    stage = request.form.get("stage", "input_module")
    moduleinfo = request.form.getlist("moduleinfo")
    tripletinfo = request.form.getlist("tripletinfo")
    check_serialNum = ""
    module_serial_number = ""
    text = ""
    config = {}
    lookup_table = {}
    currentLocation = {}
    typeinfos = {}


    if not get_pd_client():
        session['url'] = url_for('toppage_api.assemble_module')
        return redirect( url_for('user_api.itkdb_authenticate') )

    pd_client = get_pd_client()
    userinfo = request.form.get("userinfo", type=itkdb.core.User)

    userdb = client.localdbtools

    if pd_client:
        try:
            module_config = userdb["QC.module.types"].find_one()
            lookup_table = ModuleRegistration( pd_client ).make_table(module_config)
        except Exception as e:
            logger.warning( str(e) )
            stage = "download_moduletypes"


    institution = ModuleRegistration( pd_client ).user_institutions( get_pd_user() )
    
    logger.info( f'user\'s institution: {institution}' )
    
        
    if stage != "input_module":

        # moduleinfo[0]: bare_module_SNs
        # moduleinfo[1]: PCB_SN
        
        bare_serial_numbers   = [ moduleinfo[0] ]
        pcb_serial_number     = moduleinfo[1]
        carrier_serial_number = moduleinfo[2]


        assembly_doc = {}

        try:

            assembly_doc.update( { bare_serial_numbers[0] : check_component( bare_serial_numbers[0], 'BARE_MODULE', 'Bare Module', institution ) } )
            assembly_doc.update( { pcb_serial_number      : check_component( pcb_serial_number, 'PCB', 'Module PCB', institution ) } )

            bare1_tokens = SN_tokens( bare_serial_numbers[0] )
            pcb_tokens = SN_tokens( pcb_serial_number )

            module_type_candidates = { typename : data.get('XX')+data.get('YY') for typename, data in lookup_table.get('MODULE_TYPES').items() if data.get('BARE_MODULE') == bare1_tokens.get('XXYY') and data.get('PCB') == pcb_tokens.get('XXYY') }
            
            module_xxyy_candidates = [ xxyy for typename, xxyy in module_type_candidates.items() ]
            module_typename_candidates = [ name for name, xxyy in module_type_candidates.items() ]

            if len( module_xxyy_candidates ) == 0:
                raise Exception( f'ERROR: Specified PCB Type \"{pcb_tokens.get("XXYY")}\" and BareModule Type \"{bare1_tokens.get("XXYY")}\" are not compatible to deduce a valid Module Type' )

            if len( module_xxyy_candidates ) > 1:
                logger.info(f'Specified PCB Type \"{pcb_tokens.get("XXYY")}\" and BareModule Type \"{bare1_tokens.get("XXYY")}\" do not deduce a unique Module Type: candidates = {module_xxyy_candidates}' )
                logger.info(f'Attempting to resolve it using BareModule property...' )
                
                pd_bare_cpt = pd_client.get( 'getComponent', json = { 'component':bare_serial_numbers[0] } )

                bare_subcpts = pd_bare_cpt.get('children')

                sensor_sn = None
                for subcpt in bare_subcpts:
                    if subcpt.get('componentType').get('code') == 'SENSOR_TILE':
                        subcpt_info = subcpt.get('component')
                        if not subcpt_info:
                            continue
                        
                        sensor_sn = subcpt.get('component').get('serialNumber')

                if not sensor_sn:
                    raise Exception( f'Bare Module {bare_serial_numbers[0]} does not have its sensor tile!' )

                if sensor_sn:
                    
                    logger.info( 'Sensor_tile is found on the bare module. Using the bare module SENSOR_TILE property for module SN resolution' )
                    
                    sensor_xx = sensor_sn[3:5]

                    if sensor_xx == 'PG':
                        module_xxyy_candidates = [ xxyy for xxyy in module_xxyy_candidates if 'PG' in xxyy ]
                        module_typename_candidates = [ name for name, xxyy in module_type_candidates.items() if 'PG' in xxyy ]
                    else:
                        module_xxyy_candidates = [ xxyy for xxyy in module_xxyy_candidates if 'PI' in xxyy ]
                        module_typename_candidates = [ name for name, xxyy in module_type_candidates.items() if 'PI' in xxyy ]
                    
                else:
                    logger.info( 'No sensor_tile is found on the bare module. As a fallback, attempt to use the bare module SENSOR_TILE property for module SN resolution' )
                    bare_props = pd_bare_cpt.get('properties')

                    layer_info = [ prop['value'] for prop in bare_props if prop['code'] == 'SENSOR_TYPE' ][0]

                    if not layer_info:
                        raise Exception( f'No SENSOR_TYPE property is recorded on the bare module property!' )

                    if 'Outer' in layer_info:
                        module_xxyy_candidates = [ xxyy for xxyy in module_xxyy_candidates if 'PG' in xxyy ]
                        module_typename_candidates = [ name for name, xxyy in module_type_candidates.items() if 'PG' in xxyy ]
                    else:
                        module_xxyy_candidates = [ xxyy for xxyy in module_xxyy_candidates if 'PI' in xxyy ]
                        module_typename_candidates = [ name for name, xxyy in module_type_candidates.items() if 'PI' in xxyy ]

            if len( module_xxyy_candidates ) > 1:
                raise Exception( f'ERROR: Specified PCB Type \"{pcb_tokens.get("XXYY")}\" and BareModule Type \"{bare1_tokens.get("XXYY")}\" do not deduce a unique Module Type: candidates = {module_xxyy_candidates}' )

            module_xxyy = module_xxyy_candidates[0]
            module_typename = module_typename_candidates[0]
            
            module_serial_number = ''.join( [ "20", "U", module_xxyy,
                                              bare1_tokens.get('1st'),
                                              pcb_tokens.get('number')[-6:] ] )
        
            moduleinfo += [ module_typename ]
            
        except Exception as e:
            
            typeinfos = { 'BARE_MODULE' : [ SN_typeinfo( bare, 1 ) for bare in bare_serial_numbers ],
                          'PCB' : SN_typeinfo( pcb_serial_number, 1 ),
                          'MODULE' : SN_typeinfo( module_serial_number, 1 ) }

            text = str(e)
            stage = "confirm"
            return render_template("toppage.html",
                                   table_docs=table_docs,
                                   timezones=pytz.all_timezones,
                                   stage=stage,
                                   text=text,
                                   moduleInfo=moduleinfo,
                                   lookup_table=lookup_table,
                                   module_serial_number=module_serial_number,
                                   institution=institution,
                                   tripletInfo=tripletinfo,
                                   check_serialNum=check_serialNum,
                                   currentLocation=currentLocation,
                                   typeinfos= typeinfos                        )


        # Triplet Check
        bare_serial_numbers += tripletinfo

        if tripletinfo[0] or tripletinfo[1]:
            try:

                assembly_doc.update( { bare_serial_numbers[1] : check_component( bare_serial_numbers[1], 'BARE_MODULE', 'Bare Module', institution ) } )
                assembly_doc.update( { bare_serial_numbers[2] : check_component( bare_serial_numbers[2], 'BARE_MODULE', 'Bare Module', institution ) } )

                bare2_tokens = SN_tokens( bare_serial_numbers[1] )
                bare3_tokens = SN_tokens( bare_serial_numbers[2] )

                if bare1_tokens.get('XXYY') != bare2_tokens.get('XXYY'):
                    raise Exception( f'ERROR: Specified PCB Type \"{pcb_tokens.get("XXYY")}\" and BareModule Types [\"{bare1_tokens.get("XXYY")}\", \"{bare2_tokens.get("XXYY")}\", \"{bare2_tokens.get("XXYY")}\" ] are not compatible to deduce a valid Module Type' )
                
                if bare1_tokens.get('XXYY') != bare3_tokens.get('XXYY'):
                    raise Exception( f'ERROR: Specified PCB Type \"{pcb_tokens.get("XXYY")}\" and BareModule Types [\"{bare1_tokens.get("XXYY")}\", \"{bare2_tokens.get("XXYY")}\", \"{bare2_tokens.get("XXYY")}\" ] are not compatible to deduce a valid Module Type' )
                
            except Exception as e:
                text = str(e)
                stage = "confirm"
                
                typeinfos = { 'BARE_MODULE' : [ SN_typeinfo( bare, 1 ) for bare in bare_serial_numbers ],
                              'PCB' : SN_typeinfo( pcb_serial_number, 1 ),
                              'MODULE' : SN_typeinfo( module_serial_number, 1 ) }

                return render_template("toppage.html",
                                       table_docs=table_docs,
                                       timezones=pytz.all_timezones,
                                       stage=stage,
                                       text=text,
                                       moduleInfo=moduleinfo,
                                       lookup_table=lookup_table,
                                       module_serial_number=module_serial_number,
                                       institution=institution,
                                       tripletInfo=tripletinfo,
                                       check_serialNum=check_serialNum,
                                       currentLocation=currentLocation,
                                       typeinfos= typeinfos                        )

            
        logger.info( f'Synthesized Module Serial Number = {module_serial_number}' )
        
        #logger.info( 'assembly_doc = ' + pprint.pformat( assembly_doc ) )

        current_locations = [ doc.get("location") for sn, doc in assembly_doc.items() ]

        location = current_locations[0]

        if not all( loc == current_locations[0] for loc in current_locations ):
            text = 'The current location of sub-components are not all identical!'
            stage = "confirm"
            return render_template("toppage.html",
                                   table_docs=table_docs,
                                   timezones=pytz.all_timezones,
                                   stage=stage,
                                   text=text,
                                   moduleInfo=moduleinfo,
                                   lookup_table=lookup_table,
                                   module_serial_number=module_serial_number,
                                   institution=institution,
                                   tripletInfo=tripletinfo,
                                   check_serialNum=check_serialNum,
                                   currentLocation=currentLocation,
                                   typeinfos= typeinfos                        )
        
        config =  {
            "type": module_typename,
            "FECHIP": bare1_tokens.get('1st'),
            "child":{
                "BARE_MODULE": bare_serial_numbers,
                "PCB": pcb_serial_number,
                "CARRIER": carrier_serial_number if carrier_serial_number != "" else None
            },
            "serialNumber":module_serial_number
        }

        # Check the module serial number if available
        check_serialNum = ModuleRegistration( pd_client ).check_exist(module_serial_number)

        if check_serialNum == "2":
            pass
        else:
            stage = "confirm"

        typeinfos = { 'BARE_MODULE' : [ SN_typeinfo( bare, 1 ) for bare in bare_serial_numbers ],
                      'PCB' : SN_typeinfo( pcb_serial_number, 1 ),
                      'MODULE' : SN_typeinfo( module_serial_number, 1 ) }

    if stage == "complete":

        ModuleRegistration( pd_client ).register_Module( config, lookup_table, location )
        
        thread = threading.Thread( target = download_worker, args = ( module_serial_number, None, None, 'MODULE/ASSEMBLY' ) )
        thread.start()
        time.sleep(3)


    return render_template("toppage.html",
                           table_docs=table_docs,
                           timezones=pytz.all_timezones,
                           stage=stage,
                           text=text,
                           moduleInfo=moduleinfo,
                           lookup_table=lookup_table,
                           module_serial_number=module_serial_number,
                           institution=institution,
                           tripletInfo=tripletinfo,
                           check_serialNum=check_serialNum,
                           currentLocation=currentLocation,
                           typeinfos= typeinfos                        )



def check_component( serialNumber, componentType, componentTypeName, user_institution ):
    
    pd_client = get_pd_client()
    
    try:
        componentInfo = ModuleRegistration( pd_client ).get_component(serialNumber)
    except:
        text = f"{componentTypeName} {serialNumber} does not exist in ITkPD..."
        raise Exception( text )

    checked_component_type = componentInfo.get('componentType')['code']
    checked_component_assembled = componentInfo["assembled"]
    checked_component_location = componentInfo["currentLocation"]["code"]
    if checked_component_type != componentType:
        text = f"{componentTypeName} {serialNumber} component type is not compatible (is a {checked_component_type})..."
        raise Exception( text )
    # if checked_component_assembled:
    #     text = f"{componentTypeName} {serialNumber} is already used for another module..."
    #     raise Exception( text )
    if not checked_component_location in user_institution:
        text = f"{componentTypeName} {serialNumber} location is not your institution ({checked_component_location}))..."
        raise Exception( text )

    doc = { "location" : checked_component_location, "typeInfo" : SN_typeinfo( serialNumber ), "delim_SN" : delim_SN( serialNumber ) }
    
    logger.info( f'check_component(): serialNumber = {serialNumber}, doc = ' + pprint.pformat( doc ) )
    
    return doc

########################
### Download all results
@toppage_api.route("/download_allresult", methods=["GET", "POST"])
def download_allresult():

    if not session.get("logged_in", False):
        return render_template( "401.html" )

    stage = request.form.get("stage", "input")
    code1 = request.form.get("code1", "")
    code2 = request.form.get("code2", "")
    text = ""

    if os.path.exists(IF_DIR + '/doing_download.txt'):
        stage = "complete"

    if stage == "complete":
        flag = process_request(code1, code2)
        if flag == 0:
            stage = "input"
            text = "Not authorized. Please input correct codes."
        else:
            if not os.path.exists(IF_DIR + '/doing_download.txt'):
                try:
                    dic1 = {"code1": code1, "code2": code2}
                    thread_1 = threading.Thread(target=QCResultDownloader(code1,code2).download_all_results)
                    thread_1.start()
                    #download_QC_info(code1, code2, None)
                except:
                    stage = "input"
                    text = "An error occered. Please contact LocalDB admin."


    return render_template( "download_allresult.html", stage=stage, text=text )

#################
## Query Function
def query_docs(Keywords, Match):

    flag = 1
    run_ids = []
    if Keywords == "None" or Keywords == "":
        entries = userdb.viewer.query.find({})
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flag = 0
        OR_flag = 0
        print("Keyword_num", Keyword_num)
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flag = 1
            if Keyword[i].upper() == "OR":
                OR_flag = 1
        print("AND_flag", AND_flag)
        print("OR_flag", OR_flag)

        query_list = []
        if AND_flag == 1 and OR_flag == 1:
            entries = userdb.viewer.query.find({})
        elif AND_flag == 0 and OR_flag == 1:
            flag = 0
            for i in range(Keyword_num):
                if not Keyword[i] == "":
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            entries = userdb.viewer.query.find({"$or": query_list})
        else:
            Keyword_flag = []
            num = []
            for i in range(Keyword_num):
                if Keyword[i].upper() == "AND":
                    pass
                else:
                    if Match == "partial":
                        query_list.append({"data": {"$regex": Keyword[i]}})
                    if Match == "perfect":
                        query_list.append({"data": Keyword[i]})
            # Here also inside the viewer.query so I need to update this
            entries = userdb.viewer.query.find({"$and": query_list})

    components = sorted(entries, key=lambda x: x["timeStamp"], reverse=True)

    for component in components:
        run_ids.append(component["runId"])

    return run_ids


def query_keywords(Keywords, query_targets, Match):
    flag = 1
    if Keywords == "None" or Keywords == "":
        pass
    else:
        Keyword = Keywords.split(" ")
        Keyword_num = len(Keyword)
        AND_flag = 0
        OR_flag = 0
        for i in range(Keyword_num):
            if Keyword[i].upper() == "AND":
                AND_flag = 1
            if Keyword[i].upper() == "OR":
                OR_flag = 1

        if AND_flag == 1 and OR_flag == 1:
            pass
        elif AND_flag == 0 and OR_flag == 1:
            flag = 0
            for i in range(Keyword_num):
                for query_target in query_targets:
                    if Match == "partial":
                        if Keyword[i].upper() in str(query_target).upper():
                            flag = 1
                    if Match == "perfect":
                        if Keyword[i].upper() == str(query_target).upper():
                            flag = 1
        else:
            Keyword_flag = []
            num = []
            delete_num = []
            for i in range(Keyword_num):
                num.append(i)
                if Keyword[i].upper() == "AND":
                    delete_num.append(i)
                Keyword_flag.append(0)
                for query_target in query_targets:
                    if Match == "partial":
                        if Keyword[i].upper() in str(query_target).upper():
                            Keyword_flag[i] = 1
                    if Match == "perfect":
                        if Keyword[i].upper() == str(query_target).upper():
                            Keyword_flag[i] = 1
            query_num = list(set(num) - set(delete_num))
            for i in query_num:
                if Keyword_flag[i] == 0:
                    flag = 0
    return flag


def check_database_update():

    config_doc = userdb.viewer.query.find_one({"runId": "config"})
    #    query_list = [{"sys.cts": {"$gt":config_doc["sys"]["mts"]}},{"sys.mts": {"$gt":config_doc["sys"]["mts"]}}]
    #    append_docs = localdb.testRun.find({'$or':query_list}).sort([('startTime', -1)])
    queries = {"sys.cts": {"$gte": config_doc["sys"]["mts"]}}

    append_docs = list(localdb.testRun.find(queries).sort([("startTime", -1)]))

    for entry in append_docs:
        userdb.viewer.query.remove({"runId": str(entry["_id"])})
        docs = createScanCache(entry)
        userdb.viewer.query.insert_one(docs)
        
    userdb.viewer.query.update(
        {"runId": "config"}, {"$set": {"sys.mts": datetime.utcnow()}}
    )
    userdb.viewer.query.update(
        {"runId": "config"}, {"$set": {"sys.rev": config_doc["sys"]["rev"] + 1}}
    )

    return 0




