from functions.imports import *

register_api = Blueprint("register_api", __name__)


@register_api.route("/register_chip", methods=["GET", "POST"])
def register_chip():
    stage = request.form.get("stage", "input")
    chipinfo = request.form.getlist("chipinfo")
    Address = localdb.pd.institution.find({})
    if chipinfo == []:
        session.pop("chip_register", None)

    if session.get("chip_register", None):
        query = {"serialNumber": chipinfo[0]}
        thistime = datetime.utcnow()
        chips = localdb.component.find(query)

        if not session.get("logged_in", None):
            text = "Please login."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                Address=Address,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if not chips.count() == 0:
            text = "This chipname is already in use."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                Address=Address,
                nametext2=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if chipinfo[3] == "other" and chipinfo[4] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "register_chip.html",
                chipInfo=chipinfo,
                Address=Address,
                nametext3=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        else:
            if stage == "input":
                return render_template(
                    "register_chip.html",
                    chipInfo=chipinfo,
                    Address=Address,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            if stage == "confirm":
                return render_template(
                    "register_chip.html",
                    chipInfo=chipinfo,
                    Address=Address,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            else:
                query = {
                    "userName": session["username"],
                    "description": "viewer",
                    "dbVersion": dbv,
                }
                userinfo = localdb.user.find_one(query)
                if userinfo == None:
                    add_userinfo(session["username"], session["institution"], dbv)
                    userinfo = localdb.user.find_one(query)
                if chipinfo[3] == "other":
                    query = {"institution": chipinfo[4]}
                else:
                    query = {"institution": chipinfo[3]}
                addressinfo = localdb.pd.institution.find_one(query)
                if addressinfo == None:
                    if not chipinfo[4] == "":
                        com_address = (
                            ":".join(
                                [
                                    "{:02x}".format((uuid.getnode() >> ele) & 0xFF)
                                    for ele in range(0, 8 * 6, 8)
                                ][::-1]
                            ),
                        )
                        add_addressinfo(com_address, chipinfo[4], dbv)
                        query = {"institution": chipinfo[4]}
                        addressinfo = localdb.pd.institution.find_one(query)
                thistime = datetime.utcnow()
                localdb.component.insert_one(
                    {
                        "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                        "serialNumber": chipinfo[0],
                        "componentType": "front-end_chip",
                        "chipType": chipinfo[1],
                        "name": chipinfo[0],
                        "chipId": int(chipinfo[2]),
                        "address": str(addressinfo["_id"]),
                        "user_id": str(userinfo["_id"]),
                        "children": -1,
                        "proDB": False,
                        "dbVersion": dbv,
                    }
                )
                session.pop("chip_register")
                return render_template(
                    "register_chip.html",
                    chipInfo=chipinfo,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )

    chipinfo = ["", "", "", "", ""]
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["chip_register"] = True

    return render_template(
        "register_chip.html",
        chipInfo=chipinfo,
        Address=Address,
        stage=stage,
        timezones=pytz.all_timezones,
    )


@register_api.route("/register_module", methods=["GET", "POST"])
def register_module():
    query = {"componentType": "front-end_chip", "dbVersion": dbv}
    chip_entries = localdb.component.find(query)
    Address = localdb.pd.institution.find({})
    unregistered_chips = []
    for chip in chip_entries:
        query = {"child": str(chip["_id"])}
        registered_chip = localdb.childParentRelation.find_one(query)
        if registered_chip == None:
            unregistered_chips.append(chip)

    stage = request.form.get("stage", "input")
    moduleinfo = request.form.getlist("moduleinfo")
    Row = request.form.get("row")
    Col = request.form.get("col")
    row = Row
    col = Col
    if moduleinfo == []:
        session.pop("module_register", None)

    if session.get("module_register", None):
        query = {"serialNumber": moduleinfo[0]}
        thistime = datetime.utcnow()
        modules = localdb.component.find(query)
        if int(Row) > int(Col):
            row = Col
            col = Row
        row = int(row)
        col = int(col)
        if not session.get("logged_in", None):
            text = "Please login."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                Address=Address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if not modules.count() == 0:
            text = "This module is already registered."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                Address=Address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext2=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        if moduleinfo[2] == "other" and moduleinfo[3] == "":
            text = "Please fill out this brank."
            stage = "input"
            return render_template(
                "register_module.html",
                chips=unregistered_chips,
                Address=Address,
                moduleInfo=moduleinfo,
                Row=row,
                Col=col,
                nametext3=text,
                stage=stage,
                timezones=pytz.all_timezones,
            )
        else:
            if stage == "input":
                return render_template(
                    "register_module.html",
                    chips=unregistered_chips,
                    Address=Address,
                    moduleInfo=moduleinfo,
                    Row=row,
                    Col=col,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            if stage == "input2":
                return render_template(
                    "register_module.html",
                    chips=unregistered_chips,
                    Address=Address,
                    moduleInfo=moduleinfo,
                    Row=row,
                    Col=col,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            if stage == "confirm":
                chipId_list = []
                for i in range(int(row)):
                    for j in range(int(col)):
                        if moduleinfo[2 * i + j + 4] == "":
                            text = "Please fill out the brank."
                            stage = "input2"
                            return render_template(
                                "register_module.html",
                                chips=unregistered_chips,
                                Address=Address,
                                moduleInfo=moduleinfo,
                                Row=row,
                                Col=col,
                                nametext4=text,
                                stage=stage,
                                timezones=pytz.all_timezones,
                            )
                        else:
                            this_chip = localdb.component.find_one(
                                {"serialNumber": moduleinfo[2 * i + j + 4]}
                            )
                            chipId_list.append(this_chip["chipId"])
                    if not len(chipId_list) == len(set(chipId_list)):
                        text = (
                            "Please select chips so that each chipId does not overlap."
                        )
                        stage = "input2"
                        return render_template(
                            "register_module.html",
                            chips=unregistered_chips,
                            Address=Address,
                            moduleInfo=moduleinfo,
                            Row=row,
                            Col=col,
                            nametext4=text,
                            stage=stage,
                            timezones=pytz.all_timezones,
                        )
                return render_template(
                    "register_module.html",
                    chips=unregistered_chips,
                    Address=Address,
                    moduleInfo=moduleinfo,
                    Row=row,
                    Col=col,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )
            else:
                thistime = datetime.now()
                query = {
                    "userName": session["username"],
                    "description": "viewer",
                    "dbVersion": dbv,
                }
                userinfo = localdb.user.find_one(query)
                if userinfo == None:
                    add_userinfo(session["username"], session["institution"], dbv)
                    userinfo = localdb.user.find_one(query)
                if moduleinfo[2] == "other":
                    query = {"institution": moduleinfo[3]}
                else:
                    query = {"institution": moduleinfo[2]}
                addressinfo = localdb.pd.institution.find_one(query)
                if addressinfo == None:
                    if not moduleinfo[3] == "":
                        com_address = (
                            ":".join(
                                [
                                    "{:02x}".format((uuid.getnode() >> ele) & 0xFF)
                                    for ele in range(0, 8 * 6, 8)
                                ][::-1]
                            ),
                        )
                        add_addressinfo(com_address, moduleinfo[3], dbv)
                        query = {"institution": moduleinfo[3]}
                        addressinfo = localdb.pd.institution.find_one(query)
                localdb.component.insert_one(
                    {
                        "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                        "serialNumber": moduleinfo[0],
                        "componentType": "module",
                        "chipType": moduleinfo[1],
                        "name": moduleinfo[0],
                        "chipId": -1,
                        "address": str(addressinfo["_id"]),
                        "user_id": str(userinfo["_id"]),
                        "children": int(row) * int(col),
                        "proDB": False,
                        "dbVersion": dbv,
                    }
                )

                chipId_list = []
                for i in range(int(row)):
                    for j in range(int(col)):
                        #                        localdb.component.remove({'serialNumber':moduleinfo[2*i+j+4]})
                        #                        localdb.component.insert_one(
                        #                        {
                        #                            'sys'          : { 'cts': thistime,'mts': thistime,'rev': 0},
                        #                            'serialNumber' : moduleinfo[2*i+j+4],
                        #                            'componentType': 'front-end_chip',
                        #                            'chipType'     : moduleinfo[1],
                        #                            'name'         : moduleinfo[2*i+j+4],
                        #                            'chipId'       : this_chip['chipId'],
                        #                            'address'      : this_chip['address'],
                        #                            'user_id'      : this_chip['user_id'],
                        #                            'children'     : -1,
                        #                            'proDB'        : False,
                        #                            'dbVersion'    : dbv
                        #                        }
                        #                        )
                        this_chip = localdb.component.find_one(
                            {"serialNumber": moduleinfo[2 * i + j + 4]}
                        )
                        query = {
                            "serialNumber": moduleinfo[0],
                            "componentType": "module",
                        }
                        parent = localdb.component.find_one(query)
                        query = {
                            "serialNumber": moduleinfo[2 * i + j + 4],
                            "componentType": "front-end_chip",
                        }
                        child = localdb.component.find_one(query)
                        localdb.childParentRelation.insert_one(
                            {
                                "sys": {"cts": thistime, "mts": thistime, "rev": 0},
                                "parent": str(parent["_id"]),
                                "child": str(child["_id"]),
                                "chipId": this_chip["chipId"],
                                "geomId": {"x": j, "y": i},
                                "status": "active",
                                "dbVersion": dbv,
                            }
                        )

                session.pop("module_register")
                return render_template(
                    "register_module.html",
                    chips=unregistered_chips,
                    Address=Address,
                    moduleInfo=moduleinfo,
                    Row=row,
                    Col=col,
                    stage=stage,
                    timezones=pytz.all_timezones,
                )

    moduleinfo = ["", "", "", ""]
    row = ""
    col = ""
    pre_url = request.headers.get("Referer")
    session["pre_url"] = pre_url

    session["module_register"] = True

    return render_template(
        "register_module.html",
        chips=unregistered_chips,
        Address=Address,
        moduleInfo=moduleinfo,
        Row=row,
        Col=col,
        stage=stage,
        timezones=pytz.all_timezones,
    )


def add_userinfo(username, institution, dbv):
    thistime = datetime.utcnow()
    localdb.user.insert_one(
        {
            "userName": username,
            "institution": institution,
            "description": "viewer",
            "USER": os.environ["USER"],
            "HOSTNAME": "%s" % os.uname()[1],
            "sys": {"cts": thistime, "mts": thistime, "rev": 0},
            "userType": "readWrite",
            "dbVersion": dbv,
        }
    )

    return


def add_addressinfo(com_address, institution, dbv):
    thistime = datetime.utcnow()
    localdb.pd.institution.insert_one(
        {
            "address": com_address[0],
            "HOSTNAME": "%s" % os.uname()[1],
            "institution": institution,
            "sys": {"cts": thistime, "mts": thistime, "rev": 0},
            "dbVersion": dbv,
        }
    )

    return
