from functions.checkouter import *
from functions.imports import *

# import module for metrology
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.mplot3d import axes3d
from scipy import linalg
import sys
import re
from io import open
import threading

from datetime import datetime,date
import time
import hashlib

checkout_api = Blueprint("checkout_api", __name__)


@checkout_api.route( '/pixel_failure_test', methods=['GET', 'POST'] )
def pixel_failure_test():
    
    initPage()
    
    session['regeneratePlots'] = request.form.get('regeneratePlots', type=bool, default = False )
    session['expandPlots']     = request.form.get('expandPlots', type=bool, default = False )
    session['logged_in']       = session.get('logged_in',False)
    
    # If not logged in, prohibit operation
    if not session['logged_in']:
        return render_template('401.html')
        
    
    # At the beginning, 3 parameters are passed from the browser:
    #  - 'componentID': componentID of LocalDB
    #  - 'stage': stage of the QC
    #  - 'test':  name of the Test
    
    m_doc = { 'page' : 'pixel_failure_test' }
    
    # Define 4 states: selection, processing, confirmation, completed
    processFunc = { 'not_logged_in':processPFT_not_logged_in,
                    'selection'    :processPFT_selection,
                    'complete'     :processPFT_complete         }
    
    # Retrieve the previous record of the checkout record using hash
    docHash = request.form.get('documentHash', '')
    logger.info('docHash from session = {}'.format( docHash ) )
    
    if docHash != '':
        
        logger.info( 'attempt to retrieve m_doc from hash = {}'.format( docHash ) )
        
        try:
            m_doc = userdb.QC.checkout.find_one( { 'hash':docHash } )
            logger.info( 'successfully retrieved m_doc, hash = {}'.format( docHash ) )
            
        except Exception as e:
            logger.warning('{}'.format( e ) )
            pass
            
    
    # Update the current state of the processing
    m_doc['processState'] = request.form.get('processState', 'selection')
    m_doc['componentID']  = request.args.get('componentID', '')

    logger.info( f'processState = {m_doc["processState"]}' )
    
    if m_doc['processState'] == 'selection':
        component = localdb.component.find_one( {'_id':ObjectId(m_doc['componentID']) } )
        m_doc['componentName'] = str( component.get('name'))
        m_doc['address']       = str( component.get('address'))
        m_doc['stage']         = request.args.get('stage', '')
        m_doc['test']          = request.args.get('test', '')
        m_doc['messageText']   = ''

    try:
        module_sn = get_module_sn_from_fe_sn( m_doc["componentName"] )
        # MongoDB interface to organize the scan documents and carry out analysis
        checkouter = ScanCheckoutManager( m_doc['componentName'], m_doc['stage'], module_sn )
        
        m_doc = processFunc[ m_doc['processState'] ]( m_doc, checkouter )
        
    except Exception as e:
        logger.warning('{}'.format( e ) )
        pass
        
    if m_doc['processState'] == 'selection':
        t_today = datetime.now()
        s_today = t_today.strftime('%Y/%m/%d %H:%M:%S')
        m_doc['hash'] = hashlib.sha256(s_today.encode('utf-8')).hexdigest()
        
        logger.info( 'document hash = {}'.format( m_doc['hash'] ) )
        
    # Record the checkout contents to the server's memory    
    if session['logged_in']:
        userdb.QC.checkout.remove( { 'hash':m_doc['hash'] } )
        userdb.QC.checkout.insert_one( m_doc )
        
    if m_doc['processState'] != 'complete':
        return render_template( 'yarr_scans/select_table.html', m_doc = m_doc, timezones = pytz.all_timezones )

    return redirect( url_for( 'component_api.show_component', id = m_doc['componentID'], collection='component' ) )




def processPFT_not_logged_in( m_doc, checkouter ):
    
    return m_doc


def processPFT_selection( m_doc, checkouter ):

    logger.info( 'processPFT_selection' )
    
    m_doc['run']   = []
    m_doc['scans'] = {}
    m_doc['scans']['analysis_result'] = []
    m_doc['scans'].update(checkouter.getScans(m_doc['test']))
    m_doc['title'] = 'Select scan results for QC result in this stage.'
    m_doc['run'] = checkouter.createFullSummary()
    
    return m_doc


def processPFT_complete( m_doc, checkouter ):
    
    logger.info( 'processPFT_complete' )
    
    m_doc['title'] = 'Analysis process is running in the background. Stay tuned...'
    m_doc['selectedScans'] = {}
    
    this_run_ids = []
    run_ids_without_blank = []
    for scantype in m_doc["scans"]["scantypes"]:
        tr_oid = request.form.get(scantype + "_id")

        logger.info( f'selected {tr_oid} for scantype {scantype}' )
        this_run_ids.append(tr_oid)
        
        m_doc['selectedScans'][scantype] = tr_oid
        
    for _id in this_run_ids:
        if _id != "":
            run_ids_without_blank.append(_id)

    applyAllFEs = request.form.get('applyAllFEs', type=bool, default=False)

    if not applyAllFEs:
        cpt = localdb.component.find_one( {'_id': ObjectId(m_doc.get('componentID')) } )
        m_doc['serialNumber'] = cpt.get('serialNumber')
        
        docs = [ m_doc ]

    else:

        docs = []

        for cpr in localdb.childParentRelation.find( { 'child' : m_doc.get('componentID') } ):
            parent = localdb.component.find_one( { '_id':ObjectId( cpr.get('parent') ) } )
            if parent:
                if parent.get('componentType') == 'bare_module':
                    break

        for cpr in localdb.childParentRelation.find( {'parent' : str(parent.get('_id')) } ):

            doc = copy.deepcopy( m_doc )
            doc['componentID'] = cpr.get('child')

            cpt = localdb.component.find_one( {'_id': ObjectId(cpr.get('child')) } )
            doc['serialNumber'] = cpt.get('serialNumber')
            
            docs += [ doc ]


    for doc in docs:

        thread = threading.Thread( target = complex_analysis_worker, args = ( doc, run_ids_without_blank ) )
        thread.start()

    logger.info('processPFT_complete(): analysis is done. Switch to confirmation')
    return docs[0]
    


def complex_analysis_worker( doc, run_ids_without_blank ):
    
    module_sn = get_module_sn_from_fe_sn( doc['serialNumber'] )
    
    checkouter = ScanCheckoutManager( doc['serialNumber'], doc['stage'], module_sn )
    
    msg_id = create_message( f'INFO: {doc.get("test")} processing for {doc.get("serialNumber")} is ongoing',
                             'complex_analysis_worker',
                             doc.get('serialNumber'),
                             'INFO_COMPLEX_ANALYSIS_PROGRESS', False )
    
    testRun_id = checkouter.createElecTestDocs( run_ids_without_blank, doc )
    
    doc['processState'] = 'complete'
    doc['title'] = 'Check the analysis result and proceed.'
    
    userdb.message.delete_one( {'_id':msg_id} )
    
    create_message( f'INFO: {doc.get("test")} processing for {doc.get("serialNumber")} is done',
                    'complex_analysis_worker',
                    doc.get('serialNumber'),
                    'INFO_COMPLEX_ANALYSIS_PROGRESS', True )


def get_module_sn_from_fe_sn( fe_sn ):
    cpt_doc = localdb.component.find_one( { 'serialNumber' : fe_sn } )
    cprs = list( localdb.childParentRelation.find( { 'child' : str( cpt_doc.get('_id') ) } ) )

    logger.info( pprint.pformat( cprs ) )

    module_sn = None
    for cpr in cprs:
        parent_doc = localdb.component.find_one( { '_id' : ObjectId( cpr.get('parent') ) } )
        logger.info( pprint.pformat( parent_doc ) )
        if parent_doc.get('componentType') == 'module':
            module_sn = parent_doc.get('serialNumber')
            break

    return module_sn

