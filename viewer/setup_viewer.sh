#!/bin/bash
cd `dirname $0`
##################################################################
# Start the Viewer Application
# Usage : ./setup_viewer.sh [-i IP address] [-p port]
# Instruction : https://github.com/jlab-hep/Yarr/wiki/Installation
#
# Contacts : Arisa Kubota (kubota.a.af@m.titech.ac.jp)
##################################################################
ITSNAME="[LDB]"

# Usage
function usage {
    cat <<EOF

Make the config file and setup for the viewer application by:

    ./setup_viewer.sh [-i ip address] [-p port] [-c config]

Options:
    - i <IP address>  Local DB server IP address (default: 127.0.0.1)
    - p <port>        Local DB server port (default: 27017)
    - c <config>      Config file name (default: user_conf.yml or admin_conf.yml)

EOF
}

if [ `echo ${0} | grep bash` ]; then
    echo -e "${ITSNAME} DO NOT 'source'"
    usage
    return
fi

dbip=mongodb
dbport=27017
user_config=user_conf.yml
admin_config=admin_conf.yml

while getopts hi:p:c: OPT
do
    case ${OPT} in
        h ) usage
            exit;;
        i ) dbip=${OPTARG} ;;
        p ) dbport=${OPTARG} ;;
        c ) user_config=${OPTARG}
            admin_config=${OPTARG} ;;
        * ) usage
            exit ;;
    esac
done

### Confirmation
tools_dir=$(cd $(dirname $0); pwd)

# MONGO=mongo
# which mongo > /dev/null 2>&1
# if [ $? = 1 ]; then
#     which mongosh > /dev/null 2>&1
#     if [ $? = 1 ]; then
# 	printf '\033[31m%s\033[m\n' "[ERROR] 'mongo' command is required."
# 	exit 1
#     else
# 	MONGO=mongosh
#     fi
# fi

md5command=md5sum
which ${md5command} > /dev/null 2>&1
if [ $? = 1 ]; then
    md5command=md5
    which md5 >/dev/null 2>&1
    if [ $? = 1 ]; then
        printf '\033[31m%s\033[m\n' "[ERROR] 'md5sum' or 'md5' command is required."
        exit 1
    fi
fi

# Check python modules
/usr/bin/env python3 ${tools_dir}/../setting/check_python_modules.py || exit
echo -e "${ITSNAME}"

echo "Local DB Server IP address: ${dbip}"
echo "Local DB Server port: ${dbport}"
echo " "
echo "${ITSNAME} Are you sure that's correct? [y/n]"
unset answer
read -p "> " answer
while [ -z ${answer} ];
do
echo "${ITSNAME} Are you sure that's correct? [y/n]"
    read -p "> " answer
done
echo " "
if [ ${answer} != "y" ]; then
    printf '\033[31m%s\033[m\n' "[ERROR] Try again setup_viewer.sh, Exit ..."
    exit 1
fi

check if using admin function
echo "${ITSNAME} Do you use admin functions for LocalDB viewer? [y/n]"
unset Answer
read -p "> " Answer
while [ -z ${Answer} ];
do
echo "${ITSNAME} Do you use admin functions for LocalDB viewer? [y/n]"
    read -p "> " Answer
done
echo " "

if [ ${Answer} = "y" ]; then
    # input username and password
    read -p "Input localDB admin's username: " user
    if [ -z ${user} ]; then
        printf '\033[31m%s\033[m\n' "[ERROR] Does not match the administrator account."
        printf '\033[31m%s\033[m\n' "        Please input correct password. Try again setup_viewer.sh, Exit ..."
        exit 1
    fi

    read -sp "Input localDB admin's password: " password
    if [ -z ${password} ]; then
        printf '\033[31m%s\033[m\n' "[ERROR] Does not match the administrator account."
        printf '\033[31m%s\033[m\n' "        Please input correct password. Try again setup_viewer.sh, Exit ..."
        exit 1
    fi
    echo " "

    user_hash=`echo -n ${user}|${md5command}|sed -e "s/-//"|sed -e "s/ //g"`
    password_hash=`echo -n ${password}|${md5command}|sed -e "s/-//"|sed -e "s/ //g"`
    admin="-u ${user} -p ${password}"

    # confirmation
    python3 << EOF
import sys
from pymongo import MongoClient, errors
url = 'mongodb://${dbip}:${dbport}'
client = MongoClient(url,serverSelectionTimeoutMS=1)
localdb = client['localdb']
try:
    localdb.authenticate('${user}','${password}')
    sys.exit(0)
except errors.OperationFailure as err:
    print(err)
    sys.exit(1)
EOF
    if [ $? = 1 ]; then
        echo -e ""
        printf '\033[31m%s\033[m\n' "[ERROR] You cannot connect this LocalDB because protected."
        printf '\033[31m%s\033[m\n' "        Please input correct admin's information or check the address"
        exit 1
    else
        echo -e ""
        printf '%s\n' "${ITSNAME} Authentication succeeded!"
    fi

    # confirmation of config file existence
    admin_config=${tools_dir}/${admin_config}
    admin_KeyFile=${tools_dir}/.KeyFile
    echo -e "${ITSNAME} Setting the configuration file: ${admin_config}"
    if [ -f ${admin_config} ]; then
        echo -e "${ITSNAME} The file already exists."
        echo -e "${ITSNAME} Will you reset the file? [y/n]"
        unset answer
        read -p "> " answer
        while [ -z ${answer} ]; do
            echo -e "${ITSNAME} Will you reset the file? [y/n]"
            read -p "> " answer
        done
        echo -e ""
        if [ ${answer} = "y" ]; then
            rm ${admin_config}
        fi
    fi

    # create config file
    if [ ! -f ${admin_config} ]; then
        if [ -f ${admin_KeyFile} ]; then
            rm ${admin_KeyFile}
        fi
        if [ ! -f ${admin_KeyFile} ]; then
            keyfile=".KeyFile"
            echo ${user_hash} > ${keyfile}
            echo -n ${password_hash} >> ${keyfile}
            chmod 700 ${keyfile}
            chown ${USER} ${keyfile}
        fi
        cp ${tools_dir}/../scripts/yaml/web-conf.yml ${admin_config}
        sed -i -e "s/DBIPADDRESS/${dbip}/g" ${admin_config}
        sed -i -e "s/DBPORT/${dbport}/g" ${admin_config}
        sed -i -e "s/#localdbkeypass/localdbkeypass/g" ${admin_config}
        sed -i -e "s!localdbkeypass!.KeyFile!" ${admin_config}
        chmod 700 ${admin_config}
        chown ${USER} ${admin_config}
    fi
    config=${admin_config}

else
    # confirmation of config file existence
    user_config=${tools_dir}/${user_config}
    echo -e "${ITSNAME} Setting the configuration file: ${user_config}"
    if [ -f ${user_config} ]; then
        echo -e "${ITSNAME} The file already exists."
        echo -e "${ITSNAME} Will you reset the file? [y/n]"
        unset answer
        read -p "> " answer
        while [ -z ${answer} ]; do
            echo -e "${ITSNAME} Will you reset the file? [y/n]"
            read -p "> " answer
        done
        echo -e ""
        if [ ${answer} = "y" ]; then
            rm ${user_config}
        fi
     fi
    # create config file
    if [ ! -f ${user_config} ]; then
        cp ${tools_dir}/../scripts/yaml/web-conf.yml ${user_config}
        sed -i -e "s/DBIPADDRESS/${dbip}/g" ${user_config}
        sed -i -e "s/DBPORT/${dbport}/g" ${user_config}
    fi
    config=${user_config}
fi
echo -e "${ITSNAME}"

# # Check tools
# echo -e "${ITSNAME} Check plotting tool and analysis tool..."
# # Check root
# root=true
# which root > /dev/null 2>&1
# if [ $? != 0 ]; then
#     root=false
#     echo -e "${ITSNAME} Set ROOT installed directroy. (Enter path/to/dir or 'skip')"
#     unset answer
#     read -e -p "> " answer
#     while [ -z ${answer} ]; do
#         echo -e "${ITSNAME} Set ROOT installed directroy. (Enter path/to/dir or 'skip')"
#         read -e -p "> " answer
#     done
#     if [ ${answer} != "skip" ]; then
#         echo -e ""
#         ROOT_DIR=${answer}
#         if [ ! -d ${ROOT_DIR} ]; then
#             echo -e "${ITSNAME} Not found ROOT directory, Exit ..."
#             echo -e ""
#             exit
#         fi
#         source ${ROOT_DIR}/bin/thisroot.sh
#     fi
# fi

# #git submodule update -i --recursive
# #git submodule foreach git pull origin master

# cd ${tools_dir}/plotting-tool
# make >/dev/null
# cd - >/dev/null

#cd ${tools_dir}/analysis-tool
#make >/dev/null
#cd - >/dev/null
#echo -e "${ITSNAME}"

# thumbnail cache directory
mkdir -p ${tools_dir}/static/cache/thumbnail


pythonabs=`which python3`
sudo sh -c "cat service.template | \
sed -e \"s@USER@${USER}@g\" \
    -e \"s@WORKDIR@${tools_dir}@g\" \
    -e \"s@EXECSTART@${pythonabs} ${tools_dir}/app.py --config ${config}@g\" \
> /etc/systemd/system/localdb.viewer.service"

sudo systemctl daemon-reload

# echo -e "${ITSNAME} Finished setting up of Viewer Application!!"
# echo -e "${ITSNAME} "
# if ! "${root}"; then
#     echo -e "${ITSNAME} Enable ROOT functions by..."
#     echo -e "${ITSNAME}   source ${ROOT_DIR}/bin/thisroot.sh"
#     echo -e "${ITSNAME} "
# fi
echo -e "${ITSNAME} Start Viewer Webapp locally by ..."
echo -e "${ITSNAME}   python3 ${tools_dir}/app.py --config ${config}"
echo -e "${ITSNAME} "
echo -e "${ITSNAME} Start Viewer Webapp as a service:"
echo -e "${ITSNAME}   sudo systemctl enable localdb.viewer"
echo -e "${ITSNAME}   sudo systemctl start localdb.viewer"
echo -e "${ITSNAME} "
echo -e "${ITSNAME} For exposing the port 5000 outside the firewall:"
echo -e "${ITSNAME}   firewall-cmd --port=5000/tcp --zone=public --permanent"
echo -e "${ITSNAME}   firewall-cmd --reload"
echo -e "${ITSNAME} "
echo -e "${ITSNAME} Access the Local DB page on web browser ..."
echo -e "${ITSNAME}   http://localhost:5000/localdb/"
echo -e "${ITSNAME} "
echo -e "${ITSNAME} More information: https://localdb-docs.readthedocs.io/en/master/"

