#!/usr/bin/env python3
# -*- coding: utf-8 -*

import hashlib
import pickle
import pprint
import shutil
from pathlib import Path
from bson import json_util

from functions.imports import *

from datetime import datetime, timezone

import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.utils import (
    get_layer_from_serial_number,
)

import threading

import zipfile
import os

def make_archive_threadsafe(zip_name: str, path: str):
    with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zfile:
        for root, dirs, files in os.walk(path):
            for file in files:
                zfile.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), path))



##########################
## For selecting functions for electrical tests
class ScanCheckoutManager:

    def __init__( self, component_serial_number, stage_name, module_serial_number = None ):
        self.component_sn = component_serial_number
        self.sname = stage_name
        self.sname_alt = self.sname.replace('/', '__')
        self.module_sn = module_serial_number

        logger.info( f'ScanCheckoutManager.__init__(): component_sn = {self.component_sn}, module_sn = {self.module_sn}, stage_name = {stage_name}' )

        with open("{}/json-lists/scan_list.json".format(VIEWER_DIR)) as f:
            self.test_scan_map = json.load(f)


    def getScans(self, test):
        doc = {}
        if test in self.test_scan_map:
            doc["test"]      = test
            doc["setting_temp"] = self.test_scan_map[test]["setting_temp"]
            doc["scantypes"] = self.test_scan_map[test]["scantypes"]
            doc["analysis"]  = self.test_scan_map[test]["analysis"]

        if doc == {}:
            doc["test"]      = "ELECTRICAL_TEST"
            doc["setting_temp"] = None
            doc["scantypes"] = self.test_scan_map["ELECTRICAL_TEST"]["scantypes"]
            doc["analysis"]  = self.test_scan_map["ELECTRICAL_TEST"]["analysis"]

        return doc

    def getScansFromCtr(self):

        logger.info( 'ScanCheckoutManager.getScansFromCtr()' )

        tr_queries_all = [ ctr for ctr in localdb.componentTestRun.find( {"name": self.component_sn} ) ]

        return tr_queries_all

    def checkScanItems(self, temp, test_list):

        tr_queries = self.getScansFromCtr(temp)

        if not tr_queries:
            logger.info( 'ScanCheckoutManager.checkScanItems(): no tr_queries matched for temp = {}'.format( temp ) )
            return test_list

        needed_scan = []
        for testtype in test_list:
            logging.debug( 'checkScanItems(): testtype = {}, stage = {}'.format( testtype, self.sname ) )
            query = {"$and": [{"$or": tr_queries}, {"stage": self.sname}, {"testType": testtype}]}
            tr_docs = localdb.testRun.find_one( query )
            logging.debug( 'checkScanItems(): testtype = {}, tr_docs = {}'.format( testtype, tr_docs ) )
            if tr_docs == None: needed_scan.append(testtype)

        return needed_scan


    def createFullSummary(self):

        run_ids = []
        tr_queries_matched = self.getScansFromCtr()

        for tr in tr_queries_matched:
            tr_docs = localdb.testRun.find( { '_id':ObjectId( tr['testRun'] ), 'stage':self.sname } )
            tr_docs = sorted(tr_docs, key=lambda x: x["timestamp"], reverse=True)
            for tr_doc in tr_docs:
                #logger.info( str(tr_doc["_id"]) )
                run_ids.append(str(tr_doc["_id"]))

        list_ = []

        run_counts = 0

        for run in run_ids:
            run_data = getScanSummary( run )
            run_counts += 1
            list_.append( { "run_data": run_data, "nrun": run_counts } )

        #list_ = sorted( list_, key=lambda x: x["run_data"]["datetime"], reverse=True )
        return list_


    def createPartSummary( self, run_ids, scantype ):
        list_ = []
        for index,tr_oid in enumerate(run_ids):
           if tr_oid != "":
                query = {"_id": ObjectId(tr_oid)}
                this_run = localdb.testRun.find_one(query)
                testType = this_run["testType"]
                rn = this_run["runNumber"]
                list_.append({
                      "_id": tr_oid,
                      "testType": this_run["testType"],
                      "time": setTime(
                          this_run["startTime"],
                          session.get("timezone", str(get_localzone())),
                      ),
                      "rn"  : rn,
                      'temp': self.getModuleNtcTemp( tr_oid )
                    })
           else:
               list_.append({
                     "_id": tr_oid,
                     "testType": scantype[index],
                     "time":None,
                     "rn"  : None,
                     'temp': None
                   })

        return list_

    def createSourceSummary( self, run_ids ):
        list_ = []
        for tr_oid in run_ids:
            query = {"_id": ObjectId(tr_oid)}
            this_run = localdb.testRun.find_one(query)
            testType = this_run["testType"]
            rn = this_run["runNumber"]

            list_.append({
                      "_id": tr_oid,
                      "testType": testType,
                      "time": setTime(
                          this_run["startTime"],
                          session.get("timezone", str(get_localzone())),
                      ),
                      "rn"  : rn,
                      'temp': self.getModuleNtcTemp( tr_oid )
                    })
        return list_

    def createElecTestDocs( self, run_ids, m_doc ):

        anaResult = self.getAnalysisResult( run_ids, m_doc.get('test') )

        anaResult['component'] = m_doc.get('componentID')
        anaResult['address']  = m_doc.get('address')
        anaResult['testType'] = m_doc.get('test')
        anaResult['stage'] = m_doc.get('stage')
        anaResult['sys']  = { "mts":datetime.now(timezone.utc),"cts":datetime.now(timezone.utc),"rev":0 }

        # for TUNING, we mark the last config
        if m_doc.get('test') == 'TUNING':
            testRunId = sorted( run_ids )[-1]
            ctr = localdb.componentTestRun.find_one( { 'testRun' : testRunId, 'component' : m_doc.get('componentID') } )
            config_id = ctr.get('config_id')
            config_revision = ctr.get('config_revision_current')
            anaResult['config_id'] = config_id
            anaResult['config_revision'] = config_revision

            # layer option to be fetched from module serial number
            for cpr in localdb.childParentRelation.find( { 'child' : anaResult.get('component') } ):
                parent = localdb.component.find_one( { '_id':ObjectId( cpr.get('parent') ) } )
                if parent:
                    if parent.get('componentType') == 'module':
                        moduleSN = parent.get('serialNumber')
                        anaResult['layer_option'] = get_layer_from_serial_number( moduleSN )
                        break


        try:
            anaResult['user']      = session['username']
        except:
            anaResult['user']      = None


        # gather attachment files
        try:
            # seek the analysis result file inside the output path
            output=None
            output_others = []
            for curdir, dirs, files in os.walk( self.run_dir + '/output' ):
                for filename in files:
                    if 'json' in filename:
                        continue
                    else:
                        output_others += [ os.path.join(curdir, filename) ]

            logger.info( f'[{threading.current_thread().name}] subsidiary files: {output_others}' )

        except Exception as e:
            logger.warning( str(e) )
            logger.warning( traceback.format_exc() )
            raise Exception( f'[{threading.current_thread().name}] Null output file is detected: the analysis comand\n\n{command}\n\nmay have an issue\n\n\nstdout:\n\n{subout.stdout}' )


        # attach other files first
        attachments = {}
        for attachment in output_others:
            if any( attachment.endswith(ext) for ext in ['.jpg', 'jpeg', 'png', '.JPG', '.JPEG', '.PNG', '.pdf', '.log' ] ):
                item = { str(Path(attachment).name) : self.submit_binary( attachment ) }
                attachments.update( item )
                logger.info( f'[{threading.current_thread().name}] submitted {attachment} to localdb gridfs: {item}' )

        logger.info( 'attachments = ' + pprint.pformat( attachments ) )

        anaResult['gridfs_attachments'] = attachments


        testRun_id = localdb.QC.result.insert_one( anaResult ).inserted_id
        logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): created a new TestRun {testRun_id}' )

        # zipping the raw input
        logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): zipping the input files...' )
        make_archive_threadsafe(f'{self.run_dir}.zip', self.run_dir)

        zfile = Path( self.run_dir+'.zip' )
        with zfile.open('rb') as f:
            binary = f.read()
            md5 = hashlib.md5(binary).hexdigest()

            if localdb.fs.files.find_one( { "md5" : md5 } ) is None:
                archive_id = fs.put( binary )
                logging.info( f'createElecTestDocs(): the zip file {zfile.name} is new, uploaded to gridfs. md5 = {md5}' )
            else:
                logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): identical binary was detected in gridfs, not pushing. md5 = {md5}' )
                archive_id = localdb.fs.files.find_one( { "md5" : md5 } ).get("_id")

            if archive_id != None:
                attachments[zfile.name] = archive_id
                localdb.QC.result.update_one( { '_id': ObjectId(testRun_id) }, { '$set': { 'gridfs_attachments' : attachments } } )
                logging.info( f'[{threading.current_thread().name}] createElecTestDocs(): registered zip archive as gridfs_attachment of TestRun. md5 = {md5}' )
            else:
                logging.warning( f'[{threading.current_thread().name}] createElecTestDocs(): archive_id is None! not possible to register the binary as a gridfs_attachment. md5 = {md5}' )

        os.remove( str(zfile) )
        shutil.rmtree( self.run_dir )

        return testRun_id


    def submit_binary(self, path):
        fs = gridfs.GridFS(localdb)
        with open(path, "rb") as f:
            binary = f.read()
        return fs.put(binary)


    def getAnalysisResult( self, doc, test_type ):

        test_type_hyphened = test_type.replace('_', '-')

        self.run_dir = f"{AT_DIR}/{self.component_sn}_{self.sname_alt}_{test_type_hyphened}"
        os.makedirs( self.run_dir, exist_ok = True )

        logging.info( f'getAnalysisResults(): run_dir = {self.run_dir}' )

        self.info_path = self.run_dir + "/info.json"

        self.getDataFile( self.run_dir, doc )

        command = [ f"analysis-{test_type}".replace('_', '-'),
                    "-i", self.info_path,
                    '-o', str(Path(self.info_path).parent / "output") ]

        subprocess.call( command )

        output=None
        output_others = []
        for curdir, dirs, files in os.walk(f'{self.run_dir}/output'):
            for filename in files:
                if filename.endswith('.json'):
                    output = os.path.join(curdir, filename)
                else:
                    output_others += [ os.path.join(curdir, filename) ]

        if output != None:
            with open( output ) as f:
                anaResult = json.load( f )[0]


        return anaResult


    def getDataFile( self, path, commit_doc ):

        chip_api = ChipConfigAPI( client )

        config_file = {
                        "datadir": path,
                        "module":{
                          "serialNumber":self.module_sn,
                        },
                        "chip":[]
                      }

        logging.info( f'getDataFile(): path = {path}' )
        pprint.pprint( commit_doc )

        if os.path.exists( path + "/scans.json" ):
            shutil.rmtree(path)

        for i, tr_id in enumerate( commit_doc ):
            query = {"_id": ObjectId( tr_id )}
            tr_doc = localdb.testRun.find_one( query )
            if i == 0:
                config_file["module"]["chipType"] = tr_doc["chipType"]

            this_dir = tr_doc["testType"]
            os.makedirs( path + "/" + this_dir, exist_ok = True )

            query = {"testRun": str(tr_doc["_id"]), 'serialNumber' : self.component_sn }
            this_ctrs = localdb.componentTestRun.find( query )

            j = 0
            for this_ctr in this_ctrs:
                query = {"name": this_ctr["name"]}
                this_cp = localdb.component.find_one(query)
                if this_cp["componentType"] == "front-end_chip":
                    if i == 0:
                        config_file["chip"].append({ "serialNumber":this_cp["name"], "filepaths": {} })

                    # scan log

                    # ctrl config
                    ctrl_config_id = tr_doc.get('ctrlCfg')
                    pickle_data = fs.get( ObjectId( localdb.config.find_one( {'_id':ObjectId(ctrl_config_id)} ).get('data_id') ) ).read()
                    data = pickle.load(io.BytesIO(pickle_data))
                    filepath = f"{path}/{this_dir}/ctrlCfg.json"
                    with open(filepath, "w") as f:
                        json.dump( data, f, indent = 4 )

                    # connectivity

                    # scan config
                    scan_config_id = tr_doc.get('scanCfg')
                    test_type = tr_doc.get('testType')
                    pickle_data = fs.get( ObjectId( localdb.config.find_one( {'_id':ObjectId(scan_config_id)} ).get('data_id') ) ).read()
                    data = pickle.load(io.BytesIO(pickle_data))
                    filepath = f"{path}/{this_dir}/{test_type}.json"
                    with open(filepath, "w") as f:
                        json.dump( data, f, indent = 4 )

                    # testRun
                    filepath = f"{path}/{this_dir}/testRun.json"
                    with open(filepath, "w") as f:
                        json.dump( json.loads( json_util.dumps(tr_doc) ), f, indent = 4 )


                    # componentTestRun
                    filepath = f"{path}/{this_dir}/componentTestRun.json"
                    with open(filepath, "w") as f:
                        json.dump( json.loads( json_util.dumps(this_ctr) ), f, indent = 4 )


                    # configs
                    config_id        = this_ctr.get('config_id')
                    config_name      = this_ctr.get('config')
                    revision_current = this_ctr.get('config_revision_current')
                    revision_prev    = this_ctr.get('config_revision_prev')

                    config_before = chip_api.get_config( config_id, revision_prev, True )
                    config_after  = chip_api.get_config( config_id, revision_current, True )

                    with open( f"{path}/{this_dir}/{config_name}.before", 'w' ) as f:
                        json.dump( config_before, f, separators=(',', ':') )

                    with open( f"{path}/{this_dir}/{config_name}.after", 'w' ) as f:
                        json.dump( config_after, f, separators=(',', ':') )

                    # remove 'std_" from the test type
                    if tr_doc["testType"].find('std_') == 0:
                        tokens = tr_doc["testType"].split("_")[1:]
                    else:
                        tokens = tr_doc["testType"].split("_")
                        
                    tokens.append( "Config" )
                    config_keyname = "_".join( tokens ).replace('xrayscan', 'selftrigger_source')
                    config_file["chip"][j]["filepaths"].update( { config_keyname: f"{this_dir}/{config_name}.after" } )


                    for attachment in this_ctr["attachments"]:
                        if True: #attachment["filename"].split(".")[0].split("-")[0] in DATA_SELECTION_LIST[tr_doc["testType"]]:

                            pickle_data = fs.get(ObjectId(attachment["code"])).read()
                            data = pickle.load(io.BytesIO(pickle_data))

                            filename = "{0}_{1}".format( this_ctr["hexSN"], attachment["filename"] ).replace('pickle', 'json')
                            filepath = f"{path}/{this_dir}/{filename}"

                            # remove 'std_" from the test type
                            if tr_doc["testType"].find('std_') == 0:
                                tokens = tr_doc["testType"].split("_")[1:]
                            else:
                                tokens = tr_doc["testType"].split("_")
                            tokens.append( attachment.get('title').split('-')[0] )

                            keyname = "_".join( tokens ).replace('xrayscan', 'selftrigger_source')
                            #logging.info( f'filepath = {filepath}' )
                            #logging.info( f'{this_cp["name"]: attachment keyname = {keyname}' )

                            config_file["chip"][j]["filepaths"].update( { keyname: this_dir + "/" + filename } )
                            try:
                                with open(filepath, "w") as f:
                                    json.dump( data, f, separators=(',', ':') )

                            except Exception as e:
                                logging.error( str(e) )
                                logging.error( f'failed in dumping file to {filepath}' )
                    j += 1

        logging.info( 'config_file = \n' + pprint.pformat( config_file ) )

        logging.debug(path + "/scans.json")
        with open( path + "/scans.json", "w") as f:
            json.dump( commit_doc, f, ensure_ascii=False, indent=4, sort_keys=False, separators=(',', ': ') )

        logging.debug(path + "/info.json")

        with open( self.info_path, "w") as f:
            json.dump( config_file, f, ensure_ascii=False, indent=4, sort_keys=False, separators=(',', ': ') )

        return True



