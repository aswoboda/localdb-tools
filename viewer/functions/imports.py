# ==============================
# Default modules
# ==============================
import os, sys, datetime, dateutil.tz
import glob
import re
import ast
import hashlib
import shutil
import uuid  # Get mac address
import base64  # Base64 encoding scheme
import gridfs  # gridfs system
import io
import json
import string
import secrets
import pytz
from tzlocal import get_localzone
import zipfile
import requests
import threading
import subprocess
import glob
import traceback
from getpass import getpass

# ==============================
# Log
# ==============================
import logging, logging.config
import coloredlogs

# ==============================
# For input
# ==============================
import yaml, argparse

# ==============================
# Pymongo and flask
# ==============================
from flask import (
    Flask,
    request,
    redirect,
    url_for,
    render_template,
    session,
    make_response,
    jsonify,
    Blueprint,
    current_app,
    send_file,
    abort
)
from pymongo import (
    MongoClient,
    DESCENDING,
    ASCENDING,
    errors,
)  # Pymongo, oh why not use flask-pymongo?
from flask_mail import Mail, Message

# Bson
from bson.objectid import ObjectId

# ?
from PIL import Image

# ==============================
# Plot
# ==============================
import plotly
import plotly.graph_objs as plotlygo

# ==============================
# Add python path
# ==============================
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# ==============================
# functions
# ==============================
# from functions.logging import *
from functions.common import *
from functions import listset
from functions import plot_root, plot_dcs
from functions.SensorIV import *
from functions.checkouter import *

sys.path.insert(
    0,
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../itkpd-interface/lib"),
)
from RecursiveComponentsSynchronizer import *
from RecursiveUploader import *
from download_stage_info import *
from download_institution import *
from download_ModuleType import *
from register_Module import *
from upload_results import *
from download_results import *
from PDInterface import *

sys.path.insert(
    0,
    os.path.join(os.path.dirname(os.path.abspath(__file__)), "../analysis-tool"),
)


import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI
from module_qc_database_tools.core import Module
from module_qc_database_tools.core import Chip
from module_qc_database_tools.utils import (
    chip_uid_to_serial_number,
    get_layer_from_serial_number,
)



def download_worker( serialNumber, code1 = None, code2 = None, resetStage = None, skip_attachments = False, skip_synched = True, debug_mode = False ):
    
    global logger
    
    if not get_pd_client():
        process_request( code1, code2 )
        
    pd_client = get_pd_client()

    if not pd_client:
        logger.error( 'Failed in ITkPD authentication!' )
        return
        
    logger.info( f'download_worker(): serialNumber = {serialNumber}' )
    logger.info( f'download_worker(): resetStage = {resetStage}' )
    logger.info( 'download_worker(): RecursiveComponentsSynchronizer' )

    try:
        RecursiveComponentsSynchronizer( pd_client ).exec( serialNumber, resetStage, skip_attachments, skip_synched, debug_mode )
        
    except Exception as e:
        timestamp = datetime.now()
        userdb.message.remove( { 'function':'RecursiveComponentsSynchronizer', 'component':serialNumber } )
        userdb.message.insert( { 'function':'RecursiveComponentsSynchronizer', 'component':serialNumber, 'timestamp':timestamp, 'code':'ERROR_SYNC_COMPONENT',
                                 'message':f'Recursive sychronization of the component {serialNumber} encountered an error:<br /><pre>{e}</pre><br />Contact developers telling the serial number you attempted to pull with the corresponding block of the log.' } )
        logger.warning( traceback.format_exc() )
        logger.error( '{}'.format( e ) )
    
    logger.info( 'download_worker(): done' )
    
    return


def upload_worker( serialNumber, code1 = None, code2 = None ):
    
    global logger
    
    if not get_pd_client():
        process_request( code1, code2 )

    pd_client = get_pd_client()

    if not pd_client:
        logger.error( 'Failed in ITkPD authentication!' )
        return
        
    logger.info( 'upload_worker(): serialNumber = {}'.format( serialNumber ) )
    
    logger.info( 'upload_worker(): RecursiveUploader' )
    
    try:

        # step 1: upload LocalDB contents to PDB
        RecursiveUploader( pd_client ).exec( serialNumber )

        # step 2: sync FE configs
        download_configs( serialNumber )
        
    except Exception as e:
        logger.warning( traceback.format_exc() )
        logger.error( '{}'.format( e ) )
    
    logger.info( 'upload_worker(): done' )
    
    return


def download_configs( serialNumber ):
    
    global logger

    if not get_pd_client():
        process_request( code1, code2 )

    pd_client = get_pd_client()

    if not pd_client:
        logger.error( 'Failed in ITkPD authentication!' )
        return
        
    try:
        cpt_doc = localdb.component.find_one( { 'serialNumber': serialNumber } )

        if cpt_doc.get('componentType') != 'module':
            return

        qc_status_doc = localdb.QC.module.status.find_one( { 'component' : str(cpt_doc.get('_id')) } )
        stage_flow = userdb.QC.stages.find_one( {'code':'MODULE'} ).get('stage_flow')
        
        stage = qc_status_doc.get('currentStage')
        stage_index = stage_flow.index( stage )

        if stage_index <= stage_flow.index( 'MODULE/INITIAL_WARM' ):
            return

    except Exception as e:
        clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
        create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_COMPONENT_INFO' )
        logger.warning( f'Failed in getting the component info for {serialNumber}' )
        logger.error( str(e) )
        return

    logger.info( f'download_configs_worker(): called. serialNumber = {serialNumber}, stage = {stage}' )
    
    create_message( f'Started to download FE configs for module {serialNumber}.<br />This may take several minutes.<br />Try to access to this module later.', 'download_configs_worker', serialNumber, 'DOWNLOAD_CONFIG_INFO', False )

    
    # Call config generation alg
    try:
        module = Module(pd_client, serialNumber)
        logger.info("create_config(): Getting layer-dependent config from module SN...")
    except Exception as e:
        clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
        create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_MODULE_INSTANCE' )
        logger.error( str(e) )
        return

    try:
        layer_config = get_layer_from_serial_number(serialNumber)
    except Exception as e:
        clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
        create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_MODULE_LAYER_OPTION' )
        logger.error( str(e) )
        return
        

    chip_api = ChipConfigAPI(client)

    chips = []
    cprs = list( localdb.childParentRelation.find( {'parent': str(cpt_doc['_id'])} ) )
    for cpr in cprs:
        subcpt = localdb.component.find_one( {'_id':ObjectId( cpr.get('child') ) } )
        if subcpt.get("componentType") == 'front-end_chip':
            for i in range(3):
                try:
                    chip = pd_client.get( 'getComponent', json = { 'component' : subcpt.get('serialNumber'), 'noEosToken': False } )
                    chips.append( chip )
                    break
                except Exception as e:
                    if i < 2:
                        logger.warning( str(e) )
                        logger.warning( 'failed in getComponent -- retry up to 3 times' )
                        time.sleep(1)
                        continue
                    else:
                        clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
                        create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_CHIP_DOC_DOWNLOAD' )
                        logger.error( str(e) )
                        return

    # check if the config is present on PD
    for chip in chips:
        
        configs = {}

        try:
            for att in chip.get('attachments'):
                hexSN = hex( int( chip.get('serialNumber')[-7:] ) )
                if att.get("title").find( f'{hexSN}_{layer_config}' ) == 0 and att.get("title").endswith(".json") :
                    branch = att.get("title").replace(".json","").split('_')[-1]
                    configs.update( { branch : att } )
        except Exception as e:
            clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
            create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_SEEK' )
            logger.error( str(e) )
            return

        required_branches = ['warm', 'cold', 'LP']

        for branch in required_branches:

            if branch in configs:

                # config was found.

                try:
                    att = configs.get(branch)
                    
                    config_id = chip_api.checkout( chip.get('serialNumber'), stage, branch )
                    
                    if not config_id:
                        config_id = chip_api.create_config( chip.get('serialNumber'), stage, branch )

                    data = download_itkpd_attachment( att )
                    
                    config_data = json.loads( data )
                    new_revision = chip_api.commit( config_id, config_data,
                                                    f"downloaded from ITkPD : code = {att.get('code')}, description = {att.get('description')}" )
                    
                    logger.info( f'committed the contents of config_data in {chip.get("serialNumber")} to config_id {config_id}. New revision = {new_revision}' )
                
                except Exception as e:
                    clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
                    create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_SEEK' )
                    logger.error( str(e) )
                    return
                
            else:
                
                logger.warning( f'The config for {branch} is missing on ITkPD. Attempting to complement it now...' )

                try:

                    # get the previous stage
                    stage_flow = userdb.QC.stages.find_one( { 'code' : 'MODULE' } ).get('stage_flow')
                    prev_index = stage_flow.index( stage ) - 1
                    prev_stage = stage_flow[prev_index]
                    
                    # fetch the latest config of the branch
                    ref_config_id = chip_api.checkout( chip.get('serialNumber'), prev_stage, branch )

                    if not ref_config_id:
                        clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
                        create_message( f'ERROR: no matched chip config was found for ( {chip.get("serial_number")}, {prev_stage}, {branch} )',
                                        'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_REF' )
                        return

                    config_info = chip_api.get_info( ref_config_id )

                    logger.info( 'reference config info = ' + pprint.pformat( config_info ) )

                    upload_chip_config( chip.get("serialNumber"),
                                        ref_config_id,
                                        str( config_info.get("current_revision_id") ),
                                        layer_config )
                    
                    new_config_id   = chip_api.copy_config( ref_config_id, chip.get("serialNumber"), stage, branch )
                    chip_api.info( new_config_id )

                    config_data = chip_api.get_config( new_config_id, None, True )
                    chip_api.commit( new_config_id, config_data,
                                     f"copied from serialNumber {config_info.get('serialNumber')}, stage {config_info.get('stage')}, branch {config_info.get('branch')}, revision {config_info.get('current_revision_id')}, and the config_data was uploaded to ITkPD" )
                    
                except Exception as e:
                    clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
                    create_message( "ERROR: " + str(e) + "<pre>"+ traceback.format_exc() +"</pre> --> contact experts", 'downlaod_configs_worker', serialNumber, 'ERROR_DOWNLOAD_CONFIG_REGISTER' )
                    logger.error( str(e) )
                    return
                
    clear_message( { 'component' : serialNumber, 'code' : 'DOWNLOAD_CONFIG_INFO' } )
    create_message( f'Downloading FE configs for module {serialNumber} is complete.', 'download_configs_worker', serialNumber, 'DOWNLOAD_CONFIG_COMPLETE' )
    logger.info('download_configs_worker(): done.')

    
