### import
import os, pwd, sys, glob, json, base64, io, subprocess, traceback
from pymongo import MongoClient, DESCENDING, errors  # use mongodb scheme
from PIL import Image
from bson.objectid import ObjectId  # handle bson format
import gridfs  # gridfs system
import pickle
import pprint
from functions.common import *

import module_qc_database_tools
from module_qc_database_tools.chip_config_api import ChipConfigAPI

def retrieveFiles(localdb, run_oid, selection):

    logger.info( f'retrieveFiles(): testRun id = {run_oid}, selection = {selection}' )
    
    query = {"_id": ObjectId(run_oid)}
    this_run = localdb.testRun.find_one(query)
    if not this_run:
        return

    #logger.info( f'retrieveFiles(): testRun = ' + pprint.pformat( this_run ) )

    run_dir = "{0}/{1}".format(CACHE_DIR, run_oid)
    if not os.path.isdir(run_dir):
        os.makedirs(run_dir)

    # scanLog
    scan_log = {
        "connectivity": [],
        "testType": this_run["testType"],
        "targetCharge": this_run["targetCharge"],
        "targetTot": this_run["targetTot"],
    }

    # scanCfg
    file_path = "{0}/{1}.json".format(run_dir, this_run["testType"])
    if not os.path.isfile(file_path):
        # if this_run.get('scanCfg', '...')=='...': return False
        if not this_run.get("scanCfg", "...") == "...":
            query = {"_id": ObjectId(this_run["scanCfg"])}
            this_config = localdb.config.find_one(query)

            pickle_data = fs.get( ObjectId(this_config["data_id"]) ).read()
            data = pickle.load(io.BytesIO(pickle_data))
            
            file_path = file_path.replace('pickle', 'json')
            
            with open(file_path, "w") as f:
                json.dump( data, f, separators=(',', ':') )

    # data files
    query = {"testRun": run_oid}
    entries = localdb.componentTestRun.find(query)
    ctr_oids = []
    for entry in entries:
        ctr_oids.append(str(entry["_id"]))
    connectivity = {"chipType": this_run["chipType"], "chips": []}
    for ctr_oid in ctr_oids:

        query = {"_id": ObjectId(ctr_oid)}
        this_ctr = localdb.componentTestRun.find_one(query)

        try:
            hexSN = hex( int( this_ctr['name'][-7:] ) )
        except:
            hexSN = ''
        
        if this_ctr.get("enable", 1) == 0:
            continue
        if this_ctr["chip"] == "module":
            continue

        # retrieve chip config
        config_api = ChipConfigAPI( client )
        config_id = this_ctr.get('config_id')
        
        try :
            config_revision_before = config_api.get_config( config_id, this_ctr.get('config_revision_prev'), True )
        except Exception as e:
            logging.warning( str(e) )
            config_revision_before = {}

        try:
            config_revision_after = config_api.get_config( config_id, this_ctr.get('config_revision_current'), True )
        except Exception as e:
            logging.warning( str(e) )
            config_revision_after = {}
        

        try:
            with open( f'{run_dir}/{hexSN}.json.before', 'w' ) as f:
                json.dump( config_revision_before, f )
                
            with open( f'{run_dir}/{hexSN}.json.after', 'w' ) as f:
                json.dump( config_revision_after, f )
        except Exception as e:
            logging.error( str(e) )
            logging.error( 'Failed in writing chip configs!' )
        
        if not "scanSN" in this_ctr:
            connectivity["chips"].append({"config": "{}.json".format(hexSN)})
        else:
            connectivity["chips"].append({"config": "{}.json".format(this_ctr["scanSN"])})
        for key in this_ctr:
            if not "scanSN" in this_ctr:
                file_path = "{0}/{1}.json.{2}".format(run_dir, hexSN, key[:-3])
            else:
                file_path = "{0}/{1}.json.{2}".format(run_dir, this_ctr["scanSN"], key[:-3])

            file_path = file_path.replace('pickle', 'json')
            
            if not os.path.isfile(file_path):
                if not "Cfg" in key or this_ctr.get(key, "...") == "...":
                    continue
                query = {"_id": ObjectId(this_ctr[key])}
                this_config = localdb.config.find_one(query)
                pickle_data = fs.get( ObjectId(this_config["data_id"]) ).read()
                data = pickle.load(io.BytesIO(pickle_data))
                with open(file_path, "w") as f:
                    json.dump( data, f, separators=(',', ':') )
                    
        for data in this_ctr.get("attachments", []):
            try:
                if not selection or data["filename"].split(".")[0].split("-")[0] in DATA_SELECTION_LIST[this_run["testType"]]:
                    if not "scanSN" in this_ctr:
                        file_path = "{0}/{1}_{2}".format( run_dir, hexSN, data["filename"])
                    else:
                        file_path = "{0}/{1}_{2}".format( run_dir, this_ctr["scanSN"], data["filename"])
                        
                    file_path = file_path.replace('pickle', 'json')
                    
                    if not os.path.isfile(file_path):
                        pickle_data = fs.get( ObjectId( data.get("code") ) ).read()
                        data = pickle.load(io.BytesIO(pickle_data))
                        with open(file_path, "w") as f:
                            json.dump( data, f, separators=(',', ':') )
            except Exception as e:
                logging.warning( str(e) )
                logging.warning( traceback.print_exc() )
                
        scan_log["connectivity"].append(connectivity)

    file_path = "{0}/scanLog.json".format(run_dir)
    if not os.path.isfile(file_path):
        f = open(file_path, "w")
        json.dump(scan_log, f, separators=(',', ':'))
        f.close()

    return True


def plotRoot(plot_tool, run_oid, force_regenerate = False, expand = False):
    run_dir = "{0}/{1}".format(CACHE_DIR, run_oid)
    if not os.path.isdir(run_dir):
        return
    
    command = ["{}/bin/plotFromDir".format(plot_tool), "-i", run_dir, "-P", "-e", "png"]
    if not expand:
        command.append( "-X" )
        
    logger.info( ' '.join( command ) )
    
    if force_regenerate == False and os.path.isfile("{}/rootfile.root".format(run_dir)):
        return 0
        
    try:
        subprocess.call(command, timeout=30)
        return 0
    except:
        logging.warning( traceback.print_exc() )
        return 1


def setRootResult(localdb, run_oid, col, chip_oid=None):
    thum_dir = "{0}/{1}/thumbnail".format(CACHE_DIR, run_oid)
    if not os.path.isdir(thum_dir):
        return

    chip_name = None
    plots = {}
    filepath = "{0}/{1}/plotLog.json".format(CACHE_DIR, run_oid)
    with open(filepath, "r") as f:
        plotLog = json.load(f)
    if chip_oid:
        query = {"testRun": run_oid, col: chip_oid}
        this_ctr = localdb.componentTestRun.find_one(query)
        chip_name = this_ctr["name"]
    for name in plotLog["chips"]:
        if chip_name and not chip_name == name:
            continue
        for plotname in plotLog["chips"][name]:
            plots.update({plotname: {"plot": []}})
            for filename in plotLog["chips"][name][plotname]:
                filepath = "{0}/{1}_root.png".format(thum_dir, filename)
                binary_image = open(filepath, "rb")
                code_base64 = base64.b64encode(binary_image.read()).decode()
                binary_image.close()
                plots[plotname]["plot"].append(
                    {"name": filename, "url": bin2image("png", code_base64)}
                )
            plots[plotname]["num"] = len(plots[plotname]["plot"])
    return plots
