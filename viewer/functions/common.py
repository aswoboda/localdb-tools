import os, sys, pwd, json, gridfs, shutil, time, uuid, pytz, base64, itkdb, ssl
import logging, logging.config
import coloredlogs
import pprint
import subprocess
import re
import functools
from getpass import getpass
from functions.arguments import *  # Pass command line arguments into app.py
from pymongo import MongoClient, DESCENDING, errors  # use mongodb scheme
from flask import url_for, session, request, redirect, send_from_directory  # use Flask scheme
from datetime import datetime, timezone, timedelta
from pdf2image import convert_from_path  # convert pdf to image
from binascii import a2b_base64  # convert a block of base64 data back to binary
from bson.objectid import ObjectId  # handle bson format
from tzlocal import get_localzone
from urllib.parse import urlencode
from module_qc_database_tools.chip_config_api import ChipConfigAPI

from werkzeug.utils import secure_filename # for file uploading

#####################
### Database Settings
def readKey(i_path):
    file_text = open(os.path.join( VIEWER_DIR, i_path ), "r")
    file_keys = file_text.read().split()
    keys = {"username": file_keys[0], "password": file_keys[1]}
    file_text.close()
    return keys


def setDb():
    max_server_delay = 10
    url = "mongodb://{0}:{1}".format(args.host, args.port)
    username = None
    password = None
    authSource = args.db

    ### check ssl
    db_tls = args.tls
    db_ssl = args.ssl
    if db_tls:
        db_ca_certs = args.tlsCAFile
        db_certfile = args.tlsCertificateKeyFile
    elif db_ssl:
        db_ca_certs = args.sslCAFile
        db_certfile = args.sslPEMKeyFile

    ### check tls
    params = {}
    if db_ssl or db_tls:
        params['ssl'] = 'true'
        if db_ca_certs:
            params['ssl_ca_certs'] = db_ca_certs
        if db_certfile:
            params['ssl_certfile'] = db_certfile
        if args.matchHostname:
            params['ssl_match_hostname'] = 'true'
        url += f"/?{urlencode(params)}"
        ### check auth mechanism
        db_auth = args.auth
        if db_auth == "x509":
            url += "&authMechanism=MONGODB-X509"
            authSource = "$external"

    client = MongoClient(url, serverSelectionTimeoutMS=max_server_delay,)
    localdb = client[args.db]

    try:
        localdb.list_collection_names()

    except errors.ServerSelectionTimeoutError as err:
        ### Connection failed
        print("The connection of Local DB {} is BAD.".format(url))
        print(err)
        sys.exit(1)

    except errors.OperationFailure as err:
        ### Need user authentication
        print("Need user authentication.")
        ### check user and password
        if args.KeyFile:
            keys = readKey(args.KeyFile)
            username = keys["username"]
            password = keys["password"]
        if args.username:
            username = args.username
        if args.password:
            password = args.password
        through = False
        while through == False:
            if not username or not password:
                answer = input("Continue? [y/n(skip)]\n> ")
                print("")
                if answer.lower() == "y":
                    username = None
                    password = None
                else:
                    sys.exit(1)
                username = input("User name > ")
                password = getpass("Password > ")
            try:
                localdb.authenticate(username, password)
                print("Authentication succeeded.")
                through = True
            except errors.OperationFailure as err:
                args.KeyFile = None
                print("Authentication failed.")
                answer = input("Try again? [y/n(skip)]\n> ")
                print("")
                if answer.lower() == "y":
                    username = input("User name > ")
                    password = getpass("Password > ")
                else:
                    sys.exit(1)

    client = MongoClient(
        url, username=username, password=password, authSource=authSource
    )

    return client

def getpwuid():
    euid = os.geteuid()
    try:
        return pwd.getpwuid(euid).pw_name
    except KeyError:
        # the uid can't be resolved by the system
        return str(euid)

# Python logging
# https://stackoverflow.com/questions/17743019/flask-logging-cannot-get-it-to-write-to-a-file
class Logger:
    def setupLogging(self, logfile="localdbtool.log"):
        # Create log directory if need
        if len(logfile.split("/")) > 1:
            log_directory = logfile.rsplit("/", 1)[0]
            if not os.path.exists(log_directory):
                os.makedirs(log_directory)
        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
            filename="%s" % (logfile),
            filemode="a",
        )
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        formatter = logging.Formatter("%(levelname)-8s %(message)s")
        console.setFormatter(formatter)
        logging.getLogger("").addHandler(console)
        # color logging
        coloredlogs.install()

    def setFuncName(self, funcname):
        self.funcname = funcname

    def info(self, message):
        logging.info(self.toolname + " " + message)

    def warning(self, message):
        logging.warning(self.toolname + " " + message)

    def debug(self, message):
        logging.debug(self.toolname + self.funcname + " " + message)

    def error(self, message, exit_code=100):
        logging.error(self.toolname + self.funcname + " " + message)
        exit(exit_code)

    def __init__(self, toolname=""):
        self.toolname = toolname
        self.funcname = ""

args = getArgs()

# Setup logging
logger = Logger("[LDB]")
if args.logfile:
    logger.setupLogging(logfile=args.logfile)
else:
    logger.setupLogging()

itkdb_logger  = logging.getLogger('itkdb')
itkdb_logger.setLevel(logging.ERROR)
        

def pass_version( version, required_version ):
    
    version_tokens = [ int(a) for a in functools.reduce( lambda x, y : re.sub( y, '', x ), [ version, 'v', 'rc.*', '\.de.*'] ).split('.') ]
    req_tokens = [ int(a) for a in required_version.split('.') ]
    
    if version_tokens[0] > req_tokens[0]:
        return True

    elif version_tokens[0] == req_tokens[0]:
        if version_tokens[1] > req_tokens[1]:
            return True
        
        elif version_tokens[1] == req_tokens[1]:
            if len(version_tokens)>=2:
                if version_tokens[2] >= req_tokens[2]:
                    return True

    return False


###################
## global variables
if not "set_variables" in globals():

    localdb_url = "://{0}:{1}/localdb".format(args.fhost, args.fport)

    dbv = 1.01
    proddbv = 1.02
    qcAnalysisVersion = '2.2.7'

    for cmd in ['itkdb', 'mqdbt', 'mqat']:
        path = subprocess.run( f'which {cmd}', shell=True, capture_output=True, text=True ).stdout.strip()
        if len( path ) > 0:
            logger.info( f'{cmd} path: {path}' )
        else:
            logger.warning( f'{cmd} path: Not Found' )

    # mqat and mqdbt versioning
    itkdb_version = subprocess.run( 'itkdb --version', shell=True, capture_output=True, text=True ).stdout.strip().split(' ')[-1].replace('v', '')
    mqat_version  = subprocess.run( 'mqat  --version', shell=True, capture_output=True, text=True ).stdout.strip().split(' ')[-1].replace('v', '')
    mqdbt_version = subprocess.run( 'mqdbt --version', shell=True, capture_output=True, text=True ).stdout.strip().split(' ')[-1].replace('v', '')

    itkdb_version_req  = '0.4.12'
    mqdbt_version_req = '2.2.2'
    mqat_version_req  = '2.2.0'

    if not pass_version( itkdb_version, itkdb_version_req ):
        logger.error(f'Server cannot launch: itkdb >= v{itkdb_version_req} is required!')
           
    if not pass_version( mqdbt_version, mqdbt_version_req ):
        logger.error(f'Server cannot launch: module-qc-database-tools >= v{mqdbt_version_req} is required!')
           
    if not pass_version( mqat_version, mqat_version_req ):
        logger.error(f'Server cannot launch: module-qc-analysis-tools >= v{mqat_version_req} is required!')
           
    logger.info( f'This LocalDB server runs with:' )
    logger.info( f'  * itkdb {itkdb_version}' )
    logger.info( f'  * module-qc-database-tools {mqdbt_version}' )
    logger.info( f'  * module-qc-analysis-tools {mqat_version}' )

    
    VIEWER_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    TMP_DIR = "/var/tmp/localdb"
    THUMBNAIL_DIR = "{}/static/cache/thumbnail".format(VIEWER_DIR)
    JSON_DIR = "{}/json".format(TMP_DIR)
    CACHE_DIR = "{}/static/cache".format(VIEWER_DIR)
    IF_DIR = "{}/itkpd-interface".format(TMP_DIR)
    JSROOT_DIR = "{}/jsroot".format(TMP_DIR)
    AT_DIR = f"{TMP_DIR}/module-qc-analysis-tools"

    os.makedirs( TMP_DIR, exist_ok=True )
    os.makedirs( THUMBNAIL_DIR, exist_ok=True )
    os.makedirs( JSON_DIR, exist_ok=True )
    os.makedirs( CACHE_DIR, exist_ok=True )
    os.makedirs( IF_DIR, exist_ok=True )
    os.makedirs( AT_DIR, exist_ok=True )

    client = setDb()
    localdb = client[args.db]
    userdb = client[args.userdb]
    fs = gridfs.GridFS(localdb)

    
    pd_client = None
    pd_user   = None

    #if os.path.isdir(TMP_DIR):
    #    shutil.rmtree(TMP_DIR)

    with open("{}/json-lists/scan_datafile_list.json".format(VIEWER_DIR)) as f:
        DATA_SELECTION_LIST = json.load(f)

    EXTENSIONS = ["png", "jpeg", "jpg", "JPEG", "jpe", "jfif", "pjpeg", "pjp", "gif"]

    DAT_MIMETYPE = "application/octet-stream"
    JSON_MIMETYPE = "application/json"
    ZIP_MIMETYPE = "application/zip"
    ROOT_MIMETYPE = "application/root"

    set_variables = True

    plotting_mutex_lock = False
    plotting_mutex_lock_timestamp = datetime.now()

# Prefix
class PrefixMiddleware(object):
    def __init__(self, app, prefix=""):
        self.app = app
        self.prefix = prefix

    def __call__(self, environ, start_response):

        if environ["REQUEST_URI"].startswith(self.prefix):
            if environ["PATH_INFO"].startswith(self.prefix):
                environ["PATH_INFO"] = environ["PATH_INFO"][len(self.prefix) :]
            environ["SCRIPT_NAME"] = self.prefix
            return self.app(environ, start_response)
        else:
            start_response("404", [("Content-Type", "text/plain")])
            return ["This url does not belong to the app.".encode()]


#################
### Functions ###
#################
def makeDir():
    if not os.path.isdir(TMP_DIR):
        os.mkdir(TMP_DIR)
    user_dir = "{0}/{1}".format(TMP_DIR, session["uuid"])
    _DIRS = [THUMBNAIL_DIR, JSON_DIR, user_dir, JSROOT_DIR]
    for dir_ in _DIRS:
        if not os.path.isdir(dir_):
            os.makedirs(dir_)


def cleanDir(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)


def cleanDirs(dir_name):
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.makedirs(dir_name)


def bin2image(i_type, i_binary):
    if i_type in EXTENSIONS:
        data = "data:image/{};base64,{}".format(i_type, i_binary)
    elif i_type == "pdf":
        pdf_file = open("{}/image.pdf".format(TMP_DIR), "wb")
        bin_data = a2b_base64(i_binary)
        pdf_file.write(bin_data)
        pdf_file.close()
        path = "{}/image.pdf".format(TMP_DIR)
        image = convert_from_path(path)
        image[0].save("{}/image.png".format(TMP_DIR), "png")
        png_file = open("{}/image.png".format(TMP_DIR), "rb")
        binary = base64.b64encode(png_file.read()).decode()
        png_file.close()
        data = "data:image/png;base64,{}".format(binary)
    return data


def initPage():
    if not "uuid" in session:
        session["uuid"] = str(uuid.uuid4())
    if not "timezone" in session:
        # session['timezone'] = 'UTC'
        session["timezone"] = str(get_localzone())
    makeDir()


def updateData(i_col, i_oid):
    query = {"_id": ObjectId(i_oid)}
    this = localdb[i_col].find_one(query)
    now = datetime.utcnow()
    if not this:
        return
    if this.get("sys", {}) == {}:
        doc_value = {"$set": {"sys": {"cts": now, "mts": now, "rev": 0}}}
    else:
        doc_value = {
            "$set": {
                "sys": {
                    "cts": this["sys"].get("sys", now),
                    "mts": now,
                    "rev": this["sys"]["rev"] + 1,
                }
            }
        }
    localdb[i_col].update_one(query, doc_value)


#############################################################################
# itkdb

def get_user(code1, code2):
    try:
        if pd_user == None:
            pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
    except:
        logger.warning(
            "Not authorized. Please login for ITkPD by using itkpd-interface/authenticate.sh"
        )
        pd_user = None
    return pd_user

def process_request(code1, code2):
    global pd_user
    global pd_client

    request = 0
    try:
        pd_user = itkdb.core.User(access_code1=code1, access_code2=code2)
        
        if not pd_user.authenticate():
            logger.warning( 'process_request(): failure in itkdb authentication' )
            return request, None

        pd_client = itkdb.Client( user = pd_user, use_eos = True )

        logger.info(f"process_request(): itkdb authenticated as {pd_user.name}")
            
        request = 1
        
    except Exception as e:
        pd_user = None
        pd_client = None
        request = 0
        logger.warning( 'process_request(): failure in itkdb authentication' )
        
    return request, pd_client



def get_pd_client():
    global pd_client
    global pd_user

    if not pd_user:
        return None

    if not pd_user.is_authenticated():
        return None
    
    return pd_client


def get_pd_user():
    global pd_user
    return pd_user

#############################################################################
# Scan

def createScanCache(entry):

    this_run = entry

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### put tags
    query = {"runId": str(entry["_id"])}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tags = []
    for testRun_tag in testRun_tags:
        tags.append(testRun_tag)

    ### set Temperature
    #setT = 0
    #if "setT" in this_run:

    #    query = {"_id": ObjectId(this_run["setT"])}
    #    # temporary solution
    #    setT = -15

    ### component
    query = {"testRun": str(entry["_id"])}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        if not this_ctr["component"] == "...":
            cmp_id = this_ctr["component"]
            collection = "component"
        else:
            cmp_id = this_ctr["chip"]
            collection = "chip"
        components.append(
            {"name": this_ctr.get("name", "NONAME"),}
        )

    query_targets = []
    for component in components:
        query_targets.append(component["name"])
    query_targets.append(this_run["testType"])
    query_targets.append(user_name)
    query_targets.append(site_name)
    query_targets.append(this_run["startTime"].strftime('%Y/%m/%d'))
    if "setT" in this_run:
        query_targets.append(this_run["setT"])
    for tag in tags:
        query_targets.append(tag["name"])
    #query_targets.append()

    docs = {
        "runId": str(entry["_id"]),
        "timeStamp": this_run["startTime"],
        "data": query_targets,
    }

    return docs

def getScanSummary(run_id):

    query = {"_id": ObjectId(run_id)}
    this_run = localdb.testRun.find_one(query)

    ### user
    query = {"_id": ObjectId(this_run["user_id"])}
    this_user = localdb.user.find_one(query)
    user_name = this_user["userName"]

    ### site
    query = {"_id": ObjectId(this_run["address"])}
    this_site = localdb.institution.find_one(query)
    site_name = this_site["institution"]

    ### tags
    query = {}
    testRun_tag_candidates = userdb.viewer.tag.categories.find(query)
    tag_candidate = []
    for testRun_tag in testRun_tag_candidates:
        tag_candidate.append(testRun_tag)
    ### put tags
    query = {"runId": run_id}
    testRun_tags = userdb.viewer.tag.docs.find(query)
    tag = []
    for testRun_tag in testRun_tags:
        tag.append(testRun_tag)

    ### component
    query = {"testRun": run_id}
    ctr_entries = localdb.componentTestRun.find(query)
    components = []
    for this_ctr in ctr_entries:
        if not this_ctr["component"] == "...":
            cmp_id = this_ctr["component"]
            collection = "component"
        else:
            cmp_id = this_ctr["chip"]
            collection = "chip"
        components.append(
            {
                "name": this_ctr.get("name", "NONAME"),
                "enabled": this_ctr.get("enable", 1) == 1,
                "_id": cmp_id,
                "chip_id": this_ctr["chip"],
                "collection": collection,
            }
        )

    run_data = {
        "_id": run_id,
        "datetime": get_localtime_str( this_run['startTime'] ),
        "testType": this_run["testType"],
        "runNumber": this_run["runNumber"],
        "stage": this_run["stage"],
        "plots": this_run["plots"] != [],
        "components": components,
        "user": user_name,
        "site": site_name,
        "testRun_tag_candidate": tag_candidate,
        "testRun_tag": tag
    }
    if "setT" in this_run:
        TempVal = {
            "setT": this_run["setT"] # setT added
        }
        run_data.update(TempVal)

    return run_data


#############################################################################
# Serial Numbers

def delim_SN( sn ):

    if not sn:
        return ''
    
    if sn.find( '20U' ) == 0 and len(sn) == 14:
        return ' '.join( [ sn[0:3], sn[3:5], sn[5:7], sn[7:9], sn[9:] ] )
    else:
        return sn



def SN_tokens( sn ):
    
    return { 'head' : sn[0:3], 'project' : sn[3], 'XX' : sn[3:5], 'YY' : sn[5:7], 'XXYY' : sn[3:7],
             '1st' : sn[7], '2nd' : sn[8], '3rd' : sn[9], '4th' : sn[10],
             'number' : sn[-7:], 'hex' : "{0:#0{1}x}".format(int( sn[-7:] ),7) }

def SN_typeinfo( sn, verbose=0 ):

    if not sn:
        return "Invalid Serial Number"

    if not len(sn) == 14:
        return "Invalid Serial Number"

    sn_tokens = SN_tokens( sn )

    if sn_tokens.get('head') != '20U':
        return "Invalid Serial Number"

    if sn_tokens.get('project') != 'P':
        return "Wrong project SN"


    code_table = {
        
        "FE_CHIP" :     { "PGFC" : "Undef",
                          "PGXF" : "Tutorial-FE" },
        
        "SENSOR_TILE" : { "PIS6" : "Half-100um-Sensor",
                          "PGS6" : "Half-100um-Sensor", 
                          "PGS7" : "Half-150um-Sensor",
                          "PIS8" : "Full-100um-Sensor",
                          "PGS8" : "Full-100um-Sensor",
                          "PGS9" : "Full-150um-Sensor",
                          "PISG" : "Half-3D-25x100-Sensor",
                          "PGSG" : "Half-3D-25x100-Sensor",
                          "PISH" : "FUll-3D-25x100-Sensor",
                          "PGSH" : "FUll-3D-25x100-Sensor",
                          "PISI" : "Half-3D-50x50-Sensor",
                          "PGSI" : "Half-3D-50x50-Sensor",
                          "PISJ" : "FUll-3D-50x50-Sensor",
                          "PGSJ" : "FUll-3D-50x50-Sensor",
                          "PIS0" : "L0-3D-25x100-Sensor",
                          "PIS1" : "L0-3D-50x50-Sensor",
                          "PIS2" : "L1-Quad-Sensor",
                          "PGS3" : "Outer-Quad-Sensor" },
        
        "BARE_MODULE" : { "PGB1" : "Single-BareModule",
                          "PGB2" : "Double-BareModule",
                          "PGB4" : "Quad-BareModule",
                          "PGBS" : "Single-Digital-BareModule",
                          "PGBQ" : "Quad-Digital-BareModule",
                          "PGBT" : "Single-Dummy-BareModule",
                          "PGBR" : "Quad-Dummy-BareModule",
                          "PGXB" : "Tutorial-BareModule" },
        
        "PCB" :         { "PIPT" : "L0-Stave-PCB",
                          "PIP0" : "L0-R0-PCB",
                          "PIP5" : "L0-R0.5-PCB",
                          "PGPQ" : "Quad-PCB",
                          "PGPD" : "Dual-PCB",
                          "PGXP" : "Tutorial-PCB"
                         },
        "MODULE" :       { "PIMS" : "L0-Stave-Module",
                           "PIM0" : "L0-R0-Module",
                           "PIM5" : "L0-R0.5-Module",
                           "PIM1" : "L1-Quad-Module",
                           "PGM2" : "Outer-Quad-Module",
                           "PGR0" : "Single-Chip-Module",
                           "PGR2" : "Dual-Chip-Module",
                           "PIR6" : "Digital-L0-Stave-Module",
                           "PIR7" : "Digital-L0-R0-Module",
                           "PIR8" : "Digital-L0-R0.5-Module",
                           "PGR9" : "Digital-Quad-Module",
                           "PIRB" : "Digital-L1-Quad-Module",
                           "PIX3" : "Dummy-L0-Stave-Module",
                           "PIX4" : "Dummy-L0-R0-Module",
                           "PIX5" : "Dummy-L0-R0.5-Module",
                           "PGX1" : "Dummy-Quad-Module",
                           "PGXA" : "Dummy-L1-Quad-Module",
                           "PGXM" : "Tutorial-Module"
                         }
    }


    cpt_type = None
    typeinfo = "Others"
    
    for _type, table in code_table.items():
        if sn_tokens.get('XXYY') in table:
            cpt_type = _type
            typeinfo = table.get( sn_tokens.get('XXYY') )

    if cpt_type == 'FE_CHIP':
            
            batchMap = { '0':'RD53A', '1':'ITkPix' }
            
            typeinfo = 'Undef'
            try:
                typeinfo = batchMap[ sn_tokens.get('hex') [2]]
            except:
                pass
        
            wafer= '-{}-({},{})'.format(  sn_tokens.get('hex')[3:5],  int(sn_tokens.get('hex')[5], 16),  int(sn_tokens.get('hex')[6], 16) )
            
            typeinfo += wafer
    
    elif cpt_type == 'MODULE' or cpt_type == 'BARE_MODULE':
        
        fe_types = { '0' : 'RD53A',
                     '1' : 'ITkPix_v1.0',
                     '2' : 'ITkPix_v1.1',
                     '3' : 'ITkPix_v2',
                     '9' : 'No FEs'
                    }
        
        if sn_tokens.get('1st') in fe_types:
            typeinfo = '/'.join( [ typeinfo, fe_types.get( sn_tokens.get('1st') ) ] )

    elif cpt_type == 'PCB':

        pcb_types = { '0' : 'RD53A',
                      '1' : 'Prototype',
                      '2' : 'Outer Pre-prod.',
                      '3' : 'Inner Pre-prod.',
                      '4' : 'Outer Prod.',
                      '5' : 'Inner Prod.',
                      '9' : 'No FEs' }

        if sn_tokens.get('1st') in pcb_types:
            typeinfo = '/'.join( [ typeinfo, pcb_types.get( sn_tokens.get('1st') ) ] )
            

    if verbose > 0:
        if cpt_type == 'BARE_MODULE':
            vendors = { '0' : 'Advacam',
                        '1' : 'Leonardo',
                        '2' : 'IZM',
                        '3' : 'HPK',
                        '4' : 'RAL',
                        '5' : 'Glasgow',
                        '6' : 'ITk institutes' }

            if sn_tokens.get('2nd') in vendors:
                typeinfo = '/'.join( [ typeinfo, vendors.get( sn_tokens.get('2nd') ) ] )

        elif cpt_type == 'PCB':
            pcb_vendors = { '1' : 'EPEC-100um',
                            '2' : 'NCAB-100um',
                            '3' : 'ATLAFLEX-75um',
                            '4' : 'SFCircuits-100um',
                            '5' : 'PHOENIX-100um',
                            '6' : 'Yamashita-Mat-75um' }

            if sn_tokens.get('2nd') in pcb_vendors:
                typeinfo = '/'.join( [ typeinfo, pcb_vendors.get( sn_tokens.get('2nd') ) ] )

                        
            
                
    return typeinfo
    

#############################################################################
# Timestamp

def setTime(date, zone):
    # zone = session.get('timezone','UTC')
    # zone = session.get('timezone',str(get_localzone()))
    converted_time = date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))
    time = converted_time.strftime("%Y/%m/%d %H:%M:%S")
    return time


def setDatetime(date):
    zone = session.get("timezone", "UTC")
    converted_time = date.replace(tzinfo=timezone.utc).astimezone(pytz.timezone(zone))
    return converted_time

def get_localtime_str( isoformat ):
    
    gmt = pytz.timezone('GMT')
    date_gmt = gmt.localize( datetime.fromisoformat( str( isoformat ) ) )
    tz_local = get_localzone()
    date_local = date_gmt.astimezone( tz_local )
    
    return date_local.strftime('%Y-%m-%d %H:%M:%S %Z(%z)')



#############################################################################
# Messaging

def create_message( message, function = '', component = None, code = '', can_dismiss = True ):
    query = { 'message':message, 'function':function, 'component': component, 'code': code, 'can_dismiss' : can_dismiss }
    if not userdb.message.find_one( query ):
        ret = userdb.message.insert_one( query )
        return ret.inserted_id 

def clear_message( query, forced = False ):
    doc = userdb.message.find_one( query )
    if doc:
        if forced or doc.get('can_dismiss', True):
            userdb.message.delete_many( query )
        elif not forced:
            logger.warning( f'message {query} is not possible to clear by user' )

def format_messages( query = {} ):
    messages = list( userdb.message.find( query ) )
    return messages



#############################################################################
# Chip config tools

def upload_chip_config( serial_number, config_id, config_revision, layer_option, test_run_id = None, stage = None ):

    pd_client = get_pd_client()

    if not pd_client:
        raise Exception( 'upload_chip_config(): pd_client is None' )

    if test_run_id:
        if not stage:
            raise Exception( 'upload_chip_config(): stage needs to be specified when test_run_id is provided' )
        

    chip_api = ChipConfigAPI( client )
    
    config_info = chip_api.get_info( config_id )
    chip_api.info( config_id )
    
    config_data = chip_api.get_config( config_id, config_revision, True )
    if not config_data:
        raise Exception( f'upload_chip_config(): config_data is null! config_id = {config_id}, revision = {config_revision}' )
    
    for var in ['_id', 'current_revision_id']:
        config_info[var] = str( config_info[var] )

    config_info_ser = json.dumps( config_info, separators=(',', ':') )

    tmpfile = f'{IF_DIR}/{config_revision}.json'
    logger.info( f'upload_chip_config(): tmpfile = {tmpfile}' )

    with open( tmpfile, 'w' ) as f:
        json.dump( config_data, f, separators=(',', ':') )

    hexSN = hex( int( serial_number[-7:] ) )
    branch = config_info.get('branch')

    # delete existing attachment on the Component
    for i in range(3):
        try:
            cpt_doc = pd_client.get("getComponent", json = { 'component': serial_number } )
            break
        except Exception as e:
            if i < 2:
                logger.warning('pd_client getComponent failed, retrying up to 3 times...')
                continue
            else:
                raise e
        
    attachments = cpt_doc.get('attachments')
    for att in attachments:
        if att.get('title') == f'{hexSN}_{layer_option}_{branch}.json':
            data = {
                "component": cpt_doc.get('id'),
                "code" : att.get('code')
            }
            for i in range(3):
                try:
                    pd_client.post( "deleteComponentAttachment", data=data )
                    break
                except Exception as e:
                    if i < 2:
                        logger.warning('pd_client deleteComponentAttachment failed, retrying up to 3 times...')
                        continue
                    else:
                        logger.warning('pd_client deleteComponentAttachment failed 3 times, giving up.')

    # confg_data to upload under Component
    config_data_component = {
        'component' : serial_number,
        'title' : f'{hexSN}_{layer_option}_{branch}.json',
        'description' : config_info_ser,
        'url' : tmpfile,
        'type' : 'file'
    }

    # confg_data to upload under TestRun
    if test_run_id:

        config_data_testrun = {
            'testRun' : test_run_id,
            'title' : f'{hexSN}_{stage}_{layer_option}_{branch}.json',
            'description' : config_info_ser,
            'url' : tmpfile,
            'type' : 'file'
        }

    with open( tmpfile, "rb" ) as fpointer:
        files = {"data": itkdb.utils.get_file_components({"data": fpointer})}
        
        for i in range(3):
            try:
                response_component = pd_client.post("createComponentAttachment", data=config_data_component, files=files)
                break
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( pprint.pformat( config_data_component ) )
                
                if i < 2:
                    logger.warning( 'retrying up to 3 times...' )
                    time.sleep(1)
                    continue
                else:
                    logger.error( 'failed in attaching to component' )
                    raise e

        if test_run_id:
            for i in range(3):
                try:
                    response_testrun   = pd_client.post("createTestRunAttachment",   data=config_data_testrun,   files=files)
                    break
                except Exception as e:
                    logger.warning( str(e) )
                    logger.warning( pprint.pformat( config_data_testrun ) )

                    if i < 2:
                        logger.warning( 'retrying up to 3 times...' )
                        time.sleep(1)
                        continue
                    else:
                        logger.error( 'failed in attaching to component' )
                        raise e

    os.remove( tmpfile )


def download_itkpd_attachment( att ):

    pd_client = get_pd_client()

    for i in range(3):
        try:
            if att.get('type') == 'eos':
                logger.info('attachment is eos')
                doc = pd_client.get( att.get('url') )
            
            else:
                logger.info('attachment is file')
                doc = pd_client.get("uu-app-binarystore/getBinaryData", json= { 'code': att.get('code') } )
            
            return doc.fptr.read()
            break
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( f'failed in getting the attachment binary' )
            
            if i < 2:
                logger.warning( "retrying up to 3 times..." )
                continue
            else:
                logger.warning( str(e) )
                logger.warning( 'download_itkpd_attachment(): att = ' + pprint.pformat( att ) )
                raise e


def get_module_children( module ):
    module_children = [ localdb.component.find_one( { '_id' : ObjectId( cpr.get('child') ) } )
                        for cpr in localdb.childParentRelation.find( { 'parent' : str(module.get('_id')) } ) ]
    return module_children


def get_module_bare_modules( module ):
    
    return filter( lambda cpt : cpt.get('componentType') == 'bare_module',  get_module_children( module ) )
    

def get_module_sensors( module ):

    bm_cprs_list = [ list( localdb.childParentRelation.find( { 'parent' : str(bm.get('_id')) } ) )
                     for bm in get_module_bare_modules( module ) ]
    
    sensors = []

    for bm_cprs in bm_cprs_list:
        bm_children = [ localdb.component.find_one( { '_id' : ObjectId( cpr.get('child') ) } )
                        for cpr in bm_cprs ]
        bm_sensor = filter( lambda cpt : cpt.get('componentType') == 'sensor_tile', bm_children )

        sensors += bm_sensor

    return sensors
    

def get_component_property_codes( component ):

    return [ prop.get('code') for prop in component.get('properties') ]


def get_component_property_value( component, code ):
    
    values = list(  filter( lambda prop : prop.get('code') == code, component.get('properties') ) )
    
    return values[0]['value'] if len( values ) == 1 else None
