import os, sys
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class ModuleRegistration(PDInterface.PDInterface):
    
    def user_institutions(self, user):
        userinfo = self.pd_client.get("getUser", json={"userIdentity": user.identity})
        institution = [ i["code"] for i in userinfo["institutions"] ]
        return institution

    def module_institution(self, myins, childNum):
        check = "1"
        try:
            child_ins = self.pd_client.get("getComponent", json={"component": childNum})
            if myins == child_ins["currentLocation"]["code"]:
                check = "0"
            else:
                check = "1"
        except:
            check = "2"
        return check

    def get_component(self, serialNum):
        return super().getCompFromProdDB(serialNum)

    def __get_serialnumber(self, module_type, FE_type, THICKNESS, number):
        serial_num = "20" + "U" + look_up_table["MODULE_TYPES"][module_type]["XX"] + look_up_table["MODULE_TYPES"][module_type]["YY"] + look_up_table["FECHIP_VERSION"][FE_type] + "0" + str(number)
        return serial_num


    def check_assemble(self, module_type, child_type, serialnumber, look_up_table):
        check = -1
        if look_up_table["MODULE_TYPES"][module_type][child_type] == serialnumber[3:7]:
            check = 1
        else:
            check = 0
        return check


    def check_parent(self, serialnumber):
        check = "-1"
        try:
            module_ins = self.pd_client.get("getComponent", json={"component": serialnumber})
            parent = module_ins["parents"][0]["history"][0]
            if parent == None:
                check = "1"
            else:
                check = "0"
        except:
            check = "2"
        return check


    def check_exist(self, component_sn):
        check = "-1"
        try:
            component_doc = self.pd_client.get("getComponent",json={"component": component_sn})
            check = "1"
        except:
            check = "2"
        return check


    def check_FEchips(self, bare_sn):
        try:
            bare_doc = self.pd_client.get("getComponent",json={"component": bare_sn})
            for fe_info in bare_doc["children"]:
                if fe_info["componentType"]["code"] == "FE_CHIP":
                    if fe_info["component"] == None:
                        return False
        except:
            return True
        return True


    def make_table(self, config):
        look_up_table = {}
        look_up_table["MODULE_TYPES"] = {}
        look_up_table["FECHIP_VERSION"] = {}
        # module types
        for i in range(len(config["types"])):
            look_up_table["MODULE_TYPES"][ config["types"][i]["code"] ] = {"XX": config["types"][i]["subprojects"][0]["code"], "YY": config["types"][i]["snComponentIdentifier"], "BARE_MODULE": config["children"][config["types"][i]["code"].replace('.', '')]["BARE_MODULE"], "PCB": config["children"][config["types"][i]["code"].replace('.','')]["PCB"]}
        # FE chips
        for i in range(len(config["FEchip"])):
            look_up_table["FECHIP_VERSION"][config["FEchip"][i]["code"]] = config["FEchip"][i]["value"]
        return look_up_table


    def register_Module(self, config, look_up_table, location):
        check = 0

        json = {
          "project":"P",
          "subproject":look_up_table["MODULE_TYPES"][config["type"]]["XX"],
          "institution":location,
          "componentType":"MODULE",
          "type":config["type"],
          "properties":{
            "FECHIP_VERSION":config["FECHIP"],
            "ORIENTATION":eval("True")
          },
          "serialNumber":config["serialNumber"]
        }

        module_ins = self.pd_client.post("registerComponent", json=json)
        logger.info("Register module!!")

        module_doc = self.pd_client.get('getComponent', json={"component": module_ins["component"]["code"]})

        if "BARE_MODULE" in config["child"]:
            if config["child"]["BARE_MODULE"][1]:
                for j in range(len(config["child"]["BARE_MODULE"])):
                    json = {
                      "parent":module_ins["component"]["code"],
                      "slot":module_doc["children"][j+1]["id"],
                      "child":config["child"]["BARE_MODULE"][j]
                    }

                    if not json.get('child'):
                        continue
                    
                    if self.check_assemble(config["type"], "BARE_MODULE", json["child"], look_up_table) == 0:
                        logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE and the module are not the correct combination. Please check them and assemble on the Web page.")
                        check = 1
                    # elif not self.check_FEchips(config["child"]["BARE_MODULE"][j]):
                    #     logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE does not have the all FE chips as its children. Please check them and assemble on the Web page.")
                    #     check = 1
                    else:
                        # assemble the bare module to the module
                        self.pd_client.post("assembleComponentBySlot", json=json)
                        logger.info("Assemble BARE_MODULE!!")
            else:
                json = {
                  "parent":module_ins["component"]["code"],
                  "slot":module_doc["children"][1]["id"],
                  "child":config["child"]["BARE_MODULE"][0]
                }
                if self.check_assemble(config["type"], "BARE_MODULE", json["child"], look_up_table) == 0:
                    logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE and the module are not the correct combination. Please check them and assemble on the Web page.")
                    check = 1
                # elif not self.check_FEchips(config["child"]["BARE_MODULE"][0]):
                #     logger.info("Couldn't assemble BARE_MODULE because the BARE_MODULE does not have the all FE chips as its children. Please check them and assemble on the Web page.")
                #     check = 1
                else:
                    # assemble the bare module to the module
                    self.pd_client.post("assembleComponentBySlot", json=json)
                    logger.info("Assembled BARE_MODULE!!")

        if "PCB" in config["child"]:
            json = {
              "parent":module_ins["component"]["code"],
              "child":config["child"]["PCB"]
            }
            if self.check_assemble(config["type"], "PCB", json["child"], look_up_table) == 0:
                logger.info("Couldn't assemble PCB because the PCB and the module are not the correct combination. Please check them and assemble on the Web page.")
                if check == 0:
                    check = 2
                if check == 1:
                    check = 3
            else:
                # assemble the PCB to the module
                self.pd_client.post("assembleComponent", json=json)
                logger.info("Assembled PCB!!")

        if "CARRIER" in config["child"]:
            json = {
              "parent":module_ins["component"]["code"],
              "child":config["child"]["CARRIER"]
            }
            if json.get('child'):
                # assemble the module carrier to the module if need it
                self.pd_client.post("assembleComponent", json=json)
                logger.info("Assembled MODULE_CARRIER!!")
            else:
                logger.info("Skipped to assemble MODULE_CARRIER")
