import os, sys
import PDInterface
import pprint

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class ModuleStageDownloader(PDInterface.PDInterface):
    # private

    def __create_QC_status_doc(self, cptType_pd_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}

        for stage in cptType_pd_doc["stages"]:

            stage_map[stage["code"]] = stage["name"]
            test_items = []
            if "testTypes" in stage:
                try:
                    for testType in stage["testTypes"]:
                        test_item_map[testType["testType"]["code"]] = testType["testType"]["name"]
                        test_items.append(testType["testType"]["code"])
                except:
                    pass
                
            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items
            
        doc = {
            "code": cptType_pd_doc['code'],
            "name": cptType_pd_doc['name'],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow":stage_flow,
            "stage_test":stage_vs_test,
            "stages":stage_map,
            "test_items":test_item_map
        }
        
        return doc

    def __create_module_status_doc_from_itkpd(self, cptType_pd_doc, cpt_localdb_doc, currentStage):
        #pprint.pprint( cptType_pd_doc )
        
        itk_stage = self.__create_QC_status_doc(cptType_pd_doc)
        doc = {
           # "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"          : dbv,
            "proddbVersion"      : proddbv,
            "component"          : str(cpt_localdb_doc["_id"]),
            "currentStage"       : currentStage,
            "latestSyncedStage"  : currentStage,
            "status"             : "created",
            "rework_stage"       : [],
            "QC_results"         : {},
            "upload_status"      : {}
        }
        # TO DO:
        # 1. Create a case for QUAD and TRIPLET for Metrology
        # 2. Templeature setting
        for stage, tests in itk_stage["stage_test"].items():
            doc["QC_results"][stage] = {}
            for test in tests:
                if test == "SENSOR_IV" or test == "PIXEL_FAILURE_TEST":
                    doc["QC_results"][stage][test + "_30_DEGREE"] = "-1"
                    doc["QC_results"][stage][test + "_20_DEGREE"] = "-1"
                    doc["QC_results"][stage][test + "_min15_DEGREE"] = "-1"
                elif test == "CHIP_CONFIGURATION":
                    continue
                else:
                    doc["QC_results"][stage][test] = "-1"
        return doc


    def __downloadStagesAndTests(self, cpt_localdb_doc):

        # create an example of a document of QC status
        logger.info("Start downloading info of stages and QC tests")
        #if userdb.QC.status.find_one({"proddbVersion":proddbv}) == None:
        try:
            doc = self.__create_QC_status_doc(cpt_localdb_doc)
            count = userdb.QC.status.count( { 'code':doc['code'] } )
            if count > 0:
                userdb.QC.status.remove( { 'code':doc['code'] } )
                
            userdb.QC.status.insert_one( doc )
            
        except Exception as e:
            logger.error( e )
        logger.info("Finished!!\n")

        return 0


    # public
    def download_stage_info(self, component_id):

        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + '/doing_download.txt'
        with open(do_filepath, 'w') as f:
            f.write('doing now')
        
        cpt_pd_doc = self.pd_client.get("getComponent", json={"component":component_id} )
        currentStage = cpt_pd_doc['stages'][-1]['code']
        
        ids = { "module":"5bb1e1bef981520009c54bc5", "bare_module":"5f1d2df071645a000b7b2c85", "module_pcb":"5f5051fd7951dc000a2da3c1" }
        
        for cpt_name, cpt_id in ids.items():
            logger.info( ','.join( [ cpt_name, cpt_id ] ) )
            # create stage information from ITkPD
            cptType_pd_doc = self.pd_client.get("getComponentType", json={"id":cpt_id} )
            
            self.__downloadStagesAndTests(cptType_pd_doc)
            
            # create list of module to fix stage flow
            d_module = []
            if component_id != None:
                d = super().getCompFromLocalDB(component_id)
                try:
                    if d["componentType"] == cpt_name:
                        d_module.append(d["serialNumber"])
                except:
                    logger.warning( 'Module SN {} was not found on prodDB!'.format( component_id ) )
                    
            else:
                doc = localdb.component.find({"componentType" : "module"})
                for d in doc:
                    if d["serialNumber"].find('20UPG') == 0:
                        d_module.append(d["serialNumber"])
            
        for m_serial in d_module:
            logger.info( 'Handling component {}'.format(m_serial) )
            # try to fix component stage
            try:
                cpt_localdb_doc = super().getCompFromLocalDB(m_serial)
                QC_docs = self.__create_module_status_doc_from_itkpd(cptType_pd_doc, cpt_localdb_doc, currentStage)
                localdb.QC.module.status.remove({"component": QC_docs["component"]})
                QC_docs['componentType'] = cptType_pd_doc['code']
                localdb.QC.module.status.insert(QC_docs)
                
                #pprint.pprint( QC_docs )
            except:
                logger.error( traceback.format_exc() )


        logger.info("Finished!!\n")

        if os.path.exists(IF_DIR + '/doing_download.txt'):
             os.remove(IF_DIR + '/doing_download.txt')
