import os, sys, time

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class PDInterface:
    # constructor
    def __init__(self, pd_client ):

        self.pd_client = pd_client
        
        self.ComponentTypeIDs = { "MODULE"      :"5bb1e1bef981520009c54bc5",
                                  "BARE_MODULE" :"5f1d2df071645a000b7b2c85",
                                  "SENSOR_TILE" :"5a09b26d88792100056f214b",
                                  "PCB"         :"5f5051fd7951dc000a2da3c1",
                                  "FE_CHIP"     :"5a09ab9788792100056f2145"   }
        
        with open("{}/itkpd-interface/config/stage_test.json".format(VIEWER_DIR)) as f:
            self.stage_test_map = json.load(f)

        self.resetStage = None

    def getCompFromLocalDB(self, sn):
        query = {"serialNumber": sn,"proDB": True}
        return localdb.component.find_one(query)

    def getFEchipsDocs(self, msn):
        m_doc = self.getCompFromLocalDB(msn)
        cprs = localdb.childParentRelation.find({"parent":str(m_doc["_id"])})
        chip_ids = [ chip["child"] for chip in cprs ]
        docs = []
        for chip_id in chip_ids:
            doc = localdb.component.find_one({"_id":ObjectId(chip_id), "componentType":"front-end_chip"})
            if doc != None:
                logger.info( '{}'.format( doc ) )
                docs.append( doc )
        return docs

    def getQmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.module.status.find_one(query)

    def getPmsFromLocalDB(self, oid):
        query = {"component": oid}
        return localdb.QC.prop.status.find_one(query)

    def getQrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.result.find_one(query)

    def getPrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.QC.module.prop.find_one(query)

    def getTrFromLocalDB(self, oid):
        query = {"_id": ObjectId(str(oid))}
        return localdb.testRun.find_one(query)

    def getCtrFromLocalDB(self, tr_id):
        query = {"testRun": str(tr_id)}
        return localdb.componentTestRun.find(query)

    def getCprFromLocalDB(self, poid, coid):
        query = { "parent": poid, "child": coid }
        return localdb.childParentRelation.find_one(query)

    def getCompFromProdDB(self, sn):
        for i in range(3):
            try:
                return self.pd_client.get( "getComponent", json={"component": sn} )
            except Exception as e:
                logger.warning('{}'.format(e))
                logger.warning('retrying pd_client.get( "getComponent", json={"component": sn} ) ...')
                time.sleep(1)
                
        raise TimeoutError("getCompFromProdDB: Failed 3 attemts")
                

    def insertDocToLdb(self, col, doc):
        localdb[col].insert(doc)
        return

    def getBinaryDataFromProdDB(self, code):
        return self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code":code}).content

    def writeBinaryData(self, path, data):
        with open(path,"wb") as f:
            f.write(data)
        return
