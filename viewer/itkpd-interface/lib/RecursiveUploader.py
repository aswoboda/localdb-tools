import os, sys

import pprint
import dateutil.parser
import traceback
import copy
import threading

from functions.imports import *
from module_qc_database_tools.chip_config_api import ChipConfigAPI
import zipfile

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))

import PDInterface

def make_archive_threadsafe(zip_name: str, path: str):
    with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zfile:
        for root, dirs, files in os.walk(path):
            for file in files:
                zfile.write(os.path.join(root, file), os.path.relpath(os.path.join(root, file), path))


# name_map. left: LDB, right: PDB code
name_map = { "module":"MODULE", "module_pcb":"PCB", "bare_module":"BARE_MODULE", "sensor_tile":"SENSOR_TILE", "front-end_chip":"FE_CHIP" }

def stagesToProcess( topComponent ):
    
    qcStatus = localdb.QC.module.status.find_one( { 'component':str( topComponent['_id'] ) } )
    
    stageInfo   = userdb.QC.stages.find_one( { 'code':name_map[ topComponent['componentType'] ] } )
    
    #logger.info( pprint.pformat( topComponent ) )
    #logger.info( pprint.pformat( qcStatus ) )
        
    currentStage = qcStatus['currentStage']
    stages = stageInfo['stage_flow']
    
    stagesToProcess = []
    
    for stage in stages:
        if qcStatus['upload_status'][stage] == '1':
            continue
            
        stagesToProcess += [ stage ]
        break
        
        if stage == currentStage:
            break
            
    logger.info( str( stagesToProcess ) )
    
    return stagesToProcess, currentStage



class RecursiveUploader(PDInterface.PDInterface):
    
    def __submitTest(self, serialNumber, stage, test):
        
        # logger.info( pprint.pformat( test ) )

        component = localdb.component.find_one( { 'serialNumber':serialNumber } )
        if component == None:
            logger.warning( f'RecursiveUploader.__submitTest(): component for serialNumber {serialNumber} not found on LocalDB!' )
        
        componentType = name_map[ component['componentType'] ]
        
        testFormat = userdb.QC.tests.find_one( { 'code':test['testType'], 'componentType.code':componentType } )
        
        if testFormat == None:
            logger.warning( 'RecursiveUploader.__submitTest(): testFormat for {} of {} not found in LocalDB!'.format( test['testType'], componentType ) )
        
        # logger.info( pprint.pformat( testFormat ) )
        project         = testFormat['componentType']['project']
        componentType   = testFormat['componentType']['code']
        testTypeCode    = testFormat['code']

        try:

            for i in range(3):
                cpt_doc = self.pd_client.get( 'getComponent', json = { 'component':serialNumber } )
                
                if cpt_doc is None:
                    if i < 2:
                        logger.warning( f'RecursiveUploader.__submitTest(): component doc for serialNumber {serialNumber} not found on ITkPD! Retrying up to 3 times...' )
                        continue
                    else:
                        raise RuntimeError( f'component doc for serialNumber {serialNumber} not found on ITkPD!' )
                    

            if cpt_doc.get('locations'):
                if len( cpt_doc.get('locations') ) > 0:
                    # if shipment is made, pick the last destimation
                    institution = cpt_doc.get('locations')[-1].get('institution')
                    institutionCode = localdb.pd.institution.find_one( {'_id' : ObjectId( institution ) } ).get('code')
                else:
                    # otherwise, use the original institution
                    institutionCode = cpt_doc.get('institution').get('code')
            else:
                # otherwise, use the original institution
                institutionCode = cpt_doc.get('institution').get('code')
                
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( 'RecursiveUploader.__submitTest(): failed in getting instituion code from cpt_doc. Substuting with the local test record as a fallback...' )

            institutionCode = localdb.institution.find_one( {'_id' : ObjectId( test['address'] ) } ).get('code')
            
        
        for attempt in range(3):
            try:
                
                out = self.pd_client.get( "generateTestTypeDtoSample", json={ 'project':project, 'componentType':componentType, 'code':testTypeCode } )
                
            except Exception as e:
                logger.warning( str(e) )
                time.sleep(20)
                continue
                
            break
            
        out.update( {
            "component": serialNumber,
            "institution": institutionCode,
            "date": test['sys']['mts'].strftime('%Y-%m-%dT%H:%MZ'),
            "runNumber": str( test['_id'] ),
        } )

        pprint.pprint( out )

        if 'properties' in test['results']:
            properties = test['results'].pop('properties')
            if properties != {}:
                out.update( { 'properties': properties } )
            
        if 'property' in test['results']:
            properties = test['results'].pop('property')
            if properties != {}:
                out.update( { 'properties': properties } )
            
        if 'Metadata' in test['results']:
            properties = test['results'].pop('Metadata')
            
        if 'metadata' in test['results']:
            properties = test['results'].pop('Metadata')
            
        # in general, just copy the LocalDB results field...
        out.update( { 'results': test.get('results') } )
        out.update( { 'passed' : test.get('passed', False) } )

        # ... but in case the result field is testRun, a special format is required.
        for field, value in out['results'].items():
            try:
                fieldDef = next( f for f in testFormat['parameters'] if f['code'] == field )
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( traceback.format_exc() )
                raise Exception( f'ERROR: fieldDef for {field} was not found in the TestRun definition! ==> Contact the experts.' )

            if fieldDef == None:
                logger.info( pprint.pformat( testFormat['parameters'] ) )
        
            
            if fieldDef['dataType'] == 'testRun':
                
                if value == None:
                    out['results'][field] = None
                    continue

                runNumber = value
                
                # acquire test in localdb
                test = localdb.QC.result.find_one( {'_id':ObjectId( runNumber ) } )
                subComponentId = test['component']
                
                subComponentCode = localdb.component.find_one( { '_id':ObjectId( subComponentId ) } )['proID']

                testRunCandidates = None
                
                for i in range(3):
                    try:
                        testRunCandidates = self.pd_client.get('listTestRunsByComponent',
                                                               json= { 'filterMap' : { 'code': subComponentCode, 'runNumber':runNumber, 'state':'ready' } } )
                        break
                    except Exception as e:
                        logger.warning( 'Exception in fetching testRunCandidates from PD' )
                        logger.warning( str(e) )
                
                checkFlag = False

                if testRunCandidates == None:
                    logger.warning( 'RecursiveUploader.__submitTest(): failed in fetching testRunCandidates from PDB' )
                    continue
                
                for tr in testRunCandidates:

                    for key in ['stage', 'institution', 'cts', 'stateTs', 'stateUserIdentity', 'config_id', 'config_revision_current', 'config_revision_prev' ]:
                        if key in tr: tr.pop( key )
                        
                    #logger.info( pprint.pformat(tr) )
                    out['results'][field] = tr['id']
                    checkFlag = True
                    break
                    
                if checkFlag == False:
                    logger.warning( 'RecursiveUploader.__submitTest(): the corresponding test record specified by runNumber not found on ProductionDB!' )

                logger.info( f'RecursiveUploader.__submitTest(): succeeded in filling a TestRun link {fieldDef["code"]}' )
        
        logger.info( 'RecursiveUploader.__submitTest(): submit a new TestRun...' )

        for i in range(3):
            
            try:
                # submit a new TestRun
                res = self.pd_client.post( "uploadTestRunResults", json=out )

                logger.info( f'RecursiveUploader.__submitTest(): done submission of a new TestRun' )
                
                break

            except Exception as e:
                logger.warning( str(e) )

                if i < 2:
                    logger.warning( f'RecursiveUploader.__submitTest(): failed submission of a new TestRun. Retrying up to 3 times...' )
                    continue
                else:
                    logger.warning( f'RecursiveUploader.__submitTest(): failed submission of a new TestRun' )
                    raise e
        
        
        try:
            # attach config if config_id is present on test.
            if 'config_id' in test and test.get('testType') == 'TUNING':


                # Clear up attachments under component for the case obsolete EOS attachments are found.
                cpt_doc_noeos = self.pd_client.get( "getComponent", json = { 'component' : test.get("serialNumber") } )
                for att in cpt_doc_noeos.get("attachments"):
                    
                    data = {
                        "component": serialNumber,
                        "code" : att.get("code")
                    }

                    if not 'url' in att:
                        continue

                    if att.get('url').find('/eos/atlas/test') >0:
                        logger.info( f'Broken EOS attachment detected: {att.get("url")} ==> Deleting' )
                        try:
                            res = pd_client.post( "deleteComponentAttachment", data=data )
                        except:
                            pass
                        logger.info( 'deleted broken EOS attachment successfully\n\n' )
                        continue

                
                assert( 'config_revision' in test )
                assert( 'layer_option' in test )
                assert( 'serialNumber' in test )

                upload_chip_config( test.get('serialNumber'),
                                    test.get('config_id'),
                                    test.get('config_revision'),
                                    test.get('layer_option'),
                                    res.get('testRun').get('id'),
                                    stage )

        except Exception as e:
            logger.warning( str(e) )
            raise e
        
    
        #logger.info( pprint.pformat(res) )
        return res
            


    
    def __clearPreviousTestRuns(self, serialNumber, stage, testType):
        
        # logger.info( pprint.pformat( test ) )

        component = localdb.component.find_one( { 'serialNumber':serialNumber } )
        if component == None:
            logger.warning( f'RecursiveUploader.__clearPreviousTestRuns(): component for serialNumber {serialNumber} not found on LocalDB!' )
        
        componentType = name_map[ component['componentType'] ]
        
        testFormat = userdb.QC.tests.find_one( { 'code':testType, 'componentType.code':componentType } )
        
        if testFormat == None:
            logger.warning( 'RecursiveUploader.__clearPreviousTestRuns(): testFormat for {} of {} not found in LocalDB!'.format( testType, componentType ) )
        
        # logger.info( pprint.pformat( testFormat ) )
        project         = testFormat['componentType']['project']
        componentType   = testFormat['componentType']['code']
        testTypeCode    = testFormat['code']

        try:

            for i in range(3):
                cpt_doc = self.pd_client.get( 'getComponent', json = { 'component':serialNumber } )
                
                if cpt_doc is None:
                    if i < 2:
                        logger.warning( f'RecursiveUploader.__clearPreviousTestRuns(): component doc for serialNumber {serialNumber} not found on ITkPD! Retrying up to 3 times...' )
                        continue
                    else:
                        raise RuntimeError( f'component doc for serialNumber {serialNumber} not found on ITkPD!' )
                    

            if cpt_doc.get('locations'):
                if len( cpt_doc.get('locations') ) > 0:
                    # if shipment is made, pick the last destimation
                    institution = cpt_doc.get('locations')[-1].get('institution')
                    institutionCode = localdb.pd.institution.find_one( {'_id' : ObjectId( institution ) } ).get('code')
                else:
                    # otherwise, use the original institution
                    institutionCode = cpt_doc.get('institution').get('code')
            else:
                # otherwise, use the original institution
                institutionCode = cpt_doc.get('institution').get('code')
                
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( 'RecursiveUploader.__clearPreviousTestRuns(): failed in getting instituion code from cpt_doc. Substuting with the local test record as a fallback...' )

            institutionCode = localdb.institution.find_one( {'_id' : ObjectId( test['address'] ) } ).get('code')
            
        
        logger.info( 'RecursiveUploader.__clearPreviousTestRuns(): delete existing testRuns on PDB..' )
        
        try:
            # delete existing testRuns on PDB
            pastTestRuns = self.pd_client.get( 'listTestRunsByComponent',
                                               json= { 'filterMap' : { 'serialNumber': serialNumber, 'stage':[ stage ], 'testType':testTypeCode, 'state':'ready' } } )
            
            for tr in pastTestRuns:
                try:

                    # first delete testRun attachments
                    tr_doc = self.pd_client.get('getTestRun', json={ "testRun": tr['id'], 'noEosToken': False } )

                    for att in tr_doc.get("attachments"):

                        data = {
                            "testRun": tr["id"],
                            "code" : att.get("code")
                        }

                        try:
                            res = self.pd_client.post( "deleteTestRunAttachment", data=data )
                        except Exception:
                            logger.warning( f'on the way of removing testRun {tr["id"]}, failed in deleting attachment {att.get("code")} --> leaving as is' )
                            pass


                    tr_doc_noeos = self.pd_client.get('getTestRun', json={ "testRun": tr['id'], 'noEosToken': True } )

                    for att in tr_doc_noeos.get("attachments"):

                        data = {
                            "testRun": tr["id"],
                            "code" : att.get("code")
                        }

                        try:
                            res = self.pd_client.post( "deleteTestRunAttachment", data=data )
                        except Exception:
                            logger.warning( f'on the way of removing testRun {tr["id"]}, failed in deleting attachment {att.get("code")} --> leaving as is' )
                            pass


                    self.pd_client.post( 'deleteTestRun', json = { 'testRun' : tr['id'] } )
                    logger.warning( f'RecursiveUploader.__clearPreviousTestRuns(): requested deletion of TestRun {tr["id"]} for component {serialNumber} stage {stage} testType {testTypeCode}' )
                    
                except Exception as e:

                    if str(e).find("Granting token from EoS failed"):

                        logger.warning( f'RecursiveUploader.__clearPreviousTestRuns(): Granting token from EoS failed ==> retry with noEosToekn = True' )
                        
                        # first delete testRun attachments
                        tr_doc = self.pd_client.get('getTestRun', json={ "testRun": tr['id'], 'noEosToken': True } )
                        
                        for att in tr_doc.get("attachments"):
                            
                            data = {
                                "testRun": tr["id"],
                                "code" : att.get("code")
                            }
                            
                            try:
                                res = self.pd_client.post( "deleteTestRunAttachment", data=data )
                            except Exception:
                                logger.warning( f'on the way of removing testRun {tr["id"]}, failed in deleting attachment {att.get("code")} --> leaving as is' )
                                pass
                        
                    else:
                        logger.warning( f'RecursiveUploader.__clearPreviousTestRuns(): deletion request of TestRun {tr["id"]} for component {serialNumber} stage {stage} testType {testTypeCode} failed. This is permissible if the TestRun is already requested to delete.' )
                        logger.warning( str(e) )
            
        except Exception as e:
            logger.warning( str(e) )
            

        logger.info( f'RecursiveUploader.__clearPreviousTestRuns(): done clearing TestRuns for {serialNumber} {stage} {testType}' )

        return
            
        
    def __attachBinaries( self, serialNumber, stage, testRunId, test ):

        gridfs_attachments = test.get("gridfs_attachments")

        if gridfs_attachments == None:
            return

        if len( gridfs_attachments ) == 0:
            return

        zip_dir = f'{THUMBNAIL_DIR}/{testRunId}'
        os.makedirs( zip_dir, exist_ok=True )
        
        for filename, oid in gridfs_attachments.items():
            cache_file = f'{THUMBNAIL_DIR}/{testRunId}/{str(oid)}'

            if os.path.exists( cache_file ):
                pass
            else:
                data = fs.get( oid ).read()

                with open( cache_file, "wb" ) as f:
                    f.write( data )

        info = []
        for filename, oid in gridfs_attachments.items():
            data = {
                "testRun" : testRunId,
                "title": filename,
                "data_id": str(oid)
            }
            info += [ data ]
                
        with open( f'{THUMBNAIL_DIR}/{testRunId}/attachments_info.json', 'w' ) as f:
            json.dump( info, f, indent=4 )

        # Zipping the attachment pack
        zip_filename = f'{THUMBNAIL_DIR}/TestRun_{testRunId}_Attachment_Pack.zip'
        make_archive_threadsafe( zip_filename, zip_dir )

        logger.info( f'Created attachment pack zip: {zip_filename}' )

        zip_data = {
            "title"       : f'TestRun_{testRunId}_Attachment_Pack.zip',
            "url"         : zip_filename,
            "type"        : "file",
            "testRun"     : testRunId,
            "description" : zip_filename
        }

        shutil.rmtree( zip_dir )
        
        thread = threading.Thread( target = self.__attachBinaryWorker, args = ( zip_data, zip_filename ) )
        logger.info( f'RecursiveUploader.__attachBinaries(): requested to attach a binary file {filename} to TestRun {testRunId} on Production DB.' )
        
        thread.start()

        

    def __attachBinaryWorker( self, data, cache_file ):

        global logger
        
        tmp_client = itkdb.Client( user = self.pd_client.user, use_eos = True )
        
        with open( cache_file, 'rb' ) as attachment_file:
            files = {"data": itkdb.utils.get_file_components({"data": attachment_file})}

            max_try = 10
            for i in range(max_try):
                try:
                    tmp_client.post( "createTestRunAttachment", data=data, files=files )
                    logger.info( f'RecursiveUploader.__attachBinaryWorker(): attachment of file {data.get("title")} is done' )
                    break
                except Exception as e:

                    if str(e).find('necessary data rewind') >= 0:

                        logger.warning( str(e) )
                        logger.warning( f'failed in attaching a file {data.get("title")}, clean up garbage records...' )

                        for j in range(3):
                            try:
                                tr_doc = self.pd_client.get('getTestRun', json={ "testRun": data.get('testRun'), 'noEosToken': False } )
                                break
                            except:
                                pass
                        
                        for att in tr_doc.get("attachments"):

                            if data.get('title') != att.get('title'):
                                continue
                            
                            rm_data = {
                                "testRun": data.get("testRun"),
                                "code" : att.get("code")
                            }

                            try:
                                res = self.pd_client.post( "deleteTestRunAttachment", data=rm_data )
                            except Exception:
                                logger.warning( f'failed in deleting attachment {att.get("code")} --> leaving as is' )
                                pass

                            break

                        pass

                    if i < max_try-1:
                        logger.warning( f'__attachBinaryWorker(): retry #{i+1}: retrying up to {max_try} times...')
                        continue
                    
                    else:
                        logger.warning( str(e) )
                        logger.warning( '__attachBinaryWorker(): given up' )
                        return

                    
        os.remove( cache_file )
        
            
        
    def __attachRAW(self, serialNumber, stage, testRunId, raw ):
        
        if raw == None:
            return


        raw['_id'] = str( raw['_id'] )

        for r in raw['raw']:
            r['component'] = str( r['component'] )
            r['sys']['cts'] = r['sys']['cts'].isoformat()
            r['sys']['mts'] = r['sys']['mts'].isoformat()
            try:
                r['address']    = r['address']['code']
            except:
                pass

        stage_alt = stage.replace('/', '__')
        tmpFilePath = f'{IF_DIR}/{serialNumber}_{stage_alt}_{raw["raw"][0]["testType"]}_{testRunId}_{raw["_id"]}.json'

        try:
            with open( tmpFilePath, 'w') as f:
                json.dump(raw, f, indent=4)
        except Exception as e:
            logger.warning( str(e) )
            logger.info( pprint.pformat( raw ) )
            raise e
            
        try:

            data = {
                "title": "RAW",
                "url": tmpFilePath,
                "type": "file",
                "testRun" : testRunId,
                "description" : "RAW"
            }
            
            with open( tmpFilePath, 'rb' ) as attachment_file:
                files = {"data": itkdb.utils.get_file_components({"data": attachment_file})}

                for i in range(10):
                    try:
                        res = self.pd_client.post( "createTestRunAttachment", data=data, files=files )
                        break
                    except Exception as e:
                        logger.warning( str(e) )
                        if i < 2:
                            logger.warning( 'retrying createTestRunAttachment' )
                            continue
                        else:
                            raise e
                
                # logger.info( 'createTestRunAttachment: res = ' + pprint.pformat( res ) )
                
            logger.info( f'RecursiveUploader.__attachRAW(): managed to attach RAW record to TestRun {testRunId} on Production DB.' )
                
        except Exception as e:
            logger.warning( f'RecursiveUploader.__attachRAW(): attachment attempt failed for TestRun {testRunId} for component {serialNumber} stage {stage} testType {raw["raw"][0]["testType"]}.' )
            logger.warning( str(e) )
            
            
        os.remove( tmpFilePath )
        

        if raw['raw'][0]['testType'] == "VISUAL_INSPECTION":
            
            metadata = raw['raw'][0].get('results').get('metadata')

            front_image = metadata.get('front_image')
            back_image  = metadata.get('back_image')
            
            front_defect_images = metadata.get('front_defect_images')
            back_defect_images  = metadata.get('back_defect_images')

            for name, image_id in { 'front_image' : front_image, 'back_image' : back_image }.items():

                self.upload_vi_image( testRunId, name, image_id )


            if front_defect_images is not None:
                for tile, image_id in front_defect_images.items():

                    name = 'front_tile'+tile

                    self.upload_vi_image( testRunId, name, image_id )


            if back_defect_images is not None:
                for tile, image_id in back_defect_images.items():

                    name = 'back_tile'+tile

                    self.upload_vi_image( testRunId, name, image_id )


            # Defects and comments
            front_defects = metadata.get('front_defects')
            back_defects  = metadata.get('back_defects')

            if front_defects:
                for tile, defects in front_defects.items():
                    self.create_vi_defects( testRunId, 'front_tile'+tile, defects )
            
            if back_defects:
                for tile, defects in back_defects.items():
                    self.create_vi_defects( testRunId, 'back_tile'+tile, defects )
            
            front_comments = metadata.get('front_comments')
            back_comments  = metadata.get('back_comments')

            if front_comments:
                self.create_vi_comments( testRunId, front_comments, "front" )

            if back_comments:
                self.create_vi_comments( testRunId, back_comments, "back" )
            
        
    def upload_vi_image( self, testRunId, name, image_id ):

        os.makedirs( IF_DIR, exist_ok=True )
        cache_file = '{}/{}'.format( IF_DIR, image_id )

        if os.path.exists( cache_file ):
            pass
        else:
            try:
                data = fs.get( ObjectId(image_id) ).read()

                with open( cache_file, "wb" ) as f:
                    f.write( data )
            except Exception as e:
                logger.warning( str(e) )
                logger.warning( f'Failed in dumping attachment to {cache_file}. This record is not uploaded to ITkPD.' )
                return

        try:
            ext = imghdr.what( cache_file )
        except:
            ext = None

        try:

            data = {
                "testRun" : testRunId,
                "url": cache_file,
                "title": name,
                "description" : "Visual Inspection Image",
                "type": "file"
            }
            
            with open( cache_file, 'rb' ) as attachment_file:
                files = {"data": itkdb.utils.get_file_components({"data": attachment_file})}

                res = None

                max_try = 10
                for i in range(max_try):
                    try:
                        res = self.pd_client.post( "createTestRunAttachment", data=data, files=files )
                        break
                    except Exception as e:
                        
                        if str(e).find('necessary data rewind') >= 0:

                            logger.warning( str(e) )
                            logger.warning( f'RecursiveUploader.upload_vi_image(): failed in attaching a file {data.get("title")}, clean up garbage records...' )

                            for j in range(3):
                                try:
                                    tr_doc = self.pd_client.get('getTestRun', json={ "testRun": data.get('testRun'), 'noEosToken': False } )
                                    break
                                except:
                                    pass

                            for att in tr_doc.get("attachments"):

                                if data.get('title') != att.get('title'):
                                    continue

                                rm_data = {
                                    "testRun": data.get("testRun"),
                                    "code" : att.get("code")
                                }

                                try:
                                    res = self.pd_client.post( "deleteTestRunAttachment", data=rm_data )
                                except Exception:
                                    logger.warning( f'RecursiveUploader.upload_vi_image(): failed in deleting attachment {att.get("code")} --> leaving as is' )
                                    pass

                                break

                            pass

                        if i < max_try-1:
                            logger.warning( f'RecursiveUploader.upload_vi_image(): {i+1}th attempt failed in createTestRunAttachment, retrying up to {max_try} times..' )
                            continue
                        else:
                            logger.warning( str(e) )

                if res is None:
                    msg = f'RecursiveUploader.upload_vi_image(): failed to attach an image {name} (id: {image_id}) to TestRun {testRunId} on Production DB.'
                    raise Exception( msg )
                
                # logger.info( 'createTestRunAttachment: res = ' + pprint.pformat( res ) )
                
            logger.info( f'RecursiveUploader.upload_vi_image(): managed to attach an image {name} (id: {image_id}) to TestRun {testRunId} on Production DB.' )
            
        except Exception as e:
            logger.warning( str(e) )

            tr_doc = self.pd_client.get('getTestRun', json={ "testRun": testRunId, 'noEosToken': False } )
            
            for att in tr_doc.get("attachments"):

                data = {
                    "testRun": testRunId,
                    "code" : att.get("code")
                }

                try:
                    res = self.pd_client.post( "deleteTestRunAttachment", data=data )
                except Exception:
                    logger.warning( f'RecursiveUploader.upload_vi_image(): failed in deleting attachment {att.get("code")} --> leaving as is' )
                    pass
                    
                
            res = self.pd_client.post( "deleteTestRun", data={ "testRun" : testRunId } )
            logger.warning( f'testRun {testRunId} was deleted from ITkPD given that uploading process failed.' )
            raise e

        os.remove( cache_file )

        
    def create_vi_defects( self, testRunId, name, defects ):

        defects_submit = []
        
        for d in defects:
            defects_submit.append( { 'name' : name, 'description' : d } )

        self.pd_client.post( "createTestRunDefect", json = { 'testRun' : testRunId, 'defects' : defects_submit } )
        
        
    def create_vi_comments( self, testRunId, comments, side ):

        comments_submit = []
        
        for tile, comment in comments.items():
            if comment != "":
                comments_submit.append( f'tile_comment::{side}_tile{tile} : {comment}' )

        self.pd_client.post( "createTestRunComment", json = { 'testRun' : testRunId, 'comments' : comments_submit } )
        
        
    def __processCore( self, stage, componentId, doSubmit ):
        
        # acquire the test rcord of the test
        component = localdb.component.find_one( {'_id':ObjectId( componentId ) } )
        qcStatus  = localdb.QC.module.status.find_one( { 'component':componentId } )
        
        if not stage in qcStatus['QC_results']:
            logging.warning( f'RecursiveUploader.__processCore(): Stage {stage} is not present for this component (componentType: {component["componentType"]})' )
            return
        
        stageResults = qcStatus['QC_results'][stage]
        
        # initialize ProdDB link for the case of previous record being present
        for testType, resultId in stageResults.items():
            localdb.QC.module.status.update( { 'component':componentId }, { '$set': { f'QC_results_pdb.{stage}.{testType}':"-1" } } )
        
        # set the component stage on ProdDB to this stage
        try:
            
            serialNumber = component['name']
            self.pd_client.post( 'setComponentStage', json = { 'component':serialNumber, 'stage':stage } )
            
        except Exception as e:
            logger.warning( '{}'.format(e) )
            
        if doSubmit == False:
            return
            
        for testType, resultId in stageResults.items():
            logger.info( f'RecursiveUploader.__processCore(): testType = {testType}, resultId = {resultId}' )
            
            # clean up past records of the test
            res = self.__clearPreviousTestRuns( serialNumber, stage, testType )

            # acquire the test summary record
            try:
                test = localdb.QC.result.find_one( { '_id':ObjectId( resultId ) } )
                if test is None:
                    continue
            except:
                continue
            
            # acquire the raw record if present
            if 'raw_id' in test:
                raw = localdb.QC.testRAW.find_one( {'_id':test['raw_id'] } ) if test['raw_id'] != None else None
            else:
                raw = None
                
            # first, push the summary record to PD
            res = self.__submitTest( serialNumber, stage, test )

            if res == None:
                raise Exception( f'Null response in __submitTest( {serialNumber}, {stage}, {pprint.pformat(test)} )' )
                
            testRunId = res['testRun']['id']

            # next, attach the gridfs records to PD
            self.__attachBinaries( serialNumber, stage, testRunId, test )
            
            # next, attach the raw result to PD
            if raw:
                self.__attachRAW( serialNumber, stage, testRunId, raw )
            
            # next, write the receipt record to LocalDB
            for i in range(3):
                try:
                    tr_doc = self.pd_client.get("getTestRun", json={ "testRun":testRunId } )
                    break
                except Exception as e:
                    if i < 2:
                        logger.warning('failed to get TestRun {testRunId}: retrying up to 3 times...')
                    else:
                        raise e
                    
            localdb.QC.result.update( { '_id':ObjectId( resultId ) }, { '$set': { 'prodDB_record': tr_doc } } ) 
            localdb.QC.module.status.update( { 'component':componentId }, { '$set': { f'QC_results_pdb.{stage}.{testType}':testRunId } } )

            
        # update the upload status on LocalDB
        localdb.QC.module.status.update( { 'component':componentId }, { '$set': { f'upload_status.{stage}':"1" } } )
    
    
    def __recursiveProcess( self, stage, componentId, doSubmit ):
        
        parent = localdb.component.find_one( {'_id':ObjectId( componentId ) } )

        # first, process sub-components
        cprs = [ cpr for cpr in localdb.childParentRelation.find( { 'parent':componentId } ) ]
        
        # loop over childs
        threads = []
        for cpr in cprs:
            child  = localdb.component.find_one( {'_id':ObjectId( cpr['child']  ) } )
            
            if parent['componentType'] == 'module' and child['componentType'] == 'front-end_chip':
                continue
                
            thread = threading.Thread( target = self.__recursiveProcess, args = ( stage, str( child['_id'] ), doSubmit ) )
            thread.start()
            threads += [ thread ]

        [ t.join() for t in threads ]
            
        # process main part
        if parent.get('name'):
            self.__processCore( stage, componentId, doSubmit )
            logger.info( 'RecursiveUploader.__recursiveProcess(): processed {} ({})'.format( parent['name'], parent['componentType'] ) )
        
            
    def __recursiveComponentPropertiesUpdate( self, componentId ):
        
        parent = localdb.component.find_one( {'_id':ObjectId( componentId ) } )

        if not parent.get('name'):
            logger.warning('RecursiveUploader.__recursiveComponentPropertiesUpdate(): no serial number defined, skipping' )
            return
        
        # first, process sub-components
        cprs = [ cpr for cpr in localdb.childParentRelation.find( { 'parent':componentId } ) ]
        
        # loop over childs
        for cpr in cprs:
            child  = localdb.component.find_one( {'_id':ObjectId( cpr['child']  ) } )
            
            if parent['componentType'] == 'module' and child['componentType'] == 'front-end_chip':
                continue
                
            self.__recursiveComponentPropertiesUpdate( str( child['_id'] ) )
            
        # process main part
        
        for prop in parent['properties']:
            
            if prop['value'] == None or prop['value'] =="" or prop['value'] == "N/A":
                continue

            for i in range(3):
                try:
                    self.pd_client.post( 'setComponentProperty', json = { 'component':parent['name'], 'code': prop['code'], "value": prop['value'] } )
                    break
                except Exception as e:
                    if i < 2:
                        continue
                    else:
                        logger.warning( f'Property {prop["code"]} was not able to post -- possibly the property field was deleted on PDB' )
                        pass
        
        # comments processing
        lComments = localdb.comments.find( { 'component':componentId } )

        componentDoc = self.pd_client.get( 'getComponent', json = { 'component':parent['name'] } )
        pComments = componentDoc['comments']
        
        newComments = []
        
        for lComment in lComments:
            
            recorded = False
            
            if pComments != None:
                for pComment in pComments:
                    if pComment['comment'] == lComment['comment']:
                        recorded = True
                    
            if recorded:
                continue
                
            newComments.append( lComment['comment'] )
            
        for i in range(3):
            try:
                self.pd_client.post( 'createComponentComment', json = { 'component':parent['name'], 'comments':newComments } )
                break
            except:
                if i < 2:
                    continue
                else:
                    pass
        
        logger.info( 'RecursiveUploader.__recursiveComponentPropertiesUpdate(): processed {} ({})'.format( parent['name'], parent['componentType'] ) )
        
            
    # public
    
    def exec(self, serialNumber):
        
        # Activate processing indication
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
            
        do_filepath = f'{IF_DIR}/doing_upload_{serialNumber}.txt'
        with open(do_filepath, 'w') as f:
            f.write('in_progress')
        
        # Get component info
        topComponent = localdb.component.find_one( { 'name': serialNumber } )

        try:
            # First, update components properties
            self.__recursiveComponentPropertiesUpdate( str( topComponent['_id'] ) )
            
            
            stages, currentStage = stagesToProcess( topComponent )
            
            for stage in stages:
                logger.info( f'RecursiveUploader: processing to submit component "{serialNumber}" / stage "{stage}"' )
                
                doSubmit = ( stage != currentStage )
                
                self.threads = []
                
                self.__recursiveProcess( stage, str( topComponent['_id'] ), doSubmit )
                
                [ t.join() for t in self.threads ]
                
            if os.path.exists( do_filepath ):
                os.remove( do_filepath )
            
        except Exception as e:
            logger.warning('Unexpected error during uploading!')
            logger.warning( traceback.format_exc() )
            
            if os.path.exists( do_filepath ):
                os.remove( do_filepath )
                
            with open(do_filepath, 'w') as f:
                f.write( traceback.format_exc() )
                
        return
