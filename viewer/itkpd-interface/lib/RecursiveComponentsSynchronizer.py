import os, sys
import PDInterface
import copy

import pprint
import dateutil.parser
import datetime
import traceback

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class RecursiveComponentsSynchronizer(PDInterface.PDInterface):

    mutex_lock = threading.Lock()
    skip_synched = True
    max_try = 10
    is_single_thread = True

    error_list = []
    
    # private
    def __queryInstitution(self,doc):

        query = {"code": doc["institution"]["code"]}
        if localdb.pd.institution.find_one(query) == None:
            institution_info = {
                "code": doc["institution"]["code"],
                "institution": doc["institution"]["name"],
                "address": "...",
                "HOSTNAME": "...",
                "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
                "dbVersion": dbv,
            }
            localdb.pd.institution.insert_one(institution_info)
        address = localdb.pd.institution.find_one(query)

        return address

    def __createComponentDoc(self, doc, address, chipId=-1, children=-1):
        componentType = str(doc["componentType"]["name"]).lower().replace(" ", "_")
        chipType = ""
        if componentType == "module":
            for prop in doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]
        else:
            chipType = doc["type"]["name"]

        # Overwrite the address by the PD component's current location
        try:
            address = doc.get('locations')[-1].get('institution')
        except:
            pass

        component_doc = {
            "_id"           : ObjectId(doc["id"]),
            "name"          : doc["serialNumber"],
            "chipType"      : chipType,
            "serialNumber"  : doc["serialNumber"],
            "chipId"        : chipId,
            "componentType" : componentType,
            "address"       : address,
            "children"      : children,
            "sys"           : {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"     : dbv,
            "user_id"       : -1,
            "proID"         : doc["code"],
            "proDB"         : True,
            "properties"    : doc["properties"],
            "currentGrade"  : doc["currentGrade"]
        }

        return component_doc


    def __createCprDoc(self, module_doc, child_doc ):

        cpr_doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "parent": str(module_doc["_id"]),
            "child": str(child_doc["_id"]),
            "chipId": child_doc["chipId"],
            "status": "active",
        }

        return cpr_doc


    def __createStageDefinitionDoc(self, cpt_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}
        alternatives = []

        for stage in cpt_doc["stages"]:
            stage_map[stage["code"]] = stage["name"]
            test_items = []

            logger.debug( pprint.pformat( stage ) )

            if "testTypes" in stage:

                if stage['testTypes'] == None:
                    continue


                for testType in stage["testTypes"]:

                    logger.debug( pprint.pformat( testType ) )

                    try:
                        code = testType["testType"]["code"]
                        name = testType["testType"]["name"]
                    except:
                        testID = testType["testType"]

                        test_doc_local = userdb.QC.tests.find_one( { 'id':testID } )

                        if 'code' in test_doc_local:
                            code = test_doc_local['code']

                        if 'name' in test_doc_local:
                            name = test_doc_local['name']

                        pass


                    # 1 TestRun per Test
                    test_item_map[code] = name
                    test_items.append( code )

            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items

            if stage['alternative']:
                alternatives.append( stage['code'] )

        stageDefDoc = {
            "code": cpt_doc['code'],
            "name": cpt_doc['name'],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow":stage_flow,
            "stage_test":stage_vs_test,
            "stages":stage_map,
            "alternatives":alternatives,
            "test_items":test_item_map
        }

        return stageDefDoc

    def __createComponentQCStatusDoc(self, cpt_type, cpt_doc):

        # Below is the skeleton
        doc = {
            "sys"                     : {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"               : dbv,
            "proddbVersion"           : proddbv,
            "componentType"           : cpt_type,
            "component"               : cpt_doc["id"],
            "currentStage"            : cpt_doc["currentStage"]["code"],
            "latestSyncedStage"       : cpt_doc["currentStage"]["code"],
            "status"                  : "created",
            "rework_stage"            : [],
            "QC_results"              : {},
            "QC_results_pdb"          : {}
        }

        if self.resetStage:
            doc.update( { 'currentStage' : self.resetStage } )

        chipType = ""
        if None != cpt_doc["properties"]:
            for prop in cpt_doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]

        stageVersion = {}

        # Checkout the QC stage-test flow from localdbtools.QC.stages
        qcStatus = userdb.QC.stages.find_one( { "code":cpt_doc["componentType"]["code"] } )
        if qcStatus == None:
            msg = "localdbtools.QC.stages does not contain a QC status skeleton for {}".format( cpt_doc["componentType"]["code"] )
            logger.warning( msg )
            raise Exception( msg )


        # Fetch
        for stage in qcStatus["stage_flow"]:
            tests = qcStatus["stage_test"][stage]
            doc["QC_results"][stage] = {}
            doc["QC_results_pdb"][stage] = {}
            for test in tests:
                doc["QC_results"][stage][test] = "-1"
                doc["QC_results_pdb"][stage][test] = "-1"

        return doc


    def __downloadStages(self, componentTypeDoc):

        # create an example of a document of QC status
        if userdb.QC.stages.find_one( {"code":componentTypeDoc["code"] }) == None:
            userdb.QC.stages.insert_one( self.__createStageDefinitionDoc(componentTypeDoc) )
        else:
            userdb.QC.stages.remove( {"code":componentTypeDoc["code"] })
            userdb.QC.stages.insert_one( self.__createStageDefinitionDoc(componentTypeDoc) )

        logger.info("RecursiveComponentsSynchronizer: Downloaded info of stages of {}".format( componentTypeDoc['code'] ) )

        return 0


    def __checkTypeAndSerialNumber(self, d):

        #logger.info("Module document ID: " + d["code"])
        try:
            if d["serialNumber"] != None and d["type"]["name"] != None :
                #logger.info("Appended in a downloading module list.")
                return True
            else:
                # logger.warning("This module has an old type. Skip downloading.")
                return False
        except Exception as e:
            pass

        return False

    def __isFeChipsValid(self, module_doc):
        for child_info in module_doc["children"]:
            if child_info["componentType"]["code"] == "BARE_MODULE":
                if child_info["component"] == None:
                    return False
                else:
                    child_doc = super().getCompFromProdDB(child_info["component"]["code"])
                    for child_child_info in child_doc["children"]:
                        if child_child_info["componentType"]["code"] == "FE_CHIP":
                            if child_child_info["component"] == None:
                                return False
                            if child_child_info["component"]["dummy"]:
                                return False

        return True


    def __getUserName(self, user_info):
        if user_info["middleName"]: return user_info["firstName"] + user_info["middleName"] + user_info["lastName"]
        else: return user_info["firstName"] + user_info["lastName"]


    def __syncTestRun( self, componentId, tr_runNumber, pd_tr_doc, testTypeLocal ):

        # tr_runNumber: LocalDB ObjectId string of the TestRun
        # pd_tr_doc: ProdDB TestRun document

        fs = gridfs.GridFS(localdb)

        try:

            try:
                tr_oid = ObjectId( pd_tr_doc['runNumber'] )
            except:
                tr_oid = ObjectId( pd_tr_doc['id'] )
            
            
            ldb_tr_doc = {
                "_id"                   : tr_oid,
                "component"             : componentId,
                "user"                  : self.__getUserName( pd_tr_doc["user"] ),
                "address"               : str( localdb.pd.institution.find_one( { 'code':pd_tr_doc["institution"]["code"] } )['_id'] ),
                "currentStage"          : pd_tr_doc["components"][0]['testedAtStage']['code'],
                "sys"                   : {"mts": dateutil.parser.parse(pd_tr_doc["sys"]["mts"]),
                                           "cts" : dateutil.parser.parse(pd_tr_doc["sys"]["cts"]),
                                           "rev" : pd_tr_doc["sys"]["rev"] },
                "dbVersion"             : dbv,
                "testType"              : testTypeLocal,
                "results"               : {},
                "raw_id"                : None,
                'gridfs_attachments'    : {},
                "pdb_runNumber"         : pd_tr_doc['runNumber']
            }

            if 'passed' in pd_tr_doc:
                ldb_tr_doc['flags'] = {}
                ldb_tr_doc['flags']['passed'] = pd_tr_doc['passed']
                ldb_tr_doc['passed'] = pd_tr_doc['passed']

            ldb_tr_doc['results']['properties'] = {}
            if pd_tr_doc['properties'] != None:
                for field in pd_tr_doc['properties']:
                    ldb_tr_doc['results']['properties'].update( { field['code'] : field['value'] } )

            if pd_tr_doc['results'] != None:
                for field in pd_tr_doc['results']:

                    if field['dataType'] == 'testRun':

                        # This is a link to TestRun ==> Extract RunNumber and save it.

                        # logger.info( pprint.pformat(field['value']) )

                        if not( field['value'] in [ '', None ] ):

                            # search linked doc
                            try:
                                linked_doc = localdb.QC.result.find_one( { 'prodDB_record.id' : str( field['value']['id'] ) } )
                            except:
                                linked_doc = None

                            if linked_doc:
                                ldb_tr_doc['results'].update( { field['code'] : str( linked_doc.get('_id') ) } )
                            else:
                                ldb_tr_doc['results'].update( { field['code'] : field['value']['runNumber'] } )
                        else:
                            ldb_tr_doc['results'].update( { field['code'] : None } )

                    else:
                        # Normal variable
                        ldb_tr_doc['results'].update( { field['code'] : field['value'] } )

        except Exception as e:
            logger.warning( '{}'.format( e ) )
            logger.warning( 'failed in constructing LocalDB TestRun format' )
            raise e


        for i in range(3):
            try:
                tr_doc_eos = self.pd_client.get( 'getTestRun', json = { 'testRun': pd_tr_doc.get('id'), 'noEosToken': False } )
                break
            except:
                pass

        # Download RAW results if they are present
        # loop over attachments
        for attachment in pd_tr_doc["attachments"]:

            if attachment['title'] != 'RAW':
                continue

            if attachment['type'] not in ['file', 'eos']:
                logger.warning(f'RAW attachment is not eos or file type, do not know how to download it: id={attachment["id"]}, type={attachment["type"]}')
                continue

            for i in range(3):
                try:
                    if attachment['type'] == 'file':
                        attachment = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]})
                    else: # eos-type
                        attachment = self.pd_client.get(attachment['url'])

                    try:
                        binary = attachment.content
                        # decode and construct as a json
                        raw = json.loads( binary.decode('ascii') )

                    except:
                        raw = attachment


                    # format conversion for some fields needing serialization
                    raw['_id']        = ObjectId( raw['_id'] )

                    for r in raw['raw']:
                        r['component']  = ObjectId( r['component'] )
                        r['sys']['mts'] = dateutil.parser.parse( r["sys"]["mts"] )
                        r['sys']['cts'] = dateutil.parser.parse( r["sys"]["cts"] )

                    # insert RAW record to LocalDB
                    try:
                        self.mutex_lock.acquire()
                        if localdb.QC.testRAW.find_one( {'_id':ObjectId( raw['_id'] ) } ):
                            localdb.QC.testRAW.replace_one( {'_id':ObjectId( raw['_id'] ) }, raw )
                        else:
                            localdb.QC.testRAW.insert( raw )
                        self.mutex_lock.release()
                        logger.info( 'RecursiveComponentsSynchronizer: Inserted a new RAW record {} to LocalDB'.format( str( raw['_id'] ) ) )
                        
                        # link this RAW record to the TestRun record
                        ldb_tr_doc['raw_id'] = raw['_id']

                    except Exception as e:
                        logger.warning( str(e) )
                        self.error_list += [ str(e) ]

                except Exception as e:
                    logger.warning( traceback.format_exc() )
                    if str( type(e) ).find("itkdb.exceptions.Forbidden")>0:
                        logger.warning( '{ str( type(e) ) }: received 403 HTTP response' )
                        self.error_list += [ '{ str( type(e) ) }: received 403 HTTP response' ]
                    if str( type(e) ).find("itkdb.exceptions.NotFound")>0:
                        logger.warning( '{ str( type(e) ) }: received 404 HTTP response' )
                        self.error_list += [ '{ str( type(e) ) }: received 404 HTTP response' ]
                    else:
                        logger.warning( str(e) )
                        self.error_list += [ str(e) ]

                break

        attachment_pack_mode = False
        for attachment in pd_tr_doc["attachments"]:

            if attachment['title'] == 'RAW':
                continue
            
            try:
                if attachment['title'].find("Attachment_Pack") > 0:
                    attachment_pack_mode = True
            except Exception as e:
                logger.warning( f'Attachment {attachment.get("filename")} does not comply with LocalDB format, perhaps it was uploaded by other interfaces' )
                logger.warning( pprint.pformat( attachment ) )
                self.error_list += [ f'Attachment {attachment.get("filename")} does not comply with LocalDB format, perhaps it was uploaded by other interfaces' ]

                

            if attachment_pack_mode == True:

                logger.debug( "Attachment pack mode: " + pprint.pformat( attachment ) )

                if not os.path.exists(IF_DIR):
                    os.makedirs(IF_DIR)

                for i in range(self.max_try):
                    try:
                        if attachment.get('type') == 'eos':
                            doc = self.pd_client.get( attachment.get('url') )
                        else:
                            doc = self.pd_client.get("uu-app-binarystore/getBinaryData", json= { 'code': attachment.get('code') } )

                        keyname = attachment.get('title')
                        break
                    except Exception as e:
                        if i == 0:
                            logger.warning( pprint.pformat( str(e) ) )
                            logger.warning( f'syncTestRun(): failed in getting {attachment.get("type")} attachment for a test run (ITkPD code: {pd_tr_doc.get("id")}): retry up to {self.max_try} times...' )


                    if i == max_try-1:
                        logger.warning( f'syncTestRun(): failed in getting {attachment.get("type")} attachment for a test run (ITkPD code: {pd_tr_doc.get("id")}). Given up' )
                        error_list += [ f'syncTestRun(): failed in getting {attachment.get("type")} attachment for a test run (ITkPD code: {pd_tr_doc.get("id")}). Given up' ]
                        return

                    
                    time.sleep(5)
                    

                zip_pack = IF_DIR + "/" + attachment.get("title")

                logger.info( f'zipfile tmp path = {zip_pack} ' )

                doc.save( zip_pack )

                zip_dir = zip_pack.replace(".zip", "")

                try:

                    with zipfile.ZipFile( zip_pack, 'r' ) as f:
                        f.extractall( zip_dir )

                    with open( f'{zip_dir}/attachments_info.json' ) as f:
                        attachment_info = json.load( f )

                    for att in attachment_info:

                        att_title = att.get('title')
                        att_id    = att.get('data_id')

                        with open( f'{zip_dir}/{att_id}', 'rb' ) as f:
                            binary = f.read()
                            binary_id = fs.put(binary)

                        ldb_tr_doc['gridfs_attachments'][ att_title ] = binary_id

                        logger.info( f'deployed {att_title} / {att_id} to gridfs_attachments' )

                    os.remove( zip_pack )
                    shutil.rmtree( zip_dir )

                except Exception as e:
                    logger.warning( str(e) )
                    logger.warning( pprint.pformat( attachment ) )
                    self.error_list += [ str(e) ]

            else: # not attachment_pack_mode -- compatibility for old style < v2.2.5

                # other attachments should go to gridfs, except visual inspection.
                # for visual inspection, book-keeping is made in raw.

                while True:
                    threads_count = len( [ t.name for t in threading.enumerate() if t.name == 'processAttachment' ] )

                    if threads_count < 5:
                        thread = threading.Thread( target = self.__processAttachment, name = 'processAttachment', args = ( attachment, pd_tr_doc, ldb_tr_doc, testTypeLocal ) )
                        thread.start()
                        if self.is_single_thread:
                            thread.join()
                        break
                        
        # end for attachment in pd_tr_doc["attachments"]:
        

        # delete LocalDB gridfs chunk of the identical run number
        for doc in localdb.QC.result.find({ "_id": ldb_tr_doc['_id'] }):
            attachments = doc.get('gridfs_attachments')
            if attachments == None:
                continue

            for key,value in attachments.items():

                if fs.exists( value ):
                    fs.delete( value )

        # attach the prodDB testRun record
        ldb_tr_doc['prodDB_record'] = pd_tr_doc

        # insert TestRun record to LocalDB
        try:
            self.mutex_lock.acquire()
            if localdb.QC.result.find_one( { "_id": ldb_tr_doc['_id'] } ):
                localdb.QC.result.replace_one( { "_id": ldb_tr_doc['_id'] }, ldb_tr_doc )
            else:
                localdb.QC.result.insert( ldb_tr_doc, check_keys = False )
            self.mutex_lock.release()
        except Exception as e:
            logger.warning( str(e) )
            self.error_list += [ str(e) ]

        return


    
    def __processAttachment( self, attachment, pd_tr_doc, ldb_tr_doc, testTypeLocal ):

        if self.skip_attachments:

            if attachment.get('title').lower().find('zip') >=0:
                pass
            elif testTypeLocal == 'VISUAL_INSPECTION':
                pass
            else:
                return
            
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)

        try:
            ObjectId( attachment.get('title') )
            keyname = attachment.get('description')
        except:
            keyname = attachment.get('title')
            
            if keyname == None:
                logger.warning( 'processAttachment(): keyname is None for attachment ' + pprint.pformat( attachment ) )
                return



        doc = None

        for i in range(self.max_try):
            try:
                if attachment.get('type') == 'eos':
                    doc = self.pd_client.get( attachment.get('url') )
                else:
                    doc = self.pd_client.get("uu-app-binarystore/getBinaryData", json= { 'code': attachment.get('code') } )


                if i > 0:
                    logger.info( f'processAttachment(): succeeded in getting {attachment.get("type")} attachment {attachment.get("code")} for a test run (ITkPD code: {pd_tr_doc.get("id")}) in {i}-th try!' )
                    
                break
            
            except Exception as e:
                if i == 0 or i == self.max_try-1:
                    if str( type(e) ).find("itkdb.exceptions.Forbidden")>0:
                        logger.warning( f'{ str( type(e) ) }: received 403 HTTP response in getting an EOS attachment' )
                        self.error_list += [ f'{ str( type(e) ) }: received 403 HTTP response in getting an EOS attachment' ]
                    elif str( type(e) ).find("itkdb.exceptions.NotFound")>0:
                        logger.warning( f'{ str( type(e) ) }: received 404 HTTP response in getting an EOS attachment' )
                        self.error_list += [ f'{ str( type(e) ) }: received 404 HTTP response in getting an EOS attachment' ]
                        break
                    else:
                        logger.warning( str( type(e) ) + " : " + pprint.pformat( str(e) ) )

                if i == 0:
                    logger.warning( f'processAttachment(): failed in getting {attachment.get("type")} attachment {attachment.get("code")} for a test run (ITkPD code: {pd_tr_doc.get("id")}): retrying up to {self.max_try} times...' )

                if i == self.max_try-1:
                    logger.warning( f'processAttachment(): failed in getting {attachment.get("type")} attachment {attachment.get("code")} for a test run (ITkPD code: {pd_tr_doc.get("id")}). Given up' )
                    self.error_list += [ f'processAttachment(): failed in getting {attachment.get("type")} attachment {attachment.get("code")} for a test run (ITkPD code: {pd_tr_doc.get("id")}). Given up' ]
                    return
                
                time.sleep(5)

        tmpPath = IF_DIR + "/" + attachment.get("code")

        try:
            
            if doc.size == 0:
                logger.warning( f'{attachment.get("title")}: zero-size attachment was detected!' )
                self.error_list += [ f'{attachment.get("title")}: zero-size attachment was detected!' ]
                return
            
            doc.save( tmpPath )
            
        except Exception as e:

            if not doc:
                logger.warning( f'{attachment.get("title")}: the EOS attachment file was not possible to acquire' )
                self.error_list += [ f'{attachment.get("title")}: the EOS attachment file was not possible to acquire' ]
                return
            
            if pprint.pformat( doc ).find( "Response [200]" ) > 0:
                logger.warning( f'{attachment.get("title")}: the EOS attachment file size is zero' )
                self.error_list += [ f'{attachment.get("title")}: the EOS attachment file size is zero' ]
                return

            logger.warning( str(e) )
            logger.warning( 'attachment = ' + pprint.pformat( attachment ) )
            logger.warning( 'doc = ' + pprint.pformat(doc) )
            logger.warning( f'failed in saving the doc to {tmpPath}' )
            self.error_list += [ f'failed in saving the doc to {tmpPath}' ]
            return


        fs = gridfs.GridFS(localdb)

        try:
            with open( tmpPath, 'rb' ) as f:
                binary = f.read()
                binary_id = fs.put(binary)

                if testTypeLocal != "VISUAL_INSPECTION":
                    ldb_tr_doc['gridfs_attachments'][ keyname ] = binary_id
                else:

                    # logger.info( f'visual inspection attachment: keyname = {keyname}')

                    raw = localdb.QC.testRAW.find_one( ldb_tr_doc['raw_id'] )
                    if raw == None:
                        return

                    if keyname in ['front_image', 'back_image']:
                        raw['raw'][0]['results']['metadata'][keyname] = str(binary_id)

                        self.mutex_lock.acquire()
                        if localdb.QC.testRAW.find_one( {'_id' : raw['_id'] } ):
                            localdb.QC.testRAW.replace_one( {'_id' : raw['_id'] }, raw )
                        else:
                            localdb.QC.testRAW.insert( raw )
                        self.mutex_lock.release()

                    elif keyname.find( 'tile' ) > 0:
                        if keyname.find('front') == 0:
                            tile = keyname[len('front_tile'):]
                            raw['raw'][0]['results']['metadata']['front_defect_images'][tile] = str(binary_id)
                        elif keyname.find('back') == 0:
                            tile = keyname[len('back_tile'):]
                            raw['raw'][0]['results']['metadata']['back_defect_images'][tile] = str(binary_id)

                        self.mutex_lock.acquire()
                        if localdb.QC.testRAW.find_one( {'_id' : raw['_id'] } ):
                            localdb.QC.testRAW.replace_one( {'_id' : raw['_id'] }, raw )
                        else:
                            localdb.QC.testRAW.insert( raw )
                        self.mutex_lock.release()

            os.remove( tmpPath )
            
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( 'failed in storing attachment on EOS ' + pprint.pformat(attachment) )
            self.error_list += [ str(e) ]

        



    def __bookComponent(self, cpt_type, cpt_sn, cpt_doc):
        logger.info( 'RecursiveComponentsSynchronizer: Attempt to register {} {}'.format( cpt_type, cpt_sn ) )

        # QC status skeleton
        cpt_qc_doc = self.__createComponentQCStatusDoc( cpt_type, cpt_doc )

        # Attempt to get the component record from LocalDB
        cpt_localdb_doc = self.getCompFromLocalDB( cpt_doc["serialNumber"] )

        address = self.__queryInstitution( cpt_doc )
        cpt_localdb_doc = self.__createComponentDoc( doc=cpt_doc, address=address )

        # If the record is blank, insert one
        self.component_list.append( cpt_localdb_doc )

        # Synchronize the QC status to ProdDB

        # Set the sign-off record of the module up to the current stage
        ldb_componentId           = str( cpt_localdb_doc["_id"] )
        ldb_component_QC_info     = super().getQmsFromLocalDB( ldb_componentId )

        try:
            stageFlow = self.stageFlows[ cpt_type ]
        except:
            msg = '__bookComponent(): unknown cpt_type: {}'.format( cpt_type )
            logger.warning( msg )
            raise Exception( msg )


        logger.debug( pprint.pformat( stageFlow ) )
        currentStage = cpt_doc['currentStage']['code']

        if not 'upload_status' in cpt_qc_doc:
            cpt_qc_doc['upload_status'] = {}
            for s in stageFlow:
                cpt_qc_doc['upload_status'][s] = "-1"

        if self.resetStage and self.resetStage in stageFlow:

            for stage in stageFlow:
                if stage == self.resetStage:
                    break

                cpt_qc_doc['upload_status'][stage] = "1"

        else:

            for stage in stageFlow:

                if stage == currentStage:
                    break

                cpt_qc_doc['upload_status'][stage] = "1"



        # Fetch only ready-state TestRuns
        for i in range(3):
            try:
                pd_testRuns = [ tr for tr in self.pd_client.get("listTestRunsByComponent", json={"component":cpt_doc['code']} ) if tr['state'] == 'ready' ]
                break
            except Exception as e:
                logger.warning( str(e) )

        # Loop over TestRuns
        for testRun in pd_testRuns:

            tr_runNumber = testRun.get("runNumber")
            
            try:
                ObjectId( tr_runNumber )
            except:
                logger.warning( f'the runNumber recorded on PDB TestRun ({tr_runNumber}) is not compatible with mongodb ObjectId code.')
                logger.info( 'NOTE: Perhaps the TestRun was created by other clients than LocalDB. Using the ID of TestRun instead' )
                tr_runNumber = testRun.get('id')

            stage        = testRun['stage']['code']
            testType     = testRun['testType']['code']
            
            cpt_qc_doc["QC_results"][stage][testType]     = tr_runNumber
            cpt_qc_doc["QC_results_pdb"][stage][testType] = testRun.get("id")

            if stage in cpt_qc_doc["QC_results"]:
                while True:
                    if len( [ t for t in threading.enumerate() if t.name == 'processTestRun' ] ) < 5:
                        t = threading.Thread( target = self.__processTestRun, name = 'processTestRun', args = ( testRun, ldb_componentId ) )
                        t.start()
                        if self.is_single_thread:
                            t.join()
                        break
                    else:
                        time.sleep(1)
                        continue
                    
            else:
                logger.warning( f'This TestRun {testRun["id"]} is pointing to stage {stage}, which is not in the list of valid stages. Possibly the stage was deprecated.' )

                


        logger.info( "RecursiveComponentsSynchronizer: Creating QC status doc for {} {}...".format( cpt_type, cpt_sn ) )
        logger.debug( pprint.pformat( cpt_qc_doc ) )

        # Remove old record
        localdb.QC.module.status.remove(  {"component": cpt_doc["id"]}  )

        # Insert a new record
        localdb.QC.module.status.insert_one( cpt_qc_doc )

        logger.info( "RecursiveComponentsSynchronizer: Created QC status doc for {} {}.".format( cpt_type, cpt_sn ) )

        return cpt_localdb_doc

    

    def __processTestRun( self, testRun, ldb_componentId ):
        
        stage        = testRun['stage']['code']
        testType     = testRun['testType']['code']

        # logger.info( pprint.pformat( testRun ) )

        # Clean up obsolete EOS attachment first
        for i in range(3):
            try:
                tr_doc_noeos = self.pd_client.get("getTestRun", json={ "testRun":testRun["id"] } )
                break
            except Exception as e:
                logger.warning( str(e) )

        for att in tr_doc_noeos.get('attachments'):
            if att.get('url'):
                if att.get('url').find('/eos/atlas/test') >0:
                    logger.warning( f'Broken EOS attachment detected: {tr_doc_noeos.get("code")} {att.get("url")} ==> Deleting' )
                    try:
                        self.pd_client.post( "deleteTestRunAttachment", data= { "testRun" : tr_doc_noeos.get("id"), "code" : att.get("code") } )
                    except Exception as e:
                        logger.warning(str(e))
                        pass

        for i in range(self.max_try):
            try:
                tr_doc       = self.pd_client.get("getTestRun", json={ "testRun":testRun["id"], 'noEosToken': False } )
                break
            except Exception as e:
                logger.warning( str(e) )

            if i == self.max_try-1:
                logger.warning( f'Failure in __processTestRun(): not possible to perform "getTestRun" for test run ID {testRun["id"]} with "noEosToken:False"' )
                logger.warning( f'NOTE: this may indicate that some broken EOS attachments are present in the TestRun, but they were not possible to delete by the authenticated user.' )
                return

            time.sleep(5)
            
        tr_runNumber = tr_doc['runNumber']
        
        try:
            ObjectId( tr_runNumber )
        except:
            logger.warning( f'the runNumber recorded on PDB TestRun ({tr_runNumber}) is not compatible with mongodb ObjectId code.')
            logger.info( 'NOTE: Perhaps the TestRun was created by other clients than LocalDB. Using the ID of TestRun instead' )
            tr_runNumber = tr_doc['id']

        testTypeLocal = testType

        try:

            localTestRunRecord = localdb.QC.result.find_one( { "_id":ObjectId( tr_runNumber ) } )

            if localTestRunRecord is None:
                thread = threading.Thread( target = self.__syncTestRun, name='syncTestRun', args = ( ldb_componentId, tr_runNumber, tr_doc, testTypeLocal ) )
                thread.start()
                if self.is_single_thread:
                    thread.join()
                logger.info( 'RecursiveComponentsSynchronizer: Added TestRun {}.{} / {}'.format( stage, testTypeLocal, testRun['id'] ) )

            else:

                if localTestRunRecord.get("prodDB_record"):
                    if localTestRunRecord.get("prodDB_record").get("id") == testRun.get("id") and self.skip_synched:
                        logger.info( f'RecursiveComponentsSynchronizer: test run record {tr_runNumber} is already synched in LocalDB --> Skipping' )
                    else:
                        thread = threading.Thread( target = self.__syncTestRun, name='syncTestRun', args = ( ldb_componentId, tr_runNumber, tr_doc, testTypeLocal ) )
                        thread.start()
                        if self.is_single_thread:
                            thread.join()
                        logger.info( 'RecursiveComponentsSynchronizer: Added TestRun {}.{} / {}'.format( stage, testTypeLocal, testRun['id'] ) )
                else:
                    thread = threading.Thread( target = self.__syncTestRun, name='syncTestRun', args = ( ldb_componentId, tr_runNumber, tr_doc, testTypeLocal ) )
                    thread.start()
                    if self.is_single_thread:
                        thread.join()
                    logger.info( 'RecursiveComponentsSynchronizer: Added TestRun {}.{} / {}'.format( stage, testTypeLocal, testRun['id'] ) )


            localdb.QC.testRuns_pdb_ldb_map.insert( { 'pdbId': testRun["id"], 'ldbId': tr_runNumber } )

        except Exception as e:
            logger.warning( traceback.format_exc() )
            logger.warning( str(e) )
            raise e


    def __loopSubComponents( self, cpt_doc, cpt_localdb_doc ):

        if cpt_doc["children"] == None:
            return

        for child_cpt_metadata in cpt_doc["children"]:

            # Check which child component type
            child_type = child_cpt_metadata['componentType']['code']

            # Skip carrier
            if child_type.lower().find( 'carrier' ) >=0 :
                continue

            logger.debug( pprint.pformat( child_cpt_metadata ) )

            try:
                child_id = child_cpt_metadata['component']['code']
            except:
                logger.warning( 'child component ID was not found: skipping' )
                continue

            # Acquire the child component from ProdDB
            child_doc = self.getCompFromProdDB( child_id )

            child_cpt_sn = child_doc['serialNumber']

            self.cpt_sn.append( child_cpt_sn )

            child_cpt_localdb_doc = self.__bookComponent( child_type, child_cpt_sn, child_doc )

            # Create a new child-parent relation

            # For FE chips, make an additional direct link between the chip and the parent module
            if child_type == 'FE_CHIP':

                try:
                    cpr_info = self.__createCprDoc( self.parent_module_localdb_doc, child_cpt_localdb_doc )

                    if super().getCprFromLocalDB( str( self.parent_module_localdb_doc["_id"]), str( child_cpt_localdb_doc["_id"]) ):
                        localdb.childParentRelation.delete_many( { 'parent' : str( self.parent_module_localdb_doc["_id"]),
                                                                   'child' : str( child_cpt_localdb_doc["_id"]) } )
                        logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was reset.'.format( child_type, child_cpt_sn ) )

                    self.cpr_list.append(cpr_info)
                    logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was created.'.format( child_type, child_cpt_sn ) )

                except Exception as e:
                    logger.warning( str(e) )
                    pass


            # The following generic linking applies to all sub-components
            cpr_info = self.__createCprDoc( cpt_localdb_doc, child_cpt_localdb_doc )

            if super().getCprFromLocalDB( str( cpt_localdb_doc["_id"]), str( child_cpt_localdb_doc["_id"]) ):
                localdb.childParentRelation.delete_many( { 'parent' : str( cpt_localdb_doc["_id"]),
                                                           'child' : str( child_cpt_localdb_doc["_id"]) } )
                logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was reset.'.format( child_type, child_cpt_sn ) )

            self.cpr_list.append(cpr_info)
            logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was created.'.format( child_type, child_cpt_sn ) )

            # Recursively apply subcomponents loop
            self.__loopSubComponents( child_doc, child_cpt_localdb_doc )


    def recursiveReplace( self, j, a, b, nest=0 ):

        flag = False

        for k in range(self.max_try):

            for key, value in j.items():

                if isinstance( value, dict ):
                    self.recursiveReplace(value, a, b, nest+1)

                new_key = key.replace(a, b)
                if new_key != key:
                    j[new_key] = value
                    del j[key]
                    print( f'{key} ==> {new_key}' )
                    flag = True
                    break


            if not flag:
                break

        return j


    def deployStagesAndTests(self):

        logger.info( 'Fetching Stage and Test Skeletons...' )

        # ComponentTypeID is a map { name, id } defined in PDInterface
        for componentName, ComponentTypeID in self.ComponentTypeIDs.items():

            logger.info('===============================')
            logger.info( f'{componentName} {ComponentTypeID}' )

            # Comment 2022-12-12
            # if the key contains dot, then the object is not possible to store to mongo. Temporarily escaping by @.
            for i in range(3):
                try:
                    _cpt_type = self.pd_client.get("getComponentType",json={"id":ComponentTypeID})
                    componentTypeDoc = self.recursiveReplace( _cpt_type, ".", "@" )
                    break
                except Exception as e:
                    logger.warning( str(e) )
                    if i < 2:
                        logger.info( 'retrying up to 3 times' )
                        continue
                    else:
                        raise e

            userdb.componentType.remove( {'id':componentTypeDoc['id'] } )
            userdb.componentType.insert( componentTypeDoc )

            logger.info('  Stages:')
            for stage in componentTypeDoc['stages']:
                logger.info( '   - {}'.format( stage['code'] ) )

            self.__downloadStages(componentTypeDoc)

            # listTestTypes dumps all tests defined for the queried componentType
            ttlist = self.pd_client.get('listTestTypes', json={'filterMap':{ 'componentType':ComponentTypeID } } )

            logger.info('  Tests for {}:'.format( componentName ) )

            # loop over each test
            for t in ttlist:

                if t['state'] != 'closed':
                    logger.info( '   - {} [{}]'.format( t['code'], t['state'] ) )

                # create an example of a document of QC status
                userdb.QC.tests.remove( { 'code':t['code'], 'componentType.code':componentName } )
                userdb.QC.tests.insert( t )


        logger.info("RecursiveComponentsSynchronizer: Downloaded info of tests of {}".format( componentTypeDoc['code'] ) )


    # public
    def exec(self, component_id=None, resetStage = None, skip_attachments = False, skip_synched = True, debug_mode = False):

        self.error_list = []
        
        if resetStage != None:
            self.resetStage = resetStage
            logger.info( f'Running synchronizer with resetting the components stages at {self.resetStage}' )
        else:
            logger.info( f'Running synchronizer w/o resetting the components stages (default)' )

        self.skip_attachments = skip_attachments
        self.skip_synched = skip_synched
        self.is_single_thread = debug_mode

        userdb.message.remove( { 'component':component_id } )

        timestamp = datetime.now()
        ticket_id = userdb.message.insert( { 'function':'RecursiveComponentsSynchronizer',
                                             'component':component_id,
                                             'timestamp':timestamp,
                                             'code':'START_SYNC_COMPONENT',
                                             'message':f'Recursive sychronization of the component {component_id} started on {timestamp}. This process may take several minutes...' } )

        self.deployStagesAndTests()


        ######################################
        # Task:
        # 1. list up components to download
        # 2. trace-down all sub-components tree
        # 3. register components and all parent-child relations

        ######################################
        ## Step-1
        ## list up components to download

        self.stageFlows = {}

        #for cpt_name, cpt_id in self.ComponentTypeIDs.items():
        for cpt_name, cpt_id in self.ComponentTypeIDs.items():

            cpt_doc = self.pd_client.get("getComponentType",json={"id":cpt_id})

            self.stageFlows[cpt_name] = []
            for stage in cpt_doc['stages']:
                self.stageFlows[cpt_name].append( stage['code'] )


        d_cpts = {}


        ######################################
        ## Step-2
        ## Trace-down all sub-components tree

        this_cpt_sn = component_id

        self.component_list = []
        self.cpr_list = []
        self.cpt_sn = []
        
        """
        for cpt_type, cpts in d_cpts.items():

            for this_cpt_sn in cpts:
        """

        # Acquire the component doc from ProdDB
        try:
            this_cpt_doc = super().getCompFromProdDB( this_cpt_sn )

            cpt_type = this_cpt_doc.get("componentType").get("code")


            # In case of MODULE, skip if chips are incomplete or blank
            if cpt_type == "MODULE":
                if not self.__isFeChipsValid( this_cpt_doc ):
                    logger.warning( f'Detected blank or incomplete FE chips in {cpt_type} {this_cpt_sn}, Skipping to pull.' )
                    create_message( f'WARNING: Detected blank or incomplete FE chips in {cpt_type} {this_cpt_sn}.<br />This component was not pulled to LocalDB',
                                    'RecursiveComponentSynchronizer.deployStagesAndTests()',
                                    None,
                                    'INCOMPLETE_FE_CHIPS_ON_BARE_MODULE' )
                    #continue

            if not self.resetStage and not this_cpt_doc.get('currentStage').get('code') in self.stageFlows[ cpt_type ]:
                logger.warning( f'The stage on PDB record is obsolete detected ({this_cpt_doc.get("currentStage").get("code")})' )
                if cpt_type == "MODULE":
                    self.resetStage = "MODULE/ASSEMBLY"
                elif cpt_type == "PCB":
                    self.resetStage = "PCB_RECEPTION"
                elif cpt_type == "BARE_MODULE":
                    self.resetStage = "BAREMODULERECEPTION"

                if self.resetStage:
                    logger.warning( f'==> Switching to stage {self.resetStage}' )

            this_cpt_localdb_doc = self.__bookComponent( cpt_type, this_cpt_sn, this_cpt_doc )

            if cpt_type == "MODULE":
                self.parent_module_doc         = this_cpt_doc
                self.parent_module_sn          = this_cpt_sn
                self.parent_module_localdb_doc = this_cpt_localdb_doc

        except Exception as e:
            logger.warning( str(e) )
            logger.warning( 'error in acquiring component {this_cpt_sn}' )

        # Loop over sub-components
        try:
            self.__loopSubComponents( this_cpt_doc, this_cpt_localdb_doc )
        except Exception as e:
            logger.warning( str(e) )
            logger.warning( traceback.format_exc() )

            # end for this_cpt_sn in cpts:
        # end for cpt_type, cpts in d_cpts.items():

        [ t.join() for t in threading.enumerate() if t.name in [ 'syncTestRun', 'processTestRun', 'processAttachment' ] ]


        ######################################
        ## Step-3
        ## register components and all parent-child relations

        for component_item in self.component_list:

            localdb.component.remove( {"_id" : ObjectId( component_item["_id"] ) } )

            try:
                localdb.component.insert(component_item)
                logger.info( f'RecursiveComponentsSynchronizer: inserted component {component_item["name"]}.')
                logger.debug( pprint.pformat( component_item ) )
            except:
                logger.info("RecursiveComponentsSynchronizer: This module's serial number has been changed for some reason.")
                old_component_name = localdb.component.find_one({"_id":ObjectId(component_item["_id"])})["name"]
                localdb.component.update( {"_id" : ObjectId( component_item["_id"] ) },
                                          {"$set": { "name"        : component_item["name"],
                                                     "serialNumber": component_item["serialNumber"],
                                                     "proID"       : component_item["proID"],
                                                     "properties"  : component_item["properties"]    } } )

                localdb.componentTestRun.update_many( {"name":old_component_name}, {"$set": {"name": component_item["name"] } } )

                if component_item["componentType"] == "front-end_chip":
                    localdb.chip.update( {"_id":old_component_name}, {"$set": {"name": component_item["name"]} } )

        if len(self.cpr_list)>0:
            logger.info( "RecursiveComponentsSynchronizer: Inserting child-parent relations:" )
        for cpr in self.cpr_list:
            logger.info( 'RecursiveComponentsSynchronizer: parent: {}, child: {}'.format( cpr['parent'], cpr['child'] ) )
        if len(self.cpr_list):
            localdb.childParentRelation.insert_many(self.cpr_list)

        userdb.message.remove( { '_id':ticket_id } )

        if len( self.error_list ) > 0:
            timestamp = datetime.now()
            error_messages = '<pre>' + '\n'.join( [ msg.replace("<","&lt;").replace(">","&gt;") for msg in self.error_list ] ) + "</pre>"

            userdb.message.insert( { 'function':'RecursiveComponentsSynchronizer',
                                     'component':component_id,
                                     'timestamp':timestamp,
                                     'code':'END_SYNC_ERRORS',
                                     'message': "ERROR: The following errors were detected during pull. " + error_messages } )
            
        return self.cpt_sn

