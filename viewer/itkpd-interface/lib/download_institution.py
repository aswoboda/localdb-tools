import os, sys

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

import pprint

##############
## functions


def query_institution(doc):

    query = {"code": doc["code"]}
    if localdb.institution.find_one(query) == None:
        institution_info = {
            "code": doc["code"],
            "institution": doc["institution"],
            "address": "...",
            "HOSTNAME": "...",
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
        }
        localdb.institution.insert_one(institution_info)
    address = localdb.institution.find_one(query)

    return address

def create_institute(institute_doc):
    doc = {
        "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
        "dbVersion": dbv,
        "_id": ObjectId(institute_doc["id"]),
        "code": institute_doc["code"],
        "institution": institute_doc["name"],
    }
    return doc

def download_institution(code1, code2):

    u = itkdb.core.User(access_code1=code1, access_code2=code2)
    pd_client = itkdb.Client(user=u, use_eos = True )

    # download institute infomation
    logger.info("Start downloading institution info...")
    for institute_doc in pd_client.get("listInstitutions", json={}):
        doc = create_institute(institute_doc)
        if localdb.pd.institution.find_one({"_id":ObjectId(institute_doc["id"])}) == None:
            localdb.pd.institution.insert_one(doc)
        address = query_institution(doc)
    logger.info("Successfully finished downloading institutions.\n")

