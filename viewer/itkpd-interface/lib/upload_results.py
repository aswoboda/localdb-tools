import os, sys
from pickle import FALSE
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class QCResultUploader(PDInterface.PDInterface):
    
    def __upload( self, testSpecificCallback, doOverwrite = False ):
        
        # self.ldb_component             = super().getCompFromLocalDB(componentSerialNumber)
        # self.ldb_componentId           = str( self.ldb_component["_id"] )
        # self.ldb_componentType         = self.ldb_component["componentType"]
        # self.ldb_component_QC_info     = super().getQmsFromLocalDB(self.ldb_componentId)
        # self.ldb_component_prop_info   = super().getPmsFromLocalDB(self.ldb_componentId)
        # self.ldb_test_doc              = super().getQrFromLocalDB(result_id)
        # self.pdb_component_doc         = super().getCompFromProdDB(componentSerialNumber)
        # self.pdb_prev_testRuns         = [test for test in self.pd_client.get("listTestRunsByComponent", json={"component": self.pdb_component_doc["code"] }) ]
        
        isUploaded = False
        
        logger.info( '__upload(): processing stage {} / test type {}'.format( self.ldb_test_doc['currentStage'], self.testType ) )

        timestampAsString = self.__getQCStrTime( self.ldb_test_doc["sys"]["cts"] )
        
        isFreshResult = True
        
        goodTestRunIDs = []
        sameEnvGoodTestRunIDs = []
        
        # loop over ProdDB tests
        for prevTest in self.pdb_component_doc['tests']:
            if prevTest['code'] != self.testType:
                continue
            
            testRuns = prevTest['testRuns']
            
            for testRun in testRuns:
                if testRun['state'] != 'ready':
                    continue
                    
                if testRun['runNumber'] == str( self.ldb_test_doc["_id"] ):
                    isFreshResult = False
                    self.pdb_testRunId = testRun['id']
                    
                goodTestRunIDs.append( testRun['id'] )
                
                try:
                    
                    if self.testSetTemp != -9999:
                        
                        for i in range(3):
                            try:
                                tr_doc = self.pd_client.get("getTestRun", json={ "testRun":testRun["id"] } )
                                break
                            except Exception as e:
                                logger.warning( 'Exception in evaluating environment temperature: {}'.format(e) )
                        
                        props = tr_doc['properties']
                        for prop in props:
                            if prop['code'] == "ENVIRONMENT_TEMPERATURE":
                                env_temp = prop['value']
                                logger.info( 'testRun env_temp = {}, self.testSetTemp = {}'.format( env_temp, self.testSetTemp ) )
                                if float( env_temp ) == float( self.testSetTemp ):
                                    sameEnvGoodTestRunIDs.append( testRun['id'] )
                                break
                        
                            
                except Exception as e:
                    logger.warning( 'Exception in evaluating environment temperature: {}'.format(e) )
                    pass
                
        logger.info( '__upload(): good testRun ids: {}'.format( goodTestRunIDs ) )
        logger.info( '__upload(): good testRun ids of the same environment temperature setting: {}'.format( sameEnvGoodTestRunIDs ) )
        
        
        # In case of overwriting, always the localdb record is taken as a fresh one
        if doOverwrite:
            logger.warning('__upload(): overwrite flag was specified, force the prodDB testRun results by this LocalDB testRuns.' )
            isFreshResult = True
        
        
        if isFreshResult:
            
            logger.info( 'This is a fresh test run, uploading' )
            pprint.pprint( testSpecificCallback )
            pprint.pprint( 'test results = ')
            pprint.pprint( self.ldb_test_doc["results"] )
            
            # Force set the component stage on ProdDB to this stage
            try:
                self.pd_client.post( 'setComponentStage', json = { 'component':self.componentSerialNumber, 'stage':self.stage } )
                
            except Exception as e:
                logger.error( '{}'.format(e) )
                
            logger.info( 'Changed the ProdDB {} stage to {}'.format( self.componentSerialNumber, self.stage ) )
            
            if self.ldb_componentType == "module":
                
                if self.testType == "READOUT_IN_BASIC_ELECTRICAL_TEST":
                    test_result = testSpecificCallback()
                    self.__createRawResultAttachment( test_result )
                    
                else:
                    json = testSpecificCallback( self.__createTestTemplate(timestampAsString), timestampAsString )
                    
                    test_result = self.pd_client.post( "uploadTestRunResults", json=json )
                    
                    self.__createRawResultAttachment( test_result )
                     
                    #pprint.pprint( test_result )
            
                self.pdb_testRunId = test_result['testRun']['id']
            
                    
                if self.testType == "OPTICAL":
                    pic_name = self.ldb_test_doc["currentStage"] + "_optical_img" + ".png"
                    inspect_pic = self.ldb_test_doc["results"]["img_entire"]["target"]
                    data = fs.get(ObjectId(str(inspect_pic))).read()
                    with open(pic_name, 'wb') as f:
                        f.write(data)
                        f = open(pic_name, "rb")
                        os.remove(pic_name)
                        self.pd_client.post("createBinaryTestRunParameter", data=dict(testRun=test_result["testRun"]["id"], parameter="FULL_IMAGE"), files=dict(data=f) )
                        f.close()
                    
                    self.__AttachGoldens()
            
            # if the test is exclusive, delete all the rest
            # otherwise, delete only testRuns with the same temperature
            if self.isExclusiveTest:
                logger.info( 'This is an exclusive test, delete previous TestRun records...' )
                for tr_id in goodTestRunIDs:
                    self.pd_client.post("deleteTestRun", json = { "testRun": tr_id } )
                    logger.info( 'deleted testRun {}'.format( tr_id ) )
            else:
                for tr_id in sameEnvGoodTestRunIDs:
                    self.pd_client.post("deleteTestRun", json = { "testRun": tr_id } )
                    logger.info( 'deleted testRun {}'.format( tr_id ) )
            
            isUploaded = True
        else:
            logger.info( 'The test run is already recorded on ProdDB and is a ready state, skipping' )
        
            if self.isExclusiveTest:
                logger.info( 'This is an exclusive test, delete previous TestRun records...' )
                for tr_id in goodTestRunIDs:
                    if tr_id != self.pdb_testRunId:
                        self.pd_client.post("deleteTestRun", json = { "testRun": tr_id } )
                        logger.info( 'deleted testRun {}'.format( tr_id ) )
            else:
                for tr_id in sameEnvGoodTestRunIDs:
                    if tr_id != self.pdb_testRunId:
                        self.pd_client.post("deleteTestRun", json = { "testRun": tr_id } )
                        logger.info( 'deleted testRun {}'.format( tr_id ) )

        logger.info("Finished!!\n")
        #time.sleep(1)
    
        return isUploaded
        
        
    '''
    def __upload_functions(self, test_item, QC_info):
        

        if len(self.ldb_test_doc) == 1:
            logger.info("This result format is not proper.")
            return

        functions = {
            "OPTICAL"                                :  self.__upload_VI,
            "READOUT_IN_BASIC_ELECTRICAL_TEST"       :  self.__upload_electrical,
            "METROLOGY"                              :  self.__upload_metrology,
            "QUAD_METROLOGY"                         :  self.__upload_metrologyquad,
            "TRIPLET_METROLOGY"                      :  self.__upload_metrologytriplet,
            "FLATNESS"                               :  self.__upload_coplanarity,
            "SENSOR_IV"                              :  self.__upload_SensorIV,
            "SLDO_VI"                                :  self.__upload_SLDOVI,
            "MASS"                                   :  self.__upload_mass,
            "GLUE_MODULE_FLEX_ATTACH"                :  self.__upload_glueInformation,
            "WIREBONDING"                            :  self.__upload_wirebonding,
            "WIREBOND"                               :  self.__upload_wirebond,
            "PARYLENE"                               :  self.__upload_parylene,
            "THERMALCYCLE"                           :  self.__upload_thermal,
            "ADC_CALIBRATION"                        :  self.__upload_adccalibration,
            "TUNING"                                 :  self.__upload_tuning
            #"WP_ENVELOPE"                     :  self.__upload_wpenvelope
        }
        
        ti = test_item.replace("_30DEG", "").replace("_20DEG", "").replace("_MINUS15DEG", "")
        
        self.ldb_test_doc['testType'] = ti
        
        if self.ldb_test_doc != {} and ti in functions:
            functions[ti](self.pdb_component_doc, self.ldb_test_doc, ti )
        else: logger.info("There is no uploading function.")
        return

    def __upload_prop_functions(self, module_doc, prop_doc):
        if prop_doc == {}:
            logger.info("This result format is not proper.")
            return

        prop_functions = {
            "RD53A_PULL-UP_RESISTOR"    : self.__upload_pullupregistor,
            "IREFTRIM_FE"               : self.__upload_ireftrim,
            "ORIENTATION"               : self.__upload_orientation
        }

        if prop_doc != {} and prop_doc["testType"] in prop_functions: prop_functions[prop_doc["testType"]](self.pdb_component_doc, prop_doc)
        else: logger.info("There is no uploading function.")
        return


########################
## for non-electrical ##
    def __upload_VI(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createOpticalTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
            attach_golden_imgs = self.__AttachGoldens(new_test_result)
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createOpticalTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                attach_golden_imgs = self.__AttachGoldens(new_test_result)
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_metrology(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMetrologyTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMetrologyTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_metrologyquad(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMetrologyQuadTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMetrologyQuadTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_metrologytriplet(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMetrologyTripletTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMetrologyTripletTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_coplanarity(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createCoplanarityTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createCoplanarityTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_SensorIV(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                tr_doc["testType"]["code"] = self.testType
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    if previous_ldb_test_doc["0"]["testType"] == self.testType:
                        result_counter = result_counter + 1
                        previous_ldb_test_docs.append(previous_ldb_test_doc["0"]["_id"])
                except:
                    logger.warning("this is not localdb results")

        if result_counter == 0:
            new_test_result = self.__createSensorIVTestRun(timestampAsString, self.testType)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                try:
                    shutil.rmtree(cache_dir)
                except:
                    pass
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createSensorIVTestRun(timestampAsString, self.testType)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                self.__createRawResultAttachment()
                #self.__deleteResult(tr_docs[i], new_test_result["testRun"]["id"], previous_ldb_test_docs[i])

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_SLDOVI(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createSLDOVITestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createSLDOVITestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_mass(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createMassTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createMassTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_glueInformation(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createGlueTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createGlueTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_wirebonding(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createWirebondingTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            attach_bonding_program = self.__attachBondingProgram(self.ldb_test_doc, new_test_result)
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createWirebondingTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                attach_bonding_program = self.__attachBondingProgram(self.ldb_test_doc, new_test_result)
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)
    '''

    def __attachBondingProgram(self, localdb_test_doc, new_test_result):
        try:
            fsb = gridfs.GridFSBucket(localdb)
            bond_file = open('static/Bonding_Program.dat','wb+')
            fsb.download_to_stream(ObjectId(self.ldb_test_doc["results"]["property"]["Bond_program"]), bond_file)
            bond_file.seek(0)
            contents = bond_file.read()
        except:
            return

        attachment_file = open('static/Bonding_Program.dat', 'rb')
        page_attachment = {"data": ('Bonding_Program.dat', attachment_file, "dat")}

        self.pd_client.post( "createTestRunAttachment", data=dict( testRun=new_test_result["testRun"]["id"], type="file" ), files=page_attachment )
        return  os.remove('static/Bonding_Program.dat')

    '''
    def __upload_wirebond(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createWirebondTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createWirebondTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_parylene(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"] == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun": tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createParyleneTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createParyleneTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_thermal(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createThermalTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            attach_bonding_program = self.__attachThermalProgram(self.ldb_test_doc, new_test_result)
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createThermalTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                attach_bonding_program = self.__attachThermalProgram(self.ldb_test_doc, new_test_result)
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)
    '''

    def __attachThermalProgram(self, localdb_test_doc, new_test_result):
        try:
            fsb = gridfs.GridFSBucket(localdb)
            temp_file = open('static/Thermal_temp.json', 'wb+')
            fsb.download_to_stream(ObjectId(self.ldb_test_doc["results"]["Temperature_log"]), temp_file)
            temp_file.seek(0)
            contents = temp_file.read()
        except:
            return

        attachment_file_temp = open('static/Thermal_temp.json', 'rb')
        page_attachment_temp = {"data": ('Thermal_temp.json', attachment_file_temp, "json")}

        self.pd_client.post( "createTestRunAttachment", data=dict( testRun=new_test_result["testRun"]["id"], type="file" ), files=page_attachment_temp )
        os.remove('static/Thermal_temp.json')

        try:
            fsb = gridfs.GridFSBucket(localdb)
            humid_file = open('static/Thermal_humid.json', 'wb+')
            fsb.download_to_stream(
                ObjectId(self.ldb_test_doc["results"]["Humidity_log"]), humid_file)
            humid_file.seek(0)
            contents = humid_file.read()
        except:
            return

        attachment_file_humid = open('static/Thermal_humid.json', 'rb')
        page_attachment_humid = {
            "data": ('Thermal_temp.json', attachment_file_humid, "json")
        }

        self.pd_client.post("createTestRunAttachment",
                            data=dict(testRun=new_test_result["testRun"]["id"],
                                      type="file"),
                            files=page_attachment_humid)
        os.remove('static/Thermal_humid.json')

        return

    '''
    def __upload_adccalibration(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"] == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun": tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createADCcalibrationTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createADCcalibrationTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_tuning(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"] == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun": tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createTuningTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createTuningTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)

    def __upload_wpenvelope(self):
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"] == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                result_counter = result_counter + 1
                tr_doc = self.pd_client.get("getTestRun", json={"testRun": tr["id"]})
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                except:
                    logger.warning("This is not LocalDB result")
        if result_counter == 0:
            new_test_result = self.__createWPenvelopeTestRun(timestampAsString)
            self.ldb_test_doc.pop("pdb_prev_testRuns")
            new_test_attachment = self.__createRawResultAttachment()
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                #                shutil.rmtree("{}/attachments".format("."))
                logger.info("This result is already uploaded.")
            else:
                new_test_result = self.__createWPenvelopeTestRun(timestampAsString)
                self.ldb_test_doc.pop("pdb_prev_testRuns")
                new_test_attachment = self.__createRawResultAttachment()
                #self.__deleteResult(tr_doc, new_test_result["testRun"]["id"], previous_ldb_test_doc)

        logger.info("Finished!!\n")
        #time.sleep(1)
    '''

    ####################
    ## for electrical ##
    ## main function
    '''
    def __upload_electrical(self):
        #check if a result is already uploaded or not
        qr_oid = self.ldb_test_doc["_id"]
        qr_doc = super().getQrFromLocalDB(qr_oid)
        timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
        result_counter = 0
        previous_ldb_test_docs = []
        tr_docs = []
        for tr in self.ldb_test_doc["pdb_prev_testRuns"]:
            if tr["state"]  == "ready" and tr["testType"]["code"] == self.testType and tr["stage"]["code"] == self.ldb_test_doc["currentStage"]:
                tr_doc = self.pd_client.get("getTestRun", json={"testRun":tr["id"]})
                tr_doc["testType"]["code"] = self.testType
                try:
                    previous_ldb_test_doc = self.__getOldResult(tr_doc)
                    if previous_ldb_test_doc["0"]["testType"] == self.testType:
                        result_counter = result_counter + 1
                        previous_ldb_test_docs.append(previous_ldb_test_doc['0']['_id'])
                        tr_docs.append(tr_doc)
                except:
                    logger.warning("Results not uploaded from LocalDB exist")
        if result_counter == 0:
            self.__create_electrial_pdb(self.pdb_component_doc, self.ldb_test_doc, self.testType)
        else:
            if str(self.ldb_test_doc["_id"]) in previous_ldb_test_docs:
                try:
                    shutil.rmtree(cache_dir)
                except:
                    pass
                logger.info("This result is already uploaded.")
                return
            else:
                self.__create_electrial_pdb(self.pdb_component_doc, self.ldb_test_doc, self.testType)
                #for result in tr_docs[i]['results']:
                #    if result['code'] == "RESULT_IDS":
                #        for scan_id in result['value']:
                #            self.pd_client.post("deleteTestRun", json={"testRun": scan_id})
                #self.__deleteResult(tr_docs[i], new_test_result["testRun"]["id"], previous_ldb_test_docs[i])

        logger.info("Finished!!\n")
        return
        #time.sleep(1)
    '''


    def __create_electrial_pdb(self):
        cp_test_map = {}
        timestampAsString = None
        scans = []

        #create cache files
        for i, d_ in enumerate(self.ldb_test_doc["results"]["scans"]):
            scanType, tr_oid = d_["name"], d_["runId"]
            scans.append(d_["name"])
            path = "{}/{}".format(IF_DIR,d_["name"])
            if os.path.exists(path): shutil.rmtree(path)
            command = ["{}/itkpd-interface/localdb-interface/bin/localdbtool-retrieve".format(VIEWER_DIR), "pull", "--test", d_["runId"], "--directory", path]
            subprocess.call(command)
            if i == 0:
                qr_doc = super().getQrFromLocalDB(self.ldb_test_doc["_id"])
                timestampAsString = self.__getQCStrTime(qr_doc["sys"]["cts"])
                #                tr_doc = super().getTrFromLocalDB(tr_oid)
                #                timestampAsString = self.__getStrTime(tr_doc["timestamp"])
                
            # fix scan result's files if component name is changed
            scan_files = os.listdir(path)

            # create relationship between past & new components
            previous_chips = []
            new_chips = []
            previous_module_name = ""
            new_module_name = ""
            compTestRun = localdb.componentTestRun.find({"testRun" : d_["runId"]})
            for item in compTestRun:
                if "scanSN" in item:
                    if item["chip"] == "module":
                        previous_module_name = item["scanSN"]
                        new_module_name = item["name"]
                    else:
                        previous_chips.append(item["scanSN"])
                        new_chips.append(item["name"])

            if previous_module_name:
                # fix contents of scan files
                for file in scan_files:
                    f1 = open(path + "/" + file, 'r', encoding='utf-8').read()
                    f2 = open(path + "/" + file, 'w', encoding='utf-8')
                    for i in range(len(previous_chips)):
                        f1 = f1.replace(previous_chips[i], new_chips[i])
                    if file == "connectivity.json" or file == "scanLog.json":
                        f1 = f1.replace(previous_module_name, new_module_name)
                        f1 = f1.replace(path + "//", "/")
                    f2.write(f1)

                # fix file name
                for file in scan_files:
                    for i in range(len(previous_chips)):
                        if previous_chips[i] in file:
                            new_file = file.replace(previous_chips[i], new_chips[i])
                            os.rename(path + "/" + file, path + "/" + new_file)

        #for FE chips
        fe_docs = super().getFEchipsDocs(self.pdb_component_doc["serialNumber"])
        for fe_doc in fe_docs:
            new_test_result = self.__createElecTestRunForFEChip(fe_doc["serialNumber"], timestampAsString)
            cp_test_map[fe_doc["serialNumber"]] = new_test_result["testRun"]["id"]
            for scan in scans:
                logger.info('{}'.format( scan ) )
                zip_name = "{}_{}".format(fe_doc["serialNumber"],scan)
                zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
                if os.path.exists(zip_path): os.remove(zip_path)
                zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
                files = glob.glob("{}/{}/*".format(IF_DIR,scan))
                for filepath in files:
                    filename = filepath.split("/")[-1]
                    if fe_doc["serialNumber"] in filename: zip_file.write(filepath, filename)
                zip_file.close()
                self.__attachFileToTestRun(zip_path, scan + ".zip", cp_test_map[fe_doc["serialNumber"]])

        #for module
        new_module_result = self.__createElecTestRunForModule(timestampAsString, cp_test_map, self.testType)
        logger.info('here')
        new_test_attachment = self.__createRawResultAttachment( new_module_result )
        logger.info('here')
        for i, scan in enumerate(scans):
            logger.info('here2 {}'.format( scan ) )
            zip_name = "{}_{}".format(self.pdb_component_doc["serialNumber"],scan)
            zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
            if os.path.exists(zip_path): os.remove(zip_path)
            zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
            files = glob.glob("{}/{}/*".format(IF_DIR,scan))
            for filepath in files:
                filename = filepath.split("/")[-1]
                if i == 0 and (filename.split(".")[0] in [ "dbCfg", "userCfg", "siteCfg" ]): self.__attachFileToTestRun(filepath, filename, new_module_result["testRun"]["id"])
                if filename.split(".")[0] in [ scan, "scanLog"]:
                    if filename.split(".")[0] == "scanLog": self.__modifyScanLog(filepath, self.ldb_test_doc["currentStage"])
                    zip_file.write(filepath, filename)

            zip_file.close()
            self.__attachFileToTestRun(zip_path, scan + ".zip" , new_module_result["testRun"]["id"])

        # attach the best configs to the module's top page.
        for scan in scans:
            logger.info('here3 {}'.format( scan ) )
            if scan == "std_thresholdscan":
                zip_name = "{}_{}_degree_{}".format("BestCfg",str(self.testSetTemp).replace("-","minus"), self.ldb_test_doc["currentStage"])
                zip_path = "{0}/{1}.zip".format(IF_DIR, zip_name)
                if os.path.exists(zip_path): os.remove(zip_path)
                zip_file = zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED)
                for fe_doc in fe_docs:
                    filepath = "{}/{}/{}".format(IF_DIR,scan,fe_doc["serialNumber"] + ".json.after")
                    if os.path.exists(filepath): zip_file.write(filepath, filepath.split("/")[-1])
                zip_file.close()
                self.__attachFileToComp(zip_path, zip_name + ".zip", self.pdb_component_doc)

        return new_module_result



#########################
## for module property ##
## main function
    def __upload_pullupregistor(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createPullupTestRun(prop_doc, self.pdb_component_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, self.pdb_component_doc)

        logger.info("Finished!!\n")

    def __upload_ireftrim(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createIreftrimTestRun(prop_doc, self.pdb_component_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, self.pdb_component_doc)

        logger.info("Finished!!\n")

    def __upload_orientation(self, module_doc, prop_doc):
        pr_oid = prop_doc["_id"]
        pr_doc = super().getQrFromLocalDB(pr_oid)
        new_prop_result = self.__createOrientationTestRun(prop_doc, self.pdb_component_doc)
        new_prop_attachment = self.__createPropertyAttachment(prop_doc, self.pdb_component_doc)

        logger.info("Finished!!\n")


    ## sub functions
    def __getStrTime(self, time):
        this_time = time.split("_")
        scan_date = this_time[0].split("-")
        scan_time = this_time[1].split(":")
        return  str(scan_date[2] + "." + scan_date[1] + "." + scan_date[0] + " " + scan_time[0] + ":" + scan_time[1])

    #####################
    ## for non-electrical

    def __getQCStrTime(self, time):
        this_time = time.strftime('%Y-%m-%dT%H:%M:%S.%fZ')

        return  this_time

    def __createTestTemplate(self, timestampAsString):
        
        project = {"project": self.pdb_component_doc["project"]["code"]}
        componentType = {"componentType": self.pdb_component_doc["componentType"]["code"]}
        pd_testType = {"code": self.testType}

        test_template = self.pd_client.get("generateTestTypeDtoSample", json={**project, **componentType, **pd_testType})
        
        json_template = {
            **test_template,
            "component": self.pdb_component_doc["serialNumber"],
            "institution": self.pdb_component_doc["currentLocation"]["code"],
            "date": timestampAsString,
            "runNumber": str( self.ldb_test_doc["_id"] )
        }

        return json_template

    def __createRawResultAttachment(self, test_result ):
        attachment_doc = self.ldb_test_doc
        attachment_doc["_id"] = str(attachment_doc["_id"])
        pdb_testType = self.testType
        
        # pprint.pprint( self.ldb_test_doc )
        
        try:
            attachment_doc["sys"]["cts"] = self.ldb_test_doc["sys"]["cts"].isoformat()
            attachment_doc["sys"]["mts"] = self.ldb_test_doc["sys"]["mts"].isoformat()
            attachment_doc["startTime"] = self.ldb_test_doc["startTime"].isoformat()
        except:
            try:
                attachment_doc["sys"]["cts"] = self.ldb_test_doc["sys"]["cts"]
                attachment_doc["sys"]["mts"] = self.ldb_test_doc["sys"]["mts"]
                attachment_doc["startTime"] = self.ldb_test_doc["startTime"]
            except:
                pass
        
        f = open(self.testType + '_results.json', 'w')
        attachment_doc["testType"] = str(self.testType)
        #logger.info( 'attachment_doc = {}'.format( attachment_doc ) )
        json.dump(attachment_doc, f, indent=4)
        f.close()

        attachment_file = open(pdb_testType + '_results.json', 'rb')
        page_attachment = {"data": (pdb_testType + '_results.json', attachment_file, "json")}

        self.pd_client.post( "createTestRunAttachment", data=dict( testRun = test_result["testRun"]["id"], type="file" ), files=page_attachment )
        return  os.remove(pdb_testType + '_results.json')


    #######################
    ## for Optical Inspection
    def __createOpticalTestRun(self, json, timestampAsString):
        json["results"]["FULL_IMAGE"] = ""

        return json

    def __AttachGoldens(self, new_test_result):
        target_name = str(self.ldb_test_doc["results"]["img_entire"]["target"]) + ".png"
        target_pic = self.ldb_test_doc["results"]["img_entire"]["target"]
        data_target = fs.get(ObjectId(str(target_pic))).read()
        with open(target_name, 'wb') as f:
            f.write(data_target)

        golden_name = str(self.ldb_test_doc["results"]["img_entire"]["reference"]) + ".png"
        golden_pic = self.ldb_test_doc["results"]["img_entire"]["reference"]
        data_golden = fs.get(ObjectId(str(golden_pic))).read()
        with open(golden_name, 'wb') as f:
            f.write(data_golden)

        tiles = []
        for i, item in enumerate(self.ldb_test_doc["results"]["img_tile"]):
            tiles.append({})
            tiles[i]["name"] = str(self.ldb_test_doc["results"]["img_tile"][item]) + ".png"
            tiles[i]["pic"] = self.ldb_test_doc["results"]["img_tile"][item]
            tiles[i]["data"] = fs.get(ObjectId(str(tiles[i]["pic"]))).read()
            with open(tiles[i]["name"], 'wb') as f:
                f.write(tiles[i]["data"])

        zip_name = "GoldenImages_Inspect"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        zip_file.write(target_name)
        os.remove(target_name)
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)

        zip_name = "GoldenImages_Golden"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        zip_file.write(golden_name)
        os.remove(golden_name)
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)


        zip_name = "GoldenImages_Tile"
        zip_path = zip_name + ".zip"
        if os.path.exists(zip_path): os.remove(zip_path)
        zip_file = zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED)

        for item in tiles:
            zip_file.write(item["name"])
            os.remove(item["name"])
        zip_file.close()
        self.__attachFileToTestRun(zip_path, zip_path, new_test_result["testRun"]["id"])
        os.remove(zip_path)

        return


    #######################
    ## for Mass measurement
    def __createMassTestRun(self, json, timestampAsString):
        json["properties"]["ACCURACY"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["property"]["Scale_accuracy"])))
        json["results"]["MASS"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["mass_value"])))
        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ################
    ## for Metrology
    def __createMetrologyTestRun(self, json, timestampAsString):
        json["results"]["DISTANCE_TOP"]    = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["distance top"])))
        json["results"]["DISTANCE_LEFT"]   = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["distance left"])))
        json["results"]["DISTANCE_RIGHT"]  = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["distance right"])))
        json["results"]["DISTANCE_BOTTOM"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["distance bottom"])))

        json["results"]["ANGLE_OF_BARE_VS_FLEX"] = [
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle top-left"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle top-right"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle bottom-left"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle bottom-right"])))
        ]

        json["results"]["MODULE_THICKNESS_PICKUP_AREA"] = [
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness pickup area chip1"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness pickup area chip2"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness pickup area chip3"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness pickup area chip4"])))
        ]
        try:
            json["results"]["MODULE_THICKNESS_EDGE"] = [
              float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness edge chip1"]))),
              float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness edge chip2"]))),
              float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness edge chip3"]))),
              float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness edge chip4"])))
            ]
        except:
            json["results"]["MODULE_THICKNESS_EDGE"] = [1.0, 1.0, 1.0, 1.0]

        json["results"]["MODULE_THICKNESS_HV_CAPACITOR"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness HV capacitor"])))
        json["results"]["MODULE_THICKNESS_DATA_CONNECTOR"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["module thickness Data connector"])))

        try:
            json["results"]["PLANARITY_VACUUM_ON"]  = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["planarity vacuum on"])))
            json["results"]["PLANARITY_VACUUM_OFF"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["planarity vacuum off"])))
            json["results"]["PLANARITY_VACUUM_ON_STD_DEV"]  = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["planarity vacuum on std dev"])))
            json["results"]["PLANARITY_VACUUM_OFF_STD_DEV"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["planarity vacuum off std dev"])))
        except:
            json["results"]["PLANARITY_VACUUM_ON"]  = 1.0
            json["results"]["PLANARITY_VACUUM_OFF"] = 1.0
            json["results"]["PLANARITY_VACUUM_ON_STD_DEV"]  = 1.0
            json["results"]["PLANARITY_VACUUM_OFF_STD_DEV"] = 1.0

        json["comments"] = [self.ldb_test_doc["results"]["comment"], "No results about Thickness Edge and Planarity Vacuum on/off"]

        return json

    def __createMetrologyQuadTestRun(self, json, timestampAsString):
        json["results"]["DISTANCE_TOP"] = ""
        json["results"]["Z_AVERAGE"] = ""
        json["results"]["DISTANCE_LEFT"] = ""
        json["results"]["DISTANCE_RIGHT"] = ""
        json["results"]["DISTANCE_BOTTOM"] = ""
        json["results"]["ANGLE_BARE_VS_FLEX"] = ""
        json["results"]["Z_STDDEV"] = ""
        json["results"]["Z_STDDEV_PICKUP_POINTS"] = []
        json["results"]["Z_AVERAGE_PICKUP_POINTS"] = []
        json["results"]["THICKNESS_HV_CAP"] = ""
        json["results"]["THICKNESS_DATA_CONN"] = ""
        json["results"]["THICKNESS_LEFT_EDGE"] = ""
        json["results"]["THICKNESS_RIGHT_EDGE"] = ""

        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    def __createMetrologyTripletTestRun(self, json, timestampAsString):
        json["results"]["DISPLACEMENT_X"] = []
        json["results"]["DISPLACEMENT_Y"] = []
        json["results"]["THICKNESS_GLUE"] = []
        json["results"]["ENVELOPE_DATA_CONNECTOR"] = []
        json["results"]["ENVELOPE_HV_CAP"] = []
        json["results"]["LENGTH"] = []
        json["results"]["FLEX_POSITION_SIDE"] = []
        json["results"]["FLEX_POSITION_ROTATION"] = []
        json["results"]["THICKNESS_BARE_MODULE"] = []
        json["results"]["THICKNESS_GLUE_STD_DEV"] = []
        json["results"]["FLATNESS"] = []

        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ##################
    ## for coplanarity
    def __createCoplanarityTestRun(self, json, timestampAsString):
        json["results"]["ANGLE"] = [
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle alpha"]))),
          float('{:.3g}'.format(float(self.ldb_test_doc["results"]["angle beta"])))
        ]
        json["results"]["BACKSIDE_COPLANARITY"] = float('{:.3g}'.format(float(self.ldb_test_doc["results"]["coplanarity"])))

        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ################
    ## for Sensor IV
    def __createSensorIVTestRun(self, json, timestampAsString):
        json["results"]["TIME"] = []
        json["results"]["CURRENT_MEAN"] = []
        json["results"]["CURRENT_SIGMA"] = []
        json["results"]["VOLTAGE"] = []
        json["results"]["HUMIDITY"] = []
        json["results"]["TEMPERATURE"] = []
        for items in self.ldb_test_doc["results"]["Sensor_IV"]:
            json["results"]["TIME"].append(float('{:.3g}'.format(items["Time"])))
            json["results"]["CURRENT_MEAN"].append(float('{:.3g}'.format(items["Current_mean"])))
            json["results"]["CURRENT_SIGMA"].append(float('{:.3g}'.format(items["Current_sigma"])))
            json["results"]["VOLTAGE"].append(float('{:.3g}'.format(items["Voltage"])))
            if "Humidity" in items:
                json["results"]["HUMIDITY"].append(float('{:.3g}'.format(items["Humidity"])))
            if "Temperature" in items:
                json["results"]["TEMPERATURE"].append(float('{:.3g}'.format(items["Temperature"])))
        json["comments"] = [self.ldb_test_doc["results"]["comment"]]
        json["properties"]["ENVIRONMENT_TEMPERATURE"] = self.testSetTemp

        return json

    ##############
    ## for SLDO VI
    def __createSLDOVITestRun(self, json, timestampAsString):
        json["results"]["TIME"] = []
        json["results"]["CURRENT"] = []
        json["results"]["VOLTAGE_MEAN"] = []
        json["results"]["VOLTAGE_SIGMA"] = []
        json["results"]["HUMIDITY"] = []
        json["results"]["TEMPERATURE"] = []
        for items in self.ldb_test_doc["results"]["SLDO_VI"]:
            json["results"]["TIME"].append(float('{:.3g}'.format(items["Time"])))
            json["results"]["CURRENT"].append(float('{:.3g}'.format(items["Current"])))
            json["results"]["VOLTAGE_MEAN"].append(float('{:.3g}'.format(items["Voltage_mean"])))
            json["results"]["VOLTAGE_SIGMA"].append(float('{:.3g}'.format(items["Voltage_sigma"])))
            if "Humidity" in items:
                json["results"]["HUMIDITY"].append(float('{:.3g}'.format(items["Humidity"])))
            if "Temperature" in items:
                json["results"]["TEMPERATURE"].append(float('{:.3g}'.format(items["Temperature"])))
        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ################################
    ## for Glue module + flex attach
    def __createGlueTestRun(self, json, timestampAsString):
        json["properties"]["GLUE_TYPE"] = self.ldb_test_doc["results"]["property"]["Glue_name"]
        Main_A = self.ldb_test_doc["results"]["property"]["Volume_ratio_of_glue_mixture"].split(":")[1].split(",")[0]
        Sub_B = self.ldb_test_doc["results"]["property"]["Volume_ratio_of_glue_mixture"].split(":")[2]
        json["properties"]["RATIO"] = float(Main_A)/float(Sub_B)
        json["properties"]["NAME"] = self.ldb_test_doc["user"]
        json["properties"]["BATCH_NUMBER"] = self.ldb_test_doc["results"]["property"]["Glue_batch_number"]
        json["results"]["TEMP"] = self.ldb_test_doc["results"]["Room_temperature"]
        json["results"]["HUMIDITY"] = self.ldb_test_doc["results"]["Humidity"]
        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ##################
    ## for Wirebonding
    def __createWirebondingTestRun(self, json, timestampAsString):
        json["properties"]["MACHINE"] = self.ldb_test_doc["results"]["property"]["Machine"]
        json["properties"]["OPERATOR"] = self.ldb_test_doc["results"]["property"]["Operator_name"]
        json["properties"]["BOND_WIRE_BATCH"] = self.ldb_test_doc["results"]["property"]["Bond_wire_batch"]
        if self.ldb_test_doc["results"]["property"]["Bond_program"] != '':
            json["properties"]["BOND_PROGRAM"] = "Attached Bond Program"
        else:
            json["properties"]["BOND_PROGRAM"] = "No Bond Program"
        json["properties"]["BONDING_JIG"] = self.ldb_test_doc["results"]["property"]["Bonding_jig"]
        json["results"]["TEMPERATURE"] = self.ldb_test_doc["results"]["Temperature"]
        json["results"]["HUMIDITY"] = self.ldb_test_doc["results"]["Humidity"]
        json["comments"] = [self.ldb_test_doc["results"]["comment"]]

        return json

    ###############
    ## for Wirebond
    def __createWirebondTestRun(self, json, timestampAsString):
        json["properties"]["MACHINE"] = "No result"
        json["properties"]["SPEED"] = 0
        json["properties"]["SHEAR"] = 0
        json["properties"]["LOAD"] = 0
        json["properties"]["OPERATOR"] = "No result"
        json["results"]["MIN_LOAD"] = self.ldb_test_doc["results"]["minimum_load"]
        json["results"]["MAX_LOAD"] = self.ldb_test_doc["results"]["maximum_load"]
        json["results"]["MEAN_LOAD"] = self.ldb_test_doc["results"]["mean_load"]
        json["results"]["STD_DEV_LOAD"] = self.ldb_test_doc["results"]["load_standard_deviation"]
        json["results"]["PERCENT_HEEL_BREAKS"] = self.ldb_test_doc["results"]["percentage_of_heel_breaks"]
        json["comments"] = ["No properties for this test run" , self.ldb_test_doc["results"]["comment"]]

        return json

    #####################
    ## for Parylene info.
    def __createParyleneTestRun(self, json, timestampAsString):
        json["properties"]["BATCH_NUMBER"] = self.ldb_test_doc["results"]["property"]["Parylene_Batch_Number"]
        json["properties"]["PARYLENE_TYPE"] = self.ldb_test_doc["results"]["property"]["Parylene_Type"]
        json["properties"]["MASKING_OPERATOR"] = self.ldb_test_doc["results"]["property"]["Institution_of_Masking_Operator"]
        json["propertie"]["REMOVING_MASK_OPERATOR"] = self.ldb_test_doc["results"]["property"]["Institution_of_Operator_Removing_Mask"]

        json["results"]["THICKNESS"] = self.ldb_test_doc["results"]["Parylene_thickness_measured_by_vendor"]
        json["results"]["THICKNESS_ITK"] = self.ldb_test_doc["results"]["Parylene_thickness_measured_by_ITk_Institute"]

        return json

    #####################
    ### for Thermal Cycle
    def __createThermalTestRun(self, json, timestampAsString):
        json["properties"]["MACHINE"] = self.ldb_test_doc["results"]["property"]["Machine"]
        json["properties"]["MIN_TEMP"] = self.ldb_test_doc["results"]["property"]["Temp_min_value"]
        json["properties"]["MAX_TEMP"] = self.ldb_test_doc["results"]["property"]["Temp_max_value"]
        json["properties"]["NUM_CYCLES"] = self.ldb_test_doc["results"]["property"]["N_cycle"]
        json["properties"]["CYCLING_SPEED"] = self.ldb_test_doc["results"]["property"]["Cycle_speed_value"]

        return json

    ######################
    ## for ADC calibration
    def __createADCcalibrationTestRun(self, json, timestampAsString):
        try:
            json["results"]["FLAG"] = int( eval(self.ldb_test_doc["results"]["flag"]) )
        except:
            json["results"]["FLAG"] = 1

        return json

    #############
    ## for Tuning
    def __createTuningTestRun(self, json, timestampAsString):
        json["properties"] = {"ENVIRONMENT_TEMPERATURE":self.testSetTemp}
        json["results"]["FLAG"] = eval(self.ldb_test_doc["results"]["flag"])

        return json

    #############
    ## for WP envelope metrology
    def __createWPenvelopeTestRun(self, json, timestampAsString):
        json["results"]["DISTANCE_X"] = []
        json["results"]["DISTANCE_Y"] = []
        json["results"]["THICKNESS_MEAN"] = []
        json["results"]["THICKNESS_STDDEV"] = []

        return json

    #################
    ## for electrical
    def __getElecTestAnalysisResult(self, localdb_test_doc, chip_name):
        dict_ = {}
        if "analysis" in self.ldb_test_doc["results"]:
            for analysis in self.ldb_test_doc["results"]["analysis"]:
                if analysis["name"] == "bad_pixel_analysis":
                    for chip_result in analysis["result"]:
                        if chip_result["chip"] == chip_name:
                            for criteria in chip_result["result"]:
                                dict_[criteria["criteria"].upper()] = criteria["num"]
        return dict_

    def __createElecTestTemplate(self, doc, testType, timestampAsString):
        project = {"project": doc["project"]["code"]}
        componentType = {"componentType": doc["componentType"]["code"]}
        pd_testType = {"code": testType}

        test_template  = self.pd_client.get( "generateTestTypeDtoSample", json={**project, **componentType, **pd_testType} )

        json = {
          **test_template,
          "component": doc["serialNumber"],
          "institution": doc["currentLocation"]["code"],
          "date": timestampAsString,
          "properties":{},
          "results":{}
        }
        return json

    def __createElecTestRunForModule(self, timestampAsString, cp_test_map, test_item):
        json = self.__createElecTestTemplate(self.pdb_component_doc, self.testType, timestampAsString)
        json["properties"] = {"ENVIRONMENT_TEMPERATURE":self.testSetTemp}
        json["runNumber"] = str( self.ldb_test_doc["_id"] )
        json["results"]["RESULT_IDS"] = [v for i,v in cp_test_map.items()]
        json["results"]["DIGITAL_DEAD"] = []
        json["results"]["DIGITAL_BAD"] = []
        json["results"]["ANALOG_DEAD"] = []
        json["results"]["ANALOG_BAD"] = []
        json["results"]["TUNING_FAILED"] = []
        json["results"]["TUNING_BAD_THRESHOLD"] = []
        json["results"]["TUNING_BAD_TOT"] = []
        json["results"]["HIGH_ENC"] = []
        json["results"]["NOISY"] = []
        
        try:
            for chip in self.ldb_test_doc["results"]["analysis"][0]["result"]:
                logger.info( 'createElecTestRunForModule(): chip = {}'.format( chip ) ) 
                for values in chip["result"]:
                    if values["criteria"] == "digital_dead": json["results"]["DIGITAL_DEAD"].append(values["num"])
                    if values["criteria"] == "digital_bad": json["results"]["DIGITAL_BAD"].append(values["num"])
                    if values["criteria"] == "analog_dead": json["results"]["ANALOG_DEAD"].append(values["num"])
                    if values["criteria"] == "analog_bad": json["results"]["ANALOG_BAD"].append(values["num"])
                    if values["criteria"] == "tuning_failed": json["results"]["TUNING_FAILED"].append(values["num"])
                    if values["criteria"] == "tuning_bad_threshold": json["results"]["TUNING_BAD_THRESHOLD"].append(values["num"])
                    if values["criteria"] == "tuning_bad_tot": json["results"]["TUNING_BAD_TOT"].append(values["num"])
                    if values["criteria"] == "high_enc": json["results"]["HIGH_ENC"].append(values["num"])
                    if values["criteria"] == "noisy": json["results"]["NOISY"].append(values["num"])
        except Exception as e:
            logger.warning( '{}'.format( e ) )

        return self.pd_client.post("uploadTestRunResults",json=json)

    def __createElecTestRunForFEChip(self, chip_name, timestampAsString):
        child_doc = super().getCompFromProdDB(chip_name)
        json = self.__createElecTestTemplate(child_doc, self.testType, timestampAsString)
        json["properties"] = {"QC_STAGE":self.ldb_test_doc["currentStage"]}
        json["results"] = self.__getElecTestAnalysisResult(self.ldb_test_doc, chip_name)
        json["runNumber"] = str( self.ldb_test_doc["_id"] )
        
        # Set the FE test stage to an appropriate one
        json_stage = {"component": child_doc["serialNumber"], "stage": "TEST_AFTER_BUMPING"}
        self.pd_client.post("setComponentStage", json=json_stage )

        return self.pd_client.post( "uploadTestRunResults", json=json)

    def __attachFileToTestRun(self, filepath, filename, runId):
        with open(filepath, "rb") as f:
            upload_file = f
            upload_filetype = filename.split(".")[-1]
            page_attachment = { "data": (filename, upload_file, upload_filetype)}
            self.pd_client.post( "createTestRunAttachment", data=dict( testRun=runId, type="file" ), files=page_attachment )
        return

    def __attachFileToComp(self, filepath, filename, module_doc):
        for attachment in self.pdb_component_doc["attachments"]:
            if attachment["filename"] == filename:
                self.pd_client.post( "deleteComponentAttachment", json = {"component":self.pdb_component_doc["serialNumber"], "code": attachment["code"]} )
                logger.info("Delete an old config for the module page: " + filename)
        with open(filepath, "rb") as f:
            upload_file = f
            upload_filetype = filename.split(".")[-1]
            page_attachment = { "data": (filename, upload_file, upload_filetype)}
            self.pd_client.post( "createComponentAttachment", data=dict( component=self.pdb_component_doc["serialNumber"], type="file" ), files=page_attachment )
            logger.info("Attach a new config for the module page: " + filename)
        return

    def __modifyScanLog(self, filepath, curStage):
        with open(filepath) as f: df = json.load(f)
        df.pop("_id")
        df["connectivity"][0]["stage"] = curStage
        with open(filepath, 'w') as f: json.dump(df, f, indent=4)
        return


    #######################
    ## for Pullup registor
    def __createPullupTestRun(self, prop_doc, module_doc):
        for i in range(len(prop_doc["results"]["value"])):
            n = i + 1
            json_name = "json" + str(n)
            json_name = {
                "component": self.pdb_component_doc["serialNumber"],
                "code": "RD53A_PULL-UP_RESISTOR" + str(n),
                "value": prop_doc["results"]["value"]["chip"+str(n)]
            }
            self.pd_client.post("setComponentProperty", json=json_name)

        return "0"

    ########################
    ## for IrefTrim registor
    def __createIreftrimTestRun(self, prop_doc, module_doc):
        for i in range(len(prop_doc["results"]["value"])):
            n = i + 1
            json_name = "json" + str(n)
            json_name = {
                "component": self.pdb_component_doc["serialNumber"],
                "code": "IREFTRIM_FE" + str(n),
                "value": prop_doc["results"]["value"]["chip"+str(n)]
            }
            self.pd_client.post("setComponentProperty", json=json_name)

        return "0"

    ###########################
    ## for Orientation registor
    def __createOrientationTestRun(self, prop_doc, module_doc):
        json_name = {
            "component": self.pdb_component_doc["serialNumber"],
            "code": "ORIENTATION",
            "value": prop_doc["results"]["orientation"]
        }

        return self.pd_client.post("setComponentProperty", json=json_name)

    ##################################
    ## Create & Delete Attachmet for Properties
    def __createPropertyAttachment(self, prop_doc, module_doc):
        attachment_doc = prop_doc
        attachment_doc["_id"] = str(attachment_doc["_id"])
        attachment_doc["sys"]["cts"] = prop_doc["sys"]["cts"].isoformat()
        attachment_doc["sys"]["mts"] = prop_doc["sys"]["mts"].isoformat()
        f = open(prop_doc["testType"] + '_detail.json', 'w')
        json.dump(attachment_doc, f, indent=4)
        f.close()

        attachment_file = open(prop_doc["testType"] + '_detail.json', 'rb')
        page_attachment = {"data": (prop_doc["testType"] + '_detail.json', attachment_file, "json")}

        self.pd_client.post( "createComponentAttachment", data=dict( component=self.pdb_component_doc["serialNumber"], type="file" ), files=page_attachment )
        return  os.remove(prop_doc["testType"] + '_detail.json')

    def __deletePropertyAttachment(self, module_doc):
        for item in self.pdb_component_doc["attachments"]:
            if "detail.json" in item["filename"]:
                self.pd_client.post("deleteComponentAttachment", json={"component": self.pdb_component_doc["code"], "code": item["code"]} )

        return

    ########################
    ## set stage of ITkPD ##
    def __setStage(self, module_doc, module_QC_info):
        nextStage = module_QC_info["currentStage"]
        json_stage = {"component": self.pdb_component_doc["serialNumber"], "stage": nextStage}

        return self.pd_client.post("setComponentStage", json=json_stage )


    ########################
    ## delete old results ##
    def __getOldResult(self, tr_doc):
        cache_dir = "{}/attachments".format(".")
        try:
            shutil.rmtree(cache_dir)
        except:
            pass
        os.mkdir(cache_dir)
        previous_ldb_test_doc = {}
        for attachment in tr_doc["attachments"]:
            jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
            if "_results.json" in attachment["filename"]:
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    previous_ldb_test_doc["0"] = json.load(f)

        return previous_ldb_test_doc

#    def __deleteResult(self, tr_doc, new_result_id, previous_ldb_test_doc):
#        cache_dir = "{}/attachments".format(".")
#        attachment_file = open('attachments/' + "delete.json", 'rb')
#        page_attachment = {"data": (previous_ldb_test_doc["0"]["testType"] + "_deletedResults.json", attachment_file, "json")}
#        attach_ins = self.pd_client.post("createTestRunAttachment", data=dict( testRun=new_result_id, type="file" ), files=page_attachment )
#
#        self.pd_client.post("deleteTestRun", json={"testRun": tr_doc["id"]})
#        shutil.rmtree(cache_dir)
#        return page_attachment


###################
## main function ##
    def upload_results(self, componentSerialNumber, doOverwrite = False ):
        
        self.componentSerialNumber     = componentSerialNumber
        
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        doFilePath = IF_DIR + "/doing_upload_" + componentSerialNumber + ".txt"
        with open(doFilePath, "w") as f:
            f.write("doing now")

        if os.path.exists(IF_DIR + '/doing_upload_' + componentSerialNumber + '_fail' + '.txt'):
            os.remove(IF_DIR + '/doing_upload_' + componentSerialNumber + '_fail' + '.txt')
        
        logger.info("Module name: " + componentSerialNumber)
        
        
        self.doUploadFuncs = { "module":
                               {
                                   "ADC_CALIBRATION"                  : self.__createADCcalibrationTestRun,
                                   "FLATNESS"                         : self.__createCoplanarityTestRun,
                                   "READOUT_IN_BASIC_ELECTRICAL_TEST" : self.__create_electrial_pdb,
                                   "GLUE_MODULE_FLEX_ATTACH"          : self.__createGlueTestRun,
                                   "MASS"                             : self.__createMassTestRun,
                                   "METROLOGY"                        : self.__createMetrologyTestRun,
                                   "OPTICAL"                          : self.__createOpticalTestRun,
                                   "PARYLENE"                         : self.__createParyleneTestRun,
                                   "SLDO_VI"                          : self.__createSLDOVITestRun,
                                   "SENSOR_IV"                        : self.__createSensorIVTestRun,
                                   "THERMALCYCLE"                     : self.__createThermalTestRun,
                                   "TUNING"                           : self.__createTuningTestRun,
                                   "WP_ENVELOPE"                      : self.__createWPenvelopeTestRun,
                                   "WIREBOND"                         : self.__createWirebondTestRun,
                                   "WIREBONDING"                      : self.__createWirebondingTestRun
                               }
                           }
        
        
        #----------------------------------------------------------------------------------------------
        # Collect informations from LocalDB
        # Hereafter, LocalDB variables start from prefix "ldb_"
        
        self.ldb_component             = super().getCompFromLocalDB(componentSerialNumber)
        self.ldb_componentId           = str( self.ldb_component["_id"] )
        self.ldb_componentType         = self.ldb_component["componentType"]
        self.ldb_component_QC_info     = super().getQmsFromLocalDB(self.ldb_componentId)
        self.ldb_component_prop_info   = super().getPmsFromLocalDB(self.ldb_componentId)
        
        if not self.ldb_componentType in self.doUploadFuncs:
            logger.error( 'Component type "{}" is not supported yet.'.format( self.ldb_componentType ) )
            return
        
        stageFlow = [item for item in self.ldb_component_QC_info["QC_results"] ]
        
        print( super().getCompFromLocalDB(componentSerialNumber) )
        #print( '\n\n\n\n' )
        #pprint.pprint( 'self.ldb_component_QC_info: ' )
        #pprint.pprint( self.ldb_component_QC_info )
        
        try:
            self.ldb_upload_status = self.ldb_component_QC_info["upload_status"]
        except:
            self.ldb_upload_status = {}
            for stage in self.ldb_component_QC_info["stage_flow"]:
                self.ldb_upload_status[stage] = "-1"
            localdb.QC.module.status.update_one( {"_id": ObjectId(str(self.ldb_component_QC_info["_id"]))}, {"$set": {"upload_status": self.ldb_upload_status }} )
            
        
        #pprint.pprint( 'self.ldb_upload_status: ')
        #pprint.pprint( self.ldb_upload_status )
        
        
        # Include up to the previous stage wrt the currentStage as a transaction stage
        transactionStages = []
        for stage in stageFlow:
            if stage == self.ldb_component_QC_info["currentStage"]:
                break
            transactionStages.append(stage)
            
        #print( '\n\n\n\n' )
        logger.info('Transaction Stages: {}'.format( transactionStages ) )
        
        #----------------------------------------------------------------------------------------------
        # Collect ProdDB Component and TestRun docs
        # Hereafter, LocalDB variables start from prefix "pdb_"
        
        self.pdb_component_doc        = super().getCompFromProdDB(componentSerialNumber)
        self.pdb_prev_testRuns        = [test for test in self.pd_client.get("listTestRunsByComponent", json={"component": self.pdb_component_doc["code"] }) ]
        
        #print( '\n\n\n\n' )
        #pprint.pprint('self.pdb_prev_testRuns:')
        #pprint.pprint( self.pdb_prev_testRuns )
        
        
        #----------------------------------------------------------------------------------------------
        # Loop over stages
        
        for stage in transactionStages:
            
            self.stage = stage
            
            isAtLeastOneTestUploaded = False
            
            # Loop over tests of the stage
            for testType, result_id in self.ldb_component_QC_info["QC_results"][stage].items():
                
                # If the result is blank, skip
                if result_id == "-1": continue
                
                logger.info("Test : {}.{}".format( stage, testType ) )
                
                self.ldb_test_doc = super().getQrFromLocalDB(result_id)
                
                #pprint.pprint( 'self.ldb_test_doc:' )
                #pprint.pprint( self.ldb_test_doc )
                
                self.testSetTemp = -9999
                if testType.find("30DEG") >=0 or testType.find("30_DEG") >= 0:
                    self.testSetTemp = 30
                elif testType.find("20DEG") >=0 or testType.find("20_DEG") >= 0:
                    self.testSetTemp = 20
                if testType.find("MINUS15DEG") >=0 or testType.find("min15_DEG") >= 0:
                    self.testSetTemp = -15
                
                self.testType = testType.replace("_30DEG", "").replace("_20DEG", "").replace("_MINUS15DEG", "")
                
                self.isExclusiveTest = ( self.testType == testType )
                
                testSpecificCallback = None
                try:
                    
                    testSpecificCallback = self.doUploadFuncs[ self.ldb_componentType ][ self.testType ]
                    
                except Exception as e:
                    logger.error( 'Error in self.doUploadFuncs[ self.testType ] ()' )
                    logger.error( 'self.ldb_componentType = {}'.format( self.ldb_componentType ) )
                    logger.error( 'testType = {}'.format( self.testType ) )
                    logger.error( '{}'.format( e ) )
                    
                # Execute uploading
                if testSpecificCallback:
                    
                    isUploaded = self.__upload( testSpecificCallback, doOverwrite )
                    
                    if isUploaded:
                        isAtLeastOneTestUploaded = True
                    
                    # Update the QC status in LocalDB
                    if isUploaded:
                        localdb.QC.module.status.update_one(
                            { "_id": self.ldb_component_QC_info["_id"] },
                            {"$set": {"QC_results_pdb.{}.{}".format( stage, testType ) : self.pdb_testRunId }} )
                    
                
            #endfor testType, result_id in self.ldb_component_QC_info["QC_results"][stage].items()
            
            # Sign-off the stage and go to the next stage
            localdb.QC.module.status.update_one( {"_id": self.ldb_component_QC_info["_id"]}, {"$set": {"upload_status.{}".format(stage): "1" }} )
            
            next_stage = stageFlow[ stageFlow.index(stage)+1 ]
            localdb.QC.module.status.update_one( {"_id": self.ldb_component_QC_info["_id"]}, {"$set": {"currentStage": next_stage}} )
            localdb.QC.module.status.update_one( {"_id": self.ldb_component_QC_info["_id"]}, {"$set": {"latestSyncedStage": stage}} )
            
        # endfor stage in transactionStages
        
        if not os.path.exists(IF_DIR + '/doing_upload_' + componentSerialNumber + "_fail" + '.txt'):
            stage_doc = self.__setStage(self.pdb_component_doc, self.ldb_component_QC_info)
            logger.info( "Chenged Stage in ITk production DB to " + stage_doc["currentStage"] + "."  )
        else:
            logger.info("Not changed Stage in ITk production DB.")

        logger.info("Finished for all results!!\n")

        os.remove(doFilePath)
        try:
            shutil.rmtree("{}/attachments".format("."))
        except:
            pass

        if os.path.exists(IF_DIR + '/doing_upload_' + componentSerialNumber + '.txt'):
            os.remove(IF_DIR + '/doing_upload_' + componentSerialNumber + '.txt')
        
        return

        
        '''        
        #----------------------------------------------------------------------------------------------
        # Loop over stages
        
        for stage in transactionStages:
            try:
                if self.ldb_upload_status[stage] == "-1":
                    json_stage = {"component": pdb_component_doc["serialNumber"], "stage": stage}
                    self.pd_client.post("setComponentStage", json=json_stage )
                    logger.info("Stage: " + str(stage))
                    for test_item, result_id in self.ldb_component_QC_info["QC_results"][stage].items():
                        logger.info("Test Item: " + str(test_item))
                        self.ldb_test_doc = super().getQrFromLocalDB(result_id) if result_id != "-1" else {}
                        self.ldb_test_doc["pdb_prev_testRuns"] = pdb_prev_testRuns
                        if result_id != "-1":
                            new_result_id = self.__upload_functions(pdb_component_doc, self.ldb_test_doc, test_item, self.ldb_component_QC_info)

                    if stage == "MODULEWIREBONDING":
                        self.__deletePropertyAttachment(pdb_component_doc)
                        for prop_item, prop_id in self.ldb_component_prop_info["QC_properties"].items():
                            logger.info("Property Item: " + str(prop_item))
                            if prop_id != "":
                                prop_doc = super().getPrFromLocalDB(prop_id)
                                self.__upload_prop_functions(pdb_component_doc, prop_doc)

                    if stage == "MODULERECEPTION":
                        next_stage = "complete"
                        localdb.QC.module.status.update_one( {"_id": ObjectId(str(self.ldb_component_QC_info["_id"]))}, {"$set": {"latestSyncedStage": next_stage}} )
                    else:
                        next_stage = self.ldb_component_QC_info["stage_flow"][self.ldb_component_QC_info["stage_flow"].index(stage)+1]
                        #pd_client.post("setComponentStage", json = {"component":componentSerialNumber, "stage":next_stage})
                        localdb.QC.module.status.update_one( {"_id": ObjectId(str(self.ldb_component_QC_info["_id"]))}, {"$set": {"latestSyncedStage": next_stage}} )
                    self.ldb_upload_status[stage] = "0"
                    localdb.QC.module.status.update_one( {"_id": ObjectId(str(self.ldb_component_QC_info["_id"]))}, {"$set": {"upload_status": self.ldb_upload_status }} )

                logger.info("")
            except Exception as e:
                print(e)
                if result_id == "-1":
                    logger.info(stage + " is not signed off...")
                else:
                    logger.info("Failed to upload result of " + str(test_item) + "...")
                    if not os.path.exists(IF_DIR):
                        os.makedirs(IF_DIR)
                    with open(IF_DIR + '/doing_upload_' + componentSerialNumber + "_fail" + '.txt', 'w') as f:
                        f.write(str(test_item) + '\n' + result_id)

        logger.info("Finished for all results!!\n")

        if not os.path.exists(IF_DIR + '/doing_upload_' + componentSerialNumber + "_fail" + '.txt'):
            stage_doc = self.__setStage(pdb_component_doc, self.ldb_component_QC_info)
            logger.info( "Chenged Stage in ITk production DB to " + stage_doc["currentStage"] + "."  )
        else:
            logger.info("Not changed Stage in ITk production DB.")

        os.remove(doFilePath)
        try:
            shutil.rmtree("{}/attachments".format("."))
        except:
            pass

        if os.path.exists(IF_DIR + '/doing_upload_' + componentSerialNumber + '.txt'):
            os.remove(IF_DIR + '/doing_upload_' + componentSerialNumber + '.txt')
        '''
