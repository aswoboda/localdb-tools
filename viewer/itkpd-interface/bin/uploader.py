#!/usr/bin/env python3
# -*- coding: utf-8 -*
######################################################
## Author1: Hiroki Okuyama (hiroki.okuyama at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Nov. 2019
## Project: Local Database Tools
## Description: ITkPD Interface
######################################################

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from functions.imports import *

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from upload_results import *

####################################
## upload test results


def upload():

    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    token = process_request(code1, code2)
    if token == 0:
        sys.exit(1)
        
    if args.component_id == None:
        logger.warning("Input a module name from the command line with the option '--component_id'")
        sys.exit(1)
        
    if args.overwrite == None:
        doOverwrite = False
    else:
        doOverwrite = True
        
    QCResultUploader(code1,code2).upload_results(args.component_id, doOverwrite )

if __name__ == "__main__":
    upload()
