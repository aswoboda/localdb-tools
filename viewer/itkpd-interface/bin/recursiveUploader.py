#!/usr/bin/env python3
# -*- coding: utf-8 -*


######################################################
## Author1: Hideyuki Oide (Hideyuki.Oide@cern.ch)
## Date: 2022-Nov-09
## Project: Local Database Tools
## Description: ITkPD Interface
######################################################

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from functions.imports import *

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from RecursiveUploader import *



if __name__ == "__main__":
    
    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    token = process_request(code1, code2)
    if token == 0:
        sys.exit(1)
    else:
        sync = RecursiveUploader(code1, code2)
        res = sync.exec( args.component_id ) 
        #pprint.pprint( res )

