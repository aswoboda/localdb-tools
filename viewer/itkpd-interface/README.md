status in 2nd Dec. 2019

# Start up ITkPD interfacing tools

`setup_interface_tool.sh` can ...

- Initialize the configulation file

## Pre Requirement

- centOS7
- MongoDB is running
- python3

## Login ITkPD

- Login ITkPD with Access code

```bash
$ source authenticate.sh
Input Access Code 1 for ITkPD: ${ACCESS CODE 1}
Input Access Code 2 for ITkPD: ${ACCESS CODE 2}
<some texts>
You have signed in as <YOUR_NAME>. Your token expires in 7197s.
```

## Download institution's info from ITkPD

```bash
$ ./bin/downloader.py --config my_conf.yml --option Institute
<some texts>
FInished!!
```
## Download component's info from ITkPD

```bash
$ ./bin/downloader.py --config my_conf.yml --option Module
<some texts>
FInished!!
```

