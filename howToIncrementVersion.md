* update qcAnalysisVersion in `viewer/functions/common.py`
* update footer version: `viewer/templates/parts/footer.html`
* make sure the version of `itkdb, mqat, mqdbt` in `setting/requirements-pip.txt` and `viewer/functions/common.py`
* create a new tag