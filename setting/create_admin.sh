#!/bin/bash
##################################################################
# Create user for mongodb
# Usage : ./setup_viewer.sh [-i IP address] [-p port]
# Instruction : https://github.com/jlab-hep/Yarr/wiki/Installation
#
# Contacts : Hiroki Okuyama (okuyama.h.ag@m.titech.ac.jp)
##################################################################
cd ../viewer
viewer_dir=$(cd $(dirname $0); pwd)
cd - >/dev/null

# Usage
function usage {
    cat <<EOF

Usage:
    ./create_admin.sh [-i ip address] [-p port] [-a admin username]

Options:
    - h                 Show this page
    - i <IP address>    Local DB server IP address, default: 127.0.0.1
    - p <port>          Local DB server port, default: 27017
    - a <admin account> Administrator account username if authentication required

EOF
}

### Start
if [ `echo ${0} | grep bash` ]; then
    echo -e "DO NOT 'source'"
    usage
    return
fi

dbip=mongodb
dbport=27017
auth=1

while getopts i:p:a:h OPT
do
    case ${OPT} in
        i ) dbip=${OPTARG} ;;
        p ) dbport=${OPTARG} ;;
        a ) admin=${OPTARG} ;;
        h ) usage
            exit ;;
        * ) usage
            exit ;;
    esac
done

if [ ! -z "${admin}" ]; then
    read -sp "Enter password: " adminpassword
    while [ -z ${adminpassword} ]; do
        read -sp "Enter password: " adminpassword
    done
    echo -e ""
    admin="-u ${admin} -p ${adminpassword}"
fi

### Confirmation
#MONGO=mongo
#which mongo > /dev/null 2>&1
#if [ $? = 1 ]; then
#    which mongosh > /dev/null 2>&1
#    if [ $? = 1 ]; then
#	printf '\033[31m%s\033[m\n' "[ERROR] 'mongo' command is required."
#	exit 1
#    else
#	MONGO=mongosh
#    fi
#fi

md5command=md5sum
which ${md5command} > /dev/null 2>&1
if [ $? = 1 ]; then
    md5command=md5
    which md5 >/dev/null 2>&1
    if [ $? = 1 ]; then
        printf '\033[31m%s\033[m\n' "[ERROR] 'md5sum' or 'md5' command is required."
        exit 1
    fi
fi

# confirmation
unixtime=`date '+%s'`
#/usr/local/opt/mongodb-community@4.0/bin/mongo --host ${dbip} --port ${dbport} ${admin} <<EOF >/dev/null
${MONGO} --host ${dbip} --port ${dbport} ${admin} <<EOF >/dev/null

use localdb
db.createUser({user: '${unixtime}', pwd: '${unixtime}', roles: [{role: 'readWrite', db: 'localdb'},{role: 'readWrite', db: 'localdbtools'}]})
db.dropUser('${unixtime}')

EOF
if [ $? = 1 ]; then
    echo -e ""
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account because LocalDB is protected."
    printf '\033[31m%s\033[m\n' "        Please provide the admin account name with option '-a <admin account name>'"
    printf '\033[31m%s\033[m\n' "        or restart MongoDB with the autorization disabled."
    exit 1
else
    echo -e "Authentication succeeded!"
fi

echo "Local DB Server IP address: ${dbip}"
echo "Local DB Server port: ${dbport}"
echo " "
echo "Are you sure that's correct? [y/n]"
unset answer
read -p "> " answer
while [ -z ${answer} ];
do
echo "Are you sure that's correct? [y/n]"
    read -p "> " answer
done
echo " "
if [ ${answer} != "y" ]; then
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account."
    printf '\033[31m%s\033[m\n' "        Try again create_admin.sh, Exit ..."
    exit 1
fi

read -p "Register localDB admin's username: " user
if [ -z ${user} ]; then
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account."
    printf '\033[31m%s\033[m\n' "        Please input your username. Try again create_admin.sh, Exit ..."
    exit 1
fi

read -sp "Register localDB admin's password: " password
if [ -z ${password} ]; then
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account."
    printf '\033[31m%s\033[m\n' "        Please input your password. Try again create_admin.sh, Exit ..."
    exit 1
fi
echo ""

user_hash=`echo -n ${user}|${md5command}|sed -e "s/-//"|sed -e "s/ //g"`
password_hash=`echo -n ${password}|${md5command}|sed -e "s/-//"|sed -e "s/ //g"`

#/usr/local/opt/mongodb-community@4.0/bin/mongo --host ${dbip} --port ${dbport} ${admin} <<EOF >${unixtime}.log
${MONGO} --host ${dbip} --port ${dbport} ${admin} <<EOF >${unixtime}.log

use localdb
db.dropUser('${user}')
db.createUser({user: '${user}', pwd: '${password}', roles: [{role: 'readWrite', db: 'localdb'},{role: 'readWrite', db: 'localdbtools'},{role: 'userAdmin', db: 'localdb'},{role: 'userAdmin', db: 'localdbtools'}]})
db.dropUser('${user_hash}')
db.createUser({user: '${user_hash}', pwd: '${password_hash}', roles: [{role: 'readWrite', db: 'localdb'},{role: 'readWrite', db: 'localdbtools'},{role: 'userAdmin', db:'localdb'},{role: 'userAdmin', db: 'localdbtools'}]})

EOF

if [ $? = 1 ]; then
    echo -e ""
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account."
    printf '\033[31m%s\033[m\n' "        Check the error message in ${unixtime}.log"
    printf '\033[31m%s\033[m\n' "        exit..."
    exit 1
fi

${MONGO} --host ${dbip} --port ${dbport} ${admin} <<EOF >${unixtime}.log
use localdbtools
db.viewer.user.remove({'username':'${user}'})
db.viewer.user.insert({
'sys': {'rev':0, 'cts':new Date(Date.now()), 'mts':new Date(Date.now())},
'username': '${user}',
'name': '${user}',
'auth': 'adminViewer',
'institution': '',
'Email': '',
'password': '${password_hash}'
})
EOF

if [ $? = 1 ]; then
    echo -e ""
    printf '\033[31m%s\033[m\n' "[ERROR] Could not create the administrator account."
    printf '\033[31m%s\033[m\n' "        Check the error message in ${unixtime}.log"
    printf '\033[31m%s\033[m\n' "        exit..."
    exit 1
else
    echo -e "Successfully added user:"
    echo -e "    user: ${user}"
    echo -e "    roles: [ 'localdb': 'userAdmin', 'localdbtools': 'userAdmin', 'localdb': 'readWrite', 'localdbtools': 'readWrite' ]"
    echo -e ""
    rm ${unixtime}.log
fi

echo -e ""
echo -e "Finished the setting of localdb with certification!!"
echo -e ""
echo -e "For checking the setting of Local DB: /etc/mongod.conf "
echo -e ""
