# LocalDB Viewer v2.2.8

## Documentation

Documentation is compiled in the [mkdocs page](https://atlas-itk-pixel-localdb.web.cern.ch/).

## Versioning

Versioning is done via [`tbump`](https://github.com/your-tools/tbump). Install `tbump` into your python environment using your favorite tool (`pip`, `pipx`, etc...). In short, you can do:

```
tbump 2.3.0
tbump 2.2.4-rc0
```

and it will bump the version everywhere and automatically tag and push.
